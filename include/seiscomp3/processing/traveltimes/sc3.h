/***************************************************************************
 * Copyright (C) 2018 by gempa GmbH                                        *
 *                                                                         *
 * All Rights Reserved.                                                    *
 *                                                                         *
 * NOTICE: All information contained herein is, and remains                *
 * the property of gempa GmbH and its suppliers, if any. The intellectual  *
 * and technical concepts contained herein are proprietary to gempa GmbH   *
 * and its suppliers.                                                      *
 * Dissemination of this information or reproduction of this material      *
 * is strictly forbidden unless prior written permission is obtained       *
 * from gempa GmbH.                                                        *
 *                                                                         *
 * Author: Jan Becker                                                      *
 * Email: jabe@gempa.de                                                    *
 ***************************************************************************/


#ifndef __GEMPA_ARRAY_TRAVELTIMES_SC3_H__
#define __GEMPA_ARRAY_TRAVELTIMES_SC3_H__


#include <seiscomp3/seismology/ttt.h>
#include <seiscomp3/processing/traveltimes.h>


namespace Seiscomp {
namespace Processing {
namespace TravelTimes {


class SC_ARRAY_API SC3 : public AbstractTravelTimeTable {
	public:
		SC3();

	public:
		virtual bool setSource(const char *path);
		virtual bool setModel(const char *model);

		virtual std::string method() const;
		virtual std::string model() const;

		double getPredictedArrivalTime(const char *phase,
		                               double latSrc, double lonSrc, double depthSrc,
		                               const char *networkCode, const char *stationCode,
		                               const char *locationCode,
		                               double latTgt, double lonTgt, double elevTgt);


	private:
		TravelTimeTableInterfacePtr _ttt;
		std::string                 _method;
};


}
}
}


#endif
