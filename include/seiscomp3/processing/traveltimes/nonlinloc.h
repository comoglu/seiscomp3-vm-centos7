/***************************************************************************
 * Copyright (C) 2018 by gempa GmbH                                        *
 *                                                                         *
 * All Rights Reserved.                                                    *
 *                                                                         *
 * NOTICE: All information contained herein is, and remains                *
 * the property of gempa GmbH and its suppliers, if any. The intellectual  *
 * and technical concepts contained herein are proprietary to gempa GmbH   *
 * and its suppliers.                                                      *
 * Dissemination of this information or reproduction of this material      *
 * is strictly forbidden unless prior written permission is obtained       *
 * from gempa GmbH.                                                        *
 *                                                                         *
 * Author: Jan Becker                                                      *
 * Email: jabe@gempa.de                                                    *
 ***************************************************************************/


#ifndef __GEMPA_ARRAY_TRAVELTIMES_NONLINLOC_H__
#define __GEMPA_ARRAY_TRAVELTIMES_NONLINLOC_H__


#include <seiscomp3/core/enumeration.h>
#include <seiscomp3/processing/traveltimes.h>
#include <seiscomp3/core/baseobject.h>
#include <gempa/utils/list.h>
#include <fstream>


namespace Seiscomp {
namespace Processing {
namespace TravelTimes {


class SC_ARRAY_API NonLinLoc : public AbstractTravelTimeTable {
	public:
		NonLinLoc();

	public:
		virtual bool setSource(const char *path);
		virtual bool setModel(const char *model);

		virtual std::string method() const;
		virtual std::string model() const;

		double getPredictedArrivalTime(const char *phase,
		                               double latSrc, double lonSrc, double depthSrc,
		                               const char *networkCode, const char *stationCode,
		                               const char *locationCode,
		                               double latTgt, double lonTgt, double elevTgt);


	protected:
		DEFINE_SMARTPOINTER(GridDesc);

		GridDescPtr readHeader(const char *filename);


	protected:
		MAKEENUM(
			Transformation,
			EVALUES(
				TRANSFORM_NONE,
				TRANSFORM_GLOBAL,
				TRANSFORM_SIMPLE,
				TRANSFORM_LAMBERT,
				TRANSFORM_SDC
			),
			ENAMES(
				"NONE",
				"GLOBAL",
				"SIMPLE",
				"LAMBERT",
				"SDC"
			)
		);

		MAKEENUM(
			Type,
			EVALUES(
				GRID_VELOCITY,
				GRID_VELOCITY_METERS,
				GRID_SLOWNESS,
				GRID_SLOW_LEN,
				GRID_VEL2,
				GRID_SLOW2,
				GRID_SLOW2_METERS,
				GRID_TIME,
				GRID_TIME_2D,
				GRID_ANGLE,
				GRID_ANGLE_2D,
				GRID_INCLINATION,
				GRID_INCLINATION_2D,
				GRID_PROB_DENSITY,
				GRID_MISFIT,
				GRID_LIKELIHOOD,
				GRID_DEPTH,
				GRID_LENGTH,
				GRID_COULOMB
			),
			ENAMES(
				"VELOCITY",
				"VELOCITY_METERS",
				"SLOWNESS",
				"SLOW_LEN",
				"VEL2",
				"SLOW2",
				"SLOW2_METERS",
				"TIME",
				"TIME2D",
				"ANGLE",
				"ANGLE2D",
				"INCLINATION",
				"INCLINATION2D",
				"PROB_DENSITY",
				"MISFIT",
				"LIKELIHOOD",
				"DEPTH",
				"LENGTH",
				"COULOMB"
			)
		);

		struct GridDesc : Seiscomp::Core::BaseObject,
		                  Gempa::Utils::IntrusiveListItem<GridDesc*, 1> {
			GridDesc() : buffer(NULL), bigEndian(false), isFloat(false) {}
			~GridDesc() { if ( buffer ) delete[] buffer; }

			bool isValidFor(const std::string &path, const char *stationCode, const char *phase) const;

			bool transform(double &x, double &y, double lat, double lon) const;
			bool isInside(double x, double y, double z) const;
			bool checkBounds(int x, int y, int z) const;
			double readGridValue(int x, int y, int z) const;

			mutable std::ifstream  gridFile;
			char                  *buffer;
			std::string            code;
			std::string            phase;
			std::string            path;
			double                 srcx, srcy, srcz; //!< grid location of the station
			int                    numx, numy, numz; //!< grid size
			double                 origx, origy, origz; //!< orig (km)
			double                 dx, dy, dz; //!< len side (km)
			Type                   type; //!< grid type
			bool                   bigEndian; //!< flag to specify if hi/lo bytes should be swapped when reading grid from disk files
			bool                   isFloat; //!< Whether values are stored as floats or doubles
			Transformation         transformation;

			union {
				struct {
					double latRef;
					double lonRef;
					double cosAngle;
					double sinAngle;
				} simple;

				struct {
					//
				} lambert;

				struct {
					double latRef;
					double lonRef;
					double cosAngle;
					double sinAngle;
					double xltkm;
					double xlnkm;
				} sdc;
			};
		};

		typedef std::map<std::string, GridDescPtr> GridDescLookup;
		typedef Gempa::Utils::IntrusiveList<GridDesc*> GridDescCache;

		std::string    _path;
		GridDescLookup _gridDescLookup;
		GridDescCache  _gridDescCache;
};


bool NonLinLoc::GridDesc::isValidFor(const std::string &fp, const char *stationCode, const char *ph) const {
	return path == fp && code == stationCode && phase == ph;
}


}
}
}


#endif
