import seiscomp3.Kernel, seiscomp3.Config, os

class Module(seiscomp3.Kernel.Module):
  def __init__(self, env):
    seiscomp3.Kernel.Module.__init__(self, env, env.moduleName(__file__))

    self.pkgroot = self.env.SEISCOMP_ROOT

    cfg = seiscomp3.Config.Config()

    # Defaults Global + App Cfg
    cfg.readConfig(os.path.join(self.pkgroot, "etc", "defaults", "global.cfg"))
    cfg.readConfig(os.path.join(self.pkgroot, "etc", "defaults", self.name + ".cfg"))

    # Config Global + App Cfg
    cfg.readConfig(os.path.join(self.pkgroot, "etc", "global.cfg"))
    cfg.readConfig(os.path.join(self.pkgroot, "etc", self.name + ".cfg"))

    # User Global + App Cfg
    cfg.readConfig(os.path.join(os.environ['HOME'], ".seiscomp3", "global.cfg"))
    cfg.readConfig(os.path.join(os.environ['HOME'], ".seiscomp3", self.name + ".cfg"))

    try:
        self.authbind = cfg.getBool("authbind")
    except:
        self.authbind = False

  def supportsAliases(self):
    # The default handler does not support aliases
    return True

  def updateConfigProxy(self):
    return "trunk"

## Uncomment for authbind (running service on privileged ports)
  def _run(self):
     if self.authbind:
         params = "--depth 2 " + self.env.binaryFile(self.name) + " " + self._get_start_params()
         binaryPath = "authbind"
         return self.env.start(self.name, binaryPath, params)
     else:
         seiscomp3.Kernel.Module._run(self)
