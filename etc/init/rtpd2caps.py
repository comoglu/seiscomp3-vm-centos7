import os, glob, re, sys
import seiscomp3.Kernel, seiscomp3.Config, seiscomp3.System

class Module(seiscomp3.Kernel.Module):
    def __init__(self, env):
        seiscomp3.Kernel.Module.__init__(self, env, env.moduleName(__file__))
        self.config_dir = os.path.join(self.env.SEISCOMP_ROOT, "var", "lib")
        self.log_dir = os.path.join(self.env.SEISCOMP_ROOT, "var", "log")


    def _run(self):
        if self.env.syslog:
            daemon_opt = '-s'
        else:
            daemon_opt = '--log-file ' + os.path.join(self.log_dir, self.name + '.log')

        daemon_opt += ' -f ' + os.path.join(self.config_dir, self.name + '.cfg')

        prog = "run_with_lock"
        params = self.env.lockFile(self.name)

        return self.env.start(self.name, prog, params + ' ' + self.env.binaryFile(self.name) + ' ' + daemon_opt,\
                              True)


    def supportsAliases(self):
        return True


    def updateConfig(self):
        bindings_dir = os.path.join(self.env.SEISCOMP_ROOT, "etc", "key")
        key_dir = os.path.join(bindings_dir, self.name)
        cfg_file = os.path.join(self.config_dir, self.name + ".cfg")
        rx_binding = re.compile(r'(?P<module>[A-Za-z0-9_\.-]+)(:(?P<profile>[A-Za-z0-9_-]+))?$')

        files = glob.glob(os.path.join(bindings_dir, "station_*"))
        files.sort()

        try: fb = open(cfg_file, "w")
        except:
            print "error: unable to generate configuration file '%s'" % cfg_file
            return 1

        cfg = seiscomp3.Config.Config()

        try:
            # Defaults Global + App Cfg
            cfg.readConfig(os.path.join(self.env.SEISCOMP_ROOT, "etc", "defaults", "global.cfg"))
            cfg.readConfig(os.path.join(self.env.SEISCOMP_ROOT, "etc", "defaults", self.name + ".cfg"))

            # Config Global + App Cfg
            cfg.readConfig(os.path.join(self.env.SEISCOMP_ROOT, "etc", "global.cfg"))
            cfg.readConfig(os.path.join(self.env.SEISCOMP_ROOT, "etc", self.name + ".cfg"))

            # User Global + App Cfg
            cfg.readConfig(os.path.join(os.environ['HOME'], ".seiscomp3", "global.cfg"))
            cfg.readConfig(os.path.join(os.environ['HOME'], ".seiscomp3", self.name + ".cfg"))
        except:
            pass

        try: print >> fb, "queue_size = %d" % cfg.getInt("queueSize")
        except: pass

        try: print >> fb, "backfilling_buffer_size = %d" % cfg.getInt("backFillingBufferSize")
        except: pass

        has_channel_mappings = False

        try:
            print >> fb, "channels = %s" % (", ".join(cfg.getStrings("channels")))
            has_channel_mappings = True
        except: pass

        try: default_address = cfg.getString("address")
        except: default_address = None

        try: default_sink = cfg.getString("sink")
        except: default_sink = None

        units = {}

        for f in files:
            try:
                (path, net, sta) = f.split('_')
                if not path.endswith("station"):
                    print "invalid path", f

            except ValueError:
                print "invalid path", f
                continue

            fd = open(f)
            line = fd.readline()
            while line:
                line = line.strip()
                if not line or line[0] == '#':
                    line = fd.readline()
                    continue

                m = rx_binding.match(line)
                if not m:
                    #print "invalid binding in %s: %s" % (f, line)
                    line = fd.readline()
                    continue

                if m.group('module') != self.name:
                    line = fd.readline()
                    continue

                profile = m.group('profile')

                # Setup station net, sta, profile
                if profile:
                    binding_file = "profile_%s" % (profile,)
                else:
                    binding_file = "station_%s_%s" % (net, sta)

                cfg = seiscomp3.Config.Config()
                cfg.readConfig(os.path.join(key_dir, binding_file))
                try: unit = cfg.getString("unit")
                except:
                    print "warning: unit is not defined in %s" % (binding_file)
                    break

                try: channels = cfg.getStrings("channels")
                except:
                    if has_channel_mappings == False:
                        print "warning: channel mappings are not defined in %s" % (binding_file)
                        break
                    channels = None

                try: address = cfg.getString("address")
                except:
                    if default_address is None:
                        print "warning: address not defined in %s" % (binding_file)
                        break
                    address = default_address

                try: sink = cfg.getString("sink")
                except:
                    if default_sink is None:
                        print "warning: sink not defined in %s" % (binding_file)
                        break
                    sink = default_sink

                if units.has_key(unit):
                    print "error: unit %s is bound to multiple stations (%s.%s, %s.%s, ...)" % (unit, units[unit][0], units[unit][1], net,sta)
                    return 1

                units[unit] = (net,sta)

                print >> fb, "unit %s" % unit
                print >> fb, "  network  = \"%s\"" % net
                print >> fb, "  station  = \"%s\"" % sta
                if not channels is None:
                    print >> fb, "  channels = %s" % ", ".join(channels)
                print >> fb, "  address  = %s" % address
                print >> fb, "  sink     = %s" % sink

                break

            fd.close()

        fb.close()

        return 0
