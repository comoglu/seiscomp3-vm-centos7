# Configure the columns of the event list that are visible initially. The first
# column containing the origin time is always visible and cannot be hidden.
# Possible values are: Type, M, TP, Phases, Lat, Lon, Depth, Stat, FM, Agency,
# Region, ID.
eventlist.visibleColumns = M,TP,Phases,Lat,Lon,Depth,Stat,FM,Agency,Author,Region,ID

# Configures a script that is called when the bulletin button is pressed. scmtv
# will pass the log path for this particular solution. The script is then
# responsible for creating and displaying the bulletin as well as removing the
# directory.
mtv.extendedLog.script = @DATADIR@/mtv/tools/mtv-plot-extended-log.py

# Uses sensor response to deconvolve data.
automt.enableResponses = true

# Defines the goodness of fit function to be used. Allowed values are:
# - internal
# - varred
automt.GOF = internal

# Global phase settings
automt.settings.minSNR.body = 1
automt.settings.minSNR.surface = 1
automt.settings.minSNR.mantle = 1
automt.settings.minSNR.w-phase = 1

automt.settings.maxShift.body = 10
automt.settings.maxShift.surface = 10
automt.settings.maxShift.mantle = 10
automt.settings.maxShift.w-phase = 10

# Default component weights: vertical, radial and tangential
automt.settings.wZ = 1.0
automt.settings.wR = 0.25
automt.settings.wT = 0.5

# Some predefined profiles

automt.profiles = M3, M3_4, M4_5,\
                  M5_65, M65,\
                  M75, M85,\
                  W55USGS, W65USGS, W7USGS,\
                  W75USGS, W8USGS

automt.profiles.M3.name = "<M3"
automt.profiles.M3.magnitudes = -INF;3.0
automt.profiles.M3.minItemFit = 75
automt.profiles.M3.maxShift = 4
automt.profiles.M3.periods.body = 8-20
automt.profiles.M3.periods.surface = 8-20
automt.profiles.M3.maxShift.body = 3;
automt.profiles.M3.maxShift.surface = 3;

automt.profiles.M3_4.name = "M3-M4"
automt.profiles.M3_4.magnitudes = 3.0;4.0
automt.profiles.M3_4.minItemFit = 75
automt.profiles.M3_4.maxShift = 5
automt.profiles.M3_4.periods.body = 10-40
automt.profiles.M3_4.periods.surface = 10-40
automt.profiles.M3_4.maxShift.body = 4;
automt.profiles.M3_4.maxShift.surface = 4;

automt.profiles.M4_5.name = "M4-M5"
automt.profiles.M4_5.magnitudes = 4.0;5.0
automt.profiles.M4_5.minItemFit = 75
automt.profiles.M4_5.maxShift = 10
automt.profiles.M4_5.periods.body = 20-50
automt.profiles.M4_5.periods.surface = 20-50
automt.profiles.M4_5.maxShift.body = 5;
automt.profiles.M4_5.maxShift.surface = 5;

automt.profiles.M5_65.name = "M5-M6.5"
automt.profiles.M5_65.magnitudes = 5.0;6.5
automt.profiles.M5_65.minItemFit = 75
automt.profiles.M5_65.maxShift = 20
automt.profiles.M5_65.periods.body = 40-100
automt.profiles.M5_65.periods.surface = 60-150

automt.profiles.M65.name = ">=M6.5"
automt.profiles.M65.magnitudes = 6.5;INF
automt.profiles.M65.minItemFit = 75
automt.profiles.M65.maxShift = 45
automt.profiles.M65.periods.body = 90-300
automt.profiles.M65.periods.surface = 90-300

automt.profiles.M75.name = ">=M7.5"
automt.profiles.M75.magnitudes = 7.5;INF
automt.profiles.M75.minItemFit = 75
automt.profiles.M75.maxShift = 60
automt.profiles.M75.periods.body = 200-600
automt.profiles.M75.periods.surface = 200-600

automt.profiles.M85.name = ">=M8.5"
automt.profiles.M85.magnitudes = 8.5;INF
automt.profiles.M85.minItemFit = 75
automt.profiles.M85.maxShift = 60
automt.profiles.M85.periods.body = 200-1000
automt.profiles.M85.periods.surface = 200-1000

# W-Phase filter table according to Hayes, Rivera and Kanamori
# Magnitude ranges are disabled for the moment to
# not use it automatically
automt.profiles.W55USGS.name = "W-phase M5.5-M6.5 (USGS)"
automt.profiles.W55USGS.method = "W-Phase"
automt.profiles.W55USGS.minItemFit = 75
automt.profiles.W55USGS.maxShift = 25
automt.profiles.W55USGS.periods.w-phase = 50-150
#automt.profiles.W55USGS.magnitudes = 5.5;6.5

automt.profiles.W65USGS.name = "W-phase M6.5-M7 (USGS)"
automt.profiles.W65USGS.method = "W-Phase"
automt.profiles.W65USGS.minItemFit = 75
automt.profiles.W65USGS.maxShift = 45
automt.profiles.W65USGS.periods.w-phase = 100-500
#automt.profiles.W65USGS.magnitudes = 6.5;7.0

automt.profiles.W7USGS.name = "W-phase M7-M7.5 (USGS)"
automt.profiles.W7USGS.method = "W-Phase"
automt.profiles.W7USGS.minItemFit = 75
automt.profiles.W7USGS.maxShift = 45
automt.profiles.W7USGS.periods.w-phase = 100-600
#automt.profiles.W7USGS.magnitudes = 7.0;7.5

automt.profiles.W75USGS.name = "W-phase M7.5-M8 (USGS)"
automt.profiles.W75USGS.method = "W-Phase"
automt.profiles.W75USGS.minItemFit = 75
automt.profiles.W75USGS.maxShift = 60
automt.profiles.W75USGS.periods.w-phase = 200-600
#automt.profiles.W75USGS.magnitudes = 7.5;8.0

automt.profiles.W8USGS.name = "W-phase >=M8 (USGS)";
automt.profiles.W8USGS.method = "W-Phase"
automt.profiles.W8USGS.minItemFit = 75
automt.profiles.W8USGS.maxShift = 60
automt.profiles.W8USGS.periods.w-phase = 200-1000
#automt.profiles.W8USGS.magnitudes = 8.0;INF
