# ----- Jinja custom filters -------------
import datetime, math

def lat(value, prec = '.2f'):
	prec = '%' + prec
	v = float(value)
	if v < 0:
		return prec % abs(v) + '&deg; S'
	else:
		return prec % v + '&deg; N'


def lon(value, prec = '.2f'):
	prec = '%' + prec
	v = float(value)
	if v < 0:
		return prec % abs(v) + '&deg; W'
	else:
		return prec % v + '&deg; E'


def qmldate(value):
	return datetime.datetime.strptime(value, '%Y-%m-%dT%H:%M:%S.%fZ')

def dbdate(value):
	try:
		return datetime.datetime.strptime(value, '%Y-%m-%d %H:%M:%S.%f')
	except:
		return datetime.datetime.strptime(value, '%Y-%m-%d %H:%M:%S')


def deg2km(value):
	return float(value) * 111.1329149013519096


def date(value, format = '%Y-%m-%d %H:%M:%S'):
	return datetime.datetime.strftime(value, format)

def minWeight(objList, value):
	if value is None:
		return objList
	return filter(lambda x: x.weight >= value, objList)


# creates a FM bulletin from focal mechanism solution, e.g.
#
#   <pre style="font-family: monospace">
#   Moment Tensor:
#     Mrr=-1.96       Mtt= 0.21
#     Mpp= 1.74       Mrt= 0.92
#     Mrp=-0.07       Mtp=-0.00
#   Principal axes:
#     T  Val=  1.74  Plg= 1  Azm= 89
#     N        0.55      20      358
#     P       -2.30      70      183
#   Nodal planes:
#    NP1:Strike=198 Dip=47 Slip= -61
#    NP2:       340     50      -116
#   </pre>"
def fmBulletin(fm):
	mt = None
	try:
		tensor = fm.momentTensor[0].tensor
		axes = fm.principalAxes

		Mrr = tensor.Mrr.value
		Mtt = tensor.Mtt.value
		Mpp = tensor.Mpp.value
		Mrt = tensor.Mrt.value
		Mrp = tensor.Mrp.value
		Mtp = tensor.Mtp.value
		Tval = axes.tAxis.length.value
		Tplg = axes.tAxis.plunge.value
		Tazi = axes.tAxis.azimuth.value
		Nval = axes.nAxis.length.value
		Nplg = axes.nAxis.plunge.value
		Nazi = axes.nAxis.azimuth.value
		Pval = axes.pAxis.length.value
		Pplg = axes.pAxis.plunge.value
		Pazi = axes.pAxis.azimuth.value

		expo = 0
		expo = max(expo, int(math.log10(abs(Mrr))))
		expo = max(expo, int(math.log10(abs(Mtt))))
		expo = max(expo, int(math.log10(abs(Mpp))))
		expo = max(expo, int(math.log10(abs(Mrt))))
		expo = max(expo, int(math.log10(abs(Mrp))))
		expo = max(expo, int(math.log10(abs(Mtp))))
		expo = max(expo, int(math.log10(abs(Tval))))
		expo = max(expo, int(math.log10(abs(Nval))))
		expo = max(expo, int(math.log10(abs(Pval))))

		strExpo = str(expo+20-7);

		scaleStr = "Scale 10**%s Nm\n" % expo
		res = "Moment Tensor;%s" % scaleStr.rjust(19)

		div = math.pow(10.0, expo)
		res +=      "  Mrr=%s"   % ("%.2f" % (Mrr / div)).rjust(5)
		res += "       Mtt=%s\n" % ("%.2f" % (Mtt / div)).rjust(5)
		res +=      "  Mpp=%s"   % ("%.2f" % (Mpp / div)).rjust(5)
		res += "       Mrt=%s\n" % ("%.2f" % (Mrt / div)).rjust(5)
		res +=      "  Mrp=%s"   % ("%.2f" % (Mrp / div)).rjust(5)
		res += "       Mtp=%s\n" % ("%.2f" % (Mtp / div)).rjust(5)
		res += "Principal axes:\n"
		res += "  T  Val=%s"   % ("%.2f" % (Tval / div)).rjust(6)
		res +=    "  Plg=%s"   % str(int(round(Tplg))).rjust(2)
		res +=    "  Azm=%s\n" % str(int(round(Tazi))).rjust(3)
		res += "  N      %s"   % ("%.2f" % (Nval / div)).rjust(6)
		res +=    "      %s"   % str(int(round(Nplg))).rjust(2)
		res +=    "      %s\n" % str(int(round(Nazi))).rjust(3)
		res += "  P      %s"   % ("%.2f" % (Pval / div)).rjust(6)
		res +=    "      %s"   % str(int(round(Pplg))).rjust(2)
		res +=    "      %s\n" % str(int(round(Pazi))).rjust(3)

		mt = res
	except:
		pass

	np = None
	try:
		np1 = fm.nodalPlanes.nodalPlane1
		np1Strike = np1.strike.value
		np1Dip    = np1.dip.value
		np1Rake   = np1.rake.value

		np2 = fm.nodalPlanes.nodalPlane2
		np2Strike = np2.strike.value
		np2Dip    = np2.dip.value
		np2Rake   = np2.rake.value

		res  = " NP1:Strike=%s"  % str(int(round(np1Strike))).rjust(3)
		res +=      " Dip=%s"    % str(int(round(np1Dip))).rjust(2)
		res +=      " Slip=%s\n" % str(int(round(np1Rake))).rjust(4)
		res += " NP2:       %s"  % str(int(round(np2Strike))).rjust(3)
		res +=      "     %s"    % str(int(round(np2Dip))).rjust(2)
		res +=      "      %s\n" % str(int(round(np2Rake))).rjust(4)

		np = res
	except:
		pass

	res = ""
	if mt:
		res += mt
	if np:
		if mt:
			res += "\n"
		res += np
	return res

