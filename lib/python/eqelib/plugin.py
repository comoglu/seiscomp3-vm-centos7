class PluginBase:
	def __init__(self, **kwargs): pass

	"""
	Base class for all eqevents plugins which must be derived.
	"""

	def processEvent(self, ctx, path):
		"""
		Processes an event. May raise exceptions.

		@param ctx Context: Context of the event template. New members can be added to the context for template interpolation.
		@param path String: The output path of the current event

		@return Return True if operation succeeded or False
		"""

		# The default implementation just returns True
		return True

	def pushIndex(self, old_time, new_time):
		"""
		Notifies about a new event.

		@param old_time datetime: Old time of the event
		@param new_time datatime: New time of the event

		@return Return True if operation succeeded or False
		"""
		return True

	def processIndex(self, ctx, force = False):
		"""
		Processes the index. May raise exceptions.

		@param ctx Context: Context of the index template. New attributes can be added to the context for template interpolation.
		@param force Boolean: If the update should be forced even if there are not internally tracked changes
		@return Return True if operation succeeded or False
		"""
		return True

	def reportEventProcessing(self, status, msg, filename):
		"""
		Reports event processing. May raise exceptions.

		@param status int: Status of processing, 0 = Success, 1 = Warning, 2 = Error
		@param msg String: An optional status message, e.g. error message
		@param filename String: The filename of the input XML file

		@return Return True if operation succeeded or False
		"""

		# The default implementation just returns True
		return True

	def processInvalid(self, ctx, filename, path):
		"""
		Processes an invalid file and try to process the content. May raise exceptions.

		@param ctx Context: Context of the index template. New attributes can be added to the context for template interpolation.
		@param filename String: Path to the html file.
		@param path String: The event path.

		@return Return True if operation succeeded or False
		"""

		# The default implementation just returns True
		return True
