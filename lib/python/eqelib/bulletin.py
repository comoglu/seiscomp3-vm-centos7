# -*- coding: utf-8 -*-

###############################################################################
# Serialize and deserialize gempa bulletin format
#
# Author: Stephan Herrnkind
# Email: herrnkind@gempa.de
###############################################################################

CONTENT_TYPE = ["subject","text","html","attachment"]



###############################################################################
class Attachment:

	#--------------------------------------------------------------------------
	def __init__(self):
		self.name    = None
		self.path    = None
		self.content = None
		self.MIME    = None
		self.cid     = None



	#--------------------------------------------------------------------------
	def setPath(self, path):
		self.path = path
		self._guessType(path)



	#--------------------------------------------------------------------------
	def loadFromFile(self, path):
		try:
			import base64, StringIO

			f = open(path, 'r')
			b64IO = StringIO.StringIO()
			base64.encode(f, b64IO)
			f.close()
			self.content = b64IO.getvalue()
			b64IO.close()

			self._guessType(path)
		except:
			return False
		return True



	#--------------------------------------------------------------------------
	# creates an attachment from an URL, e.g. image from gempa MapServer
	def loadFromURL(self, url, data=None, maxSize=10*1024**2):
		import base64, StringIO, urllib2

		u = urllib2.urlopen(url, data)
		info = u.info()

		# content length
		length = info.getheaders("Content-Length")
		if len(length) != 1:
			raise Exception("could not read content length header")
		if int(length[0]) > maxSize:
			raise Exception("content length of %i exceeds maximum of %i" % (
			                length[0], maxSize))

		# content type
		type = info.getheaders("Content-Type")
		if len(type) != 1:
			raise Exception("could not read content type header")
		self.MIME = type[0]

		# content
		b64IO = StringIO.StringIO()
		base64.encode(u, b64IO)
		self.content = b64IO.getvalue()
		b64IO.close()



	#--------------------------------------------------------------------------
	def createCID(self, domain = None):
		import os, random, time
		# cid must be unique
		self.cid = "%s.%s.%s" % (random.randint(0, 10000), os.getpid(),
		                         time.time())
		if self.name:
			self.cid += ".%s" % self.name
		if domain:
			self.cid += "@%s" % domain



	#--------------------------------------------------------------------------
	def _guessType(self, path):
		import mimetypes
		fmt, enc = mimetypes.guess_type(path)
		self.MIME = fmt



###############################################################################
class Bulletin:

	#--------------------------------------------------------------------------
	def __init__(self):
		import re
		self.subject       = ""
		self.plain         = ""
		self.html          = ""
		self.attachments   = []
		self.extParameters = {}

		self._typePattern   = re.compile(r"^Content-Type: ([a-z]+)$")
		self._lengthPattern = re.compile(r"^Content-Length: ([0-9]+)$")
		self._namePattern   = re.compile(r"^Content-Name: (.*)$")
		self._mimePattern   = re.compile(r"^Content-MIME: (.*)$")
		self._pathPattern   = re.compile(r"^Content-Path: (.*)$")
		self._cidPattern    = re.compile(r"^Content-ID: (.*)$")



	#--------------------------------------------------------------------------
	def read(self, text):
		self.subject = ""
		self.plain = ""
		self.html = ""
		self.attachments = []
		self.extParameters = {}

		self._text = text

		while self._text.replace("\n","") != "":
			if self._text.startswith('X-'):
				self.__read_ext_parameter()
				continue

			contentType = self.__read_content_type()
			if contentType == "attachment":
				attachment = self.__read_attachment()
				self.attachments.append(attachment)
			else:
				length = self.__read_content_length()
				content = self.__read(length)
				self.__add_content(contentType,content)
				self.__read_empty_line()



	#--------------------------------------------------------------------------
	def addExtParameter(self, key, value):
		self.extParameters[key] = value;



	#--------------------------------------------------------------------------
	def __str__(self):
		content = ""
		for key, value in self.extParameters.iteritems():
			content += "X-%s: %s\n"  % (key, value)

		if self.subject:
			content += "Content-Type: subject\n"\
			           "Content-Length: %d\n"\
			           "%s\n\n" % (len(self.subject), self.subject)
		if self.plain:
			content += "Content-Type: text\n"\
			           "Content-Length: %d\n"\
			           "%s\n\n" % (len(self.plain), self.plain)

		if self.html:
			content += "Content-Type: html\n"\
			           "Content-Length: %d\n"\
			           "%s\n\n" % (len(self.html), self.html)

		for a in self.attachments:
			content += "Content-Type: attachment\n"\
			           "Content-Name: %s\n" % a.name

			if a.path:
				content += "Content-Path: %s\n" % a.path
			if a.content:
				content += "Content-Length: %d\n" % len(a.content)
				content += a.content
				content += "\n"
			if a.MIME:
				content += "Content-MIME: %s\n" % a.MIME
			if a.cid:
				content += "Content-ID: %s\n" % a.cid

			content += "\n"

		return content



	#--------------------------------------------------------------------------
	def __read_content_type(self):
		line = self.__read_line()
		contentType = self._typePattern.match(line)
		if contentType == None:
			raise Exception("missing Content-Type")
		contentType = contentType.group(1)
		if contentType not in CONTENT_TYPE:
			raise Exception("unknown Content-Type: %s" % contentType)
		return contentType



	#--------------------------------------------------------------------------
	def __read_content_length(self):
		line = self.__read_line()
		contentLength = self._lengthPattern.match(line)
		if contentLength == None:
			raise Exception("missing Content-Length")
		try:
			contentLength = int(contentLength.group(1))
		except:
			raise Exception("invalid Content-Length: %s" % contentLength.group(1))
		return contentLength



	#--------------------------------------------------------------------------
	def __read_line(self):
		try: line,self._text = self._text.split("\n",1)
		except:
			line = self._text
			self._text = ""
		return line



	#--------------------------------------------------------------------------
	def __read(self, length):
		if len(self._text.encode("utf-8")) < length:
			raise Exception("format error, line length too short")

		text = self._text.encode("utf-8")[:length].decode("utf-8")
		self._text = self._text.encode("utf-8")[length:].decode("utf-8")
		self.__read_empty_line()
		return text



	#--------------------------------------------------------------------------
	def __read_empty_line(self):
		line = self.__read_line()
		if line != "":
			raise Exception("empty line expected")



	#--------------------------------------------------------------------------
	def __read_attachment(self):
		att = Attachment()
		att.name = self.__read_content_name()
		while True:
			line = self.__read_line()
			if line == "": break

			length = self._lengthPattern.match(line)
			path = self._pathPattern.match(line)
			MIME = self._mimePattern.match(line)
			cid = self._cidPattern.match(line)

			if length:
				# Content is attached as base64 encoded binary block
				att.content = self.__read(int(length.group(1)))
			elif path:
				att.path = path.group(1)
			elif MIME:
				att.MIME = MIME.group(1)
			elif cid:
				att.cid = cid.group(1)
			else:
				raise Exception("expected Content-[Length|Path|MIME|ID] after "
				                "Content-Name for type attachment")
		return att



	#--------------------------------------------------------------------------
	def __read_content_name(self):
		line = self.__read_line()
		contentName = self._namePattern.match(line)
		if contentName == None:
			raise Exception("missing Content-Name")
		contentName = contentName.group(1)
		return contentName


	def __read_ext_parameter(self):
		line = self.__read_line()
		toks = line[2:].split(':')
		if ( len(toks) >= 2):
			self.extParameters[toks[0]] = ":".join(toks[1:]).lstrip(' ')


	#--------------------------------------------------------------------------
	def __add_content(self, contentType, content):
		if contentType == "subject":
			self.subject = content
		elif contentType == "html":
			self.html = content
		elif contentType == "text":
			self.plain = content
