<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta name="description" content="GAPS <?cs var:GAPS.version ?> frontpage, &copy; gempa GmbH"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<link rel="stylesheet" href="share/css/bootstrap3.min.css"></link>
		<script src="share/lib/jquery.min.js"></script>
		<script src="share/lib/bootstrap.min.js"></script>
	</head>
	<style>
		.btn-default{
			background-color: #7AD4E2;
		}
		/* Carousel base class */
		.carousel {
			height: 500px;
			margin-bottom: 60px;
		}
		/* Since positioning the image, we need to help out the caption */
		.carousel-caption {
			z-index: 10;
		}

		/* Declare heights because of positioning of img element */
		.carousel .item {
			height: 500px;
			background-color: #777;
		}
		.carousel-inner > .item > img {
			position: absolute;
			top: 0;
			left: 0;
			min-width: 100%;
			height: 500px;
			max-width: none;
		}
		.marketing {
			padding-left: 15px;
			padding-right: 15px;
		}

		/* Center align the text within the three columns below the carousel */
		.marketing .col-lg-4 {
			text-align: center;
			margin-bottom: 20px;
		}
		.marketing h2 {
			font-weight: normal;
		}
		.marketing .col-lg-4 p {
			margin-left: 10px;
			margin-right: 10px;
		}

	</style>
	<body>
		<div id="myCarousel" class="carousel invert slide" data-ride="carousel">
			<ol class="carousel-indicators"><?cs set:index = 0?><?cs each:item = apps?>
				<li data-target="#myCarousel" data-slide-to="<?cs var:index?>"<?cs if:index==0?> class="active"><?cs /if ?></li><?cs set:index = index+1?><?cs /each?>
			</ol>
			<div class="carousel-inner"><?cs set:index = 0?><?cs each:item = apps?>
				<div class="item<?cs if:index==0?> active<?cs /if ?>">
					<img src="<?cs var:item.path?>/static/images/carousel.png" alt="<?cs var:item.title?>">
					<div class="container">
						<div class="carousel-caption">
							<h1><?cs var:item.title?></h1>
						</div>
					</div>
				</div><?cs set:index = index+1?><?cs /each?>
			</div>
			<a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
			<a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
		</div>
		<div class="container marketing">
			<div class="row"><?cs each:item = apps?>
				<div class="col-lg-4">
					<h2><?cs var:item.title?></h2>
					<p><?cs var:item.description?></p>
					<p><a class="btn btn-default" href="<?cs var:item.path?>" role="button">Launch application</a></p>
				</div><?cs /each?>
			</div>
		</div>
	</body>
</html>
