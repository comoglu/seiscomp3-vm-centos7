#########################
Graphical user interfaces
#########################

.. toctree::
   :maxdepth: 2

   /apps/lambda
   /apps/scesv
   /apps/scheli
   /apps/scmm
   /apps/scmtv
   /apps/scmv
   /apps/scolv
   /apps/scqcv
   /apps/scrttv
   /apps/sigma

