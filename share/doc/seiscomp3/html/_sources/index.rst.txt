#######################
SeisComP3 documentation
#######################

This is the documentation for the SeisComP3 release |release| version |version|.

For examples and tutorials please check the `SeisComP3 wiki <http://www.seiscomp3.org/>`_.
Please consider :doc:`contributing</base/contributing-docs>` to this documentation.

Contents:

.. toctree::
   :maxdepth: 4
   :titlesonly:

   /base/introduction
   /base/installation
   /base/getting-started
   /base/management
   /apps/global
   /toc/acquisition
   /toc/gempa
   /toc/inventory
   /toc/messaging
   /toc/processing
   /toc/system
   /toc/utilities

   /gui
   /toc/extensions
   /base/filter-grammar
   /base/arclink-protocol
   /base/sdk
   /base/glossary
   /base/contributing-docs
   /base/coding-conventions
   /base/tests
   /base/license
