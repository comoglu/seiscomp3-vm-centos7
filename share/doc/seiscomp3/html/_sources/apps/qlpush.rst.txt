.. highlight:: rst

.. _qlpush:

######
qlpush
######

**Module to send XML files to a QuakeLink server.**


Configuration
=============

| :file:`etc/defaults/global.cfg`
| :file:`etc/defaults/qlpush.cfg`
| :file:`etc/global.cfg`
| :file:`etc/qlpush.cfg`
| :file:`~/.seiscomp3/global.cfg`
| :file:`~/.seiscomp3/qlpush.cfg`

qlpush inherits :ref:`global options<global-configuration>`.




Command-line
============

.. program:: qlpush


Options
-------

.. option:: -H, --host

   host to connect to

.. option:: -h, --help

   display this help message

.. option:: -p, --print-env

   echo event id\(s\) and revision id\(s\)

.. option:: -r, --rm

   remove the event

