.. highlight:: rst

.. _scqceval:

########
scqceval
########

**QCStationEvaluator - Listens for QC messages and enables/disables stations based on configured QC parameter thresholds.**


Configuration
=============

| :file:`etc/defaults/global.cfg`
| :file:`etc/defaults/scqceval.cfg`
| :file:`etc/global.cfg`
| :file:`etc/scqceval.cfg`
| :file:`~/.seiscomp3/global.cfg`
| :file:`~/.seiscomp3/scqceval.cfg`

scqceval inherits :ref:`global options<global-configuration>`.



.. confval:: qc.parameters

   Type: *list:string*

   Defines QC parameters to observe. Each parameter is associated with a value range. If any of the defined ranges
   is exceeded the corresponding station is disabled. Use '\-Inf' resp. 'Inf' if no upper or lower bound should exist.
   Supported QC parameters are availability, gaps count, overlaps count, latency, delay, timing quality, offset, rms
   and spikes count.
   An example configuration may look like:
   parameters \= \"availability: 40.0,100.0\", \"gaps count: 0,1\", \"overlaps count: 0,1\"


.. confval:: qc.pairs

   Type: *list:string*

   Defines rules of primaries and secondaries. The stations in a rule are sorted by their priority in descending order.
   The algorithm tries to enable the station with the highest priority that fulfills the quality parameters.
   An example may look as follows:
   pairs \= GE.VAL : GE.WLF, GE.UGM : GE.SBV : GE.RGN, GE.UGM : GE.WLF


.. confval:: qc.resetAuthorAfter

   Type: *int*

   Allow control of foreign stations which have not been modified within the given number of seconds.
   Specify 0 to force control over all configured stations.



Bindings
========


Configuration
-------------


.. confval:: enable

   Type: *boolean*

   Enables\/disables usage of station with qceval.
   Default is ``true``.

