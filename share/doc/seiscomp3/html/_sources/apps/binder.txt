.. highlight:: rst

.. _binder:

######
binder
######

**Module for binding seismic signals.**


Description
===========

Binder automatically detects seismic events of small magnitude which can bind
or group the seismic signals observed at less than four stations that arrive
close in time. Real locators such as ref:`scautoloc` require more than four
signals, or phases, in order to associate the signals as an event. The
binder binds signals even when the number of the detected signal is only
two or more.

Specification
=============

- Two or more triggered signals, which are observed within
  :confval:`binder.maxObservationTimeSpan` seconds at stations placed within
  :confval:`binder.maxStationDistance` km from each other, get bound

- The arrival time of the earliest arrival signal is set as the origin time.
  The location of the station with the earliest signal arrival is set as the
  origin location.



Configuration
=============

| :file:`etc/defaults/global.cfg`
| :file:`etc/defaults/binder.cfg`
| :file:`etc/global.cfg`
| :file:`etc/binder.cfg`
| :file:`~/.seiscomp3/global.cfg`
| :file:`~/.seiscomp3/binder.cfg`

binder inherits :ref:`global options<global-configuration>`.



.. confval:: binder.bufferSize

   Type: *int*

   Unit: *s*

   Pick buffer size in seconds. The default is to save and to process
   the last 10 minutes of real time picks.
   Default is ``600``.

.. confval:: binder.deadTime

   Type: *int*

   Unit: *s*

   Dead time after the declaration of a new event for a new event to be
   declared. A value of 0 or less disables the dead time feature.
   Default is ``0``.

.. confval:: binder.maxObservationTimeSpan

   Type: *int*

   Unit: *s*

   Maximum observation time span in seconds.
   Default is ``50``.

.. confval:: binder.maxStationDistance

   Type: *double*

   Unit: *km*

   Maximum station distance in km.
   Default is ``100``.

.. confval:: binder.minimumPhases

   Type: *int*

   Minimum number of phases that an origin needs to be published.
   Default is ``2``.

.. confval:: binder.defaultDepth

   Type: *double*

   Unit: *km*

   Depth of the created origin. Since the binder does not make use of a
   location algorithm it needs to assign a depth.
   Default is ``30``.

.. confval:: binder.travelTimeProc

   Type: *string*

   Defines the travel time procedure to be used to calculate the
   time residuals. If this parameter is empty or not set, time
   residuals of arrivals are not going to be computed. Leaving
   this parameter empty does not reduce functionality, it justs
   omits attributes when sending results.
   Default is ``LOCSAT``.

.. confval:: binder.travelTimeModel

   Type: *string*

   Defines the travel time table to be used. You can defined
   whatever is supported by the travel time proc.
   Default is ``iasp91``.


Command-line
============

.. program:: binder


Generic
-------

.. option:: -h, --help

   show help message.

.. option:: -V, --version

   show version information

.. option:: --config-file arg

   Use alternative configuration file. When this option is used
   the loading of all stages is disabled. Only the given configuration
   file is parsed and used. To use another name for the configuration
   create a symbolic link of the application or copy it, eg scautopick \-> scautopick2.

.. option:: --plugins arg

   Load given plugins.

.. option:: -D, --daemon

   Run as daemon. This means the application will fork itself and
   doesn't need to be started with \&.

.. option:: --auto-shutdown arg

   Enable\/disable self\-shutdown because a master module shutdown. This only
   works when messaging is enabled and the master module sends a shutdown
   message \(enabled with \-\-start\-stop\-msg for the master module\).

.. option:: --shutdown-master-module arg

   Sets the name of the master\-module used for auto\-shutdown. This
   is the application name of the module actually started. If symlinks
   are used then it is the name of the symlinked application.

.. option:: --shutdown-master-username arg

   Sets the name of the master\-username of the messaging used for
   auto\-shutdown. If \"shutdown\-master\-module\" is given as well this
   parameter is ignored.


Verbosity
---------

.. option:: --verbosity arg

   Verbosity level [0..4]. 0:quiet, 1:error, 2:warning, 3:info, 4:debug

.. option:: -v, --v

   Increase verbosity level \(may be repeated, eg. \-vv\)

.. option:: -q, --quiet

   Quiet mode: no logging output

.. option:: --component arg

   Limits the logging to a certain component. This option can be given more than once.

.. option:: -s, --syslog

   Use syslog logging back end. The output usually goes to \/var\/lib\/messages.

.. option:: -l, --lockfile arg

   Path to lock file.

.. option:: --console arg

   Send log output to stdout.

.. option:: --debug

   Debug mode: \-\-verbosity\=4 \-\-console\=1

.. option:: --log-file arg

   Use alternative log file.


Messaging
---------

.. option:: -u, --user arg

   Overrides configuration parameter :confval:`connection.username`.

.. option:: -H, --host arg

   Overrides configuration parameter :confval:`connection.server`.

.. option:: -t, --timeout arg

   Overrides configuration parameter :confval:`connection.timeout`.

.. option:: -g, --primary-group arg

   Overrides configuration parameter :confval:`connection.primaryGroup`.

.. option:: -S, --subscribe-group arg

   A group to subscribe to. This option can be given more than once.

.. option:: --encoding arg

   Overrides configuration parameter :confval:`connection.encoding`.

.. option:: --start-stop-msg arg

   Sets sending of a start\- and a stop message.

.. option:: --test

   Test mode where no messages are sent.


Database
--------

.. option:: --db-driver-list

   List all supported database drivers.

.. option:: -d, --database arg

   The database connection string, format: service:\/\/user:pwd\@host\/database.
   \"service\" is the name of the database driver which can be
   queried with \"\-\-db\-driver\-list\".

.. option:: --config-module arg

   The configmodule to use.

.. option:: --inventory-db arg

   Load the inventory from the given database or file, format: [service:\/\/]location

.. option:: --db-disable

   Do not use the database at all

