.. highlight:: rst

.. _scmag:

#####
scmag
#####

**Calculates magnitudes of different types.**


Description
===========

The purpose of scmag is to compute magnitudes from pre-computed amplitudes.
Instead it takes amplitudes and origins as input and produces StationMagnitudes
and (network) Magnitudes as output. It does not access waveforms.
The resulting magnitudes are sent to the "MAGNITUDE" group. scmag doesn’t access
any waveforms. It only uses amplitudes previously calculated, e.g. by :ref:`scamp`.
The purpose of scmag is the decoupling of magnitude computation from amplitude
measurements. This allows several modules to generate amplitudes concurrently,
like scautopick and scamp. As soon as an origin comes in, the amplitudes related
to the picks are taken either from the memory buffer or the database to compute
the magnitudes. Currently the following magnitude types are implemented:

MLv
   Local magnitude calculated on the vertical component using a correction term
   to fit with the standard ML

MLh
   Local magnitude calculated on the horizontal components to SED specifications.

mb
   Narrow band body wave magnitude measured on a WWSSN-SP filtered trace

mB
   Broad band body wave magnitude

Mwp
   The body wave magnitude of Tsuboi et al. (1995)

Mjma
   Mjma is computed on displacement data using body waves of period < 30s

Ms(BB)
   Broad band surface-wave magnitude

Md
   Duration magnitude as described in https://earthquake.usgs.gov/research/software/#HYPOINVERSE

Additionally, scmag computes the following derived magnitudes: 

Mw(mB)
   Estimation of the moment magnitude Mw based on mB using the Mw vs. mB
   regression of Bormann and Saul (2008)

Mw(Mwp)
   Estimation of the moment magnitude Mw based on Mwp using the Mw vs. Mwp
   regression of Whitmore et al. (2002).

M(summary)
   Summary magnitude, which consists of a weighted average of the individual
   magnitudes and attempts to be a best possible compromise between all magnitudes.
   See below for configuration and also scevent for how to add the summary magnitude
   to the list of possible preferred magnitudes or how to make it always preferred.

Mw(avg)
   Estimation of the moment magnitude Mw based on a weighted average of other
   magnitudes, currently MLv, mb and Mw(mB), in future possibly other magnitudes as
   well, especially those suitable for very large events. The purpose of Mw(avg) is
   to have, at any stage during the processing, a “best possible” estimation of the
   magnitude by combining all available magnitudes into a single, weighted average.
   Initially the average will consist of only MLv and/or mb measurements, but as soon
   as Mw(mB) measurements become available, these (and in future other large-event
   magnitudes) become progressively more weight in the average.

If an amplitude is updated, the corresponding magnitude is updated as well.
This allows the computation of preliminary, real-time magnitudes even before
the full length of the P coda is available.


Relationship between amplitudes and origins
===========================================

scmag makes use of the fact that origins sent by scautoloc and scolv include
the complete set of arrivals, which reference picks used for origin computation.
The picks in turn are referenced by a number of amplitudes, some of which are
relevant for magnitude computation.


Summary magnitude
=================

scmag can compute a summary magnitude which is a weighted sum of all available
magnitudes. This magnitude is called **M** and is computed as follows:

.. math::

   M = \frac{\sum w_{i} M_{i}}{\sum w_i}

   w_{i} = a_i stacount(M_{i}) + b_i

The coefficients a and b can be configured per magnitude type. Furthermore each
magnitude type can be included or excluded from the summary magnitude calculation.



Configuration
=============

| :file:`etc/defaults/global.cfg`
| :file:`etc/defaults/scmag.cfg`
| :file:`etc/global.cfg`
| :file:`etc/scmag.cfg`
| :file:`~/.seiscomp3/global.cfg`
| :file:`~/.seiscomp3/scmag.cfg`

scmag inherits :ref:`global options<global-configuration>`.



.. confval:: magnitudes

   Type: *list:string*

   Definition of magnitude types to be calculated from amplitudes.
   Default is ``MLv, mb, mB, Mwp``.

.. confval:: connection.sendInterval

   Type: *int*

   Interval between 2 sending processes. The interval has influence how often information is updated.
   Default is ``1``.

.. confval:: connection.minimumArrivalWeight

   Type: *double*

   The minimum weight of an arrival to be used for magnitude calculations.
   Default is ``0.5``.

.. note::
   **summaryMagnitude.\***
   *The summary magnitude is building a weighted summary above all defined magnitude types. The single magnitude value is multiplied with the magnitude type specific weight. This is summed up for all magnitude types and the resulting sum is divided through the sum of all weights.*



.. confval:: summaryMagnitude.enabled

   Type: *boolean*

   Enables summary magnitude calculation.
   Default is ``true``.

.. confval:: summaryMagnitude.type

   Type: *string*

   Define the type\/name of the summary magnitude.
   Default is ``M``.

.. confval:: summaryMagnitude.minStationCount

   Type: *int*

   This is the minimum station magnitude required for any magnitude to contribute to
   the summary magnitude at all. If this is set to 4 then no magnitude with less than
   4 station magnitudes is taken into consideration even if this results in no summary
   magnitude at all. For this reason, the default here is 1 but in a purely automatic
   system it should be higher, at least 4 is recommended.
   Default is ``4``.

.. confval:: summaryMagnitude.blacklist

   Type: *list:string*

   Define the magnitude types to be excluded from the summary magnitude calculation.


.. confval:: summaryMagnitude.whitelist

   Type: *list:string*

   Define the magnitude types to be included in the summary magnitude calculation.


.. note::
   **summaryMagnitude.coefficients.\***
   *Define the coefficients to calculate the weight of a magnitude. weight = a*magStationCount+b*



.. confval:: summaryMagnitude.coefficients.a

   Type: *list:string*

   Define the coefficients a. Unnamed values define the default value.
   Default is ``0, Mw(mB):0.4, Mw(Mwp):0.4``.

.. confval:: summaryMagnitude.coefficients.b

   Type: *list:string*

   Define the coefficients b. Unnamed values define the default value.
   Default is ``1, MLv:2, Mw(mB):-1, Mw(Mwp):-1``.

.. confval:: magnitudes.average

   Type: *list:string*

   Defines the average method to use when computing the network magnitude.
   To define the average method per magnitude type append the type, eg:
   \"magnitudes.average \= default, MLv:median\"
   
   The default behaviour is to compute the mean if less than 4 contributed
   station magnitudes exist otherwise a trimmed mean of 25 percent is used.
   Options are \"default\", \"mean\" and \"trimmedMean\".
   Default is ``default``.


Command-line
============

.. program:: scmag


Generic
-------

.. option:: -h, --help

   show help message.

.. option:: -V, --version

   show version information

.. option:: --config-file arg

   Use alternative configuration file. When this option is used
   the loading of all stages is disabled. Only the given configuration
   file is parsed and used. To use another name for the configuration
   create a symbolic link of the application or copy it, eg scautopick \-> scautopick2.

.. option:: --plugins arg

   Load given plugins.

.. option:: -D, --daemon

   Run as daemon. This means the application will fork itself and
   doesn't need to be started with \&.

.. option:: --auto-shutdown arg

   Enable\/disable self\-shutdown because a master module shutdown. This only
   works when messaging is enabled and the master module sends a shutdown
   message \(enabled with \-\-start\-stop\-msg for the master module\).

.. option:: --shutdown-master-module arg

   Sets the name of the master\-module used for auto\-shutdown. This
   is the application name of the module actually started. If symlinks
   are used then it is the name of the symlinked application.

.. option:: --shutdown-master-username arg

   Sets the name of the master\-username of the messaging used for
   auto\-shutdown. If \"shutdown\-master\-module\" is given as well this
   parameter is ignored.

.. option:: -x, --expiry time

   Time span in hours after which objects expire.


Verbosity
---------

.. option:: --verbosity arg

   Verbosity level [0..4]. 0:quiet, 1:error, 2:warning, 3:info, 4:debug

.. option:: -v, --v

   Increase verbosity level \(may be repeated, eg. \-vv\)

.. option:: -q, --quiet

   Quiet mode: no logging output

.. option:: --component arg

   Limits the logging to a certain component. This option can be given more than once.

.. option:: -s, --syslog

   Use syslog logging back end. The output usually goes to \/var\/lib\/messages.

.. option:: -l, --lockfile arg

   Path to lock file.

.. option:: --console arg

   Send log output to stdout.

.. option:: --debug

   Debug mode: \-\-verbosity\=4 \-\-console\=1

.. option:: --log-file arg

   Use alternative log file.


Messaging
---------

.. option:: -u, --user arg

   Overrides configuration parameter :confval:`connection.username`.

.. option:: -H, --host arg

   Overrides configuration parameter :confval:`connection.server`.

.. option:: -t, --timeout arg

   Overrides configuration parameter :confval:`connection.timeout`.

.. option:: -g, --primary-group arg

   Overrides configuration parameter :confval:`connection.primaryGroup`.

.. option:: -S, --subscribe-group arg

   A group to subscribe to. This option can be given more than once.

.. option:: --encoding arg

   Overrides configuration parameter :confval:`connection.encoding`.

.. option:: --start-stop-msg arg

   Sets sending of a start\- and a stop message.


Database
--------

.. option:: --db-driver-list

   List all supported database drivers.

.. option:: -d, --database arg

   The database connection string, format: service:\/\/user:pwd\@host\/database.
   \"service\" is the name of the database driver which can be
   queried with \"\-\-db\-driver\-list\".

.. option:: --config-module arg

   The configmodule to use.

.. option:: --inventory-db arg

   Load the inventory from the given database or file, format: [service:\/\/]location

.. option:: --db-disable

   Do not use the database at all


Input
-----

.. option:: --ep file

   Defines an event parameters XML file to be read and processed. This
   implies offline mode and only processes all origins contained
   in that file. It computes magnitudes for all picks associated
   with an origin where amplitudes are available and outputs an XML
   file that additionally contains station\- and network magnitudes.

