.. highlight:: rst

.. _win2caps:

########
win2caps
########

**WIN CAPS plugin. Sends data read from socket or file to CAPS.**


Configuration
=============

| :file:`etc/defaults/global.cfg`
| :file:`etc/defaults/win2caps.cfg`
| :file:`etc/global.cfg`
| :file:`etc/win2caps.cfg`
| :file:`~/.seiscomp3/global.cfg`
| :file:`~/.seiscomp3/win2caps.cfg`

win2caps inherits :ref:`global options<global-configuration>`.



.. confval:: input.port

   Type: *uint*

   Listen for incoming packets at given port
   Default is ``18000``.

.. confval:: output.host

   Type: *string*

   Data output host
   Default is ``localhost``.

.. confval:: output.port

   Type: *int*

   Data output port
   Default is ``18003``.

.. confval:: output.bufferSize

   Type: *uint*

   Size \(bytes\) of the packet buffer
   Default is ``1048576``.

.. confval:: streams.file

   Type: *string*

   File to read streams from. Each line defines a mapping between a station and stream id. Line format is [ID NET.STA].



Command-line
============

.. program:: win2caps


Generic
-------

.. option:: -h, --help

   show help message.

.. option:: -V, --version

   show version information

.. option:: --config-file arg

   Use alternative configuration file. When this option is used
   the loading of all stages is disabled. Only the given configuration
   file is parsed and used. To use another name for the configuration
   create a symbolic link of the application or copy it, eg scautopick \-> scautopick2.


Verbosity
---------

.. option:: --verbosity arg

   Verbosity level [0..4]. 0:quiet, 1:error, 2:warning, 3:info, 4:debug

.. option:: -v, --v

   Increase verbosity level \(may be repeated, eg. \-vv\)

.. option:: -q, --quiet

   Quiet mode: no logging output

.. option:: --print-component arg

   For each log entry print the component right after the
   log level. By default the component output is enabled
   for file output but disabled for console output.

.. option:: --component arg

   Limits the logging to a certain component. This option can be given more than once.

.. option:: -s, --syslog

   Use syslog logging back end. The output usually goes to \/var\/lib\/messages.

.. option:: -l, --lockfile arg

   Path to lock file.

.. option:: --console arg

   Send log output to stdout.

.. option:: --debug

   Debug mode: \-\-verbosity\=4 \-\-console\=1

.. option:: --trace

   Trace mode: \-\-verbosity\=4 \-\-console\=1 \-\-print\-component\=1 \-\-print\-context\=1

.. option:: --log-file arg

   Use alternative log file.


Input
-----

.. option:: --station arg

   Sets the station and sampling interval to use. Format is [net.sta\@?]

.. option:: -f, --file arg

   Load CREX data directly from file

.. option:: --read-from arg

   Read packets from this file

.. option:: --port arg

   Listen for incoming packets at given port


Output
------

.. option:: -H, --host arg

   Data output host

.. option:: -p, --port arg

   Data output port


Streams
-------

.. option:: --streams-file arg

   File to read streams from. Each line defines a mapping between a station and stream id. Line format is [ID NET.STA].

