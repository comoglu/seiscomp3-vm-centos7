.. highlight:: rst

.. _autosigma:

#########
autosigma
#########

**Strong event parameter processing**


Description
===========

.. _autosigma-control-param:

AUTOSIGMA is an automatic module to compute and visualize ground motion
parameters based on :term:`SeisComP3 events <sigma-glossary-events>`. The computed parameters are
stored as :term:`incidents <sigma-glossary-incidents>` in the SIGMA database.



Configuration
=============

| :file:`etc/defaults/global.cfg`
| :file:`etc/defaults/autosigma.cfg`
| :file:`etc/global.cfg`
| :file:`etc/autosigma.cfg`
| :file:`~/.seiscomp3/global.cfg`
| :file:`~/.seiscomp3/autosigma.cfg`

autosigma inherits :ref:`global options<global-configuration>`.



.. confval:: magnitudeFilterTable

   Type: *list:string*

   Magnitude dependent filter table. The format is
   \"mag1:fmin1;fmax1, mag2:fmin2;fmax2, mag3:fmin3;fmax3\".
   If a magnitude falls between two configured magnitudes the filter of
   the lower magnitude is then used. No interpolation takes place.
   Magnitude outside the configured range are clipped to the
   lowest\/highest value.
   Frequency values are given as simple positive doubles \(Hz is assumed\)
   or with suffix \"fNyquist\" which is then multiplied by the
   Nyquist frequency of the data to get the final corner frequency.
   
   The final frequency values are used as a replacement for
   the profile filter.loFreq and filter.hiFreq parameters unless
   the table is empty.


.. confval:: profiles

   Type: *list:string*

   A list of processing profiles.


.. note::
   **profile.\***
   *Profile definitions for customized regions with specific GMPEs.*
   *Internally defined models ("EEWD/CEA2014" and*
   *"EEWD/BEA2014") and external GMPEs, e.g.*
   *"py3gmpe/AbrahamsonEtAl2014" can be used.*
   *External GMPEs are defined in the gmpe section of the configuration.*



.. note::

   **profile.\$name.\***
   *Region and GMPE definition.*
   \$name is a placeholder for the name to be used and needs to be added to :confval:`profiles` to become active.

   .. code-block:: sh

      profiles = a,b
      profile.a.value1 = ...
      profile.b.value1 = ...
      # c is not active because it has not been added
      # to the list of profiles
      profile.c.value1 = ...


.. confval:: profile.\$name.region

   Type: *string*

   The region name of a configured region \(e.g. with BNA
   files in \~\/.seiscomp3\/bna\) for which the processing
   parameters are valid. The events epicenter is used for
   the inside\-test with the region polygon.


.. confval:: profile.\$name.rect

   Type: *list:double*

   Configures an alternative region filter with a
   rectangle. The 4 expected values are latMin, lonMin,
   latMax, lonMax. The region parameter takes priortity
   over the rect parameter.


.. confval:: profile.\$name.gmpe

   Type: *string*

   Defines the GMPE model to be used. The format is
   <provider>\/<model>, analogue to SIGMA,e.g.
   \"EEWD\/CEA2014\" or \"py3gmpe\/AbrahamsonEtAl2014\".


.. confval:: profile.\$name.minimumMagnitude

   Type: *double*

   Defines the minimum magnitude that must be reached to start
   processing. By default this check is disabled.


.. confval:: profile.\$name.deltaMagnitude

   Type: *double*

   Trigger re\-processing when changes in event magnitude exceed deltaMagnitude.
   Default is ``0.5``.

.. confval:: profile.\$name.deltaLocation

   Type: *double*

   Unit: *km*

   Trigger re\-processing when changes in hypocenter location exceed deltaLocation.
   Default is ``10``.

.. note::
   **profile.\$name.capture.\***
   *Parameters controlling the image capturing.*



.. confval:: profile.\$name.capture.dim.x

   Type: *int*

   Unit: *px*

   Defines the X resolution of the output map images in pixels.
   Default is ``512``.

.. confval:: profile.\$name.capture.dim.y

   Type: *int*

   Unit: *px*

   Defines the Y resolution of the output map images in pixels.
   Default is ``512``.

.. note::
   **profile.\$name.grid.\***
   *Parameters controlling the generation of grids for plotting on maps.*



.. confval:: profile.\$name.grid.extent.lat

   Type: *double*

   Unit: *deg*

   Defines the latitudinal extent of the processing grid for
   GMPEs.
   Default is ``5``.

.. confval:: profile.\$name.grid.extent.lon

   Type: *double*

   Unit: *deg*

   Defines the longitudinal extent of the processing grid for
   GMPEs.
   Default is ``5``.

.. confval:: profile.\$name.grid.dim.x

   Type: *int*

   Unit: *cnt*

   Defines the X \(longitude\) resolution of the processing grid.
   Default is ``512``.

.. confval:: profile.\$name.grid.dim.y

   Type: *int*

   Unit: *cnt*

   Defines the Y \(latitude\) resolution of the processing grid.
   Default is ``512``.

.. note::
   **profile.\$name.processing.\***
   *Processing parameters.*



.. confval:: profile.\$name.processing.targetDistricts

   Type: *list:string*

   A list of Shapefile paths used as target districts for GMPE\/intensity
   computation used for GeoJSON export.


.. confval:: profile.\$name.processing.preEventWindowLength

   Type: *int*

   Unit: *s*

   The pre event time window length in seconds.
   Default is ``60``.

.. confval:: profile.\$name.processing.postEventWindowLength

   Type: *string*

   Unit: *s*

   Default value of time window length after the event onset in seconds. This
   value is an numerical expression and can include four symbols: d \(distance in km\),
   D \(distance in degree\), az \(azimuth clockwise from North\) and M \(magnitude value\).
   Default is ``(0.36*d)+60``.

.. confval:: profile.\$name.processing.eventCutOff

   Type: *boolean*

   Enables\/disables pre event cut\-off. A hardcoded sta\/lta algorithm
   \(with sta\=0.1s, lta\=2s, sta\/lta threshold\=1.2\) is run on the time
   window defined by \(expected_P_arrival_time \- 15 s\). The pre event
   window is hence defined as
   [t\(sta\/lta \=1.2\) \- 15.5s, t\(sta\/lta \=1.2\) \- 0.5s].
   Default is ``true``.

.. confval:: profile.\$name.processing.afterShockRemoval

   Type: *boolean*

   Enables\/disables aftershock removal \(Figini, 2006; Paolucci et al., 2008\)
   Default is ``true``.

.. confval:: profile.\$name.processing.durationScale

   Type: *double*

   Defines the factor applied to the signigicant duration to define the
   processing spetra time window. If that value is <\= 0 the totalTimeWindowLength
   is used.
   Default is ``1.5``.

.. confval:: profile.\$name.processing.STAlength

   Type: *double*

   Unit: *s*

   Specifies the STA length in seconds of the applied STA\/LTA check.
   Default is ``1``.

.. confval:: profile.\$name.processing.LTAlength

   Type: *double*

   Unit: *s*

   Specifies the LTA length in seconds of the applied STA\/LTA check.
   Default is ``60``.

.. confval:: profile.\$name.processing.STALTAratio

   Type: *double*

   Specifies the minimum STALTA ratio to be reached to further process
   a station.
   Default is ``3``.

.. confval:: profile.\$name.processing.STALTAmargin

   Type: *double*

   Unit: *s*

   Specifies the number of seconds around P to be used to check the STA\/LTA ratio.
   Default is ``5``.

.. note::
   **profile.\$name.processing.filter.\***
   *Parameters of the 1st stage filter.*



.. confval:: profile.\$name.processing.filter.order

   Type: *int*

   Specifies the order of the 1st stage filter.
   Default is ``4``.

.. confval:: profile.\$name.processing.filter.loFreq

   Type: *double*

   Unit: *Hz*

   Specifies the frequency of the 1st stage hi\-pass filter. If this
   parameter is equal to 0 the hi\-pass filter is not used.
   If negative, then the absolute value is multiplied
   by the Nyquist frequency of the data to get the final corner
   frequency of the filter.
   Default is ``0.025``.

.. confval:: profile.\$name.processing.filter.hiFreq

   Type: *double*

   Unit: *Hz*

   Specifies the frequency of the 1st stage lo\-pass filter. If this
   parameter is equal to 0 the lo\-pass filter is not used.
   If negative, then the absolute value is multiplied
   by the Nyquist frequency of the data to get the final corner
   frequency of the filter.
   Default is ``40``.

.. note::
   **profile.\$name.processing.spectra.\***
   *Parameters controlling the calculation of Fourier spectra.*



.. confval:: profile.\$name.processing.spectra.taperLength

   Type: *double*

   Unit: *s*

   Defines the cosine taper length in seconds if non\-causal filters
   are activated applied on either side of the waveform. If a
   negative length is given 10 percent of the pre event window length
   is used on either side of the waveform.
   Default is ``-1``.

.. confval:: profile.\$name.processing.spectra.padLength

   Type: *double*

   Unit: *s*

   The length of the zero padding window in seconds applied on either
   side of the waveform. If negative, it is computed following
   Boore \(2005\) as 1.5\*order\/corner_freq and applied half at the
   beginning and half at the end of the waveform.
   Default is ``-1``.

.. note::
   **profile.\$name.processing.prs.\***
   *Parameters controlling the calculation of Pseudo Response Spectra.*



.. confval:: profile.\$name.processing.prs.damping

   Type: *double*

   Unit: *%*

   Specifies the damping value \(in percent\) for computation
   of the pseudo absolute acceleration elastic response
   spectrum.
   Default is ``5``.

.. confval:: profile.\$name.processing.prs.periods

   Type: *list:double*

   Unit: *s*

   Specifies the natural periods for computation of the
   pseudo absolute acceleration elastic response spectrum.
   Default is ``0.3,1,3``.

.. confval:: profile.\$name.processing.prs.Tmin

   Type: *double*

   Unit: *s*

   The lower bound of the period range to compute response spectra for.
   A negative value disables the range and only the list of
   periods is used.
   Default is ``-1``.

.. confval:: profile.\$name.processing.prs.Tmax

   Type: *double*

   Unit: *s*

   The upper bound of the period range to compute response spectra for.
   A negative value disables the range and only the list of
   periods is used.
   Default is ``-1``.

.. confval:: profile.\$name.processing.prs.naturalPeriods

   Type: *int*

   The number of natural periods that are created with linear
   spacing between Tmin and Tmax.
   Default is ``0``.

.. note::
   **acquisition.\***
   *Control parameters for data acquisition.*



.. confval:: acquisition.maximumEpicentralDistance

   Type: *double*

   Unit: *km*

   Maximum epicentral distance for considering stations.
   Default is ``400``.

.. confval:: acquisition.magnitudeDistanceTable

   Type: *list:string*

   Magnitude dependent maximum epicentral distance table. The format is
   \"mag1:km1, mag2:km2, mag3:km3\". The distance value is
   given in km. If a magnitude falls between two configured magnitudes
   the distance of the lower magnitude is used then. No interpolation
   is performed. Magnitude outside the configured range are clipped
   to the lowest\/highest value.
   
   The final value is used as a replacement for
   acquisition.maximumEpicentralDistance unless the table is empty.
   Example: \"3:400, 4:450, 5:500\"


.. confval:: acquisition.initialTimeout

   Type: *int*

   Unit: *s*

   
   Default is ``30``.

.. confval:: acquisition.runningTimeout

   Type: *int*

   Unit: *s*

   
   Default is ``2``.

.. confval:: acquisition.streams.whitelist

   Type: *list:string*

   A list of streams considered for processing.
   If set, no other streams will be used.


.. confval:: acquisition.streams.blacklist

   Type: *list:string*

   A list of streams excluded from processing.
   The listed streams will be ignored.


.. note::
   **cron.\***
   *Timing parameters controlling the event processing.*



.. confval:: cron.wakeupInterval

   Type: *int*

   Unit: *s*

   Time interval to test for new events.
   Default is ``10``.

.. confval:: cron.eventKeep

   Type: *int*

   Unit: *s*

   The time interval to keep events in the buffer. During this
   interval events are available for processing.
   Default is ``3600``.

.. confval:: cron.delayTimes

   Type: *list:int*

   Unit: *s*

   List of time intervals to delay the processing of new events.
   The delayTimes are added to the originTime.
   delayTimes > 0 s allow to load complete waveforms within the
   requested time windows. Provide a comma\-separated list to repreat
   the data processing of new events allowing for more complete
   data from stations with longer data delay or larger epicentral
   distances.


.. confval:: cron.updateDelay

   Type: *int*

   Unit: *s*

   Time delay to test for event updates of already loaded events.
   Default is ``60``.

.. note::
   **gmpe.\***
   *Parameters controlling the GMPEs.*



.. confval:: gmpe.vs30grid

   Type: *path*

   Specifies the path to a Vs30 GeoTIFF or grid file, e.g.
   the global_vs30.tif GeoTIFF file provided by USGS. Grid files
   must be in a modified Surfer6 format where dimX and
   dimY are integers rather than shorts. The magic first 4 bytes
   contain \"GGBB\". The GeoTIFF reader is
   activated if the file extension is \".tif\".


.. note::
   **gmpe.py.\***
   *Adds configuration for processing based on Python2 GMPE models.*
   *Refer to as "py2gmpe" for the provider configured in profile.gempa.*



.. confval:: gmpe.py.models

   Type: *list:string*

   The PyGMPE models available and their corresponding python filename.
   The format of each list entry is \"modelname;filename\". If
   the semicolon is omitted then the item is expected to be the
   modelname and a file with the same name and extension .py will
   be searched for.


.. confval:: gmpe.py.basePath

   Type: *path*

   The base path for all python model implementations.
   Default is ``@CONFIGDIR@/pygmpe/``.

.. note::
   **gmpe.py3.\***
   *Adds configuration for processing based on Python3 GMPE models.*
   *Refer to as "py3gmpe" for the provider configured in profile.gempa.*



.. confval:: gmpe.py3.models

   Type: *list:string*

   The Py3GMPE models available and their corresponding python filename.
   The format of each list entry is \"modelname;filename\". If
   the semicolon is omitted then the item is expected to be the
   modelname and a file with the same name and extension .py will
   be searched for.


.. confval:: gmpe.py3.basePath

   Type: *path*

   The base path for all python model implementations.
   Default is ``@CONFIGDIR@/pygmpe/``.

.. note::
   **output.\***
   *Parameters controlling the output of processing products.*



.. confval:: output.database

   Type: *string*

   Configures the URI of the output database containing the SIGMA tables.
   If nothing is
   configured then the SC3 database will be used.


.. confval:: output.exportPath

   Type: *path*

   The path where all result files are saved.
   Default is ``@LOGDIR@/autosigma``.

.. confval:: output.exportScript

   Type: *path*

   The script that will be called when an incident is
   exported. Please ensure that the script is executable.
   3 parameters are passed: The eventID, an internal
   eventID and the export path where all files are
   stored.


.. confval:: output.logfile

   Type: *path*

   Name of the logfile containing the processing logs.


.. note::
   **capture.\***
   *Parameters controlling the image capturing.*



.. confval:: capture.dim.x

   Type: *int*

   Unit: *px*

   Defines the X resolution of the output map images in pixels.
   Default is ``512``.

.. confval:: capture.dim.y

   Type: *int*

   Unit: *px*

   Defines the Y resolution of the output map images in pixels.
   Default is ``512``.

.. note::
   **grid.\***
   *Parameters controlling the generation of grids for plotting on maps.*



.. confval:: grid.extent.lat

   Type: *double*

   Unit: *deg*

   Defines the latitudinal extent of the processing grid for
   GMPEs.
   Default is ``5``.

.. confval:: grid.extent.lon

   Type: *double*

   Unit: *deg*

   Defines the longitudinal extent of the processing grid for
   GMPEs.
   Default is ``5``.

.. confval:: grid.dim.x

   Type: *int*

   Unit: *cnt*

   Defines the X \(longitude\) resolution of the processing grid.
   Default is ``512``.

.. confval:: grid.dim.y

   Type: *int*

   Unit: *cnt*

   Defines the Y \(latitude\) resolution of the processing grid.
   Default is ``512``.

.. confval:: scheme.colors.sigma.intensity

   Type: *gradient*

   The intensity color gradient for ground motion values.
   This gradient will be used without normalization.


.. confval:: scheme.colors.sigma.gradient

   Type: *gradient*

   The base color gradient for ground motion values.
   This gradient will be scaled to the final value range
   unless absolute scale is enabled in sigma.


.. confval:: scheme.colors.sigma.normalize

   Type: *boolean*

   Whether to normalize the base color gradient to the
   current value range or not.
   Default is ``true``.


Bindings
========


Configuration
-------------


.. confval:: enable

   Type: *boolean*

   Enables\/disables the station for strong motion processing.
   Default is ``true``.

.. confval:: channels

   Type: *list:string*

   Defines a list of preferred channels that are activated
   by default for that station. If this option is undefined
   then the highest sample rate channels for acceleration
   and velocity are used by default. If the list is empty
   \(which is a different state than undefined\) then no channels
   are selected by default.




Command-line
============

.. program:: autosigma


Generic
-------

.. option:: -h, --help

   show help message.

.. option:: -V, --version

   show version information

.. option:: --config-file arg

   Use alternative configuration file. When this option is used
   the loading of all stages is disabled. Only the given configuration
   file is parsed and used. To use another name for the configuration
   create a symbolic link of the application or copy it, eg scautopick \-> scautopick2.

.. option:: --plugins arg

   Load given plugins.

.. option:: -D, --daemon

   Run as daemon. This means the application will fork itself and
   doesn't need to be started with \&.

.. option:: --auto-shutdown arg

   Enable\/disable self\-shutdown because a master module shutdown. This only
   works when messaging is enabled and the master module sends a shutdown
   message \(enabled with \-\-start\-stop\-msg for the master module\).

.. option:: --shutdown-master-module arg

   Sets the name of the master\-module used for auto\-shutdown. This
   is the application name of the module actually started. If symlinks
   are used then it is the name of the symlinked application.

.. option:: --shutdown-master-username arg

   Sets the name of the master\-username of the messaging used for
   auto\-shutdown. If \"shutdown\-master\-module\" is given as well this
   parameter is ignored.


Verbosity
---------

.. option:: --verbosity arg

   Verbosity level [0..4]. 0:quiet, 1:error, 2:warning, 3:info, 4:debug

.. option:: -v, --v

   Increase verbosity level \(may be repeated, eg. \-vv\)

.. option:: -q, --quiet

   Quiet mode: no logging output

.. option:: --component arg

   Limits the logging to a certain component. This option can be given more than once.

.. option:: -s, --syslog

   Use syslog logging back end. The output usually goes to \/var\/lib\/messages.

.. option:: -l, --lockfile arg

   Path to lock file.

.. option:: --console arg

   Send log output to stdout.

.. option:: --debug

   Debug mode: \-\-verbosity\=4 \-\-console\=1

.. option:: --log-file arg

   Use alternative log file.


Database
--------

.. option:: --db-driver-list

   List all supported database drivers.

.. option:: -d, --database arg

   The database connection string, format: service:\/\/user:pwd\@host\/database.
   \"service\" is the name of the database driver which can be
   queried with \"\-\-db\-driver\-list\".

.. option:: --config-module arg

   The configmodule to use.

.. option:: --inventory-db arg

   Load the inventory from the given database or file, format: [service:\/\/]location

.. option:: --db-disable

   Do not use the database at all


Records
-------

.. option:: --record-driver-list

   List all supported record stream drivers

.. option:: -I, --record-url arg

   The recordstream source URL, format: [service:\/\/]location[#type].
   \"service\" is the name of the recordstream driver which can be
   queried with \"\-\-record\-driver\-list\". If \"service\"
   is not given \"file:\/\/\" is used.

.. option:: --record-file arg

   Specify a file as record source.

.. option:: --record-type arg

   Specify a type for the records being read.


Mode
----

.. option:: --offline

   Invoke AUTOSIGMA in offline mode without
   connecting to the SeisComP3 messaging system.

.. option:: -E, --event-ID

   ID of the event to be processed in offline mode.

.. option:: --dump-config

   Dump the configuration and exit.

