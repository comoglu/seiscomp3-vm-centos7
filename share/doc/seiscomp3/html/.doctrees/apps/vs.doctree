�cdocutils.nodes
document
q)�q}q(U	nametypesq}q(X   developmentqNX-   false alarms, missed events, solution qualityqNX0   'sed public license for seiscomp3 contributions'q�X   descriptionq	NX)   configuring and optimizing vs(s3) for eewq
NXh   http://www.seismo.ethz.ch/research/groups/alrt/people/clintonj/publ_jc/behr_et_all_srl201602_vs_sc3_.pdfq�X   vs and seiscomp3qNX   vsq�X
   referencesqNX   understanding vs outputqNX
   vs licenseqNuUsubstitution_defsq}qUparse_messagesq]qcdocutils.nodes
system_message
q)�q}q(U	rawsourceqU Uparentqcdocutils.nodes
section
q)�q}q(hU U
referencedqKhhUsourceqXZ   /home/sysop/gitlocal/bmp/jakarta-2018.327-release/seiscomp3/build-gpkg/doc/src/apps/vs.rstqUexpect_referenced_by_nameq }q!hcdocutils.nodes
target
q")�q#}q$(hX   .. _VS:hhhhUtagnameq%Utargetq&U
attributesq'}q((Uidsq)]Ubackrefsq*]Udupnamesq+]Uclassesq,]Unamesq-]Urefidq.Uvsq/uUlineq0KUdocumentq1hUchildrenq2]ubsh%Usectionq3h'}q4(h+]q5X   vsq6ah,]h*]h)]q7(h/Uid1q8eh-]q9hauh0Kh1hUexpect_referenced_by_idq:}q;h/h#sh2]q<(cdocutils.nodes
title
q=)�q>}q?(hX   VSq@hhhhh%UtitleqAh'}qB(h+]h,]h*]h)]h-]uh0Kh1hh2]qCcdocutils.nodes
Text
qDX   VSqE��qF}qG(hh@hh>ubaubcdocutils.nodes
paragraph
qH)�qI}qJ(hX9   **Near instantaneous estimates of earthquake magnitude.**qKhhhhh%U	paragraphqLh'}qM(h+]h,]h*]h)]h-]uh0K	h1hh2]qNcdocutils.nodes
strong
qO)�qP}qQ(hhKh'}qR(h+]h,]h*]h)]h-]uhhIh2]qShDX5   Near instantaneous estimates of earthquake magnitude.qT��qU}qV(hU hhPubah%UstrongqWubaubh)�qX}qY(hU hhhhh%h3h'}qZ(h+]h,]h*]h)]q[Udescriptionq\ah-]q]h	auh0Kh1hh2]q^(h=)�q_}q`(hX   DescriptionqahhXhhh%hAh'}qb(h+]h,]h*]h)]h-]uh0Kh1hh2]qchDX   Descriptionqd��qe}qf(hhahh_ubaubhH)�qg}qh(hX�  The Virtual Seismologist in SeisComP3 (VS(SC3)) provides near instantaneous
estimates of earthquake magnitude as soon as SeisComp3 origins are available. With a
well-configured SeisComP3 system running on a dense network, magnitudes for
local events can be available within 10-20 s of origin time. VS(SC3) can be a key
component of an earthquake early warning system, and can be used to provide
rapid earthquake notifications. With the capability to estimate magnitude
(given a location estimate) with 3-seconds of P-wave information at a single
station, VS(SC3) magnitude estimates are tens of seconds faster than
conventional Ml calculations, which require waiting for the peak S-wave
amplitudes. The VS magnitude estimation relationships consist of 1) a
relationship between observed ground motion ratios (between vertical
acceleration and vertical displacement) and magnitude, and 2) envelope
attenuation relationships describing how various channels of envelope
amplitudes vary as a function of magnitude and distance. These relationships
were derived from a Southern California dataset with events in the magnitude
range 2.5 <= M <= 7.6 and the Next Generation Attenuation (NGA) strong motion
dataset. Once a SeisComp3 location estimate is available, VS magnitude estimates
can be computed with as little as 3 seconds of envelope data at a single
station (i.e., 3 seconds after trigger time at a single station). Typically,
usable envelope data is available at numerous stations by the time the first
SeisComp3 origin is available. The VS magnitude estimates are then updated every
second for 30 seconds (configurable). The SeisComp3 implementation allows for use of
broadband high-gain seismometers (with clipping value selected) as well as
strong motion data. For co-located stations, VS magnitudes are calculated using
the strong motion channels if the broadband channels saturate.
VS magnitudes in SeisComp3 are called MVS.qihhXhhh%hLh'}qj(h+]h,]h*]h)]h-]uh0Kh1hh2]qkhDX�  The Virtual Seismologist in SeisComP3 (VS(SC3)) provides near instantaneous
estimates of earthquake magnitude as soon as SeisComp3 origins are available. With a
well-configured SeisComP3 system running on a dense network, magnitudes for
local events can be available within 10-20 s of origin time. VS(SC3) can be a key
component of an earthquake early warning system, and can be used to provide
rapid earthquake notifications. With the capability to estimate magnitude
(given a location estimate) with 3-seconds of P-wave information at a single
station, VS(SC3) magnitude estimates are tens of seconds faster than
conventional Ml calculations, which require waiting for the peak S-wave
amplitudes. The VS magnitude estimation relationships consist of 1) a
relationship between observed ground motion ratios (between vertical
acceleration and vertical displacement) and magnitude, and 2) envelope
attenuation relationships describing how various channels of envelope
amplitudes vary as a function of magnitude and distance. These relationships
were derived from a Southern California dataset with events in the magnitude
range 2.5 <= M <= 7.6 and the Next Generation Attenuation (NGA) strong motion
dataset. Once a SeisComp3 location estimate is available, VS magnitude estimates
can be computed with as little as 3 seconds of envelope data at a single
station (i.e., 3 seconds after trigger time at a single station). Typically,
usable envelope data is available at numerous stations by the time the first
SeisComp3 origin is available. The VS magnitude estimates are then updated every
second for 30 seconds (configurable). The SeisComp3 implementation allows for use of
broadband high-gain seismometers (with clipping value selected) as well as
strong motion data. For co-located stations, VS magnitudes are calculated using
the strong motion channels if the broadband channels saturate.
VS magnitudes in SeisComp3 are called MVS.ql��qm}qn(hhihhgubaubh)�qo}qp(hU hhXhhh%h3h'}qq(h+]h,]h*]h)]qrUdevelopmentqsah-]qthauh0K,h1hh2]qu(h=)�qv}qw(hX   Developmentqxhhohhh%hAh'}qy(h+]h,]h*]h)]h-]uh0K,h1hh2]qzhDX   Developmentq{��q|}q}(hhxhhvubaubhH)�q~}q(hX�  The Virtual Seismologist method is a Bayesian approach to earthquake early
warning (EEW) that estimates earthquake magnitude, location, and the
distribution of peak ground shaking using observed picks and ground motion
amplitudes, predefined prior information, and envelope attenuation
relationships (Cua, 2005; Cua and Heaton, 2007; Cua et al., 2009). The
application of Bayes' theorem in EEW (Cua, 2005) states that the most probable
source estimate at any given time is a combination of contributions from prior
information (candidate priors include network topology or station health status,
regional hazard maps, earthquake forecasts, and the Gutenberg-Richter
magnitude-frequency relationship) and constraints from the available
real-time ground motion and arrival observations. VS is envisioned as an
intelligent, automated system capable of mimicking how human seismologists can
make quick, relatively accurate “back-of-the-envelope” interpretations of
real-time (and at times, incomplete) earthquake information, using a mix of
experience, background information, and real-time data. The formulation of the
VS Bayesian methodology, including the development of the underlying
relationships describing the dependence of various channels of ground motion
envelopes on magnitude and distance, and how these pieces come together in EEW
source estimation, was the result of the PhD research of Dr. Georgia Cua with
Prof. Thomas Heaton at Caltech, from 1998 through 2004.q�hhohhh%hLh'}q�(h+]h,]h*]h)]h-]uh0K.h1hh2]q�hDX�  The Virtual Seismologist method is a Bayesian approach to earthquake early
warning (EEW) that estimates earthquake magnitude, location, and the
distribution of peak ground shaking using observed picks and ground motion
amplitudes, predefined prior information, and envelope attenuation
relationships (Cua, 2005; Cua and Heaton, 2007; Cua et al., 2009). The
application of Bayes' theorem in EEW (Cua, 2005) states that the most probable
source estimate at any given time is a combination of contributions from prior
information (candidate priors include network topology or station health status,
regional hazard maps, earthquake forecasts, and the Gutenberg-Richter
magnitude-frequency relationship) and constraints from the available
real-time ground motion and arrival observations. VS is envisioned as an
intelligent, automated system capable of mimicking how human seismologists can
make quick, relatively accurate “back-of-the-envelope” interpretations of
real-time (and at times, incomplete) earthquake information, using a mix of
experience, background information, and real-time data. The formulation of the
VS Bayesian methodology, including the development of the underlying
relationships describing the dependence of various channels of ground motion
envelopes on magnitude and distance, and how these pieces come together in EEW
source estimation, was the result of the PhD research of Dr. Georgia Cua with
Prof. Thomas Heaton at Caltech, from 1998 through 2004.q���q�}q�(hh�hh~ubaubhH)�q�}q�(hX~  The first real-time VS prototype system was developed by Georgia Cua and Michael
Fischer at ETH Zurich from 2006-2012 (http://www.seismo.ethz.ch/research/vs).
This first prototype used location estimates generated
by the Earthworm Binder module (Dietz, 2002) as inputs to the VS magnitude
estimation. This architecture has been undergoing continuous real-time testing
in California (since 2008) and Switzerland (since 2010). In California, VS is
one of the three EEW algorithms that make up the CISN ShakeAlert EEW system
(http://www.cisn.org/eew/). The other algorithms are the ElarmS algorithm from
UC Berkeley and the TauC/Pd OnSite algorithm from Caltech.
In 2012/13, with funding from the EU projects NERA ("Network of European
Research Infrastructures for Earthquake Risk Assessment and Mitigation") and
REAKT ("Strategies and Tools for Real-Time EArthquake RisK ReducTion"), VS was
integrated into SeisComP3 by the Seismic Network group at the SED in ETH
Zurich and gempa GmbH. Both real-time VS implementations (Binder- and SeisComp3-based)
focus on real-time processing of available pick and envelope data. Prior
information is not included.hhohhh%hLh'}q�(h+]h,]h*]h)]h-]uh0KCh1hh2]q�(hDXw   The first real-time VS prototype system was developed by Georgia Cua and Michael
Fischer at ETH Zurich from 2006-2012 (q���q�}q�(hXw   The first real-time VS prototype system was developed by Georgia Cua and Michael
Fischer at ETH Zurich from 2006-2012 (hh�ubcdocutils.nodes
reference
q�)�q�}q�(hX%   http://www.seismo.ethz.ch/research/vsq�h'}q�(Urefurih�h)]h*]h+]h,]h-]uhh�h2]q�hDX%   http://www.seismo.ethz.ch/research/vsq���q�}q�(hU hh�ubah%U	referenceq�ubhDXo  ).
This first prototype used location estimates generated
by the Earthworm Binder module (Dietz, 2002) as inputs to the VS magnitude
estimation. This architecture has been undergoing continuous real-time testing
in California (since 2008) and Switzerland (since 2010). In California, VS is
one of the three EEW algorithms that make up the CISN ShakeAlert EEW system
(q���q�}q�(hXo  ).
This first prototype used location estimates generated
by the Earthworm Binder module (Dietz, 2002) as inputs to the VS magnitude
estimation. This architecture has been undergoing continuous real-time testing
in California (since 2008) and Switzerland (since 2010). In California, VS is
one of the three EEW algorithms that make up the CISN ShakeAlert EEW system
(hh�ubh�)�q�}q�(hX   http://www.cisn.org/eew/q�h'}q�(Urefurih�h)]h*]h+]h,]h-]uhh�h2]q�hDX   http://www.cisn.org/eew/q���q�}q�(hU hh�ubah%h�ubhDX[  ). The other algorithms are the ElarmS algorithm from
UC Berkeley and the TauC/Pd OnSite algorithm from Caltech.
In 2012/13, with funding from the EU projects NERA ("Network of European
Research Infrastructures for Earthquake Risk Assessment and Mitigation") and
REAKT ("Strategies and Tools for Real-Time EArthquake RisK ReducTion"), VS was
integrated into SeisComP3 by the Seismic Network group at the SED in ETH
Zurich and gempa GmbH. Both real-time VS implementations (Binder- and SeisComp3-based)
focus on real-time processing of available pick and envelope data. Prior
information is not included.q���q�}q�(hX[  ). The other algorithms are the ElarmS algorithm from
UC Berkeley and the TauC/Pd OnSite algorithm from Caltech.
In 2012/13, with funding from the EU projects NERA ("Network of European
Research Infrastructures for Earthquake Risk Assessment and Mitigation") and
REAKT ("Strategies and Tools for Real-Time EArthquake RisK ReducTion"), VS was
integrated into SeisComP3 by the Seismic Network group at the SED in ETH
Zurich and gempa GmbH. Both real-time VS implementations (Binder- and SeisComp3-based)
focus on real-time processing of available pick and envelope data. Prior
information is not included.hh�ubeubeubh)�q�}q�(hU hhXhhh%h3h'}q�(h+]h,]h*]h)]q�Uvs-and-seiscomp3q�ah-]q�hauh0KUh1hh2]q�(h=)�q�}q�(hX   VS and SeisComP3q�hh�hhh%hAh'}q�(h+]h,]h*]h)]h-]uh0KUh1hh2]q�hDX   VS and SeisComP3q���q�}q�(hh�hh�ubaubhH)�q�}q�(hX�  Although the codes were effectively re-written, the basic architecture used in
the original Earthworm-based implementation is used in SeisComp3. The SeisComp3 modules
scautopick, scautoloc, and scevent replace the Earthworm Binder module for
providing location estimates. Two new VS-specific modules were developed to
continuously calculate envelope amplitudes and to calculate and update VS
magnitudes (MVS) once a SeisComP3 origin is available.q�hh�hhh%hLh'}q�(h+]h,]h*]h)]h-]uh0KWh1hh2]q�hDX�  Although the codes were effectively re-written, the basic architecture used in
the original Earthworm-based implementation is used in SeisComp3. The SeisComp3 modules
scautopick, scautoloc, and scevent replace the Earthworm Binder module for
providing location estimates. Two new VS-specific modules were developed to
continuously calculate envelope amplitudes and to calculate and update VS
magnitudes (MVS) once a SeisComP3 origin is available.q���q�}q�(hh�hh�ubaubcdocutils.nodes
bullet_list
q�)�q�}q�(hU hh�hhh%Ubullet_listq�h'}q�(Ubulletq�X   -h)]h*]h+]h,]h-]uh0K^h1hh2]q�(cdocutils.nodes
list_item
q�)�q�}q�(hX   :ref:`scenvelope`q�hh�hhh%U	list_itemq�h'}q�(h+]h,]h*]h)]h-]uh0Nh1hh2]q�hH)�q�}q�(hh�hh�hhh%hLh'}q�(h+]h,]h*]h)]h-]uh0K^h2]q�csphinx.addnodes
pending_xref
q�)�q�}q�(hh�hh�hhh%Upending_xrefq�h'}q�(UreftypeX   refUrefwarnqӈU	reftargetq�X
   scenvelopeU	refdomainX   stdq�h)]h*]Urefexplicit�h+]h,]h-]Urefdocq�X   apps/vsq�uh0K^h2]q�cdocutils.nodes
inline
q�)�q�}q�(hh�h'}q�(h+]h,]q�(Uxrefq�h�X   std-refq�eh*]h)]h-]uhh�h2]q�hDX
   scenvelopeqᅁq�}q�(hU hh�ubah%Uinlineq�ubaubaubaubh�)�q�}q�(hX   :ref:`scvsmag`
hh�hhh%h�h'}q�(h+]h,]h*]h)]h-]uh0Nh1hh2]q�hH)�q�}q�(hX   :ref:`scvsmag`q�hh�hhh%hLh'}q�(h+]h,]h*]h)]h-]uh0K_h2]q�h�)�q�}q�(hh�hh�hhh%h�h'}q�(UreftypeX   refhӈh�X   scvsmagU	refdomainX   stdq�h)]h*]Urefexplicit�h+]h,]h-]h�h�uh0K_h2]q�h�)�q�}q�(hh�h'}q�(h+]h,]q�(h�h�X   std-refq�eh*]h)]h-]uhh�h2]q�hDX   scvsmagq���q�}q�(hU hh�ubah%h�ubaubaubaubeubhH)�q�}q�(hX  MVS is calculated and updated (with updates attached to the preferred origin)
each second for 30 seconds (unless configured differently) after it is first
invoked by the availability of a new SeisComp3 event. If configured, Ml can also be
calculated for these events.q�hh�hhh%hLh'}q�(h+]h,]h*]h)]h-]uh0Kah1hh2]r   hDX  MVS is calculated and updated (with updates attached to the preferred origin)
each second for 30 seconds (unless configured differently) after it is first
invoked by the availability of a new SeisComp3 event. If configured, Ml can also be
calculated for these events.r  ��r  }r  (hh�hh�ubaubhH)�r  }r  (hX�   An additional module, :ref:`scvsmaglog`, creates log output and mails solutions
once a new event is fully processed. It also provides an interface to send
alerts in real-time.hh�hhh%hLh'}r  (h+]h,]h*]h)]h-]uh0Kfh1hh2]r  (hDX   An additional module, r  ��r	  }r
  (hX   An additional module, hj  ubh�)�r  }r  (hX   :ref:`scvsmaglog`r  hj  hhh%h�h'}r  (UreftypeX   refhӈh�X
   scvsmaglogU	refdomainX   stdr  h)]h*]Urefexplicit�h+]h,]h-]h�h�uh0Kfh2]r  h�)�r  }r  (hj  h'}r  (h+]h,]r  (h�j  X   std-refr  eh*]h)]h-]uhj  h2]r  hDX
   scvsmaglogr  ��r  }r  (hU hj  ubah%h�ubaubhDX�   , creates log output and mails solutions
once a new event is fully processed. It also provides an interface to send
alerts in real-time.r  ��r  }r  (hX�   , creates log output and mails solutions
once a new event is fully processed. It also provides an interface to send
alerts in real-time.hj  ubeubeubh)�r  }r  (hU hhXhhh%h3h'}r  (h+]h,]h*]h)]r   U(configuring-and-optimizing-vs-s3-for-eewr!  ah-]r"  h
auh0Kkh1hh2]r#  (h=)�r$  }r%  (hX)   Configuring and optimizing VS(S3) for EEWr&  hj  hhh%hAh'}r'  (h+]h,]h*]h)]h-]uh0Kkh1hh2]r(  hDX)   Configuring and optimizing VS(S3) for EEWr)  ��r*  }r+  (hj&  hj$  ubaubhH)�r,  }r-  (hXJ  The performance of VS(SC3) is strongly dependent on: 1) the quality and
density of the seismic network; 2) the configuration of the general SeisComp3 system.
scautoloc requires at least 6 triggers to create an origin. Given the network
geometry, maps of when VS estimates would be first available
(indicative of the size of the blind zone as a function of earthquake location
relative to stations) can be generated for regions where EEW is of interest. VS(SC3)
uses scautoloc, which was not built for EEW, so an
additional delay of at most a few seconds is required for origin processing. VS
magnitudes (MVS) can be expected within 1-2 seconds after a SeisComp3 origin is
available. In the densest part of the Swiss network, SeisComp3 origins are available
within 10-15 seconds after origin time; MVS is typically available 1-2 seconds
later.r.  hj  hhh%hLh'}r/  (h+]h,]h*]h)]h-]uh0Kmh1hh2]r0  hDXJ  The performance of VS(SC3) is strongly dependent on: 1) the quality and
density of the seismic network; 2) the configuration of the general SeisComp3 system.
scautoloc requires at least 6 triggers to create an origin. Given the network
geometry, maps of when VS estimates would be first available
(indicative of the size of the blind zone as a function of earthquake location
relative to stations) can be generated for regions where EEW is of interest. VS(SC3)
uses scautoloc, which was not built for EEW, so an
additional delay of at most a few seconds is required for origin processing. VS
magnitudes (MVS) can be expected within 1-2 seconds after a SeisComp3 origin is
available. In the densest part of the Swiss network, SeisComp3 origins are available
within 10-15 seconds after origin time; MVS is typically available 1-2 seconds
later.r1  ��r2  }r3  (hj.  hj,  ubaubhH)�r4  }r5  (hX  The VS magnitude estimation relationships in Cua (2005) were derived from a
dataset consisting of Southern California waveforms and the NGA strong motion
dataset. In theory, customizing VS to a specific region requires deriving a set
of envelope attenuation relationships (168 coefficients) and relationships
between ground motion ratios and magnitude (6 coefficients) from a regional
dataset. In practice, the VS magnitude estimation relationships derived from
Southern California have been shown to work reasonably well in Northern
California and Switzerland (Behr et al, 2012). The envelope and ground motion
ratio coefficients from Cua (2005) are hard-coded in scvsmag, and should not be
modified without full understanding of the VS methodology and potential
consequences of the modifications.r6  hj  hhh%hLh'}r7  (h+]h,]h*]h)]h-]uh0Kzh1hh2]r8  hDX  The VS magnitude estimation relationships in Cua (2005) were derived from a
dataset consisting of Southern California waveforms and the NGA strong motion
dataset. In theory, customizing VS to a specific region requires deriving a set
of envelope attenuation relationships (168 coefficients) and relationships
between ground motion ratios and magnitude (6 coefficients) from a regional
dataset. In practice, the VS magnitude estimation relationships derived from
Southern California have been shown to work reasonably well in Northern
California and Switzerland (Behr et al, 2012). The envelope and ground motion
ratio coefficients from Cua (2005) are hard-coded in scvsmag, and should not be
modified without full understanding of the VS methodology and potential
consequences of the modifications.r9  ��r:  }r;  (hj6  hj4  ubaubhH)�r<  }r=  (hXZ  Although scautoloc can produce origins at any depth, the VS magnitude estimation
relationships assume a depth of 3 km. For this reason, it is expected that MVS
will systematically underestimate magnitudes for deep earthquakes. It may be
most practical to simply add empirically derived offsets to MVS for deeper
events, or for particular regions.r>  hj  hhh%hLh'}r?  (h+]h,]h*]h)]h-]uh0K�h1hh2]r@  hDXZ  Although scautoloc can produce origins at any depth, the VS magnitude estimation
relationships assume a depth of 3 km. For this reason, it is expected that MVS
will systematically underestimate magnitudes for deep earthquakes. It may be
most practical to simply add empirically derived offsets to MVS for deeper
events, or for particular regions.rA  ��rB  }rC  (hj>  hj<  ubaubeubh)�rD  }rE  (hU hhXhhh%h3h'}rF  (h+]h,]h*]h)]rG  Uunderstanding-vs-outputrH  ah-]rI  hauh0K�h1hh2]rJ  (h=)�rK  }rL  (hX   Understanding VS outputrM  hjD  hhh%hAh'}rN  (h+]h,]h*]h)]h-]uh0K�h1hh2]rO  hDX   Understanding VS outputrP  ��rQ  }rR  (hjM  hjK  ubaubhH)�rS  }rT  (hX}   The VS system currently being offered is a test version. A tool for
dissemination of results is not part of the core modules.rU  hjD  hhh%hLh'}rV  (h+]h,]h*]h)]h-]uh0K�h1hh2]rW  hDX}   The VS system currently being offered is a test version. A tool for
dissemination of results is not part of the core modules.rX  ��rY  }rZ  (hjU  hjS  ubaubeubh)�r[  }r\  (hU hhXhhh%h3h'}r]  (h+]h,]h*]h)]r^  U+false-alarms-missed-events-solution-qualityr_  ah-]r`  hauh0K�h1hh2]ra  (h=)�rb  }rc  (hX-   False alarms, missed events, solution qualityrd  hj[  hhh%hAh'}re  (h+]h,]h*]h)]h-]uh0K�h1hh2]rf  hDX-   False alarms, missed events, solution qualityrg  ��rh  }ri  (hjd  hjb  ubaubhH)�rj  }rk  (hX  The rate of false alarms and missed events is determined by the output of the
normal SeisComp3 origin chain (:ref:`scautopick`, :ref:`scautoloc`), and will
be similar to the performance of the automatic setup for typical network
operations (i.e. if you do not trust your automatic origins for the network, you
will not trust them for VS either). A solution quality is independently estimated
by VS, combining information on location quality and station quality.
See :ref:`scvsmag` on how the VS specific solution quality is computed.hj[  hhh%hLh'}rl  (h+]h,]h*]h)]h-]uh0K�h1hh2]rm  (hDXm   The rate of false alarms and missed events is determined by the output of the
normal SeisComp3 origin chain (rn  ��ro  }rp  (hXm   The rate of false alarms and missed events is determined by the output of the
normal SeisComp3 origin chain (hjj  ubh�)�rq  }rr  (hX   :ref:`scautopick`rs  hjj  hhh%h�h'}rt  (UreftypeX   refhӈh�X
   scautopickU	refdomainX   stdru  h)]h*]Urefexplicit�h+]h,]h-]h�h�uh0K�h2]rv  h�)�rw  }rx  (hjs  h'}ry  (h+]h,]rz  (h�ju  X   std-refr{  eh*]h)]h-]uhjq  h2]r|  hDX
   scautopickr}  ��r~  }r  (hU hjw  ubah%h�ubaubhDX   , r�  ��r�  }r�  (hX   , hjj  ubh�)�r�  }r�  (hX   :ref:`scautoloc`r�  hjj  hhh%h�h'}r�  (UreftypeX   refhӈh�X	   scautolocU	refdomainX   stdr�  h)]h*]Urefexplicit�h+]h,]h-]h�h�uh0K�h2]r�  h�)�r�  }r�  (hj�  h'}r�  (h+]h,]r�  (h�j�  X   std-refr�  eh*]h)]h-]uhj�  h2]r�  hDX	   scautolocr�  ��r�  }r�  (hU hj�  ubah%h�ubaubhDXB  ), and will
be similar to the performance of the automatic setup for typical network
operations (i.e. if you do not trust your automatic origins for the network, you
will not trust them for VS either). A solution quality is independently estimated
by VS, combining information on location quality and station quality.
See r�  ��r�  }r�  (hXB  ), and will
be similar to the performance of the automatic setup for typical network
operations (i.e. if you do not trust your automatic origins for the network, you
will not trust them for VS either). A solution quality is independently estimated
by VS, combining information on location quality and station quality.
See hjj  ubh�)�r�  }r�  (hX   :ref:`scvsmag`r�  hjj  hhh%h�h'}r�  (UreftypeX   refhӈh�X   scvsmagU	refdomainX   stdr�  h)]h*]Urefexplicit�h+]h,]h-]h�h�uh0K�h2]r�  h�)�r�  }r�  (hj�  h'}r�  (h+]h,]r�  (h�j�  X   std-refr�  eh*]h)]h-]uhj�  h2]r�  hDX   scvsmagr�  ��r�  }r�  (hU hj�  ubah%h�ubaubhDX5    on how the VS specific solution quality is computed.r�  ��r�  }r�  (hX5    on how the VS specific solution quality is computed.hjj  ubeubeubh)�r�  }r�  (hU hhXhhh%h3h'}r�  (h+]h,]h*]h)]r�  U
vs-licenser�  ah-]r�  hauh0K�h1hh2]r�  (h=)�r�  }r�  (hX
   VS Licenser�  hj�  hhh%hAh'}r�  (h+]h,]h*]h)]h-]uh0K�h1hh2]r�  hDX
   VS Licenser�  ��r�  }r�  (hj�  hj�  ubaubhH)�r�  }r�  (hX	  The SeisComp3 VS modules are free and open source, and are part of the SeisComp3
distribution from Seattle v2013.200. They are distributed under the `'SED Public
License for SeisComP3 Contributions' <http://www.seismo.ethz.ch/static/seiscomp_contrib/license.txt>`_.hj�  hhh%hLh'}r�  (h+]h,]h*]h)]h-]uh0K�h1hh2]r�  (hDX�   The SeisComp3 VS modules are free and open source, and are part of the SeisComp3
distribution from Seattle v2013.200. They are distributed under the r�  ��r�  }r�  (hX�   The SeisComp3 VS modules are free and open source, and are part of the SeisComp3
distribution from Seattle v2013.200. They are distributed under the hj�  ubh�)�r�  }r�  (hXs   `'SED Public
License for SeisComP3 Contributions' <http://www.seismo.ethz.ch/static/seiscomp_contrib/license.txt>`_h'}r�  (UnameX0   'SED Public License for SeisComP3 Contributions'Urefurir�  X=   http://www.seismo.ethz.ch/static/seiscomp_contrib/license.txtr�  h)]h*]h+]h,]h-]uhj�  h2]r�  hDX0   'SED Public
License for SeisComP3 Contributions'r�  ��r�  }r�  (hU hj�  ubah%h�ubh")�r�  }r�  (hX@    <http://www.seismo.ethz.ch/static/seiscomp_contrib/license.txt>hKhj�  h%h&h'}r�  (Urefurij�  h)]r�  U.sed-public-license-for-seiscomp3-contributionsr�  ah*]h+]h,]h-]r�  hauh2]ubhDX   .��r�  }r�  (hX   .hj�  ubeubeubh)�r�  }r�  (hU hhXhhh%h3h'}r�  (h+]h,]h*]h)]r�  U
referencesr�  ah-]r�  hauh0K�h1hh2]r�  (h=)�r�  }r�  (hX
   Referencesr�  hj�  hhh%hAh'}r�  (h+]h,]h*]h)]h-]uh0K�h1hh2]r�  hDX
   Referencesr�  ��r�  }r�  (hj�  hj�  ubaubhH)�r�  }r�  (hX�   Dietz, L., 2002: Notes on configuring BINDER_EW: Earthworm's phase associator, http://folkworm.ceri.memphis.edu/ew-doc/ovr/binder_setup.html (last accessed June 2013)r�  hj�  hhh%hLh'}r�  (h+]h,]h*]h)]h-]uh0K�h1hh2]r�  (hDXO   Dietz, L., 2002: Notes on configuring BINDER_EW: Earthworm's phase associator, r�  ��r�  }r�  (hXO   Dietz, L., 2002: Notes on configuring BINDER_EW: Earthworm's phase associator, hj�  ubh�)�r�  }r�  (hX=   http://folkworm.ceri.memphis.edu/ew-doc/ovr/binder_setup.htmlr�  h'}r�  (Urefurij�  h)]h*]h+]h,]h-]uhj�  h2]r�  hDX=   http://folkworm.ceri.memphis.edu/ew-doc/ovr/binder_setup.htmlr�  ��r�  }r�  (hU hj�  ubah%h�ubhDX    (last accessed June 2013)r�  ��r�  }r�  (hX    (last accessed June 2013)hj�  ubeubhH)�r�  }r�  (hX�   Cua, G., 2005: Creating the Virtual Seismologist: developments in ground motion characterization and seismic early warning. PhD thesis, California Institute of Technology, Pasadena, California.r�  hj�  hhh%hLh'}r�  (h+]h,]h*]h)]h-]uh0K�h1hh2]r�  hDX�   Cua, G., 2005: Creating the Virtual Seismologist: developments in ground motion characterization and seismic early warning. PhD thesis, California Institute of Technology, Pasadena, California.r�  ��r�  }r�  (hj�  hj�  ubaubhH)�r�  }r�  (hX�   Cua, G., and T. Heaton, 2007: The Virtual Seismologist (VS) method: a Bayesian approach to earthquake early warning, in Seismic early warning, editors: P. Gasparini, G. Manfredi, J. Zschau, Springer Heidelberg, 85-132.r�  hj�  hhh%hLh'}r�  (h+]h,]h*]h)]h-]uh0K�h1hh2]r�  hDX�   Cua, G., and T. Heaton, 2007: The Virtual Seismologist (VS) method: a Bayesian approach to earthquake early warning, in Seismic early warning, editors: P. Gasparini, G. Manfredi, J. Zschau, Springer Heidelberg, 85-132.r�  ��r�  }r�  (hj�  hj�  ubaubhH)�r   }r  (hX�   Cua, G., M. Fischer, T. Heaton, S. Wiemer, 2009: Real-time performance of the Virtual Seismologist earthquake early warning algorithm in southern California, Seismological Research Letters, September/October 2009; 80: 740 - 747.r  hj�  hhh%hLh'}r  (h+]h,]h*]h)]h-]uh0K�h1hh2]r  hDX�   Cua, G., M. Fischer, T. Heaton, S. Wiemer, 2009: Real-time performance of the Virtual Seismologist earthquake early warning algorithm in southern California, Seismological Research Letters, September/October 2009; 80: 740 - 747.r  ��r  }r  (hj  hj   ubaubhH)�r  }r	  (hX	  Behr, Y., Cua, G., Clinton, J., Heaton, T., 2012: Evaluation of Real-Time Performance of the Virtual Seismologist Earthquake
Early Warning Algorithm in Switzerland and California. Abstract 1481084 presented at 2012 Fall Meeting, AGU, San Francisco, Calif., 3-7 Dec.r
  hj�  hhh%hLh'}r  (h+]h,]h*]h)]h-]uh0K�h1hh2]r  hDX	  Behr, Y., Cua, G., Clinton, J., Heaton, T., 2012: Evaluation of Real-Time Performance of the Virtual Seismologist Earthquake
Early Warning Algorithm in Switzerland and California. Abstract 1481084 presented at 2012 Fall Meeting, AGU, San Francisco, Calif., 3-7 Dec.r  ��r  }r  (hj
  hj  ubaubhH)�r  }r  (hX�  Behr, Y., J. F. Clinton, C. Cauzzi, E. Hauksson, K. Jónsdóttir, C. G. Marius, A. Pinar, J. Salichon, and E. Sokos (2016) The Virtual Seismologist in SeisComP3: A New Implementation Strategy for Earthquake Early Warning Algorithms `<http://www.seismo.ethz.ch/research/groups/alrt/people/clintonj/publ_jc/Behr_et_all_SRL201602_VS_SC3_.pdf>`_, Seismological Research Letters, March/March 2016, v. 87, p. 363-373, doi:10.1785/0220150235r  hj�  hhh%hLh'}r  (h+]h,]h*]h)]h-]uh0K�h1hh2]r  (hDX�   Behr, Y., J. F. Clinton, C. Cauzzi, E. Hauksson, K. Jónsdóttir, C. G. Marius, A. Pinar, J. Salichon, and E. Sokos (2016) The Virtual Seismologist in SeisComP3: A New Implementation Strategy for Earthquake Early Warning Algorithms r  ��r  }r  (hX�   Behr, Y., J. F. Clinton, C. Cauzzi, E. Hauksson, K. Jónsdóttir, C. G. Marius, A. Pinar, J. Salichon, and E. Sokos (2016) The Virtual Seismologist in SeisComP3: A New Implementation Strategy for Earthquake Early Warning Algorithms hj  ubh�)�r  }r  (hXm   `<http://www.seismo.ethz.ch/research/groups/alrt/people/clintonj/publ_jc/Behr_et_all_SRL201602_VS_SC3_.pdf>`_h'}r  (UnameXh   http://www.seismo.ethz.ch/research/groups/alrt/people/clintonj/publ_jc/Behr_et_all_SRL201602_VS_SC3_.pdfr  j�  j  h)]h*]h+]h,]h-]uhj  h2]r  hDXh   http://www.seismo.ethz.ch/research/groups/alrt/people/clintonj/publ_jc/Behr_et_all_SRL201602_VS_SC3_.pdfr  ��r  }r  (hU hj  ubah%h�ubh")�r   }r!  (hXj   <http://www.seismo.ethz.ch/research/groups/alrt/people/clintonj/publ_jc/Behr_et_all_SRL201602_VS_SC3_.pdf>hKhj  h%h&h'}r"  (Urefurij  h)]r#  Uehttp-www-seismo-ethz-ch-research-groups-alrt-people-clintonj-publ-jc-behr-et-all-srl201602-vs-sc3-pdfr$  ah*]h+]h,]h-]r%  hauh2]ubhDX]   , Seismological Research Letters, March/March 2016, v. 87, p. 363-373, doi:10.1785/0220150235r&  ��r'  }r(  (hX]   , Seismological Research Letters, March/March 2016, v. 87, p. 363-373, doi:10.1785/0220150235hj  ubeubhH)�r)  }r*  (hX  Behr, Y., J. Clinton, P. Kästli, C. Cauzzi, R. Racine,  M‐A. Meier (2015) Anatomy of an Earthquake Early Warning (EEW) Alert: Predicting Time Delays for an End‐to‐End EEW System, Seismological Research Letters, May/June 2015, v. 86, p. 830-840, doi:10.1785/0220140179r+  hj�  hhh%hLh'}r,  (h+]h,]h*]h)]h-]uh0K�h1hh2]r-  hDX  Behr, Y., J. Clinton, P. Kästli, C. Cauzzi, R. Racine,  M‐A. Meier (2015) Anatomy of an Earthquake Early Warning (EEW) Alert: Predicting Time Delays for an End‐to‐End EEW System, Seismological Research Letters, May/June 2015, v. 86, p. 830-840, doi:10.1785/0220140179r.  ��r/  }r0  (hj+  hj)  ubaubeubeubeubhhh%Usystem_messager1  h'}r2  (h+]UlevelKh)]h*]r3  h8aUsourcehh,]h-]UlineKUtypeUINFOr4  uh0Kh1hh2]r5  hH)�r6  }r7  (hX%   Duplicate implicit target name: "vs".h'}r8  (h+]h,]h*]h)]h-]uhhh2]r9  hDX%   Duplicate implicit target name: "vs".r:  ��r;  }r<  (hU hj6  ubah%hLubaubaUcurrent_sourcer=  NU
decorationr>  NUautofootnote_startr?  KUnameidsr@  }rA  (hhshj_  hj�  h	h\h
j!  hj$  hh�hh/hj�  hjH  hj�  uh2]rB  (csphinx.addnodes
highlightlang
rC  )�rD  }rE  (hU hhhhh%UhighlightlangrF  h'}rG  (UlangX   rstUlinenothresholdI9223372036854775807
h)]h*]h+]h,]h-]uh0Kh1hh2]ubh#hehU UtransformerrH  NUfootnote_refsrI  }rJ  UrefnamesrK  }rL  Usymbol_footnotesrM  ]rN  Uautofootnote_refsrO  ]rP  Usymbol_footnote_refsrQ  ]rR  U	citationsrS  ]rT  h1hUcurrent_linerU  NUtransform_messagesrV  ]rW  h)�rX  }rY  (hU h'}rZ  (h+]UlevelKh)]h*]Usourcehh,]h-]UlineKUtypej4  uh2]r[  hH)�r\  }r]  (hU h'}r^  (h+]h,]h*]h)]h-]uhjX  h2]r_  hDX(   Hyperlink target "vs" is not referenced.r`  ��ra  }rb  (hU hj\  ubah%hLubah%j1  ubaUreporterrc  NUid_startrd  KUautofootnotesre  ]rf  Ucitation_refsrg  }rh  Uindirect_targetsri  ]rj  Usettingsrk  (cdocutils.frontend
Values
rl  orm  }rn  (Ufootnote_backlinksro  KUrecord_dependenciesrp  NUlanguage_coderq  Uenrr  U	tracebackrs  �Upep_referencesrt  NUstrip_commentsru  NUtoc_backlinksrv  Uentryrw  Urfc_base_urlrx  Uhttps://tools.ietf.org/html/ry  U	datestamprz  NUreport_levelr{  KUsmartquotes_localesr|  NU_destinationr}  NU
halt_levelr~  KUstrip_classesr  NhANUerror_encoding_error_handlerr�  Ubackslashreplacer�  Udebugr�  NUembed_stylesheetr�  �Uoutput_encoding_error_handlerr�  Ustrictr�  Usectnum_xformr�  KUdump_transformsr�  NUdocinfo_xformr�  KUwarning_streamr�  NUpep_file_url_templater�  Upep-%04dr�  Uexit_status_levelr�  KUconfigr�  NUstrict_visitorr�  NUcloak_email_addressesr�  �Utrim_footnote_reference_spacer�  �Uenvr�  NUdump_pseudo_xmlr�  NUexpose_internalsr�  NUsectsubtitle_xformr�  �Usource_linkr�  NUrfc_referencesr�  NUoutput_encodingr�  Uutf-8r�  U
source_urlr�  NUinput_encodingr�  U	utf-8-sigr�  U_disable_configr�  NU	id_prefixr�  U Ucharacter_level_inline_markupr�  �U	tab_widthr�  KUerror_encodingr�  UUTF-8r�  U_sourcer�  hUgettext_compactr�  �U	generatorr�  NUdump_internalsr�  NUsmart_quotesr�  �Upep_base_urlr�  U https://www.python.org/dev/peps/r�  Usyntax_highlightr�  Ulongr�  Uinput_encoding_error_handlerr�  j�  Uauto_id_prefixr�  Uidr�  Udoctitle_xformr�  �Ustrip_elements_with_classesr�  NU_config_filesr�  ]Ufile_insertion_enabledr�  �Uraw_enabledr�  KUdump_settingsr�  NubUsymbol_footnote_startr�  K Uidsr�  }r�  (hshoh\hXh�h�j_  j[  j!  j  h8hh/hj�  j�  j�  j�  jH  jD  j�  j�  j$  j   uUsubstitution_namesr�  }r�  h%h1h'}r�  (h+]h)]h*]Usourcehh,]h-]uU	footnotesr�  ]r�  Urefidsr�  }r�  h/]r�  h#asub.