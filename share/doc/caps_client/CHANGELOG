# Change Log

All notable changes to the CAPS client library will be documented in this file.

## 2018-12-19
### Fixed
- Read journal from file in plugin application.

## 2018-12-18
### Fixed
- Do not reconnect if the plugin buffer is full. Instead of we try to read
acknowledgements from the CAPS server until the plugin buffer is below the
threshold.

## 2018-12-17
### Added
- Support to retrieve status information e.g. the number of buffered bytes from
  plugin.

## 2018-09-06
### Changed
- Enable more verbose logging for MSEED packets

## 2018-07-25
### Fixed
- unset variable of the raw data record
- trim function will return false in case of an unknown datatype

## 2018-05-30
### Fixed
- Fixed unexpected closed SSL connections

## 2018-06-05
### Fixed
- Fix RawDataRecord::setHeader

## 2018-05-16
### Fixed
- RAW data end time calculation

## 2018-03-19
### Added
- SSL support

## 2017-11-20
### Added
- float and double support for Steim encoders. All values will be converted implicitly
to int 32 values

## 2017-11-08
### Added
- timing quality parameter to push call. By default the timing quality is set to -1.

## 2017-11-07
### Fixed
- do not flush encoders after reconnect

## 2017-10-26
### Added
- SSL support


## 2017-10-24
### Fixed
- packet synchronization error after reconnect
