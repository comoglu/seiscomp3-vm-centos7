LAMBDA documentation
====================

This is the documentation for LAMBDA for array processing in SeisComP3.

Contents:

.. toctree::
   :maxdepth: 3

   /base/introduction
   /base/installation
   /base/lambda-atf
   /base/lambda-data-event
   /base/detection-methods
   /base/lambda-analysis
   /base/autolambda-analysis
   /base/references

   Lambda Configuration Parameters</apps/lambda>
   Generic SeisComP3 Configuration </apps/global>

   Glossary for SeisComP3 </base/glossary>
