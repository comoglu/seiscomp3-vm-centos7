.. _lambda-sec-atf:

Array Response Function
=======================

The :term:`array transfer function` (:term:`ATF`) also referred to as :term:`array response function`
and the inter-station distances are considered array parameters. They are
computed for a defined configuration. The configuration consists of a set of
stations, locations and channels forming an array with a defined reference station.

.. _lambda-sec-epoch:

Epoch selection
---------------

The array geometry may be time-variant as additional stations may be deployed or
others are removed or just moved by some distance. Additionally, the :term:`location code`
or the :term:`channel code` may change during maintenance. Stations within periods
of identical configuration can be combined. Such periods form epochs. Due to the
applied changes the ATF will change as will the data availability.

Therefore, the epoch can be selected by choosing the reference time of interest.
To make the choice, right click on the map and select *Set reference time* to enter
the date and the time. This will load all available station information from the
respective epoch and show the stations on the map.

.. figure:: /base/media/lambda/lambda-atf-epoch.png
   :width: 50%
   :align: center

   Main GUI with epoch selection.

.. note::

   When no epoch is selected, the current time will be assumed as the reference
   time. Stations closed before or opening in the future will not be shown on the map.

Station selection on map
------------------------

When the :term:`inventory` is loaded the stations available with the selected
:ref:`epoch <lambda-sec-epoch>` are shown on the map. Stations can be selected
on the map to form an array. To select stations forming array elements, press
the *Shift* key while
spanning an area containing the stations. The selected station are highlighted on
the map. The list of stations with the relative array coordinates appears in the widget
*Array elements*. The list is sorted alphabetically. The relative array coordinates
are computed with respect to the :ref:`reference station <lambda-sec-refstation>`.

.. figure:: /base/media/lambda/lambda-atf-main.png
   :width: 50%
   :align: center

   Main GUI for calculating ATF.

.. _lambda-sec-refstation:

Choosing the reference station
------------------------------

The reference station is used to relate the array parameters calculated during
detection to this array element. The array reference station must be part of the
array elements.
By default the array reference station is the first station appearing from top in
the list of array elements. A different reference station can be selected by double
clicking on the row of that station. The relative array coordinates will update
immediately.

Storing and loading the array configuration
-------------------------------------------

Several array configurations can be stored under a different names. To store a
configuration press one of the save buttons in the *Array elements* widget.
In order to load a stored array configuration, select the array from the list in the
*Arrays* widget. A configuration can be deleted by pressing the delete buttons in
the *Array elements* widget.


.. figure:: /base/media/lambda/lambda-array-save_select.png
   :width: 50%
   :align: center

   Store and choose array configurations.

The array parameters will automatically update when choosing an array configuration.
