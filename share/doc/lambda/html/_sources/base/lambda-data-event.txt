.. _lambda-sec-data-selection:

Interactive data selection
==========================

Data can be loaded for array analysis based on a:

#. :ref:`selected event<lambda-sec-eventdata>` while accessing waveforms using
   the `RecordStream <https://docs.gempa.de/seiscomp3/current/apps/global_recordstream.html>`_
   interface
#. :ref:`chosen time-window<lambda-sec-twdata>` while accessing waveforms using
   the `RecordStream <https://docs.gempa.de/seiscomp3/current/apps/global_recordstream.html>`_
   interface
#. :ref:`waveform file<lambda-sec-filedata>` accessed through a file browser.

Before loading the data, choose the array and the reference time if required.

.. _lambda-sec-eventdata:

Event-based data selection
--------------------------

#. For analyzing a known event, select the event in the Events tab by double-clicking.
   The epicenter of the
   event will be shown on the map along with the array elements. The reference time is
   automatically set to the origin time of the event.

   .. figure:: /base/media/lambda/lambda-data-event.png
      :width: 50%
      :align: center

      Events tab with an event loaded from the SeisComP3 database.

#. The data related to the selected event and the chosen array can be
   loaded from the configured
   `RecordStream <https://docs.gempa.de/seiscomp3/current/apps/global_recordstream.html>`_
   interface or interactively from a file using a file browser. For loading data
   click on one of the buttons in the Waveform tab.

   .. _fig-data-waveforms:

   .. figure:: /base/media/lambda/lambda-data-waveforms.png
     :width: 50%
     :align: center

     Waveforms tab with data loaded for an event.

#. When the event is selected the arrival times predicted at the array elements can be
   shown on the traces. For showing the arrival times, choose the traveltime interface
   and the traveltime table. Only phases for which a traveltime table exists can be shown.

.. _lambda-sec-twdata:

Time-window-based data selection
--------------------------------

Data can be loaded for a given time window. For loading the data adjust the time window
of the time axis in the Waveforms tab and click on the button in the Waveforms menu for
loading data from RecordStream or for loading data from file choose the "Load data"
in the File menu (Ctrl+O). LAMBDA will only load data for stations which are elements
of the selected array.

When loading the data, the reference time is set to the start
time of the data. If an event is additionally loaded, the theoretical traveltimes can
can be shown on the traces.

.. _lambda-sec-filedata:

Interactive file browsing
-------------------------

Data can be loaded from file independent of any event selection by selecting the
miniSEED file through a file browser. Load the data file from the Waveforms tab
or choose the "Load data" in the File menu (Ctrl+O). LAMBDA will only load data
for stations which are elements of the selected array.

When loading the data, the reference time is set to the start
time of the data. If an event is additionally loaded, the theoretical traveltimes can
can be shown on the traces.

.. _lambda-sec-interactiveproc:

Interactive data processing
===========================

Fourier Spectrum
----------------

For analyzing the frequency content of individual traces the Fourier spectra can
be calculated. For showing the Fourier spectra, select the trace by clicking on
the waveforms and hit the Space key. The view of the spectrum is changeable and the
spectrum can be downloaded from the opened Spectrum window.

.. figure:: /base/media/lambda/lambda-data-fft.png
   :width: 50%
   :align: center

   Waveforms tab with data and Fourier spectra.

Filtering
---------

To apply filtering of waveforms, choose the filter parameters in the
:ref:`Type window <fig-data-waveforms>` and click on the filter button in the
:ref:`Waveforms tab <fig-data-waveforms>` to toggle between viewing of unfiltered
or filtered waveforms.

When loading the data, the waveforms are filtered using the
chosen frequency window. The frequency window should be chosen according to the
expected frequency signal of the detected signal. The upper corner frequency
must be meaningful and not be higher than the
:term:`Nyquist frequency` of any of the loaded data.

Press the Apply button in the Type window in order to apply filtering also to the
data before :ref:`array processing <lambda-sec-analysis>`.
