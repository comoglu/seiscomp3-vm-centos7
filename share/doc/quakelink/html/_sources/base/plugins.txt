.. _sec-ql-plugins:

Server, Applications and Plugins
=================================


QuakeLink ships with a server, plugins and applications for connecting
`SeisComP3 <https://docs.gempa.de/seiscomp3/current/>`_
systems and for external interaction with the QuakeLink database:

- :ref:`quakelink<quakelink>` (server): QuakeLink server collecting and providing
  the event and the metadata in real time.
- :ref:`sc2ql<sc2ql>` (plugin): Export SeisComP3 objects, e.g. event parameters
  from SeisComP3 to a QuakeLink server in real time.
- `ql2sc <https://docs.gempa.de/seiscomp3/current/apps/ql2sc.html>`_ (plugin):
  Import SeisComP3 objects, e.g. event parameters
  from QuakeLink server(s) into SeisComP3 in real time. ql2sc ships with the
  free SeisComP3 package (starting with release Jakarta).
- :ref:`ql2ql<ql2ql>` (plugin): Import event parameters from other QuakeLink
  instance(s) into QuakeLink in real time.
- :ref:`qlpush<qlpush>` (application): Populate the QuakeLink database with event parameters read
  from a SeisComP3 XML file. See the :ref:`examples<sec-qlpush>`.
- :ref:`qltool<qltool>` (application): Retrieve data from a QuakeLink server.

For these tools, the usual options (-h, --debug, etc.) apply. See the individual
tool descriptions and the :ref:`Examples section<quakelink-examples>` for their
applications.



.. toctree::
   :maxdepth: 2
   :glob:

   /apps/quakelink
   /apps/sc2ql

- `ql2sc <https://docs.gempa.de/seiscomp3/current/apps/ql2sc.html>`_


.. toctree::
   :maxdepth: 2
   :glob:

   /apps/ql2ql
   /apps/qlpush
   /apps/qltool
