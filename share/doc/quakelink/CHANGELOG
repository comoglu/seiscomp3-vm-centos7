# Change Log

All notable changes to Quakelink will be documented in this file.

## 2019-04-11
### Fixed
- Segfault caused by missing check

## 2019-03-15
### Fixed
- Segfault caused by an error in the database interface implementation

## 2018-12-11
### Added
- Added support for SC3 schema 0.11

## 2018-11-29
### Fixed
- Authentication check

## 2018-09-11
### Changes
- Made password encryption thread safe

## 2018-08-20
### Changes
- Internal database connection handling

## 2018-05-29
### Changes
- Allow configuration of SSL plugin port

## 2018-05-03
### Fixed
- Fixed FocalMechanism indexing which did not work correctly with updates. Users
  are encouraged to resync their database with ```--sync-db```.

## 2018-01-24
### Fixed
- HTTP status response header

## 2017-08-03
### Changes
- Origin evaluationStatus character mapping: map REVIEWED to 'V' and FINAL to
  'F' instead of 'C'


## 2017-07-03
### Changes
- Event agencyID in summary is read from event and then from origin
  if unset

## 2017-06-15
### Changes
- Internal code clean-up

## 2017-06-14
### Fixes
- Fix another possible deadlock of Quakelink

## 2017-06-06
### Fixes
- Fix deadlock of Quakelink that occured at some long polling HTTP requests
- Fix parsing of event query filter which caused an infinite polling loop
  in combination with GAPS

## 2017-06-01
### Changes
- Quakelink will not use a fallback region if not part of an event being
  received

## 2017-02-08
### Changes
- Add ORDER BY and LIMIT statements to SELECT query for QL protocol

## 2016-06-21
### Changes
- Use socket timeout of 60s if keepAlive is activated
### Fixes
- Export preferred magnitude even if inside another origin

## 2016-05-30
### Fixes
- Export moment tensors derived origin with XML

## 2016-05-10
### Changes
- HTTP is now allowed by any license unless explicitely disabled

## 2015-08-25
### Changes
- JSON response returns now dates in ISO format and not Unix epoch. To distinguish
  between both formats the mime-type is set to ```application/json;charset=utf-8;version=2```
  including the version information

## 2015-07-07
### Fixed
- Fixed memory leak for event summary requests
