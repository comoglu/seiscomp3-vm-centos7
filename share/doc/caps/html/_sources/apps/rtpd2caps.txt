.. highlight:: rst

.. _rtpd2caps:

#########
rtpd2caps
#########

**CAPS import module for MRF packets from RTPD server.**


Description
===========

The RTPD plugin for CAPS collects DT and MRF packets through the REN protocol. It
is supposed to have very low latency suitable for real-time data transmission.

The RTPD plugin needs a configuration file which is usually created by its init
script. This configuration files lives under
:file:`$SEISCOMP_ROOT/var/lib/rtpd2caps.cfg`.
The init script reads the configuration from :file:`$SEISCOMP_ROOT/etc/rtpd2caps.cfg`
and the bindings from :file:`$SEISCOMP_ROOT/etc/key/rtpd2caps/*` and prepares the
above final configuration file.

The configuration used by rtpd2caps looks like this:

.. code-block:: sh

   # Number of records to queue if the sink connection is not available
   queue_size = 20000

   # Define the channel mapping. Each item is a tuple of source id composed
   # of stream and channel and target location and stream code. The target code
   # can be a single channel code (e.g. HNZ) or a combination of location and
   # channel code (e.g. 00.HNZ). In case of DT packets the sampling interval 
   # must be specified after the channel code separated by '@'
   # channels = 1.0:HNZ, 1.1:HN1, 1.2:HN2 MRF

   # Starts a particular unit configuration. channel mapping can be overridden
   # in a unit section as well.
   unit 200B3
     # Defines the output network code for this unit.
     network  = "RT"
     # Defines the output station code for this unit.
     station  = "TEST1"
     # The RTPD server address.
     address  = 1.2.3.4:2543
     # The CAPS server address.
     sink     = localhost:18003

   # Another unit.
   unit 200B4
     network  = "RT"
     station  = "TEST2"
     address  = 1.2.3.4:2543
     sink     = localhost


A user does not need to create this configuration file manually if using the
plugin integrated into SC3. The rtpd2caps plugin can be configured as any other
SC3 module, e.g. via :ref:`scconfig`.

An example SC3 configuration to generate the configuration above can look like this:

:file:`$SEISCOMP3_ROOT/etc/rtpd2caps.cfg`

.. code-block:: sh

   # RTP server address in format [host]:[port]. If port is omitted, 2543 is
   # assumed. This is optional and only used if the address in a binding is
   # omitted.
   address = 1.2.3.4

   # CAPS server address to send data to in format [host]:[port]. If port is
   # omitted, 18003 is assumed. This is optional and only used if the sink in a
   # binding is omitted.
   sink = localhost:18003

   # Channel mapping list where each item maps a REFTEK stream/channel id to a
   # SEED channel code with optional location code. Format:
   # {stream}.{channel}:[{loc}.]{cha}, e.g. 1.0:00.HHZ. This is the default used
   # if a station binding does not define it explicitly.
   channels = 1.0:HNZ,1.1:HN1,1.2:HN2

   # Number of packets that can be queued when a sink is not reachable.
   queueSize = 20000

:file:`$SEISCOMP3_ROOT/etc/key/rtpd2caps/station_RT_TEST1`

.. code-block:: sh

   # Mandatory REFTEK unit id (hex).
   unit = 200B3

:file:`$SEISCOMP3_ROOT/etc/key/rtpd2caps/station_RT_TEST2`

.. code-block:: sh

   # Mandatory REFTEK unit id (hex).
   unit = 200B4

Test examples
=============

To test a server and check what packages are available, rtpd2caps can be ran
in test and verify mode.

.. code-block:: sh

   $ rtpd2caps -H 1.2.3.4 --verify --test
   Requested attributes:
   DAS 'mask'             (at_dasid)  = 00000000
   Packet mask            (at_pmask)  = 0x00004000
   Stream mask            (at_smask)  = 0x0000FFFF
   Socket I/O timeout     (at_timeo)  = 30
   TCP/IP transmit buffer (at_sndbuf) = 0
   TCP/IP receive  buffer (at_rcvbuf) = 0
   blocking I/O flag      (at_block)  = TRUE
   2013:198-08:32:40 local [2195] Parameters:
   2013:198-08:32:40 local [2195]  * queue_size = 10000 records
   2013:198-08:32:40 local [2195]  * backfilling_buffer_size = 0s
   2013:198-08:32:40 local [2195] Configured 1 source(s) and 0 sink(s)
   [RTP 69.15.146.174:2543]
     XX.YYYY  unit 0
   2013:198-08:32:40 local [2195] started reading from RTP server at 1.2.3.4:2543
   2013:198-08:32:42 local [2195] Commands may not be sent
   2013:198-08:32:42 local [2195] connected to 1.2.3.4:2543
   Actual parameters:
   DAS 'mask'             (at_dasid)  = 00000000
   Packet mask            (at_pmask)  = 0x00004000
   Stream mask            (at_smask)  = 0x0000FFFF
   Socket I/O timeout     (at_timeo)  = 30
   TCP/IP transmit buffer (at_sndbuf) = 0
   TCP/IP receive  buffer (at_rcvbuf) = 0
   blocking I/O flag      (at_block)  = TRUE
   200B3 stream 1
      chamap: 7
      chacnt: 3
      cha   : 99
      dtype : 50
      time  : 2013.198 08:33:39.714000
      nsamp : 20
      bytes : 512
      rate  : 100
      chans : 0, 1, 2
   200B3 stream 1
      chamap: 7
      chacnt: 3
      cha   : 99
      dtype : 50
      time  : 2013.198 08:33:39.914000
      nsamp : 20
      bytes : 512
      rate  : 100
      chans : 0, 1, 2
   200B3 stream 1
      chamap: 7
      chacnt: 3
      cha   : 99
      dtype : 50
      time  : 2013.198 08:33:40.114000
      nsamp : 20
      bytes : 512
      rate  : 100
      chans : 0, 1, 2
   200B3 stream 1
      chamap: 7
      chacnt: 3
      cha   : 99
      dtype : 50
      time  : 2013.198 08:33:40.314000
      nsamp : 20
      bytes : 512
      rate  : 100
      chans : 0, 1, 2
   ...



Configuration
=============


.. note::

   rtpd2caps is a standalone module and does not inherit :ref:`global options <global-configuration>`.


| :file:`etc/defaults/rtpd2caps.cfg`
| :file:`etc/rtpd2caps.cfg`
| :file:`~/.seiscomp3/rtpd2caps.cfg`



.. confval:: address

   Type: *string*

   RTP server address in format [host]:[port]. If port
   is omitted, 2543 is assumed. This is optional and only used
   if the address in a binding is omitted.


.. confval:: sink

   Type: *string*

   CAPS server address to send data to in format [host]:[port].
   If port is omitted, 18003 is assumed. This is optional and only used
   if the sink in a binding is omitted.


.. confval:: channels

   Type: *list:string*

   Channel mapping list where each item maps a REFTEK
   stream\/channel id to a SEED channel code with optional
   location code. Format: {stream}.{channel}:[{loc}.]{cha}, e.g.
   1.0:00.HHZ. This is the default used if a station binding does
   not define it explicitly.


.. confval:: queueSize

   Type: *int*

   Number of packets that can be queued when a sink is not reachable.
   Default is ``10000``.

.. confval:: backFillingBufferSize

   Type: *int*

   Unit: *s*

   Length of backfilling buffer. Whenever a hole is detected, records
   will be held in a buffer and not sent out. Records are flushed from
   front to back if the buffer size is exceeded.
   Default is ``0``.


Bindings
========


.. confval:: unit

   Type: *string*

   Mandatory REFTEK unit id \(hex\).


.. confval:: address

   Type: *string*

   RTP server address in format [host]:[port]. If port
   is omitted, 2543 is assumed.


.. confval:: sink

   Type: *string*

   CAPS server address to send data to in format [host]:[port].
   If port is omitted, 18003 is assumed.


.. confval:: channels

   Type: *list:string*

   Channel mapping list where each item maps a REFTEK
   stream\/channel id to a SEED channel code with optional
   location code. Format: {stream}.{channel}:[{loc}.]{cha}, e.g.
   1.0:00.HHZ.




Command Line
============

.. option:: -h, --help

   Print program usage and exit.

.. option:: -H, --host address

   RTP server to connect to in format [host]:[port]. If port
   is omitted, 2543 is assumed.

.. option:: -S, --sink address

   CAPS server to send data to in format [host]:[port]. If port
   is omitted, 18003 is assumed.

.. option:: -n, --queue-size arg

   Maximum number of packages queued before
   the sink connection becomes blocking.

.. option:: -b, --backfilling-buffer-size arg

   Buffer size in seconds for backfilling holes.

.. option:: -s, --syslog

   Logs to syslog.

.. option:: -f, --config-file path

   Path to configuration file to be used.

.. option:: --log-file path

   Path to log file.

.. option:: --verbosity level

   Log verbosity, 4\=DEBUG, 3\=INFO, 2\=WARN, 1\=ERR, 0\=QUIET.

.. option:: --debug

   Set log level to DEBUG and log everything to stderr.

.. option:: --verify

   Dump package contents. This option is only useful for testing and debugging.

.. option:: --test

   Do not send any data to CAPS.

