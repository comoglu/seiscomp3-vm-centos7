.. highlight:: rst

.. _trisari2caps:

############
trisari2caps
############

**Trisari CAPS plugin. Reads data in Trisari format from file and pushes the data into the given CAPS server.**


Configuration
=============

| :file:`etc/defaults/global.cfg`
| :file:`etc/defaults/trisari2caps.cfg`
| :file:`etc/global.cfg`
| :file:`etc/trisari2caps.cfg`
| :file:`~/.seiscomp3/global.cfg`
| :file:`~/.seiscomp3/trisari2caps.cfg`

trisari2caps inherits :ref:`global options<global-configuration>`.



.. confval:: input.readFrom

   Type: *string*

   Read input files from this file


.. confval:: input.directory

   Type: *string*

   Watch this directory for incoming input files


.. confval:: input.watchEvents

   Type: *string*

   Listen for specific inotify event\(s\). If ommitted, close_write events are listened for. Events:
   access \- file or directory contents were read,
   modify \- file or directory contents were written,
   attrib \- file or directory attributes changed,
   close_write \- file or directory closed, after being opened in writable mode,
   close_nowrite \- file or directory closed, after being opened in read\-only mode
   close \- file or directory closed, regardless of read\/write mode
   open \- file or directory opened
   moved_to \- file or directory moved to watched directory
   moved_from \- file or directory moved from watched directory
   move \- file or directory moved to or from watched directory
   create \- file or directory created within watched directory
   delete \- file or directory deleted within watched directory
   delete_self \- file or directory was deleted
   unmount \- file system containing file or directory unmounted
   Default is ``close_write``.

.. confval:: input.watchPattern

   Type: *string*

   Process any events whose filename matches the specified regular expression


.. confval:: output.host

   Type: *string*

   Data output host
   Default is ``localhost``.

.. confval:: output.port

   Type: *int*

   Data output port
   Default is ``18003``.

.. confval:: output.bufferSize

   Type: *uint*

   Size \(bytes\) of the packet buffer
   Default is ``1048576``.

.. confval:: streams.file

   Type: *string*

   File to read streams from. Each line contains a station name, a sensor list, a sampling interval,
   and a stream ID e.g CILJI ENC;PRS;RAD 60 TEST CILJI UTZ.


.. confval:: streams.map

   Type: *string*

   File to sensor location mapping from. Each line defines a mapping between tide gauge sensor and
   sensor location code e.g. PRS 50.



Command Line
============


Generic
-------

.. option:: -h, --help

   show help message.

.. option:: -V, --version

   show version information

.. option:: --config-file arg

   Use alternative configuration file. When this option is used
   the loading of all stages is disabled. Only the given configuration
   file is parsed and used. To use another name for the configuration
   create a symbolic link of the application or copy it, eg scautopick \-> scautopick2.


Verbosity
---------

.. option:: --verbosity arg

   Verbosity level [0..4]. 0:quiet, 1:error, 2:warning, 3:info, 4:debug

.. option:: -v, --v

   Increase verbosity level \(may be repeated, eg. \-vv\)

.. option:: -q, --quiet

   Quiet mode: no logging output

.. option:: --print-component arg

   For each log entry print the component right after the
   log level. By default the component output is enabled
   for file output but disabled for console output.

.. option:: --component arg

   Limits the logging to a certain component. This option can be given more than once.

.. option:: -s, --syslog

   Use syslog logging back end. The output usually goes to \/var\/lib\/messages.

.. option:: -l, --lockfile arg

   Path to lock file.

.. option:: --console arg

   Send log output to stdout.

.. option:: --debug

   Debug mode: \-\-verbosity\=4 \-\-console\=1

.. option:: --trace

   Trace mode: \-\-verbosity\=4 \-\-console\=1 \-\-print\-component\=1 \-\-print\-context\=1

.. option:: --log-file arg

   Use alternative log file.


Input
-----

.. option:: --station arg

   Sets the station and sampling interval to use. Format is [net.sta\@?]

.. option:: -f, --file arg

   Load CREX data directly from file

.. option:: --read-from arg

   Read input files from this file


Output
------

.. option:: -H, --host arg

   Data output host

.. option:: -p, --port arg

   Data output port


Streams
-------

.. option:: --streams-file arg

   File to read streams from. Each line contains a station name, a sensor list, a sampling interval,
   and a stream ID e.g CILJI ENC;PRS;RAD 60 TEST CILJI UTZ.


Streams
-------

.. option:: --streams-map arg

   File to sensor location mapping from. Each line defines a mapping between tide gauge sensor and
   sensor location code e.g. PRS 50.

