.. _global:

*************
Configuration
*************

The configuration uses a unified schema to configure modules.
Modules which use the SeisComP3 libraries can read this configuration
directly and share global configuration options like messaging connections,
database configurations, logging and much more.

Configuration files
*******************

Configuration files are simple text files where each line is a name-value pair.

.. warning::

   Parameter names are not case-sensitive.

A simple example to assign a parameter "skyColor" the value "blue":

.. code-block:: sh

   skyColor = blue

Everything following an un-escaped '#' (hash) is a comment and ignored.
Blank lines and white spaces are ignored by the parser as well unless
quoted or escaped.

.. code-block:: sh

   skyColor = yellow  # This is a comment

   # The preceding empty line is ignored and previous setting "yellow"
   # is replaced by "blue":
   skyColor = blue

Later assignments overwrite earlier ones so the order of lines in the
configuration file is important. The file is parsed top-down.

Values can be either scalar values or lists. List items are separated by commas.

.. code-block:: sh

   # This is a list definition
   rainbowColors = red, orange, yellow, green, blue, indigo, violet

If a value needs to include a comma, white space or any other interpretable
character it can either be escaped with '\\' (backslash)
or quoted. White space is removed in unquoted and un-escaped values.

.. code-block:: sh

   # This is a comment

   # The following list definitions have 2 items: 1,2 and 3,4
   # quoted values
   tuples = "1,2", "3,4"
   # escaped values
   tuples = 1\,2, 3\,4

Values can extend over multiple lines if a backslash is appended to each line

.. code-block:: sh

   # Multiline string
   text = "Hello world. "\
          "This text spawns 3 lines in the configuration file "\
          "but only one line in the value."

   # Multiline list definition
   rainbowColors = red,\
                   orange,\
                   yellow,\
                   green, blue,\
                   indigo, violet

Environment or preceding configuration variables can be used with ``${var}``.

.. code-block:: sh

   homeDir = ${HOME}
   myPath = ${homeDir}/test

.. note::

   Values are not type-checked. Type checking is part of the application logic
   and will be handled there. The configuration file parser will not raise an
   error if a string is assigned to a parameter that is expected to be an integer.


Each module tries to read from six configuration files. Note that configuration parameters
defined earlier are overwritten if defined in files read in later:

 - `file:etc/global.cfg`
 - `file:etc/app.cfg`
 - `file:config/global.cfg`
 - `file:config/app.cfg`
 - `file:~/.seiscomp3/global.cfg`
 - `file:~/.seiscomp3//app.cfg`

The :ref:`configuration section<global-configuration>` describes common configuration parameters of a
module. Not all modules make use of all available parameters because they may be disabled, e.g. the
messaging component. So the configuration of the messaging server is disabled too.


.. _global-configuration:


Configuration
=============



.. confval:: datacenterID

   Type: *string*

   Sets the datacenter ID which is primarily used by Arclink and
   its tools. Should not contain spaces.


.. confval:: agencyID

   Type: *string*

   Defines the agency ID used to set creationInfo.agencyID in
   data model objects. Should not contain spaces.
   Default is ``GFZ``.

.. confval:: organization

   Type: *string*

   Organization name used mainly by ArcLink and SeedLink.
   Default is ``Unset``.

.. confval:: author

   Type: *string*

   Defines the author name used to set creationInfo.author in
   data model objects.


.. confval:: plugins

   Type: *list:string*

   Defines a list of modules loaded at startup.


.. confval:: logging.level

   Type: *int*

   Sets the logging level between 1 and 4 where 1\=ERROR, 2\=WARNING, 3\=INFO and 4\=DEBUG.
   Default is ``2``.

.. confval:: logging.file

   Type: *boolean*

   Enables logging into a log file.
   Default is ``true``.

.. confval:: logging.syslog

   Type: *boolean*

   Enables logging to syslog if supported by the host system.
   Default is ``false``.

.. confval:: logging.components

   Type: *list:string*

   Limit the logging to the specified list of components, e.g. 'Application, Server'


.. confval:: logging.component

   Type: *boolean*

   For each log entry print the component right after the
   log level. By default the component output is enabled
   for file output but disabled for console output.


.. confval:: logging.context

   Type: *boolean*

   For each log entry print the source file name and line
   number.
   Default is ``false``.

.. confval:: logging.utc

   Type: *boolean*

   Use UTC instead of localtime in logging timestamps.


.. confval:: logging.file.rotator

   Type: *boolean*

   Default is ``true``.

.. confval:: logging.file.rotator.timeSpan

   Type: *int*

   Unit: *s*

   Time span after which a log file is rotated.
   Default is ``86400``.

.. confval:: logging.file.rotator.archiveSize

   Type: *int*

   How many historic log files to keep.
   Default is ``7``.

.. confval:: logging.syslog.facility

   Type: *string*

   Defines the syslog facility to be used according to the
   defined facility names in syslog.h. The default is local0.
   If the given name is invalid or not available, initializing
   logging will fail and the application quits.
   Default is ``local0``.

.. confval:: connection.server

   Type: *host-with-port*

   Defines the Spread server name to connect to. Format is host[:port].
   Default is ``localhost``.

.. confval:: connection.username

   Type: *string*

   Defines the username to be used. The length is limited
   to 10 characters with Spread. By default the module name
   \(name of the executable\) is used but sometimes it exceeds
   the 10 character limit and access is denied. To prevent
   errors set a different username. An empty username will
   let Spread generate one.


.. confval:: connection.timeout

   Type: *int*

   Unit: *s*

   The connection timeout in seconds. 3 seconds are normally
   more than enough. If a client needs to connect to a
   remote system with a slow connection a larger timeout might
   be needed.
   Default is ``3``.

.. confval:: connection.primaryGroup

   Type: *string*

   Defines the primary group of a module. This is the name
   of the group where a module sends its messages to if the
   target group is not explicitely given in the send call.


.. confval:: connection.encoding

   Type: *string*

   Defines the message encoding for sending. Allowed values
   are \"binary\" or \"xml\". XML has
   more overhead in processing but is more robust when
   schema versions between client and server are different.
   Default is ``binary``.

.. confval:: connection.subscriptions

   Type: *list:string*

   Defines a list of message groups to subscribe to. The
   default is usually given by the application and does not
   need to be changed.


.. note::
   **database.\***
   *Defines the database connection. If no database is configured*
   *(which is the default) and a messaging connection is available*
   *the application will receive the parameters after the connection*
   *is established. Override these values only if you know what you*
   *are doing.*



.. confval:: database.type

   Type: *string*

   Defines the database backend to be used. The name corresponds to
   the defined name in the database plugin. Examples are: mysql, postgresql or
   sqlite3.


.. confval:: database.parameters

   Type: *string*

   The database connection parameters. This value depends on the used
   database backend. E.g. sqlite3 expects the path to the database
   file whereas MYSQL or PostgreSQL expect an URI in the format
   user:pwd\@host\/database?param1\=val1\&param2\=val2.


.. confval:: database.inventory

   Type: *string*

   *No description available*

.. confval:: database.config

   Type: *string*

   *No description available*

.. confval:: processing.whitelist.agencies

   Type: *list:string*

   Defines a whitelist of agencies that are allowed for processing separated by comma.


.. confval:: processing.blacklist.agencies

   Type: *list:string*

   Defines a blacklist of agencies that are not allowed for processing separated by comma.


.. confval:: recordstream.service

   Type: *string*

   Default is ``slink``.

.. confval:: recordstream.source

   Type: *string*

   Default is ``localhost:18000``.

.. confval:: core.plugins

   Type: *list:string*

   Default is ``dbmysql``.

.. confval:: client.startStopMessage

   Type: *boolean*

   Default is ``false``.

.. confval:: client.autoShutdown

   Type: *boolean*

   Default is ``false``.

.. confval:: client.shutdownMasterModule

   Type: *string*

   *No description available*

.. confval:: client.shutdownMasterUsername

   Type: *string*

   *No description available*

.. confval:: commands.target

   Type: *string*

   A regular expression of all clients that should handle
   a command message usually send to the GUI messaging group.
   Currently this flag is only used by GUI applications to
   set an artificial origin and to tell other clients to
   show this origin. To let all connected clients handle the
   command, \".\*\$\" can be used.



Bindings
========


.. confval:: detecStream

   Type: *string*

   Defines the channel code of the preferred stream used eg
   by scautopick and scrttv. If no component code is given,
   'Z' will be used by default.


.. confval:: detecLocid

   Type: *string*

   Defines the location code of the preferred stream used
   eg by scautopick and scrttv.


.. note::
   **amplitudes.\***
   *Defines general parameters for amplitudes of a certain type.*



.. confval:: amplitudes.enable

   Type: *boolean*

   Defines if amplitude calculation is enabled. If disabled then
   this station will be skipped for amplitudes and magnitudes.
   Default is ``true``.

.. confval:: amplitudes.saturationThreshold

   Type: *double*

   Defines the saturation threshold for the optional saturation check.
   By default the saturation check is disabled but giving a value above
   0 will enable it. Waveforms that are saturated are not used  at all
   for amplitude calculations.
   Default is ``-1``.

.. confval:: amplitudes.enableResponses

   Type: *boolean*

   Activates deconvolution for this station. If no responses are
   configured an error is raised and the data is not processed.
   Default is ``false``.

.. note::

   **amplitudes.\$name.\***
   *An amplitude profile configures global parameters for a*
   *particular amplitude type. The available amplitude types*
   *are not fixed and can be extended by plugins. The name of*
   *the type must match the one defined in the corresponding*
   *AmplitudeProcessor.*
   \$name is a placeholder for the name to be used.


.. confval:: amplitudes.\$name.enable

   Type: *boolean*

   Defines if amplitude calculation of certain type is enabled.
   Default is ``true``.

.. confval:: amplitudes.\$name.saturationThreshold

   Type: *double*

   If greater than 0 and the absolute value of an incoming
   unfiltered sample exceeds this value, the trace data is rejected and
   not used \(saturation check\).
   Default is ``-1``.

.. confval:: amplitudes.\$name.minSNR

   Type: *double*

   Defines the mininum SNR to be reached to compute the
   amplitudes. This value is amplitude type specific and
   has no global default value.


.. confval:: amplitudes.\$name.noiseBegin

   Type: *double*

   Overrides the default time \(relative to the trigger
   time\) of the begin of the noise window used to compute
   the noise offset and noise amplitude. Each amplitude
   processor sets its own noise time window and this option
   should only be changed if you know what you are doing.


.. confval:: amplitudes.\$name.noiseEnd

   Type: *double*

   Overrides the default time \(relative to the trigger
   time\) of the end of the noise window used to compute
   the noise offset and noise amplitude. Each amplitude
   processor sets its own noise time window and this option
   should only be changed if you know what you are doing.


.. confval:: amplitudes.\$name.signalBegin

   Type: *double*

   Overrides the default time \(relative to the trigger
   time\) of the begin of the signal window used to compute
   the final amplitude. Each amplitude processor sets its
   own signal time window and this option should only be
   changed if you know what you are doing.


.. confval:: amplitudes.\$name.signalEnd

   Type: *double*

   Overrides the default time \(relative to the trigger
   time\) of the end of the signal window used to compute
   the final amplitude. Each amplitude processor sets its
   own signal time window and this option should only be
   changed if you know what you are doing.


.. note::
   **mag.\***
   *Defines general parameters for magnitudes of a certain type.*



.. note::

   **mag.\$name.\***
   *An magnitude profile configures global parameters for a*
   *particular magnitude type. The available magnitude types*
   *are not fixed and can be extended by plugins. The name of*
   *the type must match the one defined in the corresponding*
   *MagnitudeProcessor.*
   \$name is a placeholder for the name to be used.


.. confval:: mag.\$name.multiplier

   Type: *double*

   Part of the magnitude station correction. The final
   magnitude value is multiplier\*M+offset. This value
   is usually not required but there for completeness.
   Default is ``1``.

.. confval:: mag.\$name.offset

   Type: *double*

   Part of the magnitude station correction. The final
   magnitude value is multiplier\*M+offset. This value
   can be used to correct station magnitudes.
   Default is ``0``.

.. note::
   **picker.BK.\***
   *Bkpicker is an implementation of the Baer/Kradolfer picker adapted*
   *to SeisComP3. It was created by converting Manfred Baers from Fortran*
   *to C++ and inserting it as a replacement for the picker algorithm.*
   *The picker interface name to be used in configuration files is*
   *"BK".*



.. confval:: picker.BK.signalBegin

   Type: *double*

   Overrides the default time \(relative to the trigger
   time\) of the begin of the signal window used to pick.
   Default is ``-20``.

.. confval:: picker.BK.signalEnd

   Type: *double*

   Overrides the default time \(relative to the trigger
   time\) of the begin of the signal window used to pick.
   Default is ``80``.

.. confval:: picker.BK.filterType

   Type: *string*

   BP \(Bandpass\) is currently the only option.
   Default is ``BP``.

.. confval:: picker.BK.filterPoles

   Type: *int*

   Number of poles.
   Default is ``2``.

.. confval:: picker.BK.f1

   Type: *double*

   Bandpass lower cutoff freq. in Hz.
   Default is ``5``.

.. confval:: picker.BK.f2

   Type: *double*

   Bandpass upper cutoff freq. in Hz.
   Default is ``20``.

.. confval:: picker.BK.thrshl1

   Type: *double*

   Threshold to trigger for pick \(c.f. paper\), default 10
   Default is ``10``.

.. confval:: picker.BK.thrshl2

   Type: *double*

   Threshold for updating sigma  \(c.f. paper\), default 20
   Default is ``20``.

.. note::
   **picker.AIC.\***
   *AIC picker is an implementation using the simple non-AR algorithm of Maeda (1985),*
   *see paper of Zhang et al. (2003) in BSSA. The picker interface name to be used in configuration files is*
   *"AIC".*



.. confval:: picker.AIC.signalBegin

   Type: *double*

   Unit: *s*

   Overrides the default time \(relative to the trigger
   time\) of the begin of the signal window used to pick.
   Default is ``-30``.

.. confval:: picker.AIC.signalEnd

   Type: *double*

   Unit: *s*

   Overrides the default time \(relative to the trigger
   time\) of the begin of the signal window used to pick.
   Default is ``10``.

.. confval:: picker.AIC.filter

   Type: *string*

   Overrides the default filter which is \"raw\". The typical filter grammar can be used.


.. confval:: picker.AIC.minSNR

   Type: *double*

   Defines the mininum SNR.
   Default is ``3``.

.. note::
   **spicker.L2.\***
   *L2 is a algorithm to pick S-phases.*



.. confval:: spicker.L2.noiseBegin

   Type: *double*

   Unit: *s*

   Overrides the relative data processing start time \(relative to the triggering
   pick\). This adds a margin to the actual processing and is useful
   to initialize the filter \(e.g. STALTA\). The data is not used
   at all until signalBegin is reached.
   Default is ``-10``.

.. confval:: spicker.L2.signalBegin

   Type: *double*

   Unit: *s*

   Overrides the relative start time \(relative to the triggering
   pick\) of the begin of the signal processing.
   Default is ``0``.

.. confval:: spicker.L2.signalEnd

   Type: *double*

   Unit: *s*

   Overrides the relative end time \(relative to the triggering
   pick\) of the end of the signal window used to pick.
   Default is ``60``.

.. confval:: spicker.L2.filter

   Type: *string*

   Configures the filter used to compute the L2 and to pick
   the onset \(with AIC\) after the detector triggered.
   Default is ``BW(4,0.3,1.0)``.

.. confval:: spicker.L2.detecFilter

   Type: *string*

   Configures the detector in the filtered L2. This filter is
   applied on top of the base L2 filter.
   Default is ``STALTA(1,10)``.

.. confval:: spicker.L2.threshold

   Type: *double*

   The detector threshold that triggers the AIC picker.
   Default is ``3``.

.. confval:: spicker.L2.timeCorr

   Type: *double*

   Unit: *s*

   The time correction \(in seconds\) added to the detection time
   before AIC time window is computed.
   Default is ``0``.

.. confval:: spicker.L2.marginAIC

   Type: *double*

   Unit: *s*

   The AIC time window around the detection used to pick. If 0 AIC is not used.
   Default is ``5``.

.. confval:: spicker.L2.minSNR

   Type: *double*

   Defines the mininum SNR as returned from AIC.
   Default is ``15``.



Command Line
============


Generic
-------

.. option:: -h, --help

   show help message.

.. option:: -V, --version

   show version information

.. option:: --config-file arg

   Use alternative configuration file. When this option is used
   the loading of all stages is disabled. Only the given configuration
   file is parsed and used. To use another name for the configuration
   create a symbolic link of the application or copy it, eg scautopick \-> scautopick2.

.. option:: --plugins arg

   Load given plugins.

.. option:: -D, --daemon

   Run as daemon. This means the application will fork itself and
   doesn't need to be started with \&.

.. option:: --auto-shutdown arg

   Enable\/disable self\-shutdown because a master module shutdown. This only
   works when messaging is enabled and the master module sends a shutdown
   message \(enabled with \-\-start\-stop\-msg for the master module\).

.. option:: --shutdown-master-module arg

   Sets the name of the master\-module used for auto\-shutdown. This
   is the application name of the module actually started. If symlinks
   are used then it is the name of the symlinked application.

.. option:: --shutdown-master-username arg

   Sets the name of the master\-username of the messaging used for
   auto\-shutdown. If \"shutdown\-master\-module\" is given as well this
   parameter is ignored.


Verbose
-------

.. option:: --verbosity arg

   Verbosity level [0..4]. 0:quiet, 1:error, 2:warning, 3:info, 4:debug

.. option:: -v, --v

   Increase verbosity level \(may be repeated, eg. \-vv\)

.. option:: -q, --quiet

   Quiet mode: no logging output

.. option:: --print-component arg

   For each log entry print the component right after the
   log level. By default the component output is enabled
   for file output but disabled for console output.

.. option:: --print-context arg

   For each log entry print the source file name and line
   number.

.. option:: --component arg

   Limits the logging to a certain component. This option can be given more than once.

.. option:: -s, --syslog

   Use syslog logging back end. The output usually goes to \/var\/lib\/messages.

.. option:: -l, --lockfile arg

   Path to lock file.

.. option:: --console arg

   Send log output to stdout.

.. option:: --debug

   Debug mode: \-\-verbosity\=4 \-\-console\=1

.. option:: --trace

   Trace mode: \-\-verbosity\=4 \-\-console\=1 \-\-print\-component\=1 \-\-print\-context\=1

.. option:: --log-file arg

   Use alternative log file.


Messaging
---------

.. option:: -u, --user arg

   Overrides configuration parameter :confval:`connection.username`.

.. option:: -H, --host arg

   Overrides configuration parameter :confval:`connection.server`.

.. option:: -t, --timeout arg

   Overrides configuration parameter :confval:`connection.timeout`.

.. option:: -g, --primary-group arg

   Overrides configuration parameter :confval:`connection.primaryGroup`.

.. option:: -S, --subscribe-group arg

   A group to subscribe to. This option can be given more than once.

.. option:: --encoding arg

   Overrides configuration parameter :confval:`connection.encoding`.

.. option:: --start-stop-msg arg

   Sets sending of a start\- and a stop message.


Database
--------

.. option:: --db-driver-list

   List all supported database drivers.

.. option:: -d, --database arg

   The database connection string, format: service:\/\/user:pwd\@host\/database.
   \"service\" is the name of the database driver which can be
   queried with \"\-\-db\-driver\-list\".

.. option:: --config-module arg

   The configmodule to use.

.. option:: --inventory-db arg

   Load the inventory from the given database or file, format: [service:\/\/]location

.. option:: --config-db arg

   Load the configuration from the given database or file, format: [service:\/\/]location


Records
-------

.. option:: --record-driver-list

   List all supported record stream drivers

.. option:: -I, --record-url arg

   The recordstream source URL, format: [service:\/\/]location[#type].
   \"service\" is the name of the recordstream driver which can be
   queried with \"\-\-record\-driver\-list\". If \"service\"
   is not given \"file:\/\/\" is used.

.. option:: --record-file arg

   Specify a file as record source.

.. option:: --record-type arg

   Specify a type for the records being read.


User interface
--------------

.. option:: -F, --full-screen

   Starts the application filling the entire screen. This only works
   with GUI applications.

.. option:: -N, --non-interactive

   Use non\-interactive presentation mode. This only works with GUI applications.

