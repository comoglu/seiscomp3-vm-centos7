.. highlight:: rst

.. _caps2caps:

#########
caps2caps
#########

**Recordstream data acquisition plugin**


Description
===========

caps2caps connects 2 CAPS instances.



Configuration
=============

| :file:`etc/defaults/global.cfg`
| :file:`etc/defaults/caps2caps.cfg`
| :file:`etc/global.cfg`
| :file:`etc/caps2caps.cfg`
| :file:`~/.seiscomp3/global.cfg`
| :file:`~/.seiscomp3/caps2caps.cfg`

caps2caps inherits :ref:`global options<global-configuration>`.



.. confval:: streams

   Type: *string*

   Comma separated list of streams. Stream format is [NET.STA.LOC.CHA].
   Streams may contain wildcards


.. confval:: begin

   Type: *string*

   Start time of data time window, default is 'GMT'. Date time format is
   [YYYY\-MM\-DD HH:MM:SS]


.. confval:: end

   Type: *string*

   End time of data time window. Date time format is
   [YYYY\-MM\-DD HH:MM:SS]


.. confval:: days

   Type: *int*

   Use to set the start time of data time window n days before the current time


.. confval:: realtime

   Type: *boolean*

   Enable\/disable realtime mode
   Default is ``true``.

.. confval:: outOfOrder

   Type: *boolean*

   Enable\/disable out of order mode
   Default is ``false``.

.. confval:: input.address

   Type: *string*

   Data input URL [[caps\|capss]:\/\/][user:pass\@]host[:port]


.. confval:: output.address

   Type: *string*

   Data output URL [[caps\|capss]:\/\/][user:pass\@]host[:port]
   Default is ``localhost:18003``.

.. confval:: output.bufferSize

   Type: *uint*

   Size \(bytes\) of the packet buffer
   Default is ``1048576``.

.. confval:: output.mseed

   Type: *boolean*

   Enables Steim2 encoding for received RAW packets
   Default is ``false``.

.. confval:: journal.file

   Type: *string*

   File to store stream states
   Default is ``@ROOTDIR@/var/run/caps2caps/journal``.

.. confval:: journal.flush

   Type: *uint*

   Flush stream states to disk every n seconds
   Default is ``10``.

.. confval:: journal.waitForAck

   Type: *uint*

   Wait when a sync has been forced, up to n seconds
   Default is ``5``.

.. confval:: journal.waitForLastAck

   Type: *uint*

   Wait on shutdown to receive acknownledgement messages, up to n seconds
   Default is ``5``.


Command Line
============


Generic
-------

.. option:: -h, --help

   show help message.

.. option:: -V, --version

   show version information

.. option:: --config-file arg

   Use alternative configuration file. When this option is used
   the loading of all stages is disabled. Only the given configuration
   file is parsed and used. To use another name for the configuration
   create a symbolic link of the application or copy it, eg scautopick \-> scautopick2.

.. option:: --plugins arg

   Load given plugins.

.. option:: -D, --daemon

   Run as daemon. This means the application will fork itself and
   doesn't need to be started with \&.


Verbosity
---------

.. option:: --verbosity arg

   Verbosity level [0..4]. 0:quiet, 1:error, 2:warning, 3:info, 4:debug

.. option:: -v, --v

   Increase verbosity level \(may be repeated, eg. \-vv\)

.. option:: -q, --quiet

   Quiet mode: no logging output

.. option:: --print-component arg

   For each log entry print the component right after the
   log level. By default the component output is enabled
   for file output but disabled for console output.

.. option:: --component arg

   Limits the logging to a certain component. This option can be given more than once.

.. option:: -s, --syslog

   Use syslog logging back end. The output usually goes to \/var\/lib\/messages.

.. option:: -l, --lockfile arg

   Path to lock file.

.. option:: --console arg

   Send log output to stdout.

.. option:: --debug

   Debug mode: \-\-verbosity\=4 \-\-console\=1

.. option:: --trace

   Trace mode: \-\-verbosity\=4 \-\-console\=1 \-\-print\-component\=1 \-\-print\-context\=1

.. option:: --log-file arg

   Use alternative log file.


Input
-----

.. option:: -I, --input arg

   Overrides configuration parameter :confval:`input.address`.


Output
------

.. option:: -O, --output arg

   Data output host

.. option:: -b, --buffer-size arg

   Size \(bytes\) of the journal buffer, if exceeded a sync of the journal is forced.

.. option:: --mseed

   Enables Steim2 encoding for received RAW packets


Streams
-------

.. option:: -i, --inventory arg

   Inventory XML defining streams to add

.. option:: -A, --add-stream arg

   List of streamIDs [net.sta.loc.cha] to add, wildcards supported.

.. option:: --begin arg

   Request start time. Applied only on streams not found in the journal.

.. option:: --days arg

   Request start time in days before now. Applied only on streams not found in the journal.

.. option:: --end arg

   Request end time.


Mode
----

.. option:: --archive

   Disables realtime mode. Only archived data is fetched.

.. option:: --out-of-order

   Enables out of order mode.


Journal
-------

.. option:: -j, --journal arg

   File to store stream states. Use an empty string to log to standard out.

.. option:: -f, --flush arg

   Flush stream states to disk every n seconds

.. option:: --waitForAck arg

   Wait when a sync has been forced, up to n seconds

.. option:: -w, --waitForLastAck arg

   Wait on shutdown to receive acknownledgement messages, up to n seconds

