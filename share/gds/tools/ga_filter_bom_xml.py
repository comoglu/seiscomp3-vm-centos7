#!/usr/bin/env python
# -*- coding: utf-8 -*-

#TODO: The licence information needs to be updated here.

import os
import sys
import time
import json
import calendar
import dateutil.parser
import datetime
import traceback

import seiscomp3.Core
import seiscomp3.DataModel
from lib import bulletin, filter
from lib.gaintersect import get_source_zone



with open('/opt/seiscomp3/share/eatws/bom_earthquake_source_zones.json', 'r') as gj:
    SOURCE_ZONES = json.load(gj)['features']



class BomXmlFilter(filter.Filter):
    def filter(self, ep):
        event_dict = self.parseEventParameters(ep)

        return self.createXml(event_dict)



    # TODO: There's a possibility that this function could be used often across a
    # lot of different filters. It might be possible to make this function into a
    #common one.
    def parseEventParameters(self, ep):
        """
        Returns the seismic event information from Quake Link (in Seiscomp ML)
        stored in a python dict.

        :param ep: The seismic event information object from Quake Link. See
        documentation for the filter.Filter.filter method for more information.

        :returns: Python dict with the seismic event information.
        """

        eventDict = {}

        if ep.eventCount() < 1:
            raise Exception('no events in input')

        event = ep.event(0)
        eventDict["id"] = event.publicID()

        for j in range(0, event.eventDescriptionCount()):
            ed = event.eventDescription(j)
            if ed.type() == seiscomp3.DataModel.REGION_NAME:
                eventDict["region"] = ed.text()
                break

        magnitude = seiscomp3.DataModel.Magnitude.Find(
            event.preferredMagnitudeID())
        if not magnitude:
            raise Exception('No preferred magnitude available')
        eventDict["magVal"] = magnitude.magnitude().value()
        eventDict["magType"] = magnitude.type()

        origin = seiscomp3.DataModel.Origin.Find(event.preferredOriginID())
        if not origin:
            raise Exception('No preferred origin available')
        eventDict["time"] = origin.time().value()
        eventDict["lat"] = origin.latitude().value()
        eventDict["lon"] = origin.longitude().value()
        try:
            eventDict["depth"] = origin.depth().value()
        except seiscomp3.Core.ValueException:
            eventDict["depth"] = 0.
        eventDict["phases"] = origin.arrivalCount()

        return eventDict


    def createXml(self, eventDict):
        """
        Returns the formatted xml (as a string).

        :param eventDict: The python dict containing the seismic event info. See the
        parseEventParamters method for how this dict is created.
        """

        def totime(etime, is_sc3_time=True):
            if is_sc3_time:
                dt = datetime.datetime.strptime(str(etime.iso()),  '%Y-%m-%dT%H:%M:%S.%fZ')
            else:
                dt = etime
            return calendar.timegm(dt.timetuple()) + 1e-6 * dt.microsecond

        xmlString = ('<message author="ATWS" id="{qid}" type="{{alert_type}}">\n'
                     '   <earthquake id="{qid}">\n'
                     '      <lat>{latitude:0.3f}</lat>\n'
                     '      <lon>{longitude:0.3f}</lon>\n'
                     '      <depth>{depth:0.0f}</depth>\n'
                     '      <time>{time:0.5f}</time>\n'
                     '      <location>{region}</location>\n'
                     '      <magnitude type="{magType}">{magValue:0.1f}</magnitude>\n'
                     '      <issueTime>{issueTime:0.5f}</issueTime>\n'
                     '   </earthquake>\n'
                     '</message>\n').format(
            qid=eventDict["id"],
            latitude=eventDict["lat"],
            longitude=eventDict["lon"],
            depth=eventDict["depth"],
            time=totime(eventDict["time"]),
            region=eventDict["region"],
            magType=eventDict["magType"],
            magValue=eventDict['magVal'],
            issueTime=time.time())

        return xmlString



if __name__ == "__main__":
    app = BomXmlFilter()
    sys.exit(app())
