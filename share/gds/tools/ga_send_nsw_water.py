#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
from azure.storage.blob import BlockBlobService
from lib import spooler
from lib.gaintersect import point_inside_polygon



VALID_ALERT_TYPES = ('automatic', 'manual', 'test')
POLY = [(157., -23.), (155., -40.), (138., -39.), (138., -26.)]



class SpoolNSWWater(spooler.Spooler):
    def __init__(self):
        spooler.Spooler.__init__(self)
        self.container = self._config.get("azure", "container")
        self.account = self._config.get("azure", "account")
        self.key = self._config.get("azure", "key")

    def spool(self, alert_type, content):
        event_dict = json.loads(content)

        if not point_inside_polygon(
                event_dict["Longitude"], event_dict["Latitude"], POLY):
            return

        alert_type = alert_type[0][1]

        if alert_type not in VALID_ALERT_TYPES:
            raise ValueError('Invalid alert type: Recieved {} but must be one of ({})'.format(
                alert_type,
                ', '.join(VALID_ALERT_TYPES)))

        blob_name ='input/{}.json'.format(event_dict['EventId'])

        block_blob_service = BlockBlobService(
            account_name=self.account,
            sas_token=self.key)

        block_blob_service.create_blob_from_text(
            self.container,
            blob_name,
            json.dumps(event_dict, indent=2, separators=(',', ': ')))


if __name__ == "__main__":
    app = SpoolNSWWater()
    app()
