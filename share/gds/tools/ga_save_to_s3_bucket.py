#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Writes SC3 XML for an event to the S3 bucket specified by :py:data:`BUCKET_NAME`.
"""

import boto
import xml.etree.ElementTree as ET
from lib import spooler

NAMESPACE = {'sc3': 'http://geofon.gfz-potsdam.de/ns/seiscomp3-schema/0.10'}



class S3BucketSpooler(spooler.Spooler):
    def __init__(self, testing=False):
        spooler.Spooler.__init__(self)
        self.bucket = self._config.get("aws", "bucket")

    def spool(self, addresses, content, key_name=None):
        event_id = ET.fromstring(content) \
            .find('sc3:EventParameters', NAMESPACE) \
            .find('sc3:event', NAMESPACE) \
            .get('publicID')

        conn = boto.connect_s3()
        b = conn.get_bucket(self.bucket)
        key = boto.s3.key.Key(b)
        key.key = '{}.xml'.format(event_id)
        key.set_contents_from_string(content)



if __name__ == "__main__":
    app = S3BucketSpooler()
    app()
