#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
# Copyright (C) 2014 by gempa GmbH                                            #
#                                                                             #
# All Rights Reserved.                                                        #
#                                                                             #
# NOTICE: All information contained herein is, and remains                    #
# the property of gempa GmbH and its suppliers, if any. The intellectual      #
# and technical concepts contained herein are proprietary to gempa GmbH       #
# and its suppliers.                                                          #
# Dissemination of this information or reproduction of this material          #
# is strictly forbidden unless prior written permission is obtained           #
# from gempa GmbH.                                                            #
#                                                                             #
# Author: Stephan Herrnkind                                                   #
# Email: herrnkind@gempa.de                                                   #
#                                                                             #
#                                                                             #
# Sends a gempa bulletin as a SMS via Skype                                   #
#                                                                             #
###############################################################################

import sys, time
import Skype4Py

from lib import bulletin, logger, spooler



###############################################################################
class SkypeConfig:
	def __init__(self, config):
		prefix = "skype"

		try:  self.maxMsgLen = config.getint(prefix, "maxMsgLen")
		except: self.maxMsgLen = 160
		try:  self.maxMsgPrice = config.getfloat(prefix, "maxMsgPrice")
		except: self.maxMsgPrice = None
		try:  self.minBalance = config.getfloat(prefix, "minBalance")
		except: self.minBalance = None
		try:  self.attachTimeout = config.getfloat(prefix, "attachTimeout")
		except: self.attachTimeout = 5.0
		try:  self.createTimeout = config.getfloat(prefix, "createTimeout")
		except: self.createTimeout = 10.0
		try:  self.sendTimeout = config.getfloat(prefix, "sendTimeout")
		except: self.sendTimeout = 20.0
		try:  self.checkDelivery = config.getboolean(prefix, "checkDelivery")
		except: self.checkDelivery = False



###############################################################################
class SpoolSkype(spooler.Spooler):

	#--------------------------------------------------------------------------
	def __init__(self):
		spooler.Spooler.__init__(self)

		# read config
		self._cfg = SkypeConfig(self._config)



	#--------------------------------------------------------------------------
	def spool(self, addresses, content):
		# create message from bulletin content
		msg = self._createMessage(content)

		# create and validate addresses
		addressMap = self._createAddressMap(addresses)
		if len(addressMap) == 0:
			raise Exception("no valid phone number found in address file")

		# create SMS
		sms = self._createSMS(msg, addressMap)

		# send message to all numbers
		self._send(sms)

		# verify delivery
		for t in sms.Targets:
			if t.Status == Skype4Py.enums.smsTargetStatusDeliverySuccessful or \
			   ( not self._cfg.checkDelivery and
			     t.Status == Skype4Py.enums.smsTargetStatusDeliveryPending ):
				self._partialSuccess = True
				continue

			if t.Number not in addressMap:
				raise Exception("could not map telephone number to target, " \
				                "delivery status unknown")
			a = addressMap[t.Number]
			self.addTargetError(a[0], a[1], "delivery failed with state: " \
			                    "%s " % t.Status)



	#--------------------------------------------------------------------------
	def _createMessage(self, content):
		# read bulletin
		logger.notice("parsing bulletin")
		try:
			b = bulletin.Bulletin()
			b.read(content)
		except Exception, e:
			raise Exception("could no parse bulletin: %s" % str(e))

		# construct plain message
		msg = b.plain + b.html
		if b.subject != "":
			msg = subject if msg == "" else (subject + ": " + msg)

		if len(msg) > self._cfg.maxMsgLen:
			msg = msg[:self._cfg.maxMsgLen]
			logger.warning("content exceeds maximum length (%i), truncating" % (
			               self._cfg.maxMsgLen))
		elif len(msg) == 0:
			raise Exception("no content found")

		return msg



	#--------------------------------------------------------------------------
	# Strips whitespaces from the target address.
	# Returns a map which maps the (possible) modified address to the original
	# subscriber/target pair needed for logging.
	def _createAddressMap(self, addresses):
		addressMap = {}
		for a in addresses:
			address = a[1].replace(" ", "")
			if address == "":
				msg = "ignoring empty number: %s" % address
				logger.warning(msg)
				self.addTargetError(a[0], a[1], msg)
			elif address in addressMap:
				msg = "ignoring duplicated number: %s" % address
				logger.warning(msg)
				self.addTargetError(a[0], a[1], msg)
			else:
				addressMap[address] = a
		return addressMap



	#--------------------------------------------------------------------------
	def _createSMS(self, message, addressMap):
		# initialize connection to Skype client
		skype = Skype4Py.Skype()
		skype.Timeout = self._cfg.attachTimeout
		logger.notice("attaching to Skype client (API version: %s)" % \
		              skype.Version)
		skype.Attach()
		profile = Skype4Py.profile.Profile(skype)
		logger.notice("connection to Skype client established, " \
		               "user: %s, balance: %s" % (profile.FullName,
		                                          profile.BalanceToText))

		# create SMS
		sms = skype.CreateSms('OUTGOING', *addressMap.keys())
		sms.Body = message
		retry = self._cfg.createTimeout * 10.0
		for i in xrange(len(sms.Targets)):
			t = sms.Targets[i]
			while retry > 0:
				if t.Status == Skype4Py.enums.smsTargetStatusAcceptable or \
				   t.Status == Skype4Py.enums.smsTargetStatusNotRoutable:
				   break
				retry -= 1
				time.sleep(0.1)
		if retry <= 0:
			raise Exception("message create timeout exceeded")
		else:
			logger.notice("message created")


		# verify price and balance limit
		#sms.ReplyToNumber = ''
		if self._cfg.maxMsgPrice is not None and \
		   sms.PriceValue > self._cfg.maxMsgPrice:
			raise Exception("message price ($s) exceeds maximum of $s $.3f" % (
			                sms.PriceToText, sms.PriceCurrency,
			                self._cfg.maxMsgPrice))
		if self._cfg.minBalance is not None and \
		   sms.PriceValue + profile.BalanceValue > self._cfg.minBalance:
			raise Exception("message price ($s) undershoots minimum balance " \
			                "of $s $.3f" % (sms.PriceToText, sms.PriceCurrency,
			                self._cfg.minBalance))

		return sms



	#--------------------------------------------------------------------------
	def _send(self, sms):
		logger.notice("sending message to %i recipients (costs: %s)" % (
		              len(sms.Targets), sms.PriceToText))

		#sms.ReplyToNumber = ""
		sms.Send()
		retry = self._cfg.sendTimeout * 10
		while retry > 0:
			retry -= 1
			if sms.Status == Skype4Py.enums.smsMessageStatusFailed:
				raise Exception("sending of message failed: %s" % \
				      sms.FailureReason)
			if sms.Status == Skype4Py.enums.smsMessageStatusDelivered or \
			   sms.Status == Skype4Py.enums.smsMessageStatusSomeTargetsFailed or \
			   ( not self._cfg.checkDelivery and \
                 sms.Status == Skype4Py.enums.smsMessageStatusSentToServer ):
				return
			time.sleep(0.1)

		logger.warning("timeout while sending message, last state: %s" % \
			           sms.Status)



###############################################################################
if __name__ == "__main__":
	app = SpoolSkype()
	app()


# vim: ts=4 noet
