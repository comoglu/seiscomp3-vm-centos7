#!/usr/bin/expect
set NUM [lindex $argv]
spawn sudo /usr/bin/cu -l /dev/ttyS0
expect "Connected."
sleep 1
send "ATDT $NUM\r"
expect
sleep 15
send "ATH\r"
send "ATZ!\r"
expect "OK"
send "~.\r"
expect "Disconnected."
