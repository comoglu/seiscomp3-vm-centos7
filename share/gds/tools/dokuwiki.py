#!/usr/bin/env python
# -*- coding: utf8 -*-

###############################################################################
# Copyright (C) 2014 by gempa GmbH                                            #
#                                                                             #
# All Rights Reserved.                                                        #
#                                                                             #
# NOTICE: All information contained herein is, and remains                    #
# the property of gempa GmbH and its suppliers, if any. The intellectual      #
# and technical concepts contained herein are proprietary to gempa GmbH       #
# and its suppliers.                                                          #
# Dissemination of this information or reproduction of this material          #
# is strictly forbidden unless prior written permission is obtained           #
# from gempa GmbH.                                                            #
#                                                                             #
# Author: Stephan Herrnkind                                                   #
# Email: herrnkind@gempa.de                                                   #
#                                                                             #
#                                                                             #
# Updates DokuWiki. Creates new page for each event.                          #
#                                                                             #
###############################################################################

import base64, datetime, os, StringIO, sys, traceback, urllib2
import dokuwikixmlrpc

from lib import logger, quakelink, spooler
from seiscomp3 import Core, DataModel, IO


def lat2str(lat):
	if lat >= 0:
		return u"%.2f°N" % lat
	return u"%.2f°S" % -lat


def lon2str(lon):
	if lon >= 0:
		return u"%.2f°E" % lon
	return u"%.2f°W" % -lon



###############################################################################
class WikiConfig:

	#--------------------------------------------------------------------------
	def __init__(self, config):
		prefix = "dokuwiki"

		try:  self.url = config.get(prefix, "url")
		except: self.url = None
		try:  self.user = config.get(prefix, "user")
		except: self.user = None
		try:  self.password = config.get(prefix, "password")
		except: self.password = None
		try:  self.baseNS = config.get(prefix, "baseNS")
		except: self.baseNS = "event"

		prefix = "quakelink"

		try:  self.qlHost = config.get(prefix, "host")
		except: self.qlHost = "localhost"
		try:  self.qlPort = config.getint(prefix, "port")
		except: self.qlPort = 18010
		try:  self.qlSSL = config.getboolean(prefix, "ssl")
		except: self.qlSSL = False
		try:  self.qlUser = config.get(prefix, "user")
		except: self.qlUser = None
		try:  self.qlPassword = config.get(prefix, "password")
		except: self.qlPassword = None
		try:  self.qlTimeout = config.getint(prefix, "timeout")
		except: self.qlTimeou = 10


###############################################################################
class DokuWiki(spooler.Spooler):

	#--------------------------------------------------------------------------
	def __init__(self):
		self._dev = None
		spooler.Spooler.__init__(self)

		# read config
		self._cfg = WikiConfig(self._config)

		if self._cfg.url is None:
			logger.error("missing config parameter dokuwiki.url")
			sys.exit(2)

		if self._cfg.user is None:
			logger.error("missing config parameter dokuwiki.user")
			sys.exit(2)

		if self._cfg.password is None:
			logger.error("missing config parameter dokuwiki.password")
			sys.exit(2)



	#--------------------------------------------------------------------------
	def __call__(self):
		try:
			logger.info("start spooling")

			self.parseXML()
			self.spool()
			sys.exit(0)
		except Exception, e:
			error = str(e)
			if len(error) == 0:
				error = e.__class__.__name__
			logger.error(error)
			if logger.noticeEnabled():
				logger.notice(traceback.format_exc())
			sys.exit(2)



	#--------------------------------------------------------------------------
	def spool(self):
		# read xml
		ep = self.parseXML()
		eventDict = self.parseEventParameters(ep)
		id = eventDict["id"]
		ot = eventDict["time"]

		# extract date components, create target path
		dt = datetime.datetime.utcfromtimestamp(ot.seconds())
		path = ":%04d:%02d:%02d:%s" % (dt.year, dt.month, dt.day, id)
		pageID = self._cfg.baseNS + path
		imageID = pageID + ".jpeg"
		commentID = self._cfg.baseNS + ":comment" + path

		# load image from MapServer
		imageData = self.createImgMapServer(eventDict)

		# create XMLRPC connection
		c = dokuwikixmlrpc.DokuWikiClient(self._cfg.url, self._cfg.user,
		                                  self._cfg.password)

		# upload image
		if imageData:
			try:
				c._xmlrpc.wiki.putAttachment(imageID, imageData, {'ow': True})
			except Exception, e:
				logger.warning("failed to upload image: %s" % str(e))
				imageID = None
		else:
			imageID = None

		# fetch revisions from QuakeLink
		eventDict["revlist"] = self.fetchRevisions(id)

		eventDict["pageID"] = pageID
		eventDict["commentID"] = commentID

		# create page content
		pageData = self.createPage(eventDict, imageID)

		# upload page
		c.put_page(pageID, pageData, "created by GDS", False)



	#--------------------------------------------------------------------------
	def parseXML(self):
		fName = self._basePath + ".content"
		if not os.path.isfile(fName):
			raise Exception("content file does not exist: %s" % fName)
		logger.notice("reading content from: %s" % fName)

		logger.notice("reading content file")
		ar = IO.XMLArchive()
		if ar.open(fName) == False:
			raise Exception("could not read content file '%s'" % fName)

		obj = ar.readObject()
		if not obj:
			raise Exception("no object found in input XML")

		ep = DataModel.EventParameters.Cast(obj)
		if not ep:
			raise Exception("XML object not of type EventParameters")

		return ep



	#--------------------------------------------------------------------------
	# parses important SC3ML parameters
	def parseEventParameters(self, ep):
		eventDict = {}
		if ep.eventCount() < 1:
			raise Exception("event element not found")

		event = ep.event(0)
		eventDict["id"] = event.publicID()

		eventDict["region"]  = ""
		for j in range(0, event.eventDescriptionCount()):
			ed = event.eventDescription(j)
			if ed.type() == DataModel.REGION_NAME:
				eventDict["region"] = ed.text()
				break

		magnitude = DataModel.Magnitude.Find(event.preferredMagnitudeID())
		if magnitude:
			eventDict["magVal"] = magnitude.magnitude().value()
			eventDict["magType"] = magnitude.type()
		else:
			logger.warning("preferred magnitude not found")

		origin = DataModel.Origin.Find(event.preferredOriginID())
		if not origin:
			raise Exception("preferred origin not found")

		eventDict["time"] = origin.time().value()
		eventDict["lat"] = origin.latitude().value()
		eventDict["lon"] = origin.longitude().value()
		try: eventDict["depth"] = origin.depth().value()
		except ValueError: pass
		eventDict["phases"] = origin.arrivalCount()
		eventDict["mode"] = ""
		try:
			if origin.evaluationMode() == 0:
				eventDict["mode"] = "manual"
			elif origin.evaluationMode() == 1:
				eventDict["mode"] = "automatic"
		except ValueError: pass
		try: eventDict["rms"] = "%.2f" % origin.quality().standardError()
		except ValueError: eventDict["rms"] = ""
		try: eventDict["gap"] = "%.2f" % origin.quality().azimuthalGap()
		except ValueError: eventDict["gap"] = ""
		try: eventDict["agency"] = origin.creationInfo().agencyID()
		except ValueError: eventDict["agency"] = ""

		return eventDict



	#--------------------------------------------------------------------------
	def createPage(self, eventDict, imageID):
		txt = """~~NOTOC~~
====== %(id)s ======
"""
		if imageID is not None:
			txt = txt + "{{:%s?direct&300 |}}" % imageID
		if "depth" in eventDict:
			eventDict["depth"] = "%i" % int(eventDict["depth"] + 0.5)
		else:
			eventDict["depth"] = ""
		eventDict["lat"] = lat2str(eventDict["lat"])
		eventDict["lon"] = lon2str(eventDict["lon"])
		txt = txt + """
| Magnitude     | %(magVal).1f (%(magType)s) |
| Region        | %(region)s |
| Origin time   | %(time)s |
| Location      | %(lat)s %(lon)s |
| Depth         | %(depth)s km |
| Phases        | %(phases)i |
| Status        | %(mode)s |
| RMS           | %(rms)s |
| Azimuthal Gap | %(gap)s |
| AgencyID      | %(agency)s |


===== Revisions =====

%(revlist)s


===== Comments =====

{{page>%(commentID)s}}

"""

		return txt % eventDict


	#--------------------------------------------------------------------------
	# fetches revision list from QuakeLink server
	def fetchRevisions(self, eventID):
		logger.notice("fetching event revisions from QuakeLink server")
		retn = "//not available//"
		try:
			ql = quakelink.Quakelink(self._cfg.qlHost, self._cfg.qlPort,
			                         self._cfg.qlSSL, self._cfg.qlUser,
			                         self._cfg.qlPassword, self._cfg.qlTimeout)
			ql.connect()
			revlist = ql.get_refinements("gfz2013nihj")
			lines = revlist.splitlines()
			if len(lines) > 1:
				retn = "^ # ^ T<sub>trigger</sub> ^ T<sub>origin</sub> ^ Mag. ^ " \
				       "Lat. ^ Lon. ^ Dep. ^ Pha. ^ Agency ^ Mode ^\n"
			for i in xrange(len(lines)-2, 0, -1):
				cols = lines[i].split(";")
				retn = retn + "| %i | %s | %s | %s%s | %s | %s | %skm | " \
				              "%s | %s | %s |\n" % (
				              int(cols[0]), cols[1].split()[1],
				              cols[2].split()[1], cols[3], cols[4],
				              lat2str(float(cols[5])), lon2str(float(cols[6])),
				              cols[7], cols[8], cols[9], cols[10])
		except Exception, e:
			logger.warning("could not fetch event revisions: %s" % str(e))

		try: ql.close()
		except: pass

		return retn



	#--------------------------------------------------------------------------
	# creates an image attachment showing epicenter location using
	# gempa MapServer
	def createImgMapServer(self, eventDict):
		lat = eventDict['lat']
		lon = eventDict['lon']
		url = "http://localhost:20001/img?reg=%f,%f,15,15" % (lat, lon)
		url += "&dim=384,384&ori=%f,%f" % (lat, lon)
		if "depth" in eventDict:
			url += ",%f" % eventDict["depth"]
		if "magVal" in eventDict:
			url += "&mag=%f" % eventDict["magVal"]
		url += "&fmt=jpg&qua=85"

		logger.notice("fetching epicenter location from url: %s" % url)

		data = None
		try:
			u = urllib2.urlopen(url)
			b64IO = StringIO.StringIO()
			base64.encode(u, b64IO)
			data = b64IO.getvalue()
			b64IO.close()
		except Exception, e:
			logger.warning("could load image from url '%s': %s" % (url, str(e)))
			data = None

		return data



	#--------------------------------------------------------------------------
	def pagelist(self, namespace):
		"""Lists all pages within a given namespace."""
		try:
			return client._xmlrpc.dokuwiki.getPagelist(namespace, {})
		except xmlrpclib.Fault, fault:
			raise DokuWikiXMLRPCError(fault)



###############################################################################
if __name__ == "__main__":
	app = DokuWiki()
	app()


# vim: ts=4 noet
