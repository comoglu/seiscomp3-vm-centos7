#!/usr/bin/env python
# -*- coding: utf-8 -*-


from lib import bulletin, logger, spooler
from twython import Twython


VALID_ENVIRONMENTS = ['test', 'prod']


class SpoolTwitter(spooler.Spooler):
    def __init__(self):
        spooler.Spooler.__init__(self)

        self.credentials = {
            'prod': {
                'appKey': self._config.get('prod', 'APP_KEY'),
                'appSecret': self._config.get('prod', 'APP_SECRET'),
                'accessToken': self._config.get('prod', 'ACCESS_TOKEN'),
                'accessSecret': self._config.get('prod', 'ACCESS_SECRET')},
            'test': {
                'appKey': self._config.get('test', 'APP_KEY'),
                'appSecret': self._config.get('test', 'APP_SECRET'),
                'accessToken': self._config.get('test', 'ACCESS_TOKEN'),
                'accessSecret': self._config.get('test', 'ACCESS_SECRET')}}

    def spool(self, environment, content):
        address = environment[0][1]

        if address not in VALID_ENVIRONMENTS:
            raise ValueError('Invalid environment: recieved {} but must be one of ({})'.format(
                address,
                ', '.join(VALID_ENVIRONMENTS)))

        credentials = self.credentials[address]
        twitter = Twython(
            credentials['appKey'],
            credentials['appSecret'],
            credentials['accessToken'],
            credentials['accessSecret'])

        logger.debug("parsing bulletin")
        try:
            b = bulletin.Bulletin()
            b.read(content)
        except Exception, e:
            raise Exception("could no parse bulletin: %s content: %s" % (str(e),content))

        msg = b.plain

        if len(msg) > 280:
            logger.warning("content exceeds maximum length (%i), truncating" % (280))
            msg = msg[:280]
        elif len(msg) == 0:
            raise Exception("no content found")

        try:
            twitter.update_status(status=msg)
        except Exception, e:
            a = environment[0]
            self.addTargetError(a[0], a[1], str(e))


if __name__ == "__main__":
    app = SpoolTwitter()
    app()
