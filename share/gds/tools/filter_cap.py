#!/usr/bin/env python
# -*- coding: utf-8 -*-

# #############################################################################
# Creates an CAP xml
#
# Author: Stephan Herrnkind
# Email: herrnkind@gempa.de
# #############################################################################

import datetime, os, sys

from seiscomp3 import Core, DataModel, Math

from lib import filter, logger, xml

###############################################################################
class CAPFilter(filter.Filter):


	#--------------------------------------------------------------------------
	# contructor
	def __init__(self):
		self.df = '%FT%T'
		self.expireOffset = 60*60*12
		self.eventURL = "http://todo%s.html"

		self.const = {}

		# TODO: read from config file or command line
		self.citiesXML = None
		if self.citiesXML is None:
			self.citiesXML = os.path.join(os.environ['SEISCOMP_ROOT'], 'share', 'cities.xml')

		self.const['country']       = 'Oman'
		self.const['sender']        = 'tsunami@dgmet.gov.om'
		self.const['status']        = 'Actual'
		self.const['msgType']       = 'Alert'
		self.const['scope']         = 'Public'
		self.const['event']         = 'Tsunami Warning'
		self.const['responseType']  = 'Prepare'
		self.const['urgency']       = 'Immediate'
		self.const['severity']      = 'Severe'
		self.const['certainty']     = 'Possible'

		# defaults, overridden by XML content
		now = datetime.datetime.utcnow()
		self.const['now']      = now.strftime(self.df)
		self.const['onset']    = self.const['now']
		self.const['expires']  = (now + datetime.timedelta(
		                         0, self.expireOffset, 0)).strftime(self.df)
		self.const['agencyID'] = "dgmet"


	#--------------------------------------------------------------------------
	# Return string with distance and direction of n nearest cities
	def nearestCities(self, lat, lon, n):
		if not os.path.isfile(self.citiesXML):
			raise Exception("citiesXML file not found: %s" % self.citiesXML)

		logger.notice("loading cities from: %s" % self.citiesXML)

		import xml.etree.ElementTree as ET
		tree = ET.parse(self.citiesXML)
		sc3 = tree.getroot()
		if sc3.tag != 'seiscomp':
			raise Exception("invalid document root, found: %s, expected: %s" % (
			                sc3.tag, 'seiscomp'))

		cities = [] # list of triple (name, distance, azimuth)
		for city in sc3:
			if city.tag != 'City': continue
			d = Math.delazi(float(city.find('latitude').text),
			                float(city.find('longitude').text), lat, lon)
			cities.append((city.find('name').text, d[0], d[1]))

		logger.debug("%i cities loaded" % len(cities))

		# sort by distance
		cities = sorted(cities, key=lambda c: c[1])

		i = 0
		s = ""
		for c in cities:
			
			if i >= n: break # stop at n cities
			elif i > 0: s += ", " # add separator

			direction = self.azi2str(c[2])
			s += u"%ikm %s of %s" % (Math.deg2km(c[1]), direction , c[0])
			i += 1

		return s.encode('utf8')



	#--------------------------------------------------------------------------
	# converts azimuth to string direction
	def azi2str(self, azi):
		azi = (azi + 22.5) % 360
		if azi <  45: return "N"
		if azi <  90: return "NE"
		if azi < 135: return "E"
		if azi < 180: return "SE"
		if azi < 225: return "S"
		if azi < 270: return "SW"
		if azi < 315: return "W"
		return "NW"


	#--------------------------------------------------------------------------
	# main filter method, compose method calls to fit your needs
	def filter(self, ep):
		self.const.update(self.parseEventParameters(ep))
		return """<?xml version="1.0" encoding="UTF-8"?>
<alert xmlns="urn:oasis:names:tc:emergency:cap:1.2">
  <identifier>%(country)s-%(agencyID)s-tsunami-%(now)s</identifier>
  <sender>%(sender)s</sender>
  <sent>%(now)s</sent>
  <status>%(status)s</status>
  <msgType>%(msgType)s</msgType>
  <scope>%(scope)s</scope>
  <code>profile:%(agencyID)s-Versi:1</code>
  <info>
    <language>en</language>
    <category>Geo</category>
    <event>%(event)s</event>
    <responseType>%(responseType)s</responseType>
    <urgency>%(urgency)s</urgency>
    <severity>%(severity)s</severity>
    <certainty>%(certainty)s</certainty>
    <onset>%(onset)s</onset>
    <expires>%(expires)s</expires>
    <senderName>%(agencyID)s</senderName>
    <headline>A Tsunami Warning is issued for %(country)s, Earthquake M%(mag)s, %(ot)s, Loc: %(lat)s, %(lon)s, %(depth)s</headline>
    <description>
A Tsunami Warning is issued for Oman

AN EARTHQUAKE HAS OCCURRED WITH THESE PRELIMINARY PARAMETERS:

Magnitude  : %(mag)s
Origin Time: %(ot)s
Latitude   : %(lat)s
Longitude  : %(lon)s
Depth      : %(depth)s

Location   : %(region)s
Remarks    : %(cities)s
    </description>
    <instruction>Province/District/City governments are expected to pay attention to this warning and wait for further instructions.</instruction>
    <web>%(eventURL)s</web>
    <parameter>
      <valueName>EventLocationName</valueName>
      <value>Five nearest area: %(cities)s</value>
    </parameter>
    <parameter>
      <valueName>ShortLocationName</valueName>
      <value>%(region)s</value>
    </parameter>
    <parameter>
      <valueName>EventOriginTime</valueName>
      <value>%(ot)s</value>
    </parameter>
    <parameter>
      <valueName>EventMagnitude</valueName>
      <value>%(mag)s</value>
    </parameter>
    <parameter>
      <valueName>EventDepth</valueName>
      <value>%(depth)s</value>
    </parameter>
    <parameter>
      <valueName>EventLatLon</valueName>
      <value>%(lat)s, %(lon)s</value>
    </parameter>
    <parameter>
      <valueName>layer:Google:SmsText:0.1</valueName>
      <value>Tsunami Warning is issued for Oman, EQ M:%(mag)s, %(ot)s, Loc: %(lat)s, %(lon)s, Dep: %(depth)s::%(agencyID)s</value>
     </parameter>
  </info>
</alert>""" % self.const



	#--------------------------------------------------------------------------
	# parses important SC3ML parameters
	def parseEventParameters(self, ep):
		eventDict = {}
		if ep.eventCount() < 1:
			raise Exception("event element not found")

		event = ep.event(0)

		# event id
		eventDict['id'] = event.publicID()
		eventDict['eventURL'] = self.eventURL % event.publicID()

		# agency, modification time
		try:
			ci = event.creationInfo()
			eventDict['agencyID'] = ci.agencyID()
			try:
				mTime = ci.modificationTime()
				eventDict['onset'] = mTime.toString(self.df)
				eventDict['expires'] = (mTime + Core.TimeSpan(
				                       self.expireOffset, 0)).toString(self.df)
			except ValueError:
				logger.warning("event modification time not found")
		except ValueError:
			logger.warning("event creation info not found")

		# region
		eventDict["region"]  = ""
		for i in xrange(event.eventDescriptionCount()):
			ed = event.eventDescription(i)
			if ed.type() == DataModel.REGION_NAME:
				eventDict["region"] = ed.text()
				break

		# magnitude
		magnitude = DataModel.Magnitude.Find(event.preferredMagnitudeID())
		if magnitude:
			eventDict["mag"] = "%.1f" % magnitude.magnitude().value()
		else:
			eventDict['mag'] = ""
			logger.warning("preferred magnitude not found")

		origin = DataModel.Origin.Find(event.preferredOriginID())
		if not origin:
			raise Exception("preferred origin not found")

		# time, location, depth
		eventDict["ot"] = origin.time().value().iso() + " UTC"
		lat = origin.latitude().value()
		lon = origin.longitude().value()
		eventDict["lat"] = "%.2f" % lat
		eventDict["lon"] = "%.2f" % lon
		try: eventDict["depth"] = "%.0f km" % origin.depth().value()
		except ValueError: pass

		# nearest cities
		eventDict['cities'] = self.nearestCities(lat, lon, 5)

		return eventDict



if __name__ == "__main__":
	app = CAPFilter()
	sys.exit(app())


# vim: ts=4 noet
