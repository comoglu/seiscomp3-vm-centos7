#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
# Copyright (C) 2014 by gempa GmbH                                            #
#                                                                             #
# All Rights Reserved.                                                        #
#                                                                             #
# NOTICE: All information contained herein is, and remains                    #
# the property of gempa GmbH and its suppliers, if any. The intellectual      #
# and technical concepts contained herein are proprietary to gempa GmbH       #
# and its suppliers.                                                          #
# Dissemination of this information or reproduction of this material          #
# is strictly forbidden unless prior written permission is obtained           #
# from gempa GmbH.                                                            #
#                                                                             #
# Author: Stephan Herrnkind                                                   #
# Email: herrnkind@gempa.de                                                   #
#                                                                             #
#                                                                             #
# Reads the SC3ML input and creates a new XML document containing only the    #
# first Event and its preferred Origin                                        #
#                                                                             #
###############################################################################

import os, sys, time
import seiscomp3.Core
import seiscomp3.DataModel

from lib import logger, filter, xml

###############################################################################
class XMLFilter(filter.Filter):

	#--------------------------------------------------------------------------
	# main filter method
	def filter(self, ep):
		if ep.eventCount() < 1:
			raise Exception("event element not found")

		event = ep.event(0)
		origin = seiscomp3.DataModel.Origin.Find(event.preferredOriginID())
		if not origin:
			raise Exception("preferred origin not found")

		# remove arrivals and stationMagnitudes
		for i in reversed(xrange(origin.arrivalCount())):
			origin.removeArrival(i)
		for i in reversed(xrange(origin.stationMagnitudeCount())):
			origin.removeStationMagnitude(i)

		# remove stationMagnitudeContributions
		for i in xrange(origin.magnitudeCount()):
			mag = origin.magnitude(i)
			for j in reversed(xrange(mag.stationMagnitudeContributionCount())):
				mag.removeStationMagnitudeContribution(j)

		# discard old ep object
		ep = seiscomp3.DataModel.EventParameters()
		origin.setParent(None)
		event.setParent(None)
		ep.add(origin)
		ep.add(event)

		return xml.objectToString(ep)



if __name__ == "__main__":
	app = XMLFilter()
	sys.exit(app())


# vim: ts=4 noet
