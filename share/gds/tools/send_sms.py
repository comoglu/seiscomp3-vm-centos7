#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
# Copyright (C) 2014 by gempa GmbH                                            #
#                                                                             #
# All Rights Reserved.                                                        #
#                                                                             #
# NOTICE: All information contained herein is, and remains                    #
# the property of gempa GmbH and its suppliers, if any. The intellectual      #
# and technical concepts contained herein are proprietary to gempa GmbH       #
# and its suppliers.                                                          #
# Dissemination of this information or reproduction of this material          #
# is strictly forbidden unless prior written permission is obtained           #
# from gempa GmbH.                                                            #
#                                                                             #
# Author: Stephan Herrnkind                                                   #
# Email: herrnkind@gempa.de                                                   #
#                                                                             #
#                                                                             #
# Sends a gempa bulletin as a SMS via USB Modem                               #
#                                                                             #
###############################################################################

import ast, math, os, serial, sys
from distutils.version import LooseVersion

from lib import bulletin, logger, spooler, gsm0338

# equipment related errors
CMEError = {
	0   : "Phone failure",
	1   : "No connection to phone",
	2   : "Phone adapter link reserved",
	3   : "Operation not allowed",
	4   : "Operation not supported",
	5   : "PH_SIM PIN required",
	6   : "PH_FSIM PIN required",
	7   : "PH_FSIM PUK required",
	10  : "SIM not inserted",
	11  : "SIM PIN required",
	12  : "SIM PUK required",
	13  : "SIM failure",
	14  : "SIM busy",
	15  : "SIM wrong",
	16  : "Incorrect password",
	17  : "SIM PIN2 required",
	18  : "SIM PUK2 required",
	20  : "Memory full",
	21  : "Invalid index",
	22  : "Not found",
	23  : "Memory failure",
	24  : "Text string too long",
	25  : "Invalid characters in text string",
	26  : "Dial string too long",
	27  : "Invalid characters in dial string",
	30  : "No network service",
	31  : "Network timeout",
	32  : "Network not allowed, emergency calls only",
	40  : "Network personalization PIN required",
	41  : "Network personalization PUK required",
	42  : "Network subset personalization PIN required",
	43  : "Network subset personalization PUK required",
	44  : "Service provider personalization PIN required",
	45  : "Service provider personalization PUK required",
	46  : "Corporate personalization PIN required",
	47  : "Corporate personalization PUK required",
	48  : "PH-SIM PUK required",
	100 : "Unknown error",
	103 : "Illegal MS",
	106 : "Illegal ME",
	107 : "GPRS services not allowed",
	111 : "PLMN not allowed",
	112 : "Location area not allowed",
	113 : "Roaming not allowed in this location area",
	126 : "Operation temporary not allowed",
	132 : "Service operation not supported",
	133 : "Requested service option not subscribed",
	134 : "Service option temporary out of order",
	148 : "Unspecified GPRS error",
	149 : "PDP authentication failure",
	150 : "Invalid mobile class",
	256 : "Operation temporarily not allowed",
	257 : "Call barred",
	258 : "Phone is busy",
	259 : "User abort",
	260 : "Invalid dial string",
	261 : "SS not executed",
	262 : "SIM Blocked",
	263 : "Invalid block",
	772 : "SIM powered down"
}

# network related errors
CMSError = {
	1   : "Unassigned number",
	8   : "Operator determined barring",
	10  : "Call bared",
	21  : "Short message transfer rejected",
	27  : "Destination out of service",
	28  : "Unindentified subscriber",
	29  : "Facility rejected",
	30  : "Unknown subscriber",
	38  : "Network out of order",
	41  : "Temporary failure",
	42  : "Congestion",
	47  : "Recources unavailable",
	50  : "Requested facility not subscribed",
	69  : "Requested facility not implemented",
	81  : "Invalid short message transfer reference value",
	95  : "Invalid message unspecified",
	96  : "Invalid mandatory information",
	97  : "Message type non existent or not implemented",
	98  : "Message not compatible with short message protocol",
	99  : "Information element non-existent or not implemente",
	111 : "Protocol error, unspecified",
	127 : "Internetworking , unspecified",
	128 : "Telematic internetworking not supported",
	129 : "Short message type 0 not supported",
	130 : "Cannot replace short message",
	143 : "Unspecified TP-PID error",
	144 : "Data code scheme not supported",
	145 : "Message class not supported",
	159 : "Unspecified TP-DCS error",
	160 : "Command cannot be actioned",
	161 : "Command unsupported",
	175 : "Unspecified TP-Command error",
	176 : "TPDU not supported",
	192 : "SC busy",
	193 : "No SC subscription",
	194 : "SC System failure",
	195 : "Invalid SME address",
	196 : "Destination SME barred",
	197 : "SM Rejected-Duplicate SM",
	198 : "TP-VPF not supported",
	199 : "TP-VP not supported",
	208 : "D0 SIM SMS Storage full",
	209 : "No SMS Storage capability in SIM",
	210 : "Error in MS",
	211 : "Memory capacity exceeded",
	212 : "Sim application toolkit busy",
	213 : "SIM data download error",
	255 : "Unspecified error cause",
	300 : "ME Failure",
	301 : "SMS service of ME reserved",
	302 : "Operation not allowed",
	303 : "Operation not supported",
	304 : "Invalid PDU mode parameter",
	305 : "Invalid Text mode parameter",
	310 : "SIM not inserted",
	311 : "SIM PIN required",
	312 : "PH-SIM PIN required",
	313 : "SIM failure",
	314 : "SIM busy",
	315 : "SIM wrong",
	316 : "SIM PUK required",
	317 : "SIM PIN2 required",
	318 : "SIM PUK2 required",
	320 : "Memory failure",
	321 : "Invalid memory index",
	322 : "Memory full",
	330 : "SMSC address unknown",
	331 : "No network service",
	332 : "Network timeout",
	340 : "No +CNMA expected",
	500 : "Unknown error",
	512 : "User abort",
	513 : "Unable to store",
	514 : "Invalid Status",
	515 : "Device busy or Invalid Character in string",
	516 : "Invalid length",
	517 : "Invalid character in PDU",
	518 : "Invalid parameter",
	519 : "Invalid length or character",
	520 : "Invalid character in text",
	521 : "Timer expired",
	522 : "Operation temporary not allowed",
	532 : "SIM not ready",
	534 : "Cell Broadcast error unknown",
	535 : "Protocol stack busy",
	538 : "Invalid parameter"
}



def checkError(res):
	err = None
	cmeError = "+CME ERROR: "
	cmsError = "+CMS ERROR: "

	if res.startswith(cmeError):
		try:
			code = res[len(cmeError):-2]
			id = int(code)
			err = "equipment error (%i): %s" % (id, CMEError[id])
		except:
			err = "unknown equipment error: %s" % code
	elif res.startswith(cmsError):
		try:
			code = int(res[len(cmsError):-2])
			id = int(code)
			err = "network error (%i): %s" % (id, CMSError[id])
		except:
			err = "unknown network error: %s" % code
	elif res.startswith("ERROR"):
		err = "unknown error"

	return err



def assertNoError(res):
	err = checkError(res)
	if err:
		raise Exception(err)



###############################################################################
class ModemConfig:
	def __init__(self, config):
		prefix = "modem"

		try:  self.path = config.get(prefix, "path")
		except: self.path = "/dev/serial/by-id/"
		try: self.device = config.get(prefix, "device")
		except: self.device = "if00"
		try:  self.pin = config.get(prefix, "pin")
		except: self.pin = None
		try:  self.baudrate = config.getint(prefix, "baudrate")
		except: self.baudrate = 115200
		try:  self.maxMsg = config.getint(prefix, "maxMsg")
		except: self.maxMsg = 1
		try:  self.cmdTimeout = config.getfloat(prefix, "cmdTimeout")
		except: self.cmdTimeout = 2.0
		try:  self.sendTimeout = config.getfloat(prefix, "sendTimeout")
		except: self.sendTimeout = 20.0
		try:  self.charset = config.get(prefix, "charset").upper()
		except: self.charset = "GSM"
		try: self.handleInvalidChar = config.get(prefix, "handleInvalidChar").lower()
		except: self.handleInvalidChar = 'strict'
		try:  self.disableCUR = config.getboolean(prefix, "disableCUR")
		except: self.disableCUR = False
		try:  self.flushInput = config.getboolean(prefix, "flushInput")
		except: self.flushInput = False

		# If the message is split into multiple parts a unique reference number
		# must be specified in the user data header (UDH). By default a 8bit
		# number is used but may be enlarged to 16bit.
		# Note: If set to true, the UDH will be enlarged from 6 to 7 byte and
		# the maximum message payload per part will be reduced from 134 to 133
		# byte.
		self.mp16bitRefNum = False



###############################################################################
class SpoolSMS(spooler.Spooler):

	#--------------------------------------------------------------------------
	def __init__(self):
		self._dev = None
		spooler.Spooler.__init__(self)

		# read config
		self._modem = ModemConfig(self._config)

		# check if charset is supported
		if self._modem.charset == 'GSM7':
			self._modem.charset = 'GSM'
		elif self._modem.charset == 'UTF8' or self._modem.charset == 'UTF-8':
			self._modem.charset = 'UCS2'
		elif self._modem.charset not in [ 'GSM', 'RAW', 'UCS2' ]:
			logger.error("charset '%s' no supported" % self._modem.charset)
			sys.exit(2)

		# assert minimum serial version 2.5
		if LooseVersion(serial.VERSION) < LooseVersion("2.5"):
			logger.error("minimum python-serial version 2.5 required, "
			             "found %s" % serial.VERSION)
			sys.exit(2)

		# search device
		device = None
		logger.notice("searching modem device")
		if os.path.exists(self._modem.path):
			for fname in os.listdir(self._modem.path):
				if self._modem.device in fname:
					path = os.path.join(self._modem.path, fname)
					device = os.path.realpath(path)
					break

		if device == None:
			logger.error("no modem device found in: %s" % self._modem.path)
			sys.exit(2)
		else:
			logger.debug("using device: %s" % device)

		# initialize serial connection
		logger.notice("initializing serial connection")
		try:
			self._dev = serial.Serial(device, self._modem.baudrate,
			                          timeout = self._modem.cmdTimeout)
		except serial.serialutil.SerialException, error:
			logger.error("could not connect to device: %s" % error)
			sys.exit(2)

		# check for bytes in read buffer indicating other instance using
		# device
		inWaiting = self._dev.inWaiting()
		if inWaiting > 0:
			logger.warning("found %i bytes in receive buffer, simultaneous "
			               "access to device?" % inWaiting)
			if self._modem.flushInput:
				self._dev.flushInput()
				inWaiting = self._dev.inWaiting()
			if inWaiting > 0:
				logger.error("found %i bytes in receive buffer, "
				             "terminating" % inWaiting)
				sys.exit(2)

		# check invalid character handling identifier
		if self._modem.handleInvalidChar not in [ 'ignore', 'replace', 'strict' ]:
			logger.error("unsupported 'invalid character handling' identifier")
			sys.exit(2)


	#--------------------------------------------------------------------------
	def __del__(self):
		if self._dev:
			self._dev.close()



	#--------------------------------------------------------------------------
	def spool(self, addresses, content):
		# read bulletin
		logger.debug("parsing bulletin")
		try:
			b = bulletin.Bulletin()
			b.read(content)
		except Exception, e:
			raise Exception("could not parse bulletin: %s" % str(e))

		# construct plain message
		msg = b.plain + b.html
		if b.subject != "":
			msg = subject if msg == "" else (subject + ": " + msg)

		# split message into several parts
		msgParts = self._splitMessage(msg)
		if len(msgParts) == 0 or len(msgParts[0]) == 0:
			raise Exception("no content found")

		if len(msgParts) > 0 and self._logID is None:
			raise Exception("could not extract logID from file name needed " \
			                "for multi-part messages")

		# create and validate addresses
		addressMap = self._createAddressMap(addresses)
		if len(addressMap) == 0:
			raise Exception("no valid phone number found in address file")

		# disable message echo
		self._disableEcho()

		# disable messages send periodically by some devices pr
		if self._modem.disableCUR:
			self._disableCUR()

		# unlock SIM if required
		self._checkPIN()

		# set modem to PDU mode
		self._checkMode()

		# make sure character set is supported
		self._checkCharset()

		# check signal quality
		self._checkSignal()

		# send message to all numbers
		exception = None
		for number in addressMap.keys():
			if exception == None:
				try:
					self._sendMessage(msgParts, number, addressMap[number])
				except Exception, e:
					if self._partialSuccess:
						exception = e
					else:
						raise e
			else:
				a = addressMap[number]
				self.addTargetError(a[0], a[1], str(e))



	#--------------------------------------------------------------------------
	def _readline(self):
		msg = self._dev.readline()
		if len(msg) == 0:
			raise Exception("read empty response, timeout of %.2fs exceeded" % \
			                 self._dev.timeout)
		return msg



	#--------------------------------------------------------------------------
	def _readRN(self, prefix=''):
		res = self._readline()
		if res == ("%s\r\n" % prefix):
			logger.debug("read: %s\\r\\n" % prefix)
		else:
			raise Exception("protocol error: expected %s\\r\\n, got %s" % (
			                prefix, res))



	#--------------------------------------------------------------------------
	def _send(self, msg):
		logger.debug("sending: %s" % msg)
		if self._dev.write(msg + "\r") <= len(msg):
			raise Exception("sent fewer bytes than expected")
		self._readRN()



	#--------------------------------------------------------------------------
	def _readOK(self):
		res = self._readline()
		if res == "OK\r\n":
			logger.debug("read: OK\\r\\n")
		else:
			assertNoError(res)
			raise Exception("protocol error: expected OK\\r\\n, got: %s" % res)



	#--------------------------------------------------------------------------
	def _readResp(self, prefix, timeout=None):
		if timeout is not None:
			self._dev.timeout = timeout
			res = self._readline()
			self._dev.timeout = self._modem.cmdTimeout
		else:
			res = self._readline()

		assertNoError(res)
		logger.debug("read: %s\\r\\n" % res)
		self._readRN()
		self._readOK()
		prefix += ": "
		if not res.startswith(prefix):
			raise Exception("protocol error: expected line prefix %s, "\
			                "got: %s" % (prefix, res))
		return res[len(prefix):-2]



	#--------------------------------------------------------------------------
	def _disableEcho(self):
		logger.debug("sending: ATE0")
		if self._dev.write("ATE0\r") !=5:
			raise Exception("sent fewer bytes than expected")
		msg = self._readline()
		if msg != "\r\n" and msg != "ATE0\r\r\n":
			raise Exception("protocol error: expected '\\r\\n' or "\
			                "'ATE0\\r\\r\\n', got %s" % msg)
		self._readOK()



	#--------------------------------------------------------------------------
	def _disableCUR(self):
		self._send("AT^CURC=0")
		self._readOK()



	#--------------------------------------------------------------------------
	def _checkPIN(self, first = True):
		if first:
			logger.notice("checking SIM lock")
		self._send("AT+CPIN?")
		res = self._readResp("+CPIN")
		if res == "READY":
			logger.debug("SIM unlocked")
			return
		elif res == "SIM PIN":
			if not self._modem.pin:
				raise Exception("SIM locked, PIN required but non configured")
			if first:
				logger.debug("trying to unlocking SIM with configured PIN")
				self._send("AT+CPIN=\"%s\"" % self._modem.pin)
				self._readOK()
				self._checkPIN(False)
			else:
				raise Exception("SIM locked, configured PIN invalid")
		elif res == "SIM PUK":
			raise Exception("SIM locked, PUK required")
		else:
			raise Exception("SIM locked, state: %s" % res)



	#--------------------------------------------------------------------------
	def _checkMode(self, first = True):
		if first:
			logger.notice("checking send mode")
		self._send("AT+CMGF?\r")
		res = self._readResp("+CMGF")
		if res == "0":
			logger.debug("send mode is set to '0' (PDU)")
		else:
			if first:
				logger.debug("current send mode: '%s', setting to '0' "\
				             "(PDU)" % res)
				self._send("AT+CMGF=0")
				self._readOK()
				self._checkMode(False)
			else:
				raise Exception("setting send mode to '0' (PDU) failed")



	#--------------------------------------------------------------------------
	def _checkSignal(self):
		self._send("AT+CSQ\r")
		res = self._readResp("+CSQ", self._modem.sendTimeout)

		value = 99
		try:
			value = int(res.split(',')[0])
		except:
			logger.warning("invalid network coverage response: %s" % res)
			return

		if value == 99:
			logger.warning("network coverage not known or not detectable")
			return

		q = ""
		db = -113 + 2 * value
		if value < 2   : q = "bad"
		elif value < 10: q = "marginal"
		elif value < 15: q = "ok"
		elif value < 20: q = "good"
		else           : q = "excellent"
		logger.notice("network coverage: %s (%i dBm)" % (q, db))



	#--------------------------------------------------------------------------
	def _checkCharset(self, first = True):
		if first:
			logger.notice("checking character set")
		cs = self._modem.charset
		self._send("AT+CSCS=?\r")
		res = self._readResp("+CSCS")

		# parse expected response, e.g.: ("IRA","GSM","UCS2"), into array
		charsets = [ i.strip().upper() for i in ast.literal_eval(res) ]
		if self._modem.charset not in charsets:
			raise Exception("charset %s not supported, available sets: %s" % (
			                self._modem.charset, res))



	#--------------------------------------------------------------------------
	# Strips whitespaces from the target address.
	# Returns a map which maps the (possible) modified address to the original
	# subscriber/target pair needed for logging.
	def _createAddressMap(self, addresses):
		addressMap = {}
		for a in addresses:
			address = a[1].replace(" ", "")
			if address == "":
				msg = "ignoring empty number: %s" % address
				logger.warning(msg)
				self.addTargetError(a[0], a[1], msg)
			elif address in addressMap:
				msg = "ignoring duplicated number: %s" % address
				logger.warning(msg)
				self.addTargetError(a[0], a[1], msg)
			else:
				addressMap[address] = a
		return addressMap



	#--------------------------------------------------------------------------
	# Splits the message into several parts if necessary. In case of GSM charset
	# the message is encoded using the GSM7 character tables.
	# Returns array of message parts
	def _splitMessage(self, msg):

		if self._modem.charset == 'GSM':
			msg = gsm0338.encode(msg, self._modem.handleInvalidChar)
			bits = 7
		elif self._modem.charset == 'RAW':
			bits = 8
		elif self._modem.charset == 'UCS2':
			bits = 16
		else:
			return []

		msgLen = len(msg)

		# A SMS is limited to 140 bytes per message. If it needs to be split
		# 6-7 additional bytes per part are required for the user data header
		maxPayload = 140
		if msgLen <= maxPayload * 8 / bits:
			return [ msg ]

		maxParts = self._modem.maxMsg

		# if multipart SMS are allowed, reduce payload bytes by bytes needed
		# for UDH
		if maxParts > 1:
			maxPayload -= 7 if self._modem.mp16bitRefNum else 6

		# characters per msg
		maxChars = maxPayload * 8 / bits

		if msgLen > maxChars * maxParts:
			logger.warning("content exceeds maximum allowed message parts " \
			               "(%i), truncating" % maxParts)
			msgLen = maxChars * maxParts

		# split message
		msgs = [ msg[i:i+maxChars ] for i in range(0, msgLen, maxChars) ]
		if len(msgs) > 1:
			logger.notice("message split into '%d' parts" % len(msgs))

		return msgs



	#--------------------------------------------------------------------------
	# converts number to pdu format
	# Octet       0: length of phone number
	# Octet       1: type of address
	#   bit    7: EXT, always 1, meaning 'no extension'
	#   bit  6-4: TON, 000 = unknown, 001 = international
	#   bit  3-0: NPI, 0001 = telephone number
	# Octet 2-(n+2): number, digets twisted per octed, n=floor(len/2)
	# e.g. +4901234567 becomes 0A9149103254F7
	def _createPDUNumber(self, number):

		# international phone number
		if number.startswith("+"):
			number = number[1:]
			pduNumber = '%02X' % len(str(number))
			pduNumber += '91'
		# unknown phone number
		else:
			pduNumber = '%02X' % len(str(number))
			pduNumber += '81'

		# twist digits per octet, e.g. 01234567 becomes 103254F7
		pduNumber += ''.join(["%s%s" % (c2,c1) for c1,c2 in zip(number[::2], number[1::2])])
		if len(number) % 2 == 1:
			# odd number: add last digit of number with an leading F
			pduNumber += 'F' + number[-1]

		return pduNumber



	#--------------------------------------------------------------------------
	def _createPDUMessage(self, pduNumber, msgParts, iPart=0):

		isMultiPart = len(msgParts) > 1

		# length of SMSC information, 0: use sms center of phone
		pdu = '00'

		# message flags
		#  bit   7: reply path             :  0 = off
		#  bit   6: user data header       :  0 = no UDH present
		#  bit   5: status report request  :  0 = off
		#  bit 4-3: validity period        : 00 = no VP field present
		#  bit   2: reject duplicates      :  0 = false
		#  bit 1-0: message type indication: 01 = SMS-SUBMIT MS to SMSC
		if isMultiPart:
			pdu += '41' # add UDH for multipart messages
		else:
			pdu += '01'

		# mesage reference number, 00 = let the phone create an csms reference
		# number
		pdu += '00'

		# phone number
		pdu += pduNumber

		# protocol identifier
		pdu += '00'

		# data coding scheme
		if self._modem.charset == 'GSM':
			pdu += '00'
		elif self._modem.charset == 'RAW':
			pdu += '04'
		else:
			pdu += '08'

		# validity, format defefined in bit 3-4 of message flag (TP-VPF),
		# 0 byte for TP-VPF=00 (off)
		# 7 byte for TP-VPF=01 (enhanced format)
		# 1 byte for TP-VPF=10 (relative format)
		# 7 byte for TP-VPF=11 (absolute format)
		#
		# definition of relative format,
		# value     period
		# 0-143     TP-VP + 1) x 5 minutes
		# 144-167   (12 + (TP-VP - 143) / 2 ) hours
		# 168-196   (TP-VP - 166) days
		# 197-255   (TP-VP - 192) weeks
		#
		# e.g. 170 = 4days:
		#pdu += "AA"

		# user data header (used only for multipart messages)
		udh = ''
		if isMultiPart:
			# reference number, same for all message parts
			if self._modem.mp16bitRefNum:
				udh  = '06' # UHDL (user data header length)
				udh += '08' # IEI (information element identifier)
				udh += '04' # IEDL (information element data length)
				udh += '%04X' % (self._logID % 0x10000) # reference number
			else:
				udh  = '05' # UHDL (user data header length)
				udh += '00' # IEI (information element identifier)
				udh += '03' # IEDL (information element data length)
				udh += '%02X' % (self._logID % 0x100) # reference number

			udh += '%02X' % len(msgParts) # total number of parts
			udh += '%02X' % (iPart + 1)   # index of this part (1-based)

		# user data header length
		udhl = len(udh) / 2

		msg = msgParts[iPart]
		if self._modem.charset == 'GSM':
			hexMsg = gsm0338.pack7bits(msg, udhl)
			msgLen = math.ceil(udhl * 7 / 8.0) + len(msg) # in septets
		elif self._modem.charset == 'RAW':
			hexMsg = "".join(['%02X' % ord(c) for c in msg])
			msgLen = udhl + len(hexMsg) / 2               # in octets
		elif self._modem.charset == 'UCS2':
			hexMsg = "".join(['%04X' % ord(c) for c in msg])
			msgLen = udhl + len(hexMsg) / 2               # in octets

		# message length
		pdu += '%02X' % msgLen

		# user data
		pdu += udh + hexMsg
		logger.debug("PDU %d/%d: %s" % (iPart+1, len(msgParts), pdu))

		return pdu



	#--------------------------------------------------------------------------
	def _sendMessage(self, msgParts, number, a):

		pduNumber = self._createPDUNumber(number)

		for i in xrange(len(msgParts)):
			# convert message to PDU hex representation
			pduMsg = self._createPDUMessage(pduNumber, msgParts, i)

			# write message length in bytes-1, pduMsg is a hex string
			# representing 1 byte with 2 characters
			msgLen = (len(pduMsg) / 2 ) - 1
			self._send("AT+CMGS=%d" % msgLen)
			logger.debug("writing message: %s" % pduMsg)
			self._dev.write(pduMsg)

			# terminate message with CTRL+Z
			self._dev.write(chr(26))
			self._readRN('> ')

			# wait for response using send timeout
			logger.notice("waiting for message delivery")
			self._dev.timeout = self._modem.sendTimeout
			res = self._readline()
			self._dev.timeout = self._modem.cmdTimeout

			# read return code or error
			res = self._readline()
			err = checkError(res)
			if err == None or err == "":
				prefix = "+CMGS: "
				if not res.startswith(prefix):
					raise Exception("protocol error: expected line prefix %s, "\
					                "got: %s" % (prefix, res))
				logger.info("message send to %s (msg id: %s)" % (
				            number, res[len(prefix):-2]))
				self._partialSuccess = True
				self._readRN()
				self._readOK()
			else:
				self.addTargetError(a[0], a[1], err)



###############################################################################
if __name__ == "__main__":
	app = SpoolSMS()
	app()


# vim: ts=4 noet
