#!/bin/bash
# -*- coding: utf-8 -*-

###############################################################################
# Copyright (C) 2014 by gempa GmbH                                            #
#                                                                             #
# All Rights Reserved.                                                        #
#                                                                             #
# NOTICE: All information contained herein is, and remains                    #
# the property of gempa GmbH and its suppliers, if any. The intellectual      #
# and technical concepts contained herein are proprietary to gempa GmbH       #
# and its suppliers.                                                          #
# Dissemination of this information or reproduction of this material          #
# is strictly forbidden unless prior written permission is obtained           #
# from gempa GmbH.                                                            #
#                                                                             #
# Author: Stephan Herrnkind                                                   #
# Email: herrnkind@gempa.de                                                   #
#                                                                             #
#                                                                             #
# GDS proxy spooler which transfers content and address file to different     #
# host using scp for further processing.                                      #
#                                                                             #
#  code | stdout           | stderr    | description                          #
#  -----+------------------+-----------+-----------------------------------   #
#   0   | empty/ignored    | warnings  | content was sent to all addresses    #
#   1   | failed addresses | warnings  | content was sent to some addresses   #
#   >1  | empty/ignored    | error msg | content was not sent at all          #
#                                                                             #
###############################################################################



usage() {
	echo "usage: $0 [options] <basepath>"
	echo "    -h|--help    : print this help message"
	echo "    -v|--verbose : verbose log output"
	echo "    -H|--host    : SSH connection string of target host"
	echo "    -i|--identity: SSH identity file to use"
	echo "    -d|--dir     : target directory on remote host"
}

fail() {
	2>&1 echo $1
	exit 2
}

debug=0
host="$USER@localhost"
dst_dir="/tmp"
identity=""

while [ $# -gt 1 ]; do
	key="$1"
	case $key in
		-h|--help)
			usage
			exit 0
			;;
		-v|--verbose)
			debug=1
			;;
		-H|--host)
			host="$2"
			shift
			;;
		-i|--identity)
			identity=" -i $2"
			shift
			;;
		-d|--dir)
			dst_dir="$2"
			shift
			;;
		*)
			>&2 echo "unknown option: $key"
			>&2 usage
			exit 2
			;;
	esac
	shift
done

if [ $# -ne 1 ]; then
	>&2 echo "missing base path parameter"
	>&2 usage
	exit 2
fi

base_path="$1"
src_dir="$(dirname $base_path)"
src_address="${base_path}.address"
src_content="${base_path}.content"
filebase=$(basename "$base_path")
dst_address="${dst_dir}/${filebase}.address"
dst_addrtmp="${dst_address}.tmp"
dst_content="${dst_dir}/${filebase}.content"

[ $debug -eq 1 ] && >&2 echo "processing base path: $base_path"

# Test existence of address and content file
[ -f "$src_address" ] || fail "address file not found: $src_address"
[ -f "$src_content" ] || fail "content file not found: $src_content"

# Create target directory on remote side
[ $debug -eq 1 ] && >&2 echo "creating target dir: $host:$dst_dir"
ssh${identity} $host "mkdir -p \"${dst_dir}\"" || fail "could not create remote dir: $host:$dst_dir"

# Copy address file to temporary file name on remote side
[ $debug -eq 1 ] && >&2 echo "copying address file to: $host:$dst_addrtmp"
scp${identity} "$src_address" "$host:\"${dst_addrtmp}\"" || fail "could not copy address file to: $host:$dst_addrtmp"

# Copy content file to remote side
[ $debug -eq 1 ] && >&2 echo "copying content file to: $host:$dst_content"
scp${identity} "$src_content" "$host:\"$dst_content\"" || fail "could not copy content file to: $host:$dst_content"

# Move temporary address file on remote site
[ $debug -eq 1 ] && >&2 echo "moving temporary address file to: $host:$dst_address"
ssh${identity} $host "mv \"${dst_addrtmp}\" \"${dst_address}\"" || fail "could not move temporary address file to: $host:$dst_address"

[ $debug -eq 1 ] && >&2 echo "base path successfully processed: $base_path"
exit 0


# vim: ts=4 noet
