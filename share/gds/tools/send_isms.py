#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
# Copyright (C) 2018 by gempa GmbH                                            #
#                                                                             #
# All Rights Reserved.                                                        #
#                                                                             #
# NOTICE: All information contained herein is, and remains                    #
# the property of gempa GmbH and its suppliers, if any. The intellectual      #
# and technical concepts contained herein are proprietary to gempa GmbH       #
# and its suppliers.                                                          #
# Dissemination of this information or reproduction of this material          #
# is strictly forbidden unless prior written permission is obtained           #
# from gempa GmbH.                                                            #
#                                                                             #
# Author: Stephan Herrnkind                                                   #
# Email: herrnkind@gempa.de                                                   #
#                                                                             #
#                                                                             #
# Sends a gempa bulletin via HTTP GET request to isms API                     #
# http://www.multitech.net/developer/software/isms/isms-send-api/             #
#                                                                             #
###############################################################################

import sys, time, urllib, urllib2

from lib import bulletin, logger, spooler

ISMSCode = {
	0 : "Done",
	1 : "Done with error",
	2 : "In progress",
	3 : "Request Received",
	4 : "Error",
	5 : "Message ID Not Found",
	6 : "Distributed to Slave",
	7 : "Distribution resulted in error",
	8 : "Distributed among many Slaves",
	9 : "API is canceled"
}

ISMSError = {
	601 : "Authentication failed",
	602 : "Parse error",
	603 : "Invalid category",
	604 : "SMS message size is greater than 160 chars",
	605 : "Recipient overflow",
	606 : "Invalid recipient",
	607 : "No recipient",
	608 : "MultiModem iSMS is busy, can’t accept this request",
	609 : "Timeout waiting for a TCP API request",
	610 : "Unknown action trigger",
	611 : "Error in broadcast trigger",
	612 : "System error – memory allocation failure",
	613 : "Invalid modem index",
	614 : "Invalid device model number",
	615 : "Invalid encoding type",
	616 : "Invalid time/date input",
	617 : "Invalid count input",
	618 : "Service not available",
	619 : "Invalid addressee",
	620 : "Invalid priority value",
	621 : "Invalid SMS text"
}

###############################################################################
class ISMSConfig:

	#--------------------------------------------------------------------------
	def __init__(self, config):
		prefix = "isms"

		try:  self.url = config.get(prefix, "url")
		except: self.url = None
		try:  self.user = config.get(prefix, "user")
		except: self.user = "admin"
		try:  self.pw = config.get(prefix, "pw")
		except: self.pw = "admin"
		try:  self.modem = int(config.get(prefix, "modem"))
		except: self.modem = 0
		try:  self.span = int(config.get(prefix, "span"))
		except: self.span = 1


###############################################################################
class SpoolISMS(spooler.Spooler):

	

	#--------------------------------------------------------------------------
	def __init__(self):
		spooler.Spooler.__init__(self)

		self._isms = ISMSConfig(self._config)
		if not self._isms.url:
			logger.error("missing configuration parameter: url")
			sys.exit(2)
		if len(self._isms.user) > 50:
			logger.error("configuration parameter 'user' exceeds limit of 50 characters")
			sys.exit(2)
		if len(self._isms.pw) > 50:
			logger.error("configuration parameter 'pw' exceeds limit of 50 characters")
			sys.exit(2)
		if self._isms.modem < 0 or self._isms.modem > 8:
			logger.error("configuration parameter 'modem' outside range [0-8]")
			sys.exit(2)
		if self._isms.span < 1 or self._isms.span > 8:
			logger.error("configuration parameter 'span' outside range [1-8]")
			sys.exit(2)



	#--------------------------------------------------------------------------
	def spool(self, addresses, content):
		if len(addresses) == 0:
			raise Exception("no phone numbers specified")

		# read bulletin
		try:
			b = bulletin.Bulletin()
			b.read(content)
		except Exception, e:
			raise Exception("could not parse bulletin: %s" % str(e))
		if b.subject != "":
			logger.warning("subject found, ignoring...")
		if b.html != "":
			logger.warning("html content found, ignoring...")
		if len(b.attachments) > 0:
			logger.warning("attachments found, ignoring...")
		if b.plain == "":
			raise Exception("no plain content found")

		# determine required encoding
		text = b.plain
		textLen = len(text)
		enc = 0
		try:
			text = text.decode('ascii')
			# check if extended ascii format is needed
			for c in text:
				if ord(c) < 32 or ord(c) > 126:
					enc = 1
					break
		except UnicodeEncodeError:
			# hexadecimal unicode encoding is required
			enc = 2

		# check maximum message length
		if enc == 0:
			logger.notice("detected standard ASCII character set")
			maxLen = 160 if self._isms.span == 1 else self._isms.span * 154
		elif enc == 1:
			logger.notice("detected extended ASCII character set")
			maxLen = 140 if self._isms.span == 1 else self._isms.span * 134
		else:
			logger.notice("detected unicode character set")
			maxLen = 70 if self._isms.span == 1 else self._isms.span * 64

		if textLen > maxLen:
			logger.warning("the message exeeds %i characters and will be " \
			               "truncated (limit results from span configuration " \
			               "and detected encoding)" % maxLen)
			text = text[:maxLen]
			textLen = maxLen

		# convert chars to unicode value separated by semicolon
		if enc == 2:
			text = ";".join(format(ord(c), 'x') for c in text)

		# group addresses by 'to', 'ton' and 'group' prefix, addresses without
		# prefix are assigned to 'to' group
		to    = [] # phone number, may include country code
		ton   = [] # name of contact in iSMS address book
		group = [] # group of contacts in iSMS address book
		for a in addresses:
			toks = a[1].split('=', 1)
			if len(toks) == 1:
				to.append(toks[0])
			elif toks[0] == 'to':
				to.append(toks[1])
			elif toks[0] == 'ton':
				ton.append(toks[1])
			elif toks[0] == 'group':
				group.append(toks[1])
			else:
				self.addTargetError(a[0], a[1],
				                    "invalid address prefix '%s'" % toks[0])

		params = {}
		params['user']   = self._isms.user
		params['passwd'] = self._isms.pw
		params['cat']    = 1
		params['enc']    = enc
		params['text']   = text
		if len(to) > 0:
			params['to'] = '"%s"' % '","'.join(to)
		if len(ton) > 0:
			params['ton'] = '"%s"' % '","'.join(ton)
		if len(group) > 0:
			params['group'] = '"%s"' % '","'.join(group)

		url = "%s/sendmsg?%s" % (self._isms.url, urllib.urlencode(params))
		request = urllib2.Request(url)

		# start request, throws exception if HTTP code is != 200
		logger.info('sending message to %s' % self._isms.url)
		logger.notice('sending msg of length %i to %i phone numbers, %i ' \
		              'contacts, %i contact groups' % (
		              textLen, len(to), len(ton), len(group)))
		logger.debug('request: %s' % url)

		u = urllib2.urlopen(request, None, self._timeout)

		# read and parse response
		response = u.read()[:1000].strip()
		logger.debug('read sendmsg response: %s' % response)

		msgID = 0
		if response.startswith('ID:'):
			msgID = int(response[3:].strip())
		elif response.startswith('Err:'):
			err = int(response[4:].strip())
			errMsg = ISMSError[err] if err in ISMSError else "Unknown"
			raise Exception("message delivery failed with error code " \
			                "%i (%s)" % (err, errMsg))
		else:
			raise Exception("could not parse sendmsg response: %s" % response)

		# query status of message delivery
		params = {}
		params['user']     = self._isms.user
		params['passwd']   = self._isms.pw
		params['apimsgid'] = msgID

		url = "%s/querymsg?%s" % (self._isms.url, urllib.urlencode(params))
		request = urllib2.Request(url)

		while self.timeout() is None or self.timeout() > 0:
			logger.notice('querying status for ID %i at %s' % (
			              msgID, self._isms.url))
			u = urllib2.urlopen(request, None, self._timeout)

			# read and parse status response
			response = u.read()[:1000].strip()
			logger.debug('read querymsg response: %s' % response)

			expResponse = 'ID: %i Status:' % msgID
			if not response.startswith(expResponse):
				raise Exception("could not parse querymsg response: %s" % response)
			code = int(response[len(expResponse):].strip())
			codeMsg = ISMSCode[code] if code in ISMSCode else "Unknown"
			logger.notice("status of message (#%i) delivery: %i (%s)" % (
			              msgID, code, codeMsg))
			
			if not code in ISMSCode:
				raise Exception("received unknown status code %i for " \
				                "message %i" % (code, msgID))

			if code == 2 or code == 3: # in progress / received
				time.sleep(1)
				continue

			if code == 4: # error
				raise Exception("message delivery failed")
			elif code == 5 or code == 7: # unknown id / distribution failed
				raise Exception("message delivery failed: %s" % codeMsg)

			break



###############################################################################
if __name__ == "__main__":
	app = SpoolISMS()
	app()


# vim: ts=4 noet
