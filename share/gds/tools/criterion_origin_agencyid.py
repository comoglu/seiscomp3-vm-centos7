#!/usr/bin/env python
# -*- coding: utf-8 -*-

# #############################################################################
# Evaluates the agencyID of the preferred origin.
#
# Author: Stephan Herrnkind
# Email: herrnkind@gempa.de
# #############################################################################


import os, sys

from seiscomp3 import Core, DataModel
from lib import logger, xml

###############################################################################
class Criterion:

	#--------------------------------------------------------------------------
	def __call__(self):

		try:
			if len(sys.argv) < 2:
				print >> sys.stderr, "usage: %s <agencyID>" % sys.argv[0]
				return 1

			ep = xml.readEventParameters()
			if not ep:
				return 1

			if ep.eventCount() < 1:
				logger.error('event element not found')
				return 1

			event = ep.event(0)
			logger.info('evaluating criterion for event: %s' % event.publicID())

			# search agency in preferred Origin
			origin = DataModel.Origin.Find(event.preferredOriginID())
			if not origin:
				logger.error("preferred origin not found")
				return 1

			agencyID = ""
			try:
				agencyID = origin.creationInfo().agencyID()
			except:
				logger.info("could not read agencyID from preferred origin")
				return 1

			if agencyID == sys.argv[1]:
				logger.info("agencyID matches '%s'" % agencyID)
				return 0
			else:
				logger.info("agencyID '%s' does not match '%s'" % (
				            agencyID, sys.argv[1]))
				return 1

		except Exception, e:
			error = str(e)
			if len(error) == 0:
				error = e.__class__.__name__
			logger.error(error)
			if logger.noticeEnabled():
				import traceback
				logger.notice(traceback.format_exc())
			return 1

		return 1

	#--------------------------------------------------------------------------
	def containsML(self, origin):
		for i in range(origin.magnitudeCount()):
			mag = origin.magnitude(i)
			if mag.type() in [ 'ML', 'MLv', 'MLh' ]:
				logger.info("found magnitude type: %s" % mag.type())
				return True

		return False
		



if __name__ == "__main__":
	app = Criterion()
	sys.exit(app())


# vim: ts=4 noet
