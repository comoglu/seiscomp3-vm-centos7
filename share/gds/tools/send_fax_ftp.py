#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
# Copyright (C) 2014 by gempa GmbH                                            #
#                                                                             #
# All Rights Reserved.                                                        #
#                                                                             #
# NOTICE: All information contained herein is, and remains                    #
# the property of gempa GmbH and its suppliers, if any. The intellectual      #
# and technical concepts contained herein are proprietary to gempa GmbH       #
# and its suppliers.                                                          #
# Dissemination of this information or reproduction of this material          #
# is strictly forbidden unless prior written permission is obtained           #
# from gempa GmbH.                                                            #
#                                                                             #
# Author: Stephan Herrnkind                                                   #
# Email: herrnkind@gempa.de                                                   #
#                                                                             #
#                                                                             #
# Sends a fax to various recipients by uploading a PDF and a text file with   #
# fax numbers to a FTP directory.                                             #
#                                                                             #
# The name of the text file is part of the name of the PDF, e.g.              #
#   1455188432.714966-37-ncs2016cwoe-fax.txt                                  #
#   @F5001455188432.714966-37-ncs2016cwoe-fax@.pdf                            #
#                                                                             #
# The text file is upload first. For each fax number a line with the following#
# format is added:                                                            #
#                                                                             #
#   @F211 <fax number> @F110 66173903 @F311 2196147 @ @F299@                  #
#                                                                             #
# e.g.                                                                        #
#                                                                             #
#   @F211 +493312882831 @F110 66173903 @F311 2196147 @ @F299@                 #
#                                                                             #
# After processing both files are deleted by the remote fax service.          #
#                                                                             #
###############################################################################

import ftplib, os, re, sets, sys, StringIO

from lib import logger, spooler

###############################################################################
class FTPConfig:

	#--------------------------------------------------------------------------
	def __init__(self, config):
		prefix = "ftp"

		try:  self.host = config.get(prefix, "host")
		except: self.host = None
		try: self.port = config.getint(prefix, "port")
		except: self.port = 21
		try:  self.user = config.get(prefix, "user")
		except: self.user = None
		try:  self.pw = config.get(prefix, "pw")
		except: self.pw = ""
		try:  self.passive = config.getboolean(prefix, "passive")
		except: self.passive = True


###############################################################################
class SpoolFaxFTP(spooler.Spooler):

	#--------------------------------------------------------------------------
	def __init__(self):
		spooler.Spooler.__init__(self)

		self._ftp = FTPConfig(self._config)
		if not self._ftp.host:
			logger.error("missing configuration parameter: host")
			sys.exit(2)



	#--------------------------------------------------------------------------
	def spool(self, addresses, content):
		# create a set with fax numbers, strip all whitespaces
		numbers = sets.Set()
		pattern = re.compile('[\s]+')

		for a in addresses:
			oldLen = len(numbers)
			numbers.add(pattern.sub('', a[1]))
			if len(numbers) == oldLen:
				logger.warning("ignoring duplicated address: %s" % a[1])

		# connect to FTP server
		logger.info("connecting to %s:%i" % (self._ftp.host, self._ftp.port))
		ftp = ftplib.FTP()
		ftp.connect(self._ftp.host, self._ftp.port, self._timeout)

		# login if user is specified
		if self._ftp.user is not None:
			logger.debug("logging in with user: %s" % self._ftp.user)
			ftp.login(self._ftp.user, self._ftp.pw)

		# set transfer mode
		ftp.set_pasv(self._ftp.passive)

		# store address file
		txtName = os.path.basename(self._basePath) + '.txt'
		txt = ""
		for n in numbers:
			txt += "@F211 %s @F110 66173903 @F311 2196147 @ @F299@\n" % n
		logger.debug("address file content:\n%s\n" % txt)
		txtFile = StringIO.StringIO(txt)
		logger.info("storing address file: %s" % txtName)
		ftp.storlines("STOR %s" % txtName, txtFile)
		txtFile.close()

		# store PDF
		pdfName = "@F500%s@.pdf" % txtName
		logger.info("storing content file: %s" % pdfName)
		ftp.storbinary("STOR %s" % pdfName, content)
		content.close()
		ftp.quit()


	#--------------------------------------------------------------------------
	# Overrides base class method. Instead of reading the content just open the
	# file and return the file hanle.
	def _readContent(self):
		fName = self._basePath + ".content"
		if not os.path.isfile(fName):
			raise Exception("content file does not exist: %s" % fName)
		logger.notice("using content file: %s" % fName)
		return open(fName, 'r')



###############################################################################
if __name__ == "__main__":
	app = SpoolFaxFTP()
	app()


# vim: ts=4 noet
