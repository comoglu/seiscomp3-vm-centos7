#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
# Copyright (C) 2014 by gempa GmbH                                            #
#                                                                             #
# All Rights Reserved.                                                        #
#                                                                             #
# NOTICE: All information contained herein is, and remains                    #
# the property of gempa GmbH and its suppliers, if any. The intellectual      #
# and technical concepts contained herein are proprietary to gempa GmbH       #
# and its suppliers.                                                          #
# Dissemination of this information or reproduction of this material          #
# is strictly forbidden unless prior written permission is obtained           #
# from gempa GmbH.                                                            #
#                                                                             #
# Author: Stephan Herrnkind                                                   #
# Email: herrnkind@gempa.de                                                   #
#                                                                             #
#                                                                             #
# Creates an SMS in gempa bulletin format                                     #
#                                                                             #
###############################################################################

import sys, time
import seiscomp3.Core
import seiscomp3.DataModel

from lib import bulletin, filter

class SMSFilter(filter.Filter):

	def filter(self, ep):
		b = bulletin.Bulletin()
		b.plain = "EventID: %(id)s, " \
		          "Reg: %(region)s, " \
		          "Mag: %(magVal)s (%(magType)s), " \
		          "T: %(time)s, " \
		          "Lat: %(lat)s, Lon: %(lon)s, " \
		          "Dep: %(depth)skm" % self.parseEventParameters(ep)
		return str(b)

	def parseEventParameters(self, ep):
		eventDict = {}
		eventDict["id"]     = ""
		eventDict["region"] = ""
		eventDict["magVal"] = ""
		eventDict["magType"] = ""
		eventDict["time"]   = ""
		eventDict["lat"]    = ""
		eventDict["long"]   = ""
		eventDict["depth"]  = ""
		eventDict["phases"] = ""

		if ep.eventCount() < 1:
			return eventDict

		event = ep.event(0)
		eventDict["id"] = event.publicID()

		for j in range(0, event.eventDescriptionCount()):
			ed = event.eventDescription(j)
			if ed.type() == seiscomp3.DataModel.REGION_NAME:
				eventDict["region"] = ed.text()
				break

		magnitude = seiscomp3.DataModel.Magnitude.Find(event.preferredMagnitudeID())
		if magnitude:
			eventDict["magVal"] = "%0.1f" %magnitude.magnitude().value()
			eventDict["magType"] = magnitude.type()

		origin = seiscomp3.DataModel.Origin.Find(event.preferredOriginID())
		if origin:
			eventDict["time"] = origin.time().value().toString("%FT%T")
			eventDict["lat"] = "%.2f" % origin.latitude().value()
			eventDict["lon"] = "%.2f" % origin.longitude().value()
			try: eventDict["depth"] = "%.0f" % origin.depth().value()
			except ValueError: pass
			eventDict["phases"] = origin.arrivalCount()

		return eventDict


if __name__ == "__main__":
	app = SMSFilter()
	sys.exit(app())


# vim: ts=4 noet
