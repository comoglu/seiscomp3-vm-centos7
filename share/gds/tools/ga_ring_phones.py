#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Rings A set of phone numbers.
"""

from twilio.rest import Client
from lib import spooler



ONGOING_STATUSES = ["QUEUED", "RINGING", "IN-PROGRESS"]



class TwilioConfig:
    def __init__(self, config):
        prefix = "twilio"
        self.account_id  = config.get(prefix, "ACCOUNT_ID")
        self.api_token   = config.get(prefix, "API_TOKEN")
        self.from_number = config.get(prefix, "FROM_NUMBER")
        self.message_url = config.get(prefix, "MESSAGE_URL")



class FauxCall(object):
    def __init__(self, number):
        self.to = number



class TwilioSpooler(spooler.Spooler):
    def __init__(self, testing=False):
        spooler.Spooler.__init__(self)
        self.twilio = TwilioConfig(self._config)

    def spool(self, numbers, content, key_name=None):

        def make_call(number, twilio):
            """
            Make a phone call.

            Returns a tuple of length 2. The first element is the number that
            was called and the second element is *None* if the call was made
            successfully or a message stating what went wrong if the call
            failed.
            """

            try:
                call = client.calls.create(
                    to=number,
                    from_=twilio.from_number,
                    url=twilio.message_url,
                    method="GET")
            except Exception as e:
                return FauxCall(number), 'failed to make call: {}'.format(e)

            return call, None


        # keep track of whether we had any failures
        client = Client(self.twilio.account_id, self.twilio.api_token)

        # the numbers to ring
        numbers = set(number.strip() for tup in numbers for number in tup[1].split(','))

        # numbers of calls in progress. This is not perfect, as calls may get
        # made in the iterim, but it is better than not checking at all!
        existing_calls = set(call.to for call in client.api.account.calls.list() if \
            call.status.upper() in ONGOING_STATUSES)

        # keep track of who was already being called
        messages = [(n, 'call already in progresss') for n in numbers & existing_calls]
        if len(messages):
            self._partialSuccess = True

        # the numbers to ring
        numbers -= existing_calls

        if len(numbers):
            # only make a call if the number is not already being called (rememeber
            # that the same account may be being used by multiple stacks and hence
            # call about an earthquake may already be in progress to a given number)
            calls = [make_call(number, self.twilio) for number in numbers]

            # keep track of failed calls
            messages += [call for call in calls if call[1] is not None]

        if len(messages):
            for message in messages:
                self.addTargetError('default', str(message[0].to), message[1])



if __name__ == "__main__":
    app = TwilioSpooler()
    app()
