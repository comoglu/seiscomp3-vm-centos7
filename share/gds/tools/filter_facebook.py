#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
# Copyright (C) 2014 by gempa GmbH                                            #
#                                                                             #
# All Rights Reserved.                                                        #
#                                                                             #
# NOTICE: All information contained herein is, and remains                    #
# the property of gempa GmbH and its suppliers, if any. The intellectual      #
# and technical concepts contained herein are proprietary to gempa GmbH       #
# and its suppliers.                                                          #
# Dissemination of this information or reproduction of this material          #
# is strictly forbidden unless prior written permission is obtained           #
# from gempa GmbH.                                                            #
#                                                                             #
# Author: Stephan Herrnkind                                                   #
# Email: herrnkind@gempa.de                                                   #
#                                                                             #
#                                                                             #
# Creates an SMS in gempa bulletin format                                     #
#                                                                             #
###############################################################################

import sys, time
import seiscomp3.Core
import seiscomp3.DataModel

from lib import bulletin, filter

class FacebookFilter(filter.Filter):

	def filter(self, ep):
		eventDict = self.parseEventParameters(ep)
		b = bulletin.Bulletin()
		b.plain = "Region: %(region)s\n" \
		          "Mag: %(magVal)s\n" \
		          "UTC: %(time)s\n" \
		          "Lat: %(lat)s, Lon:%(lon)s\n" \
		          "Dep: %(depth)skm\n" % eventDict
		b.plain += ep.event(0).publicID()

		#if "lat" in eventDict:
		#	b.lat = eventDict["lat"]
		#	b.lon = eventDict["lon"]
		#else:
		#	b.lat = b.lon = None
		return str(b)

	def parseEventParameters(self, ep):
		eventDict = {}
		eventDict["id"]     = ""
		eventDict["region"] = ""
		eventDict["magVal"] = ""
		eventDict["magType"] = ""
		eventDict["time"]   = ""
		eventDict["lat"]    = ""
		eventDict["long"]   = ""
		eventDict["depth"]  = ""
		eventDict["phases"] = ""

		if ep.eventCount() < 1:
			return eventDict

		event = ep.event(0)
		eventDict["id"] = event.publicID()

		for j in range(0, event.eventDescriptionCount()):
			ed = event.eventDescription(j)
			if ed.type() == seiscomp3.DataModel.REGION_NAME:
				eventDict["region"] = ed.text()
				break

		magnitude = seiscomp3.DataModel.Magnitude.Find(event.preferredMagnitudeID())
		if magnitude:
			eventDict["magVal"] = "%0.1f" %magnitude.magnitude().value()
			eventDict["magType"] = magnitude.type()
		else:
			eventDict["magVal"]="no magnitude available."

		origin = seiscomp3.DataModel.Origin.Find(event.preferredOriginID())
		if origin:
			eventDict["time"] = origin.time().value().toString("%F %T")
			eventDict["lat"] = "%.2f" % origin.latitude().value()
			eventDict["lon"] = "%.2f" % origin.longitude().value()
			try: eventDict["depth"] = "%.0f" % origin.depth().value()
			except ValueError: pass
			eventDict["phases"] = origin.arrivalCount()

		return eventDict


if __name__ == "__main__":
	app = FacebookFilter()
	sys.exit(app())


# vim: ts=4 noet
