#!/usr/bin/env python
# -*- coding: utf-8 -*-

import base64
import mimetypes
import random
import time
from email import encoders
from email.MIMEMultipart import MIMEMultipart
from email.Utils import formatdate
from email.mime.audio import MIMEAudio
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email.mime.text import MIMEText
import boto3 as boto

from lib import bulletin, logger, spooler



class SpoolMail(spooler.Spooler):

    def __init__(self):
        spooler.Spooler.__init__(self)
        self.env = self._config.get("environment", "environment")
        self.aws_region = self._config.get("aws", "region")
        self.from_address = self._config.get("email", "from")


    def spool(self, addresses, content):
        # the list of email addresses we have been requested to send to
        email_addresses = [a[1] for a in addresses]

        # if we don't have any addresses, fail
        if len(email_addresses) == 0:
            raise Exception("no email addresses found")

        client = boto.client('ses', region_name=self.aws_region)

        # get the verfication status of all email addresses we have been
        # requested to send to. Note that if we have never attempted to varify
        # an email, it will not be present in the response.
        ses_addresses = client.get_identity_verification_attributes(
                Identities=email_addresses)

        # the list of verified emails addresses we have been requested to send
        # to
        verified_addresses = [a[0] for a in \
                ses_addresses['VerificationAttributes'].iteritems() \
                if a[1]['VerificationStatus'].lower() == 'success']

        # email addresses which we don't know anything about (i.e. have not
        # attempted to varify)
        unknown_addresses = set(email_addresses) \
                - ses_addresses['VerificationAttributes'].viewkeys()

        # email addresses which have not (yet) been varified (i.e. have had a
        # response to a verification email yet)
        unverified_addresses = ses_addresses['VerificationAttributes'].viewkeys() \
                - set(verified_addresses)

        if not len(verified_addresses):
            raise Exception('no varified email addresses')
        elif len(unknown_addresses) or len(unverified_addresses):
            self._partialSuccess = True

        # add warnings for unknown or unverified addresses email addresses
        for addr in addresses:
            if addr[1] in unknown_addresses:
                self.addTargetError(addr[0], addr[1], 'unknown address')
            if addr[1] in unverified_addresses:
                self.addTargetError(addr[0], addr[1], 'unverified address')

        # read the bulletin
        try:
            b = bulletin.Bulletin()
            b.read(content)
        except Exception, e:
            raise Exception("could not parse bulletin: %s" % str(e))
        if b.subject == "":
            logger.warning("no subject found")
            b.subject = "information"
        if b.plain == "" and b.html == "" and len(b.attachments) == 0:
            raise Exception("no content found")

        # create mail from bulletin
        mail = self._createMail(b)

        # add headers
        mail["From"] = self.from_address
        mail["To"] = self.from_address
        mail["Date"] = str(formatdate(localtime=True))
        mail["Subject"] = ('[TEST] ' if self.env.lower() != 'prod' else '') \
                + b.subject

        # send the email to all verified email addresses. Note that we have not
        # included the recipients in the 'raw' email. Specifying them only in
        # the Destinations parameter only sends them as BCC.
        client.send_raw_email(
            Destinations=verified_addresses,
            RawMessage={'Data': mail.as_string()})


    def _createAttachment(self, a):
        content, mime = None, a.MIME

        # split mime type
        if not mime: raise Exception("MIME type not set")
        main, sub = a.MIME.split('/', 1)

        # read content either from inline or from path
        if a.content:
            if main == 'text':
                content = a.content
            else:
                content = base64.b64decode(a.content)

        elif a.path:
            f = open(a.path, "r")
            content = f.read()
            f.close()
            if not mime: mime, enc = mimetypes.guess_type(path)

        else:
            raise Exception("neither content nor path set")


        if main == 'text':
            msg = MIMEText(content, _subtype=sub)
        elif main == 'image':
            msg = MIMEImage(content, _subtype=sub, name=a.name)
        elif main == 'audio':
            msg = MIMEAudio(content, _subtype=sub, name=a.name)
        else:
            msg = MIMEBase(main, sub, name=a.name)
            msg.set_payload(content)
            encoders.encode_base64(msg)
        return msg



    #--------------------------------------------------------------------------
    # structure rules:
    # - single part if only plain, html, or one attachement is supplied
    # - multipart/mixed for plain and attachements
    # - multipart/alternative for plain and html
    # - multipart/relative if html contains cid references for e.g. inline img
    # - most complex structure example:
    #   multipart/mixed
    #       multipart/alternative
    #           text/plain
    #           multipart/related
    #               text/html
    #               image/gif
    #               image/gif
    #       some/thing (disposition: attachment)
    #       some/thing (disposition: attachment)
    def _createMail(self, bulletin):
        plain     = None
        html      = None
        mail      = None
        multiPart = False

        if bulletin.plain:
            plain = MIMEText(bulletin.plain, 'plain', 'UTF-8')

        if bulletin.html:
            html = MIMEText(bulletin.html, 'html', 'UTF-8')

        # collect attachements
        inlines = []
        attachments = []
        for a in bulletin.attachments:
            if not a.name or a.name == "":
                logger.notice("skipping attachment with empty name")
                continue

            # create attachment
            logger.notice("creating attachment: %s" % a.name)
            try: msg = self._createAttachment(a)
            except Exception, e:
                logger.warning("could not create attachment '%s': %s" % (
                               a.name, str(e)))
                continue

            # use name for cid search if cid is unset
            cid = a.cid if a.cid else a.name

            # inline or normal attachment
            if bulletin.html and cid and "cid:" + cid in bulletin.html:
                msg.add_header('Content-ID', '<%s>' % (cid))
                msg.add_header('Content-Disposition', 'inline')
                inlines.append(msg)
            else:
                msg.add_header('Content-Disposition',
                               'attachment; filename="%s"' % a.name)
                attachments.append(msg)

        # create multi-part/related structure if inline attachments are present
        if inlines:
            logger.notice("creating multi-part/related structure with HTML " \
                          "part and %i inline image(s)" % len(inlines))
            related = MIMEMultipart('related')
            related.attach(html)
            for i in inlines:
                related.attach(i)

            # replace html with related part
            html = related
            multiPart = True

        # create multi-part/alternate structure if text and html are present
        if plain and html:
            logger.notice("creating multi-part/alternative structure")
            mail = MIMEMultipart('alternative')
            mail.attach(plain)
            mail.attach(html)
            multiPart = True
        elif plain:
            mail = plain
        elif html:
            mail = html

        # create multi-part/mixed structure if attachments are present
        if len(attachments) > 1 or ( mail and attachments ):
            logger.notice("creating multi-part/mixed structure with %i " \
                          "attachments" % len(attachments))
            mixed = MIMEMultipart('mixed')
            if mail:
                mixed.attach(mail)
            for a in attachments:
                mixed.attach(a)
            mail = mixed
            multiPart = True
        elif attachments:
            mail = attachments[0]

        if multiPart:
            mail.preamble = 'This is a multi-part message in MIME format.'

        return mail


    def _createMessageID(self):
        r = "%s.%s" % (time.time(), random.randint(0, 100))
        return "<%s@%s>" % (r, 'localhost')



if __name__ == "__main__":
    app = SpoolMail()
    app()
