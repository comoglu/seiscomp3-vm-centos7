#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import json
import seiscomp3.Core
import seiscomp3.DataModel
from lib import filter



class RunWphaseFilter(filter.Filter):
    def filter(self, ep):
        return json.dumps(self.parseEventParameters(ep))


    def parseEventParameters(self, ep):
        """
        Returns the seismic event information stored in a python dict.
        """

        eventDict = {}

        if ep.eventCount() < 1:
            raise Exception('no events in input')

        event = ep.event(0)
        eventDict["id"] = event.publicID()

        for j in range(0, event.eventDescriptionCount()):
            ed = event.eventDescription(j)
            if ed.type() == seiscomp3.DataModel.REGION_NAME:
                eventDict["region"] = ed.text()
                break

        magnitude = seiscomp3.DataModel.Magnitude.Find(
            event.preferredMagnitudeID())
        if not magnitude:
            raise Exception('No preferred magnitude available')
        eventDict["magVal"] = magnitude.magnitude().value()
        eventDict["magType"] = magnitude.type()

        origin = seiscomp3.DataModel.Origin.Find(event.preferredOriginID())
        if not origin:
            raise Exception('No preferred origin available')
        eventDict["time"] = str(origin.time().value())
        eventDict["lat"] = origin.latitude().value()
        eventDict["lon"] = origin.longitude().value()

        try:
            eventDict["depth"] = origin.depth().value()
        except seiscomp3.Core.ValueException:
            eventDict["depth"] = 0.

        return eventDict



if __name__ == "__main__":
    app = RunWphaseFilter()
    sys.exit(app())
