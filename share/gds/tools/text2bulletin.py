#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
# Copyright (C) 2014 by gempa GmbH                                            #
#                                                                             #
# All Rights Reserved.                                                        #
#                                                                             #
# NOTICE: All information contained herein is, and remains                    #
# the property of gempa GmbH and its suppliers, if any. The intellectual      #
# and technical concepts contained herein are proprietary to gempa GmbH       #
# and its suppliers.                                                          #
# Dissemination of this information or reproduction of this material          #
# is strictly forbidden unless prior written permission is obtained           #
# from gempa GmbH.                                                            #
#                                                                             #
# Author: Stephan Herrnkind                                                   #
# Email: herrnkind@gempa.de                                                   #
#                                                                             #
#                                                                             #
# Converts text to gempa bulletin format                                      #
#                                                                             #
###############################################################################

import os, sys
from lib.bulletin import Bulletin, Attachment

def text2Bulletin(argv=None):
	if argv is None:
		argv = sys.argv

	if len(argv) < 2:
		print >> sys.stderr, "usage: %s <TEXT file> " % os.path.basename(argv[0])
		return 1

	# create content and set subject
	b = Bulletin()

	# read TEXT file
	try:
		f = open(argv[1], 'r')
		b.plain = f.read()
		f.close()
	except:
		print >> sys.stderr, "could not read Text file: %s" % argv[1]
		return 2

	print >> sys.stdout, str(b)

if __name__ == "__main__":
	sys.exit(text2Bulletin())


# vim: ts=4 noet
