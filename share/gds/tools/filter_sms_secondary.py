#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
# Copyright (C) 2014 by gempa GmbH                                            #
#                                                                             #
# All Rights Reserved.                                                        #
#                                                                             #
# NOTICE: All information contained herein is, and remains                    #
# the property of gempa GmbH and its suppliers, if any. The intellectual      #
# and technical concepts contained herein are proprietary to gempa GmbH       #
# and its suppliers.                                                          #
# Dissemination of this information or reproduction of this material          #
# is strictly forbidden unless prior written permission is obtained           #
# from gempa GmbH.                                                            #
#                                                                             #
# Author: Stephan Herrnkind                                                   #
# Email: herrnkind@gempa.de                                                   #
#                                                                             #
#                                                                             #
# Creates an SMS in gempa bulletin format                                     #
#                                                                             #
###############################################################################

import sys, time
import seiscomp3.Core
import seiscomp3.DataModel

from lib import bulletin, filter

class SMSFilter(filter.Filter):

	def filter(self, eventDict):
		b = bulletin.Bulletin()
		b.plain = "EventID: %(id)s, " \
		          "Reg: %(region)s, " \
		          "Mag: %(magVal)s (%(magType)s), " \
		          "T: %(time)s, " \
		          "Lat: %(lat)s, Lon: %(lon)s, " \
		          "Dep: %(depth)skm" % eventDict
		return str(b)

	def readData(self):
		return self.readDict()


if __name__ == "__main__":
	app = SMSFilter()
	sys.exit(app())


# vim: ts=4 noet
