#!/usr/bin/env python
# -*- coding: utf-8 -*-

import socket
import json
from lib import spooler
import xml.etree.ElementTree as ET



VALID_ALERT_TYPES = ('automatic', 'manual', 'testman', 'testauto')



class SpoolBomXml(spooler.Spooler):
    def __init__(self):
        spooler.Spooler.__init__(self)
        env = self._config.get("environment", "environment")
        self.ip_and_port = (
            self._config.get(env, "IP"),
            int(self._config.get(env, "PORT")))

    def spool(self, alert_type, content):
        event_id = ET.fromstring(content).attrib['id']
        alert_type = alert_type[0][1]

        if alert_type not in VALID_ALERT_TYPES:
            raise ValueError('Invalid alert type: Recieved {} but must be one of ({})'.format(
                alert_type,
                ', '.join(VALID_ALERT_TYPES)))

        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.connect(self.ip_and_port)
        content = content.format(alert_type = alert_type)
        client.send(content)




if __name__ == "__main__":
    app = SpoolBomXml()
    app()
