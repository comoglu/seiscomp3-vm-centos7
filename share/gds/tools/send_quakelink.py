#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
# Copyright (C) 2014 by gempa GmbH                                            #
#                                                                             #
# All Rights Reserved.                                                        #
#                                                                             #
# NOTICE: All information contained herein is, and remains                    #
# the property of gempa GmbH and its suppliers, if any. The intellectual      #
# and technical concepts contained herein are proprietary to gempa GmbH       #
# and its suppliers.                                                          #
# Dissemination of this information or reproduction of this material          #
# is strictly forbidden unless prior written permission is obtained           #
# from gempa GmbH.                                                            #
#                                                                             #
# Author: Stephan Herrnkind                                                   #
# Email: herrnkind@gempa.de                                                   #
#                                                                             #
#                                                                             #
# Sends and event encoded in SC3ML to QuakeLink server(s)                     #
#                                                                             #
# The hostname and port of each server is read from the address file. By      #
# default the GDS match parameter is evaluated and events are deleted on the  #
# target QuakeLink server if the dissemination criteria are no longer         #
# satisfied. To prevent events from being deleted set the configuration       #
# parameter 'ql.deleteOnMismatch' to false.                                   #
#                                                                             #
###############################################################################

import getopt, os, socket, sys

from lib import logger, spooler

###############################################################################
class QLConfig:

	#--------------------------------------------------------------------------
	def __init__(self, config):
		prefix = "quakelink"

		try:  self.delete = config.getboolean(prefix, "deleteOnMismatch")
		except: self.delete = True


###############################################################################
class SpoolQuakeLink(spooler.Spooler):

	#--------------------------------------------------------------------------
	def __init__(self):
		spooler.Spooler.__init__(self)

		self._ql = QLConfig(self._config)



	#--------------------------------------------------------------------------
	def spool(self, addresses, content):

		if self._timestamp is None:
			raise Exception("could not read timestamp from filename")
		timestamp = self._timestamp.strftime("%FT%T.%f")

		for subscriber, target, service, match in addresses:
			logger.info("processing target: %s" % target)
			comps = target.split(":")
			port = 18011
			if len(comps) == 2:
				try:
					port = int(comps[1])
				except:
					port = None
				if port is None or port < 0 or port > 65535:
					self.addTargetError(subscriber, target,
					                    "target port invalid")
					continue
			elif len(comps) > 2:
				self.addTargetError(subscriber, target,
				                    "target invalid, expected host[:port]")
				continue
			host = comps[0].strip()
			if len(host) == 0:
				self.addTargetError(subscriber, target, "target host invalid")
				continue

			sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			try:
				logger.debug("connecting to QL server %s:%s" % (host, port))
				sock.connect((host, port))
				logger.debug("connection established")
			except socket.error, e:
				self.addTargetError(subscriber, target, "could not connect " \
				                    "QuakeLink Server: %s" % e)
				continue

			try:
				if match == 0:
					self.removeID(sock, timestamp, self._eventID)
				else:
					self.sendData(sock, timestamp, content)
			except Exception, e:
				sock.close()
				self.addTargetError(subscriber, target, str(e))
				continue

			sock.close()
			self._partialSuccess = True


	#--------------------------------------------------------------------------
	# Overrides base class method. Read content as binary blob.
	def _readContent(self):
		fName = self._basePath + ".content"
		if not os.path.isfile(fName):
			raise Exception("content file does not exist: %s" % fName)

		logger.notice("reading content from: %s" % fName)

		try:
			f = open(fName, 'rb')
			content = f.read()
			f.close()
		except Exception, e:
			logger.error("could not read content file '%s': %s" % (
			             fName, str(e)))
			sys.exit(2)

		return content


	#--------------------------------------------------------------------------
	# 
	# the file name.
	def sendData(self, sock, timestamp, data):

		logger.notice("sending new event revision with timestamp %s" %
		              timestamp)
		sock.send("Content-Type: text/xml\r\n")
		sock.send("Content-Timestamp: %s\r\n" % timestamp)
		sock.send("Content-Length: %d\r\n" % len(data))
		sock.send("\r\n")
		sock.send(data)
		sock.send("\r\n")

		resp = sock.recv(1024)
		respLine = resp.replace("\r","").replace("\n",", ")
		if resp.strip()[:3] == "200":
			eid = None
			rev = None
			for line in resp.splitlines():
				lineUpper = line.upper()
				if lineUpper[:9] == "EVENT-ID:":
					eid = line[9:].strip()
				if lineUpper[:15] == "EVENT-REVISION:":
					try:
						rev = int(line[15:])
					except ValueError, e:
						logger.warning("Received invalid revision")
			if eid is not None and rev is not None:
				logger.notice("server acknowledged event revision: %s %i" % (
				              eid, rev))
		else:
			raise Exception("server response: %s" % respLine)

		logger.debug("server response: %s" % respLine)


	#--------------------------------------------------------------------------
	# Removes event from QuakeLink server
	def removeID(self, sock, timestamp, eventID):
		logger.notice("removing event %s with timestamp %s" % (
		              eventID, timestamp))
		sock.send("Content-Type: text/plain\r\n")
		sock.send("Content-Timestamp: %s\r\n" % timestamp)
		sock.send("Content-Operation: dispose\r\n")
		sock.send("Content-Length: %d\r\n" % len(eventID))
		sock.send("\r\n")
		sock.send(eventID)
		sock.send("\r\n")

		resp = sock.recv(1024)
		respLine = resp.replace("\r","").replace("\n",", ")
		if resp.strip()[:3] == "200":
			logger.notice("server acknowledged event removal")
		else:
			raise Exception("server response: %s" % respLine)

		logger.debug("server response: %s" % respLine)




###############################################################################
if __name__ == "__main__":
	app = SpoolQuakeLink()
	app()


# vim: ts=4 noet
