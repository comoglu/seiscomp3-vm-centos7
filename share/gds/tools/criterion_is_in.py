#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
# Copyright (C) 2014 by gempa GmbH                                            #
#                                                                             #
# All Rights Reserved.                                                        #
#                                                                             #
# NOTICE: All information contained herein is, and remains                    #
# the property of gempa GmbH and its suppliers, if any. The intellectual      #
# and technical concepts contained herein are proprietary to gempa GmbH       #
# and its suppliers.                                                          #
# Dissemination of this information or reproduction of this material          #
# is strictly forbidden unless prior written permission is obtained           #
# from gempa GmbH.                                                            #
#                                                                             #
# Author: Stephan Herrnkind                                                   #
# Email: herrnkind@gempa.de                                                   #
#                                                                             #
#                                                                             #
# Checks if the event is inside an area specifed in a BNA file                #
#                                                                             #
# NOTE: This script is obsolete since starting with version 2017.079 the GDS  #
# supports evaluation of polygon regions.                                     #
#                                                                             #
# #############################################################################


import os, sys
import seiscomp3.Core
import seiscomp3.DataModel
import seiscomp3.Geo
import seiscomp3.Math

from lib import logger, xml

###############################################################################
class Criterion:

	#--------------------------------------------------------------------------
	def __call__(self):
		try:
			if len(sys.argv) != 2:
				logger.error("no BNA file specified")
				return 2

			logger.info("evaluating criterion")

			# parse latitude, longitude
			ep = xml.readEventParameters()
			if not ep:
				return 3

			if ep.eventCount() < 1:
				logger.error("event element not found")
				return 4

			event = ep.event(0)
			origin = seiscomp3.DataModel.Origin.Find(event.preferredOriginID())
			if not origin:
				logger.error("preferred origin not found")
				return 5

			coord = seiscomp3.Math.CoordF(origin.latitude().value(),
			                              origin.longitude().value())
			logger.debug("using event location at %f, %f" % (
			             coord.latitude(), coord.longitude()))

			# parse BNA file
			bnaFile = sys.argv[1]
			logger.notice("parsing BNA file: %s" % bnaFile)

			gfs = seiscomp3.Geo.GeoFeatureSet()
			cat = seiscomp3.Geo.Category(0)

			if not gfs.readBNAFile(sys.argv[1], cat):
				logger.error("could not read BNA file: %s" % bnaFile)
				return 6

			logger.debug("read %d features" % len(gfs.features()))

			for f in gfs.features():
				isIn = f.contains(coord)
				logger.debug("feature '%s': %s" % (f.name(), str(isIn)))
				if isIn:
					logger.info("event at %f, %f is inside feature: %s" % (
					            coord.latitude(), coord.longitude(), f.name()))
					return 0
			logger.info("event at %f, %f not inside any feature" % (
			            coord.latitude(), coord.longitude()))
			return 1

		except Exception, e:
			error = str(e)
			if len(error) == 0:
				error = e.__class__.__name__
			logger.error(error)
			if logger.noticeEnabled():
				logger.trace(traceback.format_exc())
			return 1

		return 0



if __name__ == "__main__":
	app = Criterion()
	sys.exit(app())


# vim: ts=4 noet
