#!/usr/bin/env python
# -*- coding: utf-8 -*-

#TODO: The licence information needs to be updated here.

import os
import sys
import time
import json
import calendar
import dateutil.parser
import datetime
import traceback
import seiscomp3.Core
import seiscomp3.DataModel
from lib import bulletin, filter



class NSWWaterFilter(filter.Filter):
    def filter(self, ep):
        return json.dumps(self.parseEventParameters(ep))

    def parseEventParameters(self, ep):
        """
        Returns the seismic event information from Quake Link (in Seiscomp ML)
        stored in a python dict.

        :param ep: The seismic event information object from Quake Link. See
        documentation for the filter.Filter.filter method for more information.

        :returns: Python dict with the seismic event information.
        """

        eventDict = {}

        if ep.eventCount() < 1:
            raise Exception('no events in input')

        event = ep.event(0)
        eventDict["EventId"] = event.publicID()

        for j in range(0, event.eventDescriptionCount()):
            ed = event.eventDescription(j)
            if ed.type() == seiscomp3.DataModel.REGION_NAME:
                eventDict["ApproxLocation"] = ed.text()
                break

        magnitude = seiscomp3.DataModel.Magnitude.Find(
            event.preferredMagnitudeID())
        if not magnitude:
            raise Exception('No preferred magnitude available')
        eventDict["Magnitude"] = magnitude.magnitude().value()
        eventDict["MagnitudeType"] = magnitude.type()

        origin = seiscomp3.DataModel.Origin.Find(event.preferredOriginID())
        if not origin:
            raise Exception('No preferred origin available')
        etime = origin.time().value()
        eventDict['UTCDate'] = etime.toString(fmt='%Y-%m-%d')
        eventDict['UTCTime'] = etime.toString(fmt='%H:%M:%S')
        eventDict["Latitude"] = origin.latitude().value()
        eventDict["Longitude"] = origin.longitude().value()
        try:
            eventDict["Depth"] = origin.depth().value()
        except seiscomp3.Core.ValueException:
            eventDict["Depth"] = 0.

        return eventDict



if __name__ == "__main__":
    app = NSWWaterFilter()
    sys.exit(app())
