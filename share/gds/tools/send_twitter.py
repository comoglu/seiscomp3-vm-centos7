#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
# Copyright (C) 2014 by gempa GmbH                                            #
#                                                                             #
# All Rights Reserved.                                                        #
#                                                                             #
# NOTICE: All information contained herein is, and remains                    #
# the property of gempa GmbH and its suppliers, if any. The intellectual      #
# and technical concepts contained herein are proprietary to gempa GmbH       #
# and its suppliers.                                                          #
# Dissemination of this information or reproduction of this material          #
# is strictly forbidden unless prior written permission is obtained           #
# from gempa GmbH.                                                            #
#                                                                             #
# Author: Frank Tributh                                                       #
# Email: ftributh@gempa.de                                                    #
#                                                                             #
#                                                                             #
# Sends a gempa bulletin via Twitter                                          #
#                                                                             #
###############################################################################


from lib import bulletin, logger, spooler

from twython import Twython


class TwitterConfig:
	def __init__(self, config):
		prefix = "twitter"

		self.appKey = config.get(prefix, "APP_KEY")
		self.appSecret = config.get(prefix, "APP_SECRET")
		self.accessToken = config.get(prefix, "ACCESS_TOKEN")
		self.accessSecret = config.get(prefix, "ACCESS_SECRET")


###############################################################################
class SpoolTwitter(spooler.Spooler):

	#--------------------------------------------------------------------------
	def __init__(self):
		self._dev = None
		spooler.Spooler.__init__(self)

		# read config
		self._twconf = TwitterConfig(self._config)

		self._twitter = Twython(self._twconf.appKey, self._twconf.appSecret, self._twconf.accessToken, self._twconf.accessSecret)



	#--------------------------------------------------------------------------
	def __del__(self):
		if self._dev:
			self._dev.close()



	#--------------------------------------------------------------------------
	def spool(self, addresses, content):
		# read bulletin
		logger.debug("parsing bulletin")
		try:
			b = bulletin.Bulletin()
			b.read(content)
		except Exception, e:
			raise Exception("could no parse bulletin: %s content: %s" % (str(e),content))

		msg = b.plain

		if len(msg) > 140:
			msg = msg[:140]
			logger.warning("content exceeds maximum length (%i), truncating" % (140))
		elif len(msg) == 0:
			raise Exception("no content found")

		try:
			self._twitter.update_status(status=msg)#,lat=52.11,long=13.21,display_coordinates=True)
		except Exception, e:
			a = addresses[0]
			self.addTargetError(a[0], a[1], str(e))



###############################################################################
if __name__ == "__main__":
	app = SpoolTwitter()
	app()


# vim: ts=4 noet
