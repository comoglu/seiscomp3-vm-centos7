#!/usr/bin/env python
# -*- coding: utf-8 -*-

# #############################################################################
# Creates an email in gempa bulletin format. Provided content:
#  - subject
#  - text: simple overview or scbulletin format
#  - html: simple HTML table making use of CID image reference
#  - att: epicenter image fetch from gempa Mapserver or created by scmapcut
#
# Author: Stephan Herrnkind
# Email: herrnkind@gempa.de
# #############################################################################

import os, sys
import seiscomp3.Core
import seiscomp3.DataModel

from lib import bulletin, filter, logger, xml

KML_TEMPLATE = """<kml xmlns="http://earth.google.com/kml/2.0">
  <Placemark>
     <name>Magnitude {mag:.1f}, {location}</name>
        <description><![CDATA[<table><tr><td>date and time</td><td>{datetime}</td></tr><tr><td>location:</td><td>{location}</td></td><tr><td>depth:</td><td>{depth:.0f}km</td><tr></table><a href="https://earthquakes.ga.gov.au/event/{eventId}">view on Earthquakes@GA</a>]]></description>
           <Point>
              <coordinates> {lon}, {lat} </coordinates>
           </Point>
  </Placemark>
</kml>"""
###############################################################################
class EmailFilter(filter.Filter):

    #--------------------------------------------------------------------------
    # main filter method, compose method calls to fit your needs
    def filter(self, ep):
        eventDict = self.parseEventParameters(ep)
        b = bulletin.Bulletin()

        # create email subject
        self.createSubject(b, eventDict)

        # create text content
        self.createTextSCBulletin(b, ep, "autoloc3")

        sc3Str = xml.objectToString(ep)

        # attach image of epicenter location using gempa ImageServer
        self.attachEpicenterImageServer(b, eventDict, sc3Str)

        # attach image of traces of stations closest to the event
        self.attachTracesImageServer(b, eventDict, sc3Str)

        # attach a KML file for Andy
        self.attachKML(b, eventDict)

        # create HTML content
        return str(b)


    #--------------------------------------------------------------------------
    def attachKML(self, b, eventDict):
        import traceback
        try:
            kml = KML_TEMPLATE.format(
                eventId=eventDict['id'],
                mag=eventDict['magVal'],
                lat=eventDict['lat'],
                lon=eventDict['lon'],
                depth=eventDict['depth'],
                datetime=eventDict['time'],
                location=eventDict['region'])

            att = bulletin.Attachment()
            att.name    = 'epicenter.kml'
            att.content = kml
            att.createCID("eatws")
            att.MIME    = 'text/xml'
            b.attachments.append(att)
        except Exception, e:
            logger.warning("could not create attachment from URL: %s" % str(e))
            logger.debug(str(traceback.format_exc()))


    #--------------------------------------------------------------------------
    # creates email subject
    def createSubject(self, b, eventDict):
        b.subject = "Event %(id)s" % eventDict
        if "magVal" in eventDict:
            b.subject += ": M%(magVal).1f %(region)s" % eventDict
        if "mode" in eventDict:
            b.subject = "{} - {}".format(eventDict["mode"], b.subject)


    #--------------------------------------------------------------------------
    # creates an image attachment showing epicenter location using
    # gempa ImageServer
    def attachEpicenterImageServer(self, b, eventDict, postData=None):
        import traceback
        try:
            lat = eventDict['lat']
            lon = eventDict['lon']
            dist = eventDict['maxDist']
            url = "http://localhost:20001/map?reg=%f,%f,%f,%f" % (lat, lon, dist, dist)
            url += "&stations=*&dim=512,512&fmt=jpg&qua=85"
            if postData is None:
                # XML available: request origin explicitly
                url += "&ori=%f,%f" % (lat, lon)
                if "depth" in eventDict:
                    url += ",%f" % eventDict["depth"]
                if "magVal" in eventDict:
                    url += "&mag=%f" % eventDict["magVal"]

            logger.notice("fetching epicenter location from url: %s" % url)

            att = bulletin.Attachment()
            att.name = "epicenter.jpg"
            att.loadFromURL(url, postData, timeout=self.timeout())
            att.createCID("gempa.de")
            b.attachments.append(att)
        except Exception, e:
            logger.warning("could not create attachment from URL: %s" % str(e))
            logger.debug(str(traceback.format_exc()))


    #--------------------------------------------------------------------------
    # creates an image attachment showing traces of stations nearest to the
    # epicenter using gempa ImageServer
    def attachTracesImageServer(self, b, eventDict, postData):
        try:
            if postData is not None and "firstArrival" in eventDict:
                firstArrival = eventDict["firstArrival"]
                firstArrival -= seiscomp3.Core.TimeSpan(60.0)

                url = "http://localhost:20001/traces?dim=1024,1024&num=7"
                url += "&fmt=png&buffersize=300&orderby=distance"
                url += "&removeoffset=1&weight=1.0&filter=BW(3,1.0,10)"
                url += "&showtt=1&showpicks=1&start=%s" % firstArrival.iso()

                logger.notice("fetching traces from url: %s" % url)

                att = bulletin.Attachment()
                att.name = "traces.png"
                att.loadFromURL(url, postData, timeout=self.timeout())
                att.createCID("gempa.de")
                b.attachments.append(att)
        except Exception, e:
            logger.warning("could not create attachment from URL: %s" % str(e))


    #--------------------------------------------------------------------------
    # creates text using scbulletin
    def createTextSCBulletin(self, b, ep, fmt):
        # Seattle
        try:
            from lib.gascbulletin import Bulletin
        except ImportError:
            raise Exception("could not import scbulletin module")

        if fmt != "autoloc3" and fmt != "autoloc3extra":
            fmt = "autoloc1"

        scb = Bulletin(None)
        scb.format = fmt
        if ep.eventCount() < 1:
            raise Exception("scbulletin: event element not found")
        b.plain = scb.printEvent(ep.event(0))


    #--------------------------------------------------------------------------
    # parses important SC3ML parameters
    def parseEventParameters(self, ep):
        eventDict = {}
        if ep.eventCount() < 1:
            raise Exception("event element not found")

        event = ep.event(0)
        eventDict["id"] = event.publicID()

        eventDict["region"]  = ""
        for i in xrange(event.eventDescriptionCount()):
            ed = event.eventDescription(i)
            if ed.type() == seiscomp3.DataModel.REGION_NAME:
                eventDict["region"] = ed.text()
                break

        magnitude = seiscomp3.DataModel.Magnitude.Find(event.preferredMagnitudeID())
        if magnitude:
            eventDict["magVal"] = magnitude.magnitude().value()
            eventDict["magType"] = magnitude.type()
        else:
            logger.warning("preferred magnitude not found")

        origin = seiscomp3.DataModel.Origin.Find(event.preferredOriginID())
        if not origin:
            raise Exception("preferred origin not found")

        eventDict["time"] = origin.time().value().iso()
        eventDict["lat"] = origin.latitude().value()
        eventDict["lon"] = origin.longitude().value()

        try: eventDict["depth"] = origin.depth().value()
        except ValueError: pass

        try: eventDict["mode"] = seiscomp3.DataModel.EEvaluationModeNames.name(origin.evaluationMode())
        except: pass

        eventDict["phases"] = origin.arrivalCount()
        try:
            maxDist = origin.quality().maximumDistance()
            if maxDist > 20: maxDist = 20
            elif maxDist < 2: maxDist = 2
        except:
            maxDist = 10

        eventDict["maxDist"] = maxDist


        firstArrival = seiscomp3.Core.Time()
        for i in xrange(origin.arrivalCount()):
            arrival = origin.arrival(i)
            pick = seiscomp3.DataModel.Pick.Find(arrival.pickID())
            if not pick:
                continue
            if not firstArrival.valid() or pick.time().value() < firstArrival:
                firstArrival = pick.time().value()
        if firstArrival.valid():
            eventDict["firstArrival"] = firstArrival

        return eventDict


if __name__ == "__main__":
    app = EmailFilter()
    sys.exit(app())
