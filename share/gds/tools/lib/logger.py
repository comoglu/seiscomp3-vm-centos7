# -*- coding: utf-8 -*-

###############################################################################
# Copyright (C) 2014 by gempa GmbH                                            #
#                                                                             #
# All Rights Reserved.                                                        #
#                                                                             #
# NOTICE: All information contained herein is, and remains                    #
# the property of gempa GmbH and its suppliers, if any. The intellectual      #
# and technical concepts contained herein are proprietary to gempa GmbH       #
# and its suppliers.                                                          #
# Dissemination of this information or reproduction of this material          #
# is strictly forbidden unless prior written permission is obtained           #
# from gempa GmbH.                                                            #
#                                                                             #
# Author: Stephan Herrnkind                                                   #
# Email: herrnkind@gempa.de                                                   #
#                                                                             #
#                                                                             #
# Logging facility supporting stderr and file (including file rotation)       #
#                                                                             #
###############################################################################

import os, sys, time, codecs
import ConfigParser

LOG_NONE    = -1
LOG_ERROR   =  0
LOG_WARNING =  1
LOG_INFO    =  2
LOG_NOTICE  =  3
LOG_DEBUG   =  4


###############################################################################
class Rotator:

	#--------------------------------------------------------------------------
	def __init__(self, logFile, timeSpan = 60*60*24, historySize = 7):
		self._timeSpan = timeSpan
		self._historySize = historySize
		self._logFile = logFile
		self._filename = logFile



	#--------------------------------------------------------------------------
	def __call__(self):
		self._currentTime = time.time()
		try: creationTime = os.path.getmtime(self._logFile)
		except: creationTime = time.time()
		if ( int(self._currentTime - creationTime) > self._timeSpan):
			self.rotateLogs()



	#--------------------------------------------------------------------------
	def removeLog(self, index):
		os.remove("%s.%d" %(self._filename, index))



	#--------------------------------------------------------------------------
	def renameLog(self, oldIndex, newIndex):
		oldFile = self._filename
		if ( oldIndex > 0 ):
			oldFile = "%s.%d" % (oldFile, oldIndex)
		if not os.path.exists(oldFile):
			return

		newFile = self._filename
		if ( newFile > 0 ):
			newFile = "%s.%d" % (newFile, newIndex)

		os.rename(oldFile, newFile)



	#--------------------------------------------------------------------------
	def rotateLogs(self):
		# Rotate the log files
		try: self.removeLog(self._historySize)
		except: pass

		i = self._historySize - 1
		while (i >= 0):
			self.renameLog(i, i+1)
			i -= 1


###############################################################################
class Logger:

	#--------------------------------------------------------------------------
	def __init__(self, path):
		self._stderrLevel = LOG_WARNING
		self._fileLevel   = LOG_NONE
		self._file = None
		self._rotator = None

		workDir, app = os.path.split(os.path.abspath(path))
		name, ext = os.path.splitext(app)
		cfgFile = os.path.join(workDir, name + ".cfg")
		if not os.path.isfile(cfgFile):
			return

		config = ConfigParser.SafeConfigParser()
		config.read(cfgFile)

		# stderrLevel
		try: self._stderrLevel = config.getint('logging', 'stderrLevel')
		except: pass

		# fileLevel
		try: self._fileLevel = config.getint('logging', 'fileLevel')
		except: pass

		# log to file?
		if self._fileLevel >= LOG_ERROR:
			logDir = self._logDir()
			if logDir:
				self._file = os.path.join(logDir, name + ".log")

		# no file no rotation
		if not self._file:
			return

		# rotate?
		try: rotate = config.getboolean('logging', 'rotator')
		except: rotate = True

		if rotate:
			# timespan in seconds for the log time before rotating
			try: time = config.getint('logging', 'timespan')
			except: time = 24*60*60

			# number of files to keep
			try: size = config.getint('logging', 'archivesize')
			except: size = 7

			self._rotator = Rotator(self._file, time, size)




	#--------------------------------------------------------------------------
	def enabled(self, level):
		return self._stderrLevel >= level or self._fileLevel >= level



	#--------------------------------------------------------------------------
	def _log(self, level, level_str, msg):
		if self._stderrLevel >= level:
			text = "[%s] %s" % (level_str, msg)
			print >> sys.stderr, text.encode("utf-8")
		if self._fileLevel >= level:
			t = time.strftime("%Y/%m/%d %H:%M:%S", time.localtime())
			text = "%s [%s] %s" % (t, level_str, msg)
			self._writeLog(text)



	#--------------------------------------------------------------------------
	def _logDir(self):
		homeDir = os.environ["HOME"]
		logDir = os.path.join(homeDir, ".seiscomp3/log")

		if not os.path.isdir(logDir):
			try: os.makedirs(logDir)
			except Exception, e:
				self.error("Could not create log directory: %s" % logDir)
				return None
		return logDir



	#--------------------------------------------------------------------------
	def _writeLog(self, text):
		if self._rotator:
			self._rotator()

		try: cTime = os.path.getmtime(self._logfile)
		except: cTime = time.time()

		mTime = time.time()
		logFile = codecs.open(self._file, 'a', 'utf-8')
		logFile.write(text + "\n")
		logFile.close()

		# Access time is modification time and modification time is creation
		# time it's a hack because POSIX don't know the creation time
		os.utime(self._file,(mTime, cTime))



###############################################################################
_Logger = Logger(sys.argv[0])

def errorEnabled():
	return _Logger.enabled(LOG_ERROR)
def error(text):
	if errorEnabled(): _Logger._log(LOG_ERROR, "error", text)

def warningEnabled():
	return _Logger.enabled(LOG_WARNING)
def warning(text):
	if warningEnabled(): _Logger._log(LOG_WARNING, "warning", text)

def infoEnabled():
	return _Logger.enabled(LOG_INFO)
def info(text):
	if infoEnabled(): _Logger._log(LOG_INFO, "info", text)

def noticeEnabled():
	return _Logger.enabled(LOG_NOTICE)
def notice(text):
	if noticeEnabled(): _Logger._log(LOG_NOTICE, "notice", text)

def debugEnabled():
	return _Logger.enabled(LOG_DEBUG)
def debug(text):
	if debugEnabled(): _Logger._log(LOG_DEBUG, "debug", text)


# vim: ts=4 noet
