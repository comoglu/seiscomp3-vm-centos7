# -*- coding: utf-8 -*-

###############################################################################
# Copyright (C) 2014 by gempa GmbH                                            #
#                                                                             #
# All Rights Reserved.                                                        #
#                                                                             #
# NOTICE: All information contained herein is, and remains                    #
# the property of gempa GmbH and its suppliers, if any. The intellectual      #
# and technical concepts contained herein are proprietary to gempa GmbH       #
# and its suppliers.                                                          #
# Dissemination of this information or reproduction of this material          #
# is strictly forbidden unless prior written permission is obtained           #
# from gempa GmbH.                                                            #
#                                                                             #
# Author: Stephan Herrnkind                                                   #
# Email: herrnkind@gempa.de                                                   #
#                                                                             #
#                                                                             #
# Abstract base class for GDS spooler commands                                #
#                                                                             #
# A spooler sends content to a list of addresses. The content and addresses   #
# are read from the files '<path>.content' resp. '<path>.address' where       #
# 'path' is the base path to both files which is submitted as the first       #
# command line argument.                                                      #
#                                                                             #
# The format of the content file is application specific but will be in many  #
# cases a gempa bulletin ('application/gds', see bulletin.py).                #
#                                                                             #
# The address file contains lines of the form                                 #
# 'subscriber#target#service#match' where subscriber is a name and target the #
# destination information (e.g. phone number, email address). The match       #
# indicates whether the criteria were fulfilled (=1) or not (=0).             #
#                                                                             #
# The spooler must follow a strict response protocol to allow the GDS to      #
# create proper subscription specific log entries:                            #
#                                                                             #
#  code | stdout           | stderr    | description                          #
#  -----+------------------+-----------+-----------------------------------   #
#   0   | empty/ignored    | warnings  | content was sent to all addresses    #
#   1   | failed addresses | warnings  | content was sent to some addresses   #
#   >1  | empty/ignored    | error msg | content was not sent at all          #
#                                                                             #
# If the content could only be sent to some addresses (exit code 1) the       #
# spooler must report the failed addresses in lines of the form               #
# 'subscriber#target#error message' where subscriber and target must match    #
# the corresponding tuple of the address file.                                #
#                                                                             #
# A derived class must implement the spool(self, addresses, content) method.  #
#                                                                             #
###############################################################################

import codecs, getopt, os, sys, time, traceback
from datetime import datetime

import bulletin, logger
import ConfigParser

###############################################################################
class Spooler():

	#--------------------------------------------------------------------------
	def __init__(self):
		logger.notice("initializing")

		# read timeout parameter
		opts, args = getopt.getopt(sys.argv[1:], 't:', ['timeout='])
		for o, a in opts:
			if o in ("-t", "--timeout"):
				try:
					self._timeout = float(a)
					logger.notice("using timeout of %f" % self._timeout)
				except ValueError:
					logger.error("invalid timeout value")
					sys.exit(2)

		# read base path to address/content files
		if len(args) == 0:
			logger.error("to few arguments, missing base path of "
			             "content/address file")
			sys.exit(2)

		self._basePath = args[0]

		# extract filename name components
		self._timestamp = None
		self._logID     = None
		self._eventID   = None
		self._service   = None
		self._filter    = None
		comps = os.path.basename(self._basePath).split("-")
		if len(comps) > 0:
			try:
				self._timestamp = datetime.fromtimestamp(float(comps[0]))
			except: pass
		if len(comps) > 1:
			try:
				self._logID = int(comps[1])
			except: pass
		if len(comps) > 2:
			self._eventID = comps[2]
		if len(comps) > 3:
			self._service = comps[3]
		if len(comps) > 4:
			self._filter = comps[4]

		if logger.debugEnabled():
			logger.debug("T: %s, logID: %s, eID: %s, serv: %s, fil: %s" % (
			             "-" if self._timestamp is None \
			                 else self._timestamp.strftime("%FT%T.%f"),
			             "-" if self._logID is None else str(self._logID),
			             "-" if self._eventID is None else self._eventID,
			             "-" if self._service is None else self._service,
			             "-" if self._filter is None else self._filter))

		# read config file
		self._config = None
		app, ext = os.path.splitext(os.path.abspath(sys.argv[0]))
		configFile = app + ".cfg"
		if not os.path.isfile(configFile):
			logger.notice("configuration file not found: %s" % configFile)
		else:
			logger.notice("reading configuration from: %s" % configFile)
			self._config = ConfigParser.SafeConfigParser()
			self._config.read(configFile)

		self._targetErrors = ""
		self._partialSuccess = False



	#--------------------------------------------------------------------------
	def __call__(self):
		self._startTime = None
		self._timeout = None
		try:
			self._startTime = time.time()
			logger.info("start spooling")

			addresses = self._readAddresses()
			content = self._readContent()
			self.spool(addresses, content)
			if len(self._targetErrors) == 0:
				sys.exit(0)
			else:
				print >> sys.stdout, self._targetErrors
		except Exception, e:
			error = str(e)
			if len(error) == 0:
				error = e.__class__.__name__
			logger.error(error)
			if logger.noticeEnabled():
				logger.notice(traceback.format_exc())

		sys.exit(1 if self._partialSuccess else 2)



	#--------------------------------------------------------------------------
	# implement this method in derived classes
	def spool(self, addresses, content):
		raise Exception("method 'spool' not implemented in derived class")



	#--------------------------------------------------------------------------
	# returns elapsed time since application start
	def elapsedTime(self):
		return time.time() - self._startTime



	#--------------------------------------------------------------------------
	# returns remaining time or None if no timeout parameter was passed
	def timeout(self):
		if self._timeout is None:
			return None

		remaining = self._timeout - self.elapsedTime()
		if remaining < 0:
			return 0
		return remaining



	#--------------------------------------------------------------------------
	# added target specific errors which will lead to an exit code of 1
	def addTargetError(self, subscriber, target, error):
		if error and len(error) > 0:
			err = error.replace("\r","")
			err = error.replace("\n", "")
		else:
			err = ""

		targetError = "%s#%s#%s" % (subscriber, target, err)
		self._targetErrors += "%s\n" % targetError
		logger.warning("added target error: %s" % targetError)



	#--------------------------------------------------------------------------
	def _readAddresses(self):
		fName = self._basePath + ".address"
		if not os.path.isfile(fName):
			raise Exception("address file does not exist: %s" % fName)
		logger.notice("reading addresses from: %s" % fName)

		addresses = []
		try:
			f = open(fName, "r")
			for line in f.readlines():
				if len(line) == 0: continue
				toks = line.split("#", 3)
				if len(toks) != 4:
					logger.error("invalid address file format: %s" % fName)
					sys.exit(2)
				toks[3] = int(toks[3])
				addresses.append((toks[0], toks[1], toks[2], toks[3]))
			f.close()
		except Exception, e:
			logger.error("could not read address file '%s': %s" % (
			             fName, str(e)))
			sys.exit(2)

		logger.notice("%i addresse(s) read" % len(addresses))
		if logger.debugEnabled() and len(addresses) > 0:
			text = "address list:"
			for a in addresses:
				text += "\n  %s#%s#%s#%i" % tuple(a)
			logger.debug(text)
		return addresses



	#--------------------------------------------------------------------------
	def _readContent(self):
		fName = self._basePath + ".content"
		if not os.path.isfile(fName):
			raise Exception("content file does not exist: %s" % fName)
		logger.notice("reading content from: %s" % fName)

		content = ""
		try:
			f = codecs.open(fName, "r", "utf-8")
			content = f.read()
			f.close()
		except Exception, e:
			logger.error("could not read content file '%s': %s" % (
			             fName, str(e)))
			sys.exit(2)

		logger.notice("content of length %i read" % len(content.encode("utf8")))
		logger.debug(content)
		return content


# vim: ts=4 noet
