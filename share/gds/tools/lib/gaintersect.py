def point_inside_polygon(x, y, poly):
    """
    Determine if a point is inside a given polygon.

    Cut and paste from:
    http://www.ariel.com.au/a/python-point-int-poly.html

    :param float x: The x position of the point.
    :param float y: The y position of the point.
    :param list poly: xy pairs of the points in the polygon. This does not need
        to stat and end with the same point.
    """

    n = len(poly)
    inside =False

    p1x, p1y = poly[0]
    for i in range(n+1):
        p2x, p2y = poly[i % n]
        if y > min(p1y, p2y):
            if y <= max(p1y, p2y):
                if x <= max(p1x, p2x):
                    if p1y != p2y:
                        xinters = (y-p1y)*(p2x-p1x) / (p2y-p1y) + p1x
                    if p1x == p2x or x <= xinters:
                        inside = not inside
        p1x, p1y = p2x, p2y

    return inside


def get_source_zone(x, y, geojson_features):
    inside = False
    intersection_found = False
    for f in geojson_features:
        for i, p in enumerate(f['geometry']['coordinates']):
            if point_inside_polygon(x, y, p):
                # first polygoin is outer ring
                inside = i == 0
                intersection_found = True
        if inside:
            return f['properties']['Zone']
        if intersection_found:
            return 'Other'
    return 'Other'
