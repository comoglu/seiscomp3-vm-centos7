import unittest as ut
from gaintersect import point_inside_polygon

POLY1 = [(157., -23.), (155., -40.), (138., -39.), (138., -26.)]
POLY2 = [(150., -20.), (150., -40.), (130., -40.), (130., -20.)]
POLY3 = [(150., -20.), (150., -40.), (130., -40.), (130., -20.), (150., -20.)]

class Tests(ut.TestCase):
    def test1(self):
        """
        Check that the corner points are all outside.
        """

        for i in range(4):
            self.assertFalse(point_inside_polygon(*POLY1[i], poly=POLY1))

    def _doer(self, poly):
        """
        Check that this work very near the bounds.
        """

        # left edge
        self.assertTrue(point_inside_polygon(130. + 1e-6, -30., poly))
        self.assertFalse(point_inside_polygon(130. - 1e-6, -30., poly))

        # top edge
        self.assertTrue(point_inside_polygon(140., -20. - 1e-6, poly))
        self.assertFalse(point_inside_polygon(140., -20. + 1e-6, poly))

        # right edge
        self.assertTrue(point_inside_polygon(150. - 1e-6, -30., poly))
        self.assertFalse(point_inside_polygon(150. + 1e-6, -30., poly))

        # bottom edge
        self.assertTrue(point_inside_polygon(140., -40. + 1e-6, poly))
        self.assertFalse(point_inside_polygon(140., -40. - 1e-6, poly))

    def test2(self):
        """
        Check that points very close to the edges are OK.
        """
        self._doer(POLY2)

    def test3(self):
        """
        Check that the polygon can be open or closed.
        """

        self._doer(POLY3)

    def test4(self):
        """
        Check that the polygon can be clockwise or anti-clockwise.
        """

        poly = POLY2[:]
        poly.reverse()
        self._doer(poly)
