# -*- coding: utf-8 -*-

###############################################################################
# Copyright (C) 2014 by gempa GmbH                                            #
#                                                                             #
# All Rights Reserved.                                                        #
#                                                                             #
# NOTICE: All information contained herein is, and remains                    #
# the property of gempa GmbH and its suppliers, if any. The intellectual      #
# and technical concepts contained herein are proprietary to gempa GmbH       #
# and its suppliers.                                                          #
# Dissemination of this information or reproduction of this material          #
# is strictly forbidden unless prior written permission is obtained           #
# from gempa GmbH.                                                            #
#                                                                             #
# Author: Stephan Herrnkind                                                   #
# Email: herrnkind@gempa.de                                                   #
#                                                                             #
#                                                                             #
# Seismic utility funtions                                                    #
#                                                                             #
###############################################################################

import logger

###############################################################################
# Converts a sc3 time (UTC) to a specific time zone and formats it accordingly
#
# Parameters:
# t  : sc3 time object or seconds since epoch
# tz : name of target timezone, see '/usr/share/zoneinfo', e.g. 'Asia/Calcutta'
# fmt: date time format, see 'man strftime'
def formatTZ(t, tz='UTC', fmt='%d/%m/%Y %H:%M:%S %Z'):
	try:
		import datetime, pytz

		tzUTC = pytz.timezone('UTC')
		tzNew = pytz.timezone(tz)

		if type(t) is long:
			sec = t
		else:
			sec = t.seconds()

		time = datetime.datetime.utcfromtimestamp(sec)
		time = time.replace(tzinfo=tzUTC)
		time = tzNew.normalize(time.astimezone(tzNew))

		return time.strftime(fmt)
	except Exception, e:
		logger.error("could not format time: %s" % str(e))

	return ""



###############################################################################
# format focal mechanism parameters, e.g.
#
# <pre style="font-family: monospace">
# Moment Tensor:
#   Mrr=-1.96       Mtt= 0.21
#   Mpp= 1.74       Mrt= 0.92
#   Mrp=-0.07       Mtp=-0.00
# Principal axes:
#   T  Val=  1.74  Plg= 1  Azm= 89
#   N        0.55      20      358
#   P       -2.30      70      183
# Nodal planes:
#  NP1:Strike=198 Dip=47 Slip= -61
#  NP2:       340     50      -116
# </pre>"
def formatFocalMechanism(fm):
	if fm is None:
		logger.warning("focal mechanism object is None")
		return ""

	mt = None
	try:
		import math

		if fm.momentTensorCount() == 0:
			raise Exception("no moment tensor available")
			
		tensor = fm.momentTensor(0).tensor()
		axes = fm.principalAxes()

		Mrr = tensor.Mrr().value()
		Mtt = tensor.Mtt().value()
		Mpp = tensor.Mpp().value()
		Mrt = tensor.Mrt().value()
		Mrp = tensor.Mrp().value()
		Mtp = tensor.Mtp().value()
		Tval = axes.tAxis().length().value()
		Tplg = axes.tAxis().plunge().value()
		Tazi = axes.tAxis().azimuth().value()
		Nval = axes.nAxis().length().value()
		Nplg = axes.nAxis().plunge().value()
		Nazi = axes.nAxis().azimuth().value()
		Pval = axes.pAxis().length().value()
		Pplg = axes.pAxis().plunge().value()
		Pazi = axes.pAxis().azimuth().value()

		expo = 0
		expo = max(expo, int(math.log10(abs(Mrr))))
		expo = max(expo, int(math.log10(abs(Mtt))))
		expo = max(expo, int(math.log10(abs(Mpp))))
		expo = max(expo, int(math.log10(abs(Mrt))))
		expo = max(expo, int(math.log10(abs(Mrp))))
		expo = max(expo, int(math.log10(abs(Mtp))))
		expo = max(expo, int(math.log10(abs(Tval))))
		expo = max(expo, int(math.log10(abs(Nval))))
		expo = max(expo, int(math.log10(abs(Pval))))

		strExpo = str(expo+20-7);

		scaleStr = "Scale 10**%s Nm\n" % expo
		res = "Moment Tensor;%s" % scaleStr.rjust(19)

		div = math.pow(10.0, expo)
		res +=      "  Mrr=%s"   % ("%.2f" % (Mrr / div)).rjust(5)
		res += "       Mtt=%s\n" % ("%.2f" % (Mtt / div)).rjust(5)
		res +=      "  Mpp=%s"   % ("%.2f" % (Mpp / div)).rjust(5)
		res += "       Mrt=%s\n" % ("%.2f" % (Mrt / div)).rjust(5)
		res +=      "  Mrp=%s"   % ("%.2f" % (Mrp / div)).rjust(5)
		res += "       Mtp=%s\n" % ("%.2f" % (Mtp / div)).rjust(5)
		res += "Principal axes:\n"
		res += "  T  Val=%s"   % ("%.2f" % (Tval / div)).rjust(6)
		res +=    "  Plg=%s"   % str(int(round(Tplg))).rjust(2)
		res +=    "  Azm=%s\n" % str(int(round(Tazi))).rjust(3)
		res += "  N      %s"   % ("%.2f" % (Nval / div)).rjust(6)
		res +=    "      %s"   % str(int(round(Nplg))).rjust(2)
		res +=    "      %s\n" % str(int(round(Nazi))).rjust(3)
		res += "  P      %s"   % ("%.2f" % (Pval / div)).rjust(6)
		res +=    "      %s"   % str(int(round(Pplg))).rjust(2)
		res +=    "      %s\n" % str(int(round(Pazi))).rjust(3)

		mt = res
	except Exception, e:
		logger.warning("could not format MT: %s" % str(e))

	np = None
	try:
		np1 = fm.nodalPlanes().nodalPlane1()
		np1Strike = np1.strike().value()
		np1Dip    = np1.dip().value()
		np1Rake   = np1.rake().value()

		np2 = fm.nodalPlanes().nodalPlane2()
		np2Strike = np2.strike().value()
		np2Dip    = np2.dip().value()
		np2Rake   = np2.rake().value()

		res  = " NP1:Strike=%s"  % str(int(round(np1Strike))).rjust(3)
		res +=      " Dip=%s"    % str(int(round(np1Dip))).rjust(2)
		res +=      " Slip=%s\n" % str(int(round(np1Rake))).rjust(4)
		res += " NP2:       %s"  % str(int(round(np2Strike))).rjust(3)
		res +=      "     %s"    % str(int(round(np2Dip))).rjust(2)
		res +=      "      %s\n" % str(int(round(np2Rake))).rjust(4)

		np = res
	except Exception, e:
		logger.warning("could not format nodal planes: %s" % str(e))

	if not mt and not np:
		return ""
	if not mt:
		return np
	if not np:
		return mt

	return mt + '\n' + np


# vim: ts=4 noet
