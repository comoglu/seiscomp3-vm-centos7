# -*- coding: utf-8 -*-

###############################################################################
# Copyright (C) 2014 by gempa GmbH                                            #
#                                                                             #
# All Rights Reserved.                                                        #
#                                                                             #
# NOTICE: All information contained herein is, and remains                    #
# the property of gempa GmbH and its suppliers, if any. The intellectual      #
# and technical concepts contained herein are proprietary to gempa GmbH       #
# and its suppliers.                                                          #
# Dissemination of this information or reproduction of this material          #
# is strictly forbidden unless prior written permission is obtained           #
# from gempa GmbH.                                                            #
#                                                                             #
# Author: Stephan Herrnkind                                                   #
# Email: herrnkind@gempa.de                                                   #
#                                                                             #
#                                                                             #
# Utility funtions to parse and export SeisComP XML                           #
#                                                                             #
###############################################################################

import StringIO
import seiscomp3
import logger


###############################################################################
class Sink(seiscomp3.IO.ExportSink):

	#--------------------------------------------------------------------------
	def __init__(self, buf):
		seiscomp3.IO.ExportSink.__init__(self)
		self.buf = buf
		self.written = 0


	#--------------------------------------------------------------------------
	def write(self, data, size):
		self.buf.write(data[:size])
		self.written += size
		return size


###############################################################################
def readEventParameters(fileName = "-"):
	logger.notice("reading XML")
	ar = seiscomp3.IO.XMLArchive()
	if ar.open(fileName) == False:
		logger.error("could not read XML from '%s'" % fileName)
		return None

	obj = ar.readObject()
	if not obj:
		logger.error("no object found in input XML")
		return None

	ep = seiscomp3.DataModel.EventParameters.Cast(obj)
	if not ep:
		logger.error("XML object not of type EventParameters")
		return None

	return ep


###############################################################################
def objectToString(obj, expName = "trunk"):
	error = "could not serialize object: "
	if not obj:
		logger.error(error + "NULL")
		return None

	exp = seiscomp3.IO.Exporter.Create(expName)
	if not exp:
		logger.error(error + "exporter '%s' not found" % expName)
		return None

	try:
		io = StringIO.StringIO()
		sink = Sink(io)
		exp.write(sink, obj)
		return io.getvalue()
	except Exception, e:
		logger.warning(error + str(e))

	return None


# vim: ts=4 noet
