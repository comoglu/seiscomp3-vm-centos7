# -*- coding: utf-8 -*-

###############################################################################
# Copyright (C) 2014 by gempa GmbH                                            #
#                                                                             #
# All Rights Reserved.                                                        #
#                                                                             #
# NOTICE: All information contained herein is, and remains                    #
# the property of gempa GmbH and its suppliers, if any. The intellectual      #
# and technical concepts contained herein are proprietary to gempa GmbH       #
# and its suppliers.                                                          #
# Dissemination of this information or reproduction of this material          #
# is strictly forbidden unless prior written permission is obtained           #
# from gempa GmbH.                                                            #
#                                                                             #
# Author: Stephan Herrnkind                                                   #
# Email: herrnkind@gempa.de                                                   #
#                                                                             #
#                                                                             #
# Abstract base class for GDS primary content filter commands                 #
#                                                                             #
# A primary content filter reads SC3ML on stdin and outputs formated content  #
# on stdout. In many cases this output is a gempa bulletin                    #
# ('application/gds') which is generated using the Bulletin class, see        #
# bulletin.py.                                                                #
#                                                                             #
# A derived class must implement the filter(self, ep) method.                 #
#                                                                             #
###############################################################################

import ast, getopt, sys, time, traceback
import logger
import xml

###############################################################################
class Filter:

	#--------------------------------------------------------------------------
	def __call__(self):
		self._startTime = None
		self._timeout = None
		try:
			self._startTime = time.time()
			logger.info("start filtering")

			# read timeout parameter
			opts, args = getopt.getopt(sys.argv[1:], 't:', ['timeout='])
			for o, a in opts:
				if o in ("-t", "--timeout"):
					self._timeout = float(a)
					logger.notice("using timeout of %f" % self._timeout)

			# read data, by default a SC3XML document is read into a
			# EventParameters object
			data = self.readData()
			if data is None:
				return 2

			logger.notice("generating content")
			result = self.filter(data)

			print >> sys.stdout, result
			logger.notice("filter output of length %i created in %fs" % (
			              len(result), self.elapsedTime()))
			logger.debug(result)
		except Exception, e:
			error = str(e)
			if len(error) == 0:
				error = e.__class__.__name__
			logger.error(error)
			if logger.noticeEnabled():
				logger.notice(traceback.format_exc())
			return 1

		return 0



	#--------------------------------------------------------------------------
	# implement this method in derived classes, by default data will an
	# SC3 EventParameters object
	def filter(self, data):
		raise Exception("method 'content' not implemented in derived class")



	#--------------------------------------------------------------------------
	# override this method in derived classes if input is not XML, e.g.
	# secondary filter may read a serialized python dictionary
	def readData(self):
		return xml.readEventParameters()



	#--------------------------------------------------------------------------
	# reads python dictionary from file
	def readDict(self):
		data = sys.stdin.readlines()
		return ast.literal_eval(data[0])



	#--------------------------------------------------------------------------
	# returns elapsed time since application start
	def elapsedTime(self):
		return time.time() - self._startTime



	#--------------------------------------------------------------------------
	# returns remaining time or None if no timeout parameter was passed
	def timeout(self):
		if self._timeout is None:
			return None

		remaining = self._timeout - self.elapsedTime()
		if remaining < 0:
			return 0
		return remaining


# vim: ts=4 noet
