#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
# Copyright (C) 2014 by gempa GmbH                                            #
#                                                                             #
# All Rights Reserved.                                                        #
#                                                                             #
# NOTICE: All information contained herein is, and remains                    #
# the property of gempa GmbH and its suppliers, if any. The intellectual      #
# and technical concepts contained herein are proprietary to gempa GmbH       #
# and its suppliers.                                                          #
# Dissemination of this information or reproduction of this material          #
# is strictly forbidden unless prior written permission is obtained           #
# from gempa GmbH.                                                            #
#                                                                             #
# Author: Stephan Herrnkind                                                   #
# Email: herrnkind@gempa.de                                                   #
#                                                                             #
#                                                                             #
# Sends a gempa bulletin via email (SMTP)                                     #
#                                                                             #
###############################################################################

import base64, mimetypes, os, random, smtplib, sys, time

from email import encoders
from email.MIMEMultipart import MIMEMultipart
from email.Utils import formatdate
from email.mime.audio import MIMEAudio
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email.mime.text import MIMEText

from lib import bulletin, logger, spooler

###############################################################################
class SMTPConfig:

	#--------------------------------------------------------------------------
	def __init__(self, config):
		prefix = "smtp"

		try:  self.server = config.get(prefix, "server")
		except: self.server = None
		try: self.port = config.getint(prefix, "port")
		except: self.port = 25
		try:  self.user = config.get(prefix, "user")
		except: self.user = None
		try:  self.pw = config.get(prefix, "pw")
		except: self.pw = ""
		try:  self.host = config.get(prefix, "host")
		except: self.host = "localhost"
		try:  self.email = config.get(prefix, "email")
		except: self.email = "gds@%s" % self.host
		try:  self.ssl = config.getboolean(prefix, "ssl")
		except: self.ssl = False
		try:  self.tls = config.getboolean(prefix, "tls")
		except: self.tls = True
		try:  self.debug = config.getboolean(prefix, "debug")
		except: self.debug = False
		try:  self.bcc = config.getboolean(prefix, "bcc")
		except: self.bcc = True
		try:  self.extraBCC = config.get(prefix, "extraBCC")
		except: self.extraBCC = None


###############################################################################
class SpoolMail(spooler.Spooler):

	#--------------------------------------------------------------------------
	def __init__(self):
		spooler.Spooler.__init__(self)

		self._smtp = SMTPConfig(self._config)
		self._dlm = self._config.get('security', 'dlm')

		if not self._smtp.server:
			logger.error("missing configuration parameter: server")
			sys.exit(2)



	#--------------------------------------------------------------------------
	def spool(self, addresses, content):
		# read bulletin
		try:
			b = bulletin.Bulletin()
			b.read(content)
		except Exception, e:
			raise Exception("could not parse bulletin: %s" % str(e))
		if b.subject == "":
			logger.warning("no subject found")
			b.subject = "information"
		if b.plain == "" and b.html == "" and len(b.attachments) == 0:
			raise Exception("no content found")

		# create and validate addresses
		addressMap = self._createAddressMap(addresses)
		if len(addressMap) == 0: raise Exception("no email addresses found")

		# create mail from bulletin
		mail = self._createMail(b)

		# create headers
		mail["From"] = self._smtp.email
		if self._smtp.bcc and len(addressMap) > 1:
			mail["To"] = "\"Undisclosed Recipients\""
		else:
			addressList = addressMap.keys()
			mail["To"] = ",".join(addressList)
		mail["Subject"] = '{} [{}]'.format(b.subject, self._dlm)
		mail["Date"] = str(formatdate(localtime=True))
		mail['Message-ID'] = self._createMessageID()

		# send the mail to all recipients
		self._send(self._smtp, addressMap, mail)



	#--------------------------------------------------------------------------
	# Adds subscriber name to addresses if the target contains no brackets.
	# Returns a map which maps the (possible) modified address to the original
	# subscriber/target pair needed for logging.
	def _createAddressMap(self, addresses):
		addressMap = {}
		for a in addresses:
			if "<" in a[1] or ">" in a[1]:
				address = a[1]
			else:
				address = "%s <%s>" % (a[0], a[1])
			if address in addressMap:
				msg = "ignoring duplicated address: %s" % address
				logger.warning(msg)
				self.addTargetError(a[0], a[1], msg)
			else:
				addressMap[address] = a
		return addressMap



	#--------------------------------------------------------------------------
	def _createAttachment(self, a):
		content, mime = None, a.MIME
		# read content either from inline or from path
		if a.content:
			content = base64.b64decode(a.content)
		elif a.path:
			f = open(a.path, "r")
			content = f.read()
			f.close()
			if not mime: mime, enc = mimetypes.guess_type(path)
		else:
			raise Exception("neither content nor path set")

		# split mime type
		if not mime: raise Exception("MIME type not set")
		main, sub = a.MIME.split('/', 1)

		if main == 'text':
			msg = MIMEText(content, _subtype=sub, name=a.name)
		elif main == 'image':
			msg = MIMEImage(content, _subtype=sub, name=a.name)
		elif main == 'audio':
			msg = MIMEAudio(content, _subtype=sub, name=a.name)
		else:
			msg = MIMEBase(main, sub, name=a.name)
			msg.set_payload(content)
			encoders.encode_base64(msg)
		return msg



	#--------------------------------------------------------------------------
	# structure rules:
	# - single part if only plain, html, or one attachement is supplied
	# - multipart/mixed for plain and attachements
	# - multipart/alternative for plain and html
	# - multipart/relative if html contains cid references for e.g. inline img
	# - most complex structure example:
	#   multipart/mixed
	#       multipart/alternative
	#           text/plain
	#           multipart/related
	#               text/html
	#               image/gif
	#               image/gif
	#       some/thing (disposition: attachment)
	#       some/thing (disposition: attachment)
	def _createMail(self, bulletin):
		plain     = None
		html      = None
		mail      = None
		multiPart = False

		if bulletin.plain:
			plain = MIMEText(bulletin.plain, 'plain', 'UTF-8')

		if bulletin.html:
			html = MIMEText(bulletin.html, 'html', 'UTF-8')

		# collect attachements
		inlines = []
		attachments = []
		for a in bulletin.attachments:
			if not a.name or a.name == "":
				logger.notice("skipping attachment with empty name")
				continue

			# create attachment
			logger.notice("creating attachment: %s" % a.name)
			try: msg = self._createAttachment(a)
			except Exception, e:
				logger.warning("could not create attachment '%s': %s" % (
				               a.name, str(e)))
				continue

			# use name for cid search if cid is unset
			cid = a.cid if a.cid else a.name

			# inline or normal attachment
			if bulletin.html and cid and "cid:" + cid in bulletin.html:
				msg.add_header('Content-ID', '<%s>' % (cid))
				msg.add_header('Content-Disposition', 'inline')
				inlines.append(msg)
			else:
				msg.add_header('Content-Disposition',
				               'attachment; filename="%s"' % a.name)
				attachments.append(msg)

		# create multi-part/related structure if inline attachments are present
		if inlines:
			logger.notice("creating multi-part/related structure with HTML " \
			              "part and %i inline image(s)" % len(inlines))
			related = MIMEMultipart('related')
			related.attach(html)
			for i in inlines:
				related.attach(i)

			# replace html with related part
			html = related
			multiPart = True

		# create multi-part/alternate structure if text and html are present
		if plain and html:
			logger.notice("creating multi-part/alternative structure")
			mail = MIMEMultipart('alternative')
			mail.attach(plain)
			mail.attach(html)
			multiPart = True
		elif plain:
			mail = plain
		elif html:
			mail = html

		# create multi-part/mixed structure if attachments are present
		if len(attachments) > 1 or ( mail and attachments ):
			logger.notice("creating multi-part/mixed structure with %i " \
			              "attachments" % len(attachments))
			mixed = MIMEMultipart('mixed')
			if mail:
				mixed.attach(mail)
			for a in attachments:
				mixed.attach(a)
			mail = mixed
			multiPart = True
		elif attachments:
			mail = attachments[0]

		if multiPart:
			mail.preamble = 'This is a multi-part message in MIME format.'

		return mail



	#--------------------------------------------------------------------------
	def _send(self, cfg, addressMap, mail):
		logger.notice("connecting to server: %s/%s%s" % (cfg.server, cfg.port,
		              (" using SSL" if cfg.ssl else "")))

		# create server connection
		if cfg.ssl:
			server = smtplib.SMTP_SSL(cfg.server, cfg.port)
			if cfg.debug:
				server.set_debuglevel(1)
		else:
			server = smtplib.SMTP(cfg.server, cfg.port)
			if cfg.debug:
				server.set_debuglevel(1)
			if cfg.tls:
				logger.notice("starting TLS session")
				resp, msg = server.starttls()
				if resp != 220:
					raise Exception("could not enable TLS: %s" % msg)

		# authenticate
		if cfg.user:
			logger.notice("starting to login")
			resp, msg = server.login(cfg.user, cfg.pw)
			if resp != 235:
				raise Exception("could not login: %s" % msg)

		mailText = mail.as_string()
		logger.info("sending email")
		addressList = addressMap.keys()
		if logger.noticeEnabled():
			logger.notice("address list: %s" % ", ".join(addressList))
		if self._smtp.extraBCC is not None:
			extraAddr = self._smtp.extraBCC.split(',')
			logger.notice("extra BCC address list: %s" % ", ".join(extraAddr))
			addressList += extraAddr
#		if logger.debugEnabled():
#			logger.debug("content:\n%s" % mailText)

		# send mail
		try:
			refused = server.sendmail(cfg.email, addressList, mailText)
		except Exception, e:
			raise Exception("could not send mail: %s" % (str(e)))

		# at least one email could be delivered
		if len(refused) < len(addressMap):
			self._partialSuccess = True

		# log refused mail addresses (if any), which are reported as map of
		# form: { 'address', (error code, 'message') }
		if len(refused) > 0:
			logger.warning("%i email address(es) have been refused by SMTP "\
			               "server" % len(refused))
			for key in refused.keys():
				val = refused[key]
				msg = "refused by SMTP server (error %i): %s" % (val[0], val[1])
				if addressMap.has_key(key):
					a = addressMap[key]
					self.addTargetError(a[0], a[1], msg)
				else:
					logger.warning("unexpected email address '%s' %s" % (
					               key, msg))

		# logout
		resp, msg = server.quit()
		if resp != 221:
			logger.warning("could not logout properly: %s" % msg)



	#--------------------------------------------------------------------------
	def _createMessageID(self):
		r = "%s.%s" % (time.time(), random.randint(0, 100))
		return "<%s@%s>" % (r, self._smtp.host)



###############################################################################
if __name__ == "__main__":
	app = SpoolMail()
	app()


# vim: ts=4 noet
