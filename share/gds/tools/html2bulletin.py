#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
# Copyright (C) 2014 by gempa GmbH                                            #
#                                                                             #
# All Rights Reserved.                                                        #
#                                                                             #
# NOTICE: All information contained herein is, and remains                    #
# the property of gempa GmbH and its suppliers, if any. The intellectual      #
# and technical concepts contained herein are proprietary to gempa GmbH       #
# and its suppliers.                                                          #
# Dissemination of this information or reproduction of this material          #
# is strictly forbidden unless prior written permission is obtained           #
# from gempa GmbH.                                                            #
#                                                                             #
# Author: Stephan Herrnkind                                                   #
# Email: herrnkind@gempa.de                                                   #
#                                                                             #
#                                                                             #
# Converts standard HTML to gempa bulletin format                             #
#                                                                             #
###############################################################################

import os, sys
from lib.bulletin import Bulletin, Attachment

def html2Email(argv=None):
	if argv is None:
		argv = sys.argv

	if len(argv) < 3:
		print >> sys.stderr, "usage: %s <subject> <HTML file> "\
		                     "[attachments...]" % os.path.basename(argv[0])
		return 1

	# create content and set subject
	b = Bulletin()
	b.subject = argv[1]

	# read HTML file
	try:
		f = open(argv[2], 'r')
		b.html = f.read()
		f.close()
	except:
		print >> sys.stderr, "could not read HTML file: %s" % argv[2]
		return 2

	# add attachments
	for i in xrange(3, len(argv)):
		filePath = argv[i]

		# check for regular file
		if not os.path.isfile(filePath):
			print >> sys.stderr, "attachment not a regular file: %s" % filePath
			return 3

		# create attachment with name, cid and inline content
		a = Attachment()
		a.name = os.path.basename(filePath)
		a.createCID()
		if not a.loadFromFile(filePath):
			print >> sys.stderr, "could not read attachment file: %s" % filePath
			return 4
		b.attachments.append(a)

		# replace attachment reference by cid
		b.html = b.html.replace("src=\"%s\"" % a.name, "src=\"cid:%s\"" % a.cid)

	print >> sys.stdout, str(b)

if __name__ == "__main__":
	sys.exit(html2Email())


# vim: ts=4 noet
