#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Rings A set of phone numbers using a modem.
"""

import os
import time
from lib import spooler


CUR_DIR = os.path.dirname(os.path.abspath(__file__))


class PhoneSpooler(spooler.Spooler):
    def __init__(self, testing=False):
        spooler.Spooler.__init__(self)

    def spool(self, numbers, content, key_name=None):
        def make_call(number):
            msg = None
            try:
                os.sytem('{}/bom_call_phones.sh {}'.format(CUR_DIR, number))
            except Exception as e:
                msg = 'failed to make call: {}'.format(e)
            return number, msg

        numbers = set((number.strip() for tup in numbers for number in tup[1].split(',')))
        calls = [make_call(number) for number in numbers]
        messages = ['{}: {}'.format(*call) for call in calls if call[1] is not None]

        if len(messages):
            raise Exception(','.join(messages))


if __name__ == "__main__":
    app = PhoneSpooler()
    app()
