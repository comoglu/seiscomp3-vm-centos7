#!/usr/bin/env python

import os
import json
import requests
import socket
import boto3
from lib import spooler
from lib.gaintersect import get_source_zone

METADATA_URL = r'http://169.254.169.254/latest/meta-data/'

# Note that the instance must be kept in sync with the lambda which is set up to
# delete old wphase instances (which looks for instances based on it).
AUTO_INSTANCE_TAGS = [{"Key": "Name", "Value": "wphase-instance"}]

AUTO_USER_DATA_TEMPLATE = """#!/bin/bash

ttw=$(python -c "from datetime import datetime as dt; import dateutil.parser as p; s=(dt.utcnow()-p.parse('{time}')).seconds; print max(0, 1200-s)")

if (( "$ttw" > 0 )); then
    sleep $ttw

    sudo su -c "wphase \
        -H '{messaging_host}' \
        --lat '{lat}' \
        --lon '{lon}' \
        --depth '{depth}' \
        --sourcezone '{sourcezone}' \
        --time '{time}' \
        --magval '{mag_val}' \
        --magtype '{mag_type}' \
        --outputs '{output_dir}' \
        --server 'http://{fdsn_host}:{fdsn_host_port}' \
        --networks '{networks}' \
        --region '{region}' \
        --evid '{evid}' \
        --resultid 20-min \
        --alerttype '{alert_type}' \
        --bomip '{bom_ip}' \
        --bomport '{bom_port}' \
        --notificationemail '{notification_email}' \
        --lambdafunction '{lambda_function}' \
        --lambdainvokearn '{lambda_invoke_arn}' \
        --eatwsenv '{eatws_env}' \
        --writeS3 yes \
        --bucketname '{s3_bucket}'" \
        - centos
fi

ttw=$(python -c "from datetime import datetime as dt; import dateutil.parser as p; s=(dt.utcnow()-p.parse('{time}')).seconds; print max(0, 1800-s)")

if (( "$ttw" > 0 )); then
    sleep $ttw
    postfix=30-min
else
    ttw=$(python -c "from datetime import datetime as dt; import dateutil.parser as p; print (dt.utcnow()-p.parse('{time}')).seconds")
    postfix="$ttw"-seconds
fi

sudo su -c "wphase \
    -H '{messaging_host}' \
    --lat '{lat}' \
    --lon '{lon}' \
    --depth '{depth}' \
    --sourcezone '{sourcezone}' \
    --time '{time}' \
    --magval '{mag_val}' \
    --magtype '{mag_type}' \
    --outputs '{output_dir}' \
    --server 'http://{fdsn_host}:{fdsn_host_port}' \
    --networks '{networks}' \
    --region '{region}' \
    --evid '{evid}' \
    --resultid $postfix \
    --alerttype '{alert_type}' \
    --bomip '{bom_ip}' \
    --bomport '{bom_port}' \
    --notificationemail '{notification_email}' \
    --lambdafunction '{lambda_function}' \
    --lambdainvokearn '{lambda_invoke_arn}' \
    --eatwsenv '{eatws_env}' \
    --writeS3 yes \
    --bucketname '{s3_bucket}'" \
    - centos

shutdown -h now
"""



def _generate_user_data(**kwargs):
    return AUTO_USER_DATA_TEMPLATE.format(**kwargs)



class WphaseSpooler(spooler.Spooler):
    def __init__(self, testing=False):
        spooler.Spooler.__init__(self)
        self.eatws_env = self._config.get("environment", "environment")
        source_zones_geojson = self._config.get('sourcezone', 'sourcezone')
        with open(source_zones_geojson, 'r') as gj:
            self.source_zones = json.load(gj)['features']

    def spool(self, targets, content, key_name=None):
        eqinfo = json.loads(content)
        event_id = eqinfo.pop('id')
        region = eqinfo.pop('region')
        mag_val = eqinfo.pop('magVal')
        mag_type = eqinfo.pop("magType")
        sourcezone = get_source_zone(
            eqinfo['lon'],
            eqinfo['lat'],
            self.source_zones)

        # mac address (needed to get vpc and subnet)
        macs = requests.get(METADATA_URL + 'network/interfaces/macs/').text

        boto3.client('ec2').run_instances(
            MinCount = 1,
            MaxCount = 1,
            ImageId = self._config.get('aws', 'ami_id'),
            InstanceType = self._config.get('aws', 'machine_type'),
            KeyName = self._config.get('aws', 'ssh_key_name'),
            SecurityGroupIds = [self._config.get('aws', 'sg_id')],
            # launch in the same subnet as the current (GDS) instance
            #SubnetId = requests.get(METADATA_URL+ 'network/interfaces/macs/' + macs + 'subnet-id').text,
            SubnetId = self._config.get('aws', 'subnet_id'),
            InstanceInitiatedShutdownBehavior='terminate',
            BlockDeviceMappings = [{
                "DeviceName": "/dev/sda1",
                "Ebs": {
                    "VolumeSize": int(self._config.get('aws', 'disk_size')),
                    "DeleteOnTermination": True,
                    "VolumeType": "gp2"
                }
            }],
            UserData=_generate_user_data(
                s3_bucket = self._config.get('aws', 'result_bucket_name'),
                messaging_host = self._config.get('sc3', 'messaging_host'),
                lat = eqinfo['lat'],
                lon = eqinfo['lon'],
                depth = eqinfo['depth'],
                sourcezone = sourcezone,
                time = eqinfo['time'],
                mag_val = mag_val,
                mag_type = mag_type,
                output_dir = os.path.sep + 'tmp',
                fdsn_host = socket.gethostbyname(self._config.get('sc3', 'fdsn_host')),
                fdsn_host_port = int(self._config.get('sc3', 'fdsn_host_port')),
                networks = self._config.get('sc3', 'networks'),
                region = region,
                evid = event_id,
                alert_type = self._config.get(self.eatws_env, "alert_type"),
                bom_ip = self._config.get(self.eatws_env, "IP"),
                bom_port = int(self._config.get(self.eatws_env, "PORT")),
                notification_email = self._config.get('environment', 'notification_email'),
                lambda_function = self._config.get('aws', 'send_bom_xml_lambda_name'),
                lambda_invoke_arn = self._config.get('aws', 'send_bom_xml_role_arn'),
                eatws_env = self.eatws_env),
            TagSpecifications = [
                { "ResourceType": "instance", "Tags": AUTO_INSTANCE_TAGS },
                { "ResourceType": "volume",   "Tags": AUTO_INSTANCE_TAGS }
            ],
            IamInstanceProfile={
                'Name': self._config.get('aws', 'wphase_instance_profile_name')
            }
        )



if __name__ == "__main__":
    app = WphaseSpooler()
    app()
