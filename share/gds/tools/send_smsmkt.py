#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
# Copyright (C) 2014 by gempa GmbH                                            #
#                                                                             #
# All Rights Reserved.                                                        #
#                                                                             #
# NOTICE: All information contained herein is, and remains                    #
# the property of gempa GmbH and its suppliers, if any. The intellectual      #
# and technical concepts contained herein are proprietary to gempa GmbH       #
# and its suppliers.                                                          #
# Dissemination of this information or reproduction of this material          #
# is strictly forbidden unless prior written permission is obtained           #
# from gempa GmbH.                                                            #
#                                                                             #
# Author: Stephan Herrnkind                                                   #
# Email: herrnkind@gempa.de                                                   #
#                                                                             #
#                                                                             #
# Sends a gempa bulletin via HTTP POST request to smsmkt.com API              #
# https://www.smsmkt.com/download/source/API/SMSMKT-API-HTTP-English-Version.pdf #
#                                                                             #
###############################################################################

#import base64, mimetypes, os, random, smtplib, sys, time
import urllib, urllib2

from lib import bulletin, logger, spooler

###############################################################################
class SMSMktConfig:

	#--------------------------------------------------------------------------
	def __init__(self, config):
		prefix = "smsmkt"

		try:  self.url = config.get(prefix, "url")
		except: self.url = "https://member.smsmkt.com/SMSLink/SendMsg/index.php"
		try:  self.user = config.get(prefix, "user")
		except: self.user = None
		try:  self.pw = config.get(prefix, "pw")
		except: self.pw = None
		try:  self.sender = config.get(prefix, "sender")
		except: self.sender = Sender


###############################################################################
class SpoolSMSMkt(spooler.Spooler):

	#--------------------------------------------------------------------------
	def __init__(self):
		spooler.Spooler.__init__(self)

		self._smsmkt = SMSMktConfig(self._config)
		if not self._smsmkt.url:
			logger.error("missing configuration parameter: url")
			sys.exit(2)
		if not self._smsmkt.user:
			logger.error("missing configuration parameter: user")
			sys.exit(2)
		if not self._smsmkt.pw:
			logger.error("missing configuration parameter: pw")
			sys.exit(2)
		if not self._smsmkt.sender:
			logger.error("missing configuration parameter: sender")
			sys.exit(2)



	#--------------------------------------------------------------------------
	def spool(self, addresses, content):
		# limit phone numbers to 1000
		if len(addresses) > 1000:
			logger.warning("%i phone numbers specfied, truncating to smsmkt " \
			               "service limit of 1000", len(addresses))
			addresses = addresses[:1000]
		elif len(addresses) == 0:
			raise Exception("no phone numbers specified")

		# read bulletin
		try:
			b = bulletin.Bulletin()
			b.read(content)
		except Exception, e:
			raise Exception("could not parse bulletin: %s" % str(e))
		if b.subject != "":
			logger.warning("subject found, ignoring...")
		if b.html != "":
			logger.warning("html content found, ignoring...")
		if len(b.attachments) > 0:
			logger.warning("attachments found, ignoring...")
		if b.plain == "":
			raise Exception("no plain content found")

		params = {}
		params['User']     = self._smsmkt.user
		params['Password'] = self._smsmkt.pw
		params['Msnlist']  = ";".join([a[1] for a in addresses])
		params['Msg']      = b.plain.encode('utf-8')
		params['Sender']   = self._smsmkt.sender
		postData = urllib.urlencode(params)
		
		logger.notice('sending msg of length %i to %i recipients' % (
		                 len(b.plain), len(addresses)))
		logger.debug('message: %s' % params['Msg'])
		logger.debug('recipients: %s' % params['Msnlist'])

		request = urllib2.Request(self._smsmkt.url, data=postData)

		# start request, throws exception if HTTP code is != 200
		logger.notice('starting request to %s' % self._smsmkt.url)
		u = urllib2.urlopen(request, None, self._timeout)

		response = u.read()[:1000]
		logger.notice('read response: %s' % response)

		status = 0
		try:
			output = response.split(",", 1)
			status = int(output[0].split("=")[1])
			details = output[0].split("=")[1]
		except:
			raise Exception("could not parse service status code from "\
			                "response line: %s" % response)
		if status != 0:
			raise Exception("message delivery failed: %s" % response)



###############################################################################
if __name__ == "__main__":
	app = SpoolSMSMkt()
	app()


# vim: ts=4 noet
