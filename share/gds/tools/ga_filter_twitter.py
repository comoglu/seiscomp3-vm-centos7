#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
# Copyright (C) 2014 by gempa GmbH                                            #
#                                                                             #
# All Rights Reserved.                                                        #
#                                                                             #
# NOTICE: All information contained herein is, and remains                    #
# the property of gempa GmbH and its suppliers, if any. The intellectual      #
# and technical concepts contained herein are proprietary to gempa GmbH       #
# and its suppliers.                                                          #
# Dissemination of this information or reproduction of this material          #
# is strictly forbidden unless prior written permission is obtained           #
# from gempa GmbH.                                                            #
#                                                                             #
# Author: Stephan Herrnkind                                                   #
# Email: herrnkind@gempa.de                                                   #
#                                                                             #
# Creates an SMS in gempa bulletin format                                     #
###############################################################################

import sys, os, time
import json
import urllib
import urllib2
import datetime
import seiscomp3.Core
import seiscomp3.DataModel

try:
	import configparser
except ImportError:
	import ConfigParser as configparser

from lib import bulletin, filter

class TwitterFilter(filter.Filter):

	def __init__(self):
		cfg_path, ext = os.path.splitext(__file__)
		config = configparser.RawConfigParser()
		config.read("{0}{1}{2}".format(cfg_path, os.extsep, "cfg"))
		self.askgeo_id = config.get("askgeo", "SERVICE_ID")
		self.askgeo_apikey = config.get("askgeo", "SERVICE_API_KEY")
		self.url_template = config.get("eqatga", "LINK_TEMPLATE")


	def filter(self, ep):
		eventDict = self.parseEventParameters(ep)
		b = bulletin.Bulletin()
		b.plain = "Region: {0}\n".format(eventDict["region"])
		b.plain += "Mag: {0}\n".format(eventDict["magVal"])
		b.plain += self.time(eventDict)
		b.plain += "Lat: {0}, Lon:{1}\n".format(eventDict["lat"],
												eventDict["lon"])
		b.plain += "Dep: {0}km\n".format(eventDict["depth"])
		b.plain += self.shorten(ep.event(0).publicID())
		return str(b)


	def shorten(self, ev_id):
		return self.url_template.format(ev_id)


	def parseEventParameters(self, ep):
		eventDict = {}
		eventDict["id"] = ""
		eventDict["region"] = ""
		eventDict["magVal"] = ""
		eventDict["magType"] = ""
		eventDict["time"] = ""
		eventDict["lat"] = ""
		eventDict["lon"] = ""
		eventDict["depth"] = ""
		eventDict["phases"] = ""

		if ep.eventCount() < 1:
			return eventDict

		event = ep.event(0)
		eventDict["id"] = event.publicID()

		for j in range(0, event.eventDescriptionCount()):
			ed = event.eventDescription(j)
			if ed.type() == seiscomp3.DataModel.REGION_NAME:
				eventDict["region"] = ed.text()
				break

		magnitude = seiscomp3.DataModel.Magnitude.Find(event.preferredMagnitudeID())
		if magnitude:
			eventDict["magVal"] = "%0.1f" %magnitude.magnitude().value()
			eventDict["magType"] = magnitude.type()
		else:
			eventDict["magVal"]="no magnitude available."

		origin = seiscomp3.DataModel.Origin.Find(event.preferredOriginID())
		if origin:
			eventDict["time"] = origin.time().value()
			eventDict["lat"] = "%.2f" % origin.latitude().value()
			eventDict["lon"] = "%.2f" % origin.longitude().value()
			try: eventDict["depth"] = "%.0f" % origin.depth().value()
			except ValueError: pass
			eventDict["phases"] = origin.arrivalCount()

		return eventDict


	def time(self, eventDict):
		"""
		Returns the string that will be used to show the time of the event in the
		bulletin. If the local time for the event's location can be found, that
		date and time is used. Otherwise, the UTC time of the event is used.

		The getLocalTime method is used to retrieve the local time for the event.

		When the time is the local time, the return string will be formatted as:
		"Event Local Time: %day-%month-%year %hour:%mins:%secs %timezone".

		When the time is UTC, the return string will be formatted as:
		"UTC: %day-%month-%year %hour:%mins:%secs".
		"""

		# For security purposes, ensure that the values passed in are numeric.
		# This helps with ensuring potentially dangerous strings are not used
		# in the url.
		try:
			float(eventDict["lat"])
			float(eventDict["lon"])
			float(eventDict["time"])
		except ValueError:
			return "UTC: {0}\n".format(eventDict["time"].toString("%F %T"))

		try:
			local_time, time_zone = self.getLocalTime(
				eventDict["lat"], eventDict["lon"], eventDict["time"])
			return "Event Local Time: {0}\n".format(
				self.createTimeString(local_time, time_zone))
		except UserWarning, urllib2.HTTPError:
			return "UTC: {0}\n".format(eventDict["time"].toString("%F %T"))
		# except urllib2.HTTPError:
		# 	return "UTC: {0}\n".format(eventDict["time"].toString("%F %T"))


	def getLocalTime(self, lat, lng, time):
		"""
		Gets the local time for the given UTC time and geographical position.
		This is done by making the web service get request to retrieve the
		required info. It uses the askgeo.com web service.

		The time parameter needs to be in seconds.

		Returns the local time (in unix time seconds) and the abbreviated
		name of the local timezone.

		Raises UserWarning if there was an issue with getting a response from
		the web service.
		"""

		# Do the get request to the askgeo.com service, storing the results
		# as a dict.
		params = {"points": "{0},{1}".format(lat, lng),
				  "databases": "TimeZone",
				  "dateTime": time.toString("%FT%T.%fZ")}
		url_params = urllib.urlencode(params)
		url = "https://api.askgeo.com/v1/{0}/{1}/query.json?{2}".format(
			self.askgeo_id, self.askgeo_apikey, url_params)
		resp = json.loads(urllib2.urlopen(url).read())
		if resp[u"message"] != u"ok":
			raise UserWarning(
				"Response from web get request returned status of {0}.".format(
					resp[u"message"]))

		# Askgeo.com returns time as milliseconds, so need to account for this
		# difference when calculating the local time.
		offset = resp[u"data"][0][u"TimeZone"][u"CurrentOffsetMs"]
		local_timestamp = time.seconds() + (offset * 0.001)
		return local_timestamp, resp[u"data"][0][u"TimeZone"][u"ShortName"]


	def createTimeString(self, time, zone_name):
		"""
		Creates the formatted time string which displays the time for the
		tweet.

		time must be in seconds.

		Returns the string.
		"""

		time_string = datetime.datetime.fromtimestamp(time).strftime(
			"%d-%m-%Y %H:%M:%S")
		return "{0} {1}".format(time_string, zone_name)


if __name__ == "__main__":
	app = TwitterFilter()
	sys.exit(app())
