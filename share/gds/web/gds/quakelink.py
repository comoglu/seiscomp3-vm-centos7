# -*- coding: utf-8 -*-

############################################################################
# Copyright (C) 2014 by gempa GmbH                                         #
#                                                                          #
# All Rights Reserved.                                                     #
#                                                                          #
# NOTICE: All information contained herein is, and remains                 #
# the property of gempa GmbH and its suppliers, if any. The intellectual   #
# and technical concepts contained herein are proprietary to gempa GmbH    #
# and its suppliers.                                                       #
# Dissemination of this information or reproduction of this material       #
# is strictly forbidden unless prior written permission is obtained        #
# from gempa GmbH.                                                         #
#                                                                          #
# Author: Jan Becker                                                       #
# Email: jabe@gempa.de                                                     #
#                                                                          #
#                                                                          #
# QuakeLink client library                                                 #
#                                                                          #
############################################################################

import socket, ssl, sys

class Options:
    Defaults          = 0x0001
    XMLIndent         = 0x0002
    DataPicks         = 0x0004
    DataAmplitudes    = 0x0008
    DataStaMags       = 0x0010
    DataArrivals      = 0x0020
    DataStaMts        = 0x0040
    DataPreferred     = 0x0080

class Quakelink():

    def __init__(self, host="localhost", port=18010, ssl=False, user=None,
                 password=None, options=None, where="", timeout=10,
                 native=False):
        self._host = host
        self._port = port
        self._ssl = ssl
        self._user = user
        self._password = password
        self._options = options
        self._where = where
        self._timeout = timeout
        self._native = native
        self._msg = ""


    def connect(self, sendOptions=False):
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        if self._ssl:
            self._socket = ssl.wrap_socket(self._socket)
        self._socket.settimeout(self._timeout)
        self._socket.connect((self._host, self._port))
        if self._user and self._password:
            self.auth()

        if self._options is not None and sendOptions:
            if self._options & Options.Defaults:
                self._socket.send("SET DEFAULTS\r\n")
            switch = "ON" if self._options & Options.XMLIndent else "OFF"
            self._socket.send("SET XML.INDENT %s\r\n" % switch)
            switch = "ON" if self._options & Options.DataPicks else "OFF"
            self._socket.send("SET DATA.PICKS %s\r\n" % switch)
            switch = "ON" if self._options & Options.DataAmplitudes else "OFF"
            self._socket.send("SET DATA.AMPLITUDES %s\r\n" % switch)
            switch = "ON" if self._options & Options.DataStaMags else "OFF"
            self._socket.send("SET DATA.STAMAGS %s\r\n" % switch)
            switch = "ON" if self._options & Options.DataArrivals else "OFF"
            self._socket.send("SET DATA.Arrivals %s\r\n" % switch)
            switch = "ON" if self._options & Options.DataStaMts else "OFF"
            self._socket.send("SET DATA.StaMts %s\r\n" % switch)
            switch = "ON" if self._options & Options.DataPreferred else "OFF"
            self._socket.send("SET DATA.PREFERRED %s\r\n" % switch)


    def close(self):
        self._socket.close()

    def auth(self):
        self._socket.send("AUTH %s %s\r\n" % (self._user, self._password))

    def hello(self):
        self._socket.send("HELLO\r\n")
        return self.read_head()

    def get_api(self):
        # API version is returned in 2nd line
        for line in self.hello().split('\n'):
            toks = line.split('=')
            if len(toks) == 2 and toks[0].strip().upper() == 'API':
                try:
                    return int(toks[1].strip())
                except ValueError: break

        return 0

    def get_event_list(self, starttime, endtime):
        self._msg = ""
        where = ""
        if self._where:
            where = " WHERE %s" % self._where
        self._socket.send("SELECT ARCHIVED EVENTS FROM %s TO %s AS SUMMARY%s\r\n" % (
                          starttime, endtime, where))
        self.read_message()
        return self._msg

    def get_event_list_recent(self, limit):
        self._msg = ""
        where = ""
        if self._where:
            where = " WHERE %s" % self._where
        self._socket.send("SELECT ARCHIVED EVENTS AS SUMMARY%s "
                          "ORDER BY OTIME DESC LIMIT %i\r\n" % (where, limit))
        self.read_message()
        return self._msg

    def get_refinements(self, eventid):
        self._msg = ""
        self._socket.send("GET UPDATES OF EVENT %s AS SUMMARY\r\n" % eventid)
        summary = self.read_message()
        return self._msg

    def get_event_xml(self, eventid, revision):
        self._msg = ""
        self._socket.send("GET UPDATE %d OF EVENT %s AS %s\r\n" % (
                          revision, eventid, 'NATIVE' if self._native else 'XML'))
        xml = self.read_message()
        return self._msg

    def read_line(self):
        import array
        line = ""
        while ( True ):
            buf = self._socket.recv(1)
            if len(buf) != 1: break
            if ( buf == '\r' ): continue
            if ( buf == '\n' ):
                return line

            line += buf

        raise Exception("Quakelink closed connection")

    def read_head(self):
        line = ""
        while len(line) == 0:
            line = self.read_line()
        head = ""

        while ( len(line) > 0 ):
            head += line + "\n"
            if ( len(line) == 19 and line == "EOD/SELECT/ARCHIVED" ):
                return head
            line = self.read_line()
        return head


    def read_message(self):
        size =- 1
        headdata = False
        headtype = False
        headdataend = False
        headend = False
        head = self.read_head()

        for line in head.split("\n"):
            if ( len(line) == 27 and line == "DATA/SELECT/ARCHIVED 200 OK" ):
                headdata = True

            if line == "DATA/GET 200 OK":
                headdataend = True

            if line == "Content-Type: quakelink/evsum; charset=\"utf-8\"":
                headtype = True

            if line == "Content-Type: quakelink/evlog; charset=\"utf-8\"":
                headtype = True

            if line == "Content-Type: quakelink/xml":
                headtype = True

            if line == "EOD/SELECT/ARCHIVED":
                headend = True

            if len(line) >= 15 and line[0:15] == "Content-Length:":
                # get Content Length
                toks = line.split(" ")

                if len(toks) != 2:
                    print >> sys.stderr, "Invalid Length-line: %s" % line
                    return "";

                try: size = int(toks[1])
                except: return ""

                if size < 0 :
                    return ""

        if not headdata:
            print >> sys.stderr, "Expect DATA/SELECT/ARCHIVED in head: %s" % head

        if not headtype:
            print >> sys.stderr, "Expect Content-Type in head: %s" % head

        if headend: return

        if size > 0:
            msg = ""

            while len(msg) < size:
                read_size = size-len(msg)
                msg += self.receive(read_size)

            if headend or headdataend:
                self._msg += msg
                return
            else:
                self._msg += msg + "\n"
                self.read_message()
        else:
            print >> sys.stderr, "Expect Content-Length in head: %s" % head
            return ""


    def receive(self, bufsize):
        data = ""
        if bufsize == 0: return data

        while bufsize > 0:
            req = 1024
            if req > bufsize: req = bufsize
            buf = self._socket.recv(req)
            if len(buf) == 0: break
            data += buf
            bufsize -= len(buf)
        return data


# vim: ts=4 et
