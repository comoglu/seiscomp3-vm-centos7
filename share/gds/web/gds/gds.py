############################################################################
# Copyright (C) 2014 by gempa GmbH                                         #
#                                                                          #
# All Rights Reserved.                                                     #
#                                                                          #
# NOTICE: All information contained herein is, and remains                 #
# the property of gempa GmbH and its suppliers, if any. The intellectual   #
# and technical concepts contained herein are proprietary to gempa GmbH    #
# and its suppliers.                                                       #
# Dissemination of this information or reproduction of this material       #
# is strictly forbidden unless prior written permission is obtained        #
# from gempa GmbH.                                                         #
#                                                                          #
# Author: Jan Becker                                                       #
# Email: jabe@gempa.de                                                     #
#                                                                          #
############################################################################

import socket, sys

class GDSException(Exception):
    def __init__(self, log_id, msgs):
        self.log_id = log_id
        self.messages = msgs


class GDS():
    def __init__(self, host="localhost", port=20000, timeout=10):
        self._host = host
        self._port = port
        self._timeout = timeout;
        self._msg = ""

    def connect(self):
        self._socket= socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        self._socket.settimeout(self._timeout)
        self._socket.connect((self._host, self._port))

    def close(self):
        self._socket.close()

    def evaluate(self, xml):
        self.send("GETSUBSCRIPTIONS %d\n" % len(xml))
        self.send(xml)
        line, bytes = self.read_line()
        print >> sys.stderr, "[gds] response: %s" % line
        toks = line.split()
        if len(toks) != 2:
            raise Exception("protocol error: %d toks received" % len(toks))

        stat = toks[0]
        try:
            bytesToRead = int(toks[1])
        except:
            raise Exception("protocol error: invalid length")

        if bytesToRead > 0:
            data = self.receive(bytesToRead)
        else:
            data = ""

        if len(data) != bytesToRead:
            raise Exception("transmission error")

        if stat != "OK":
            msgs = []
            for l in data.splitlines():
                toks = l.split(' ' , 1)
                if len(toks) < 2:
                    raise Exception("protocol error: only %d token(s) received" % len(toks))
                if toks[0] == "MESSAGE":
                    msgs.append(toks[1])

            raise GDSException(None, msgs)

        return data


    def evaluateBulletins(self, xml):
        self.send("GETBULLETINS %d\n" % len(xml))
        self.send(xml)
        line, bytes = self.read_line()
        print >> sys.stderr, "[gds] response: %s" % line
        toks = line.split()
        if len(toks) != 2:
            raise Exception("protocol error: %d toks received" % len(toks))

        stat = toks[0]
        try:
            bytesToRead = int(toks[1])
        except:
            raise Exception("protocol error: invalid length")

        msgs = []
        bulletins = {}
        subscriptions = ""

        if stat == "OK":
            while bytesToRead > 0:
                line, bytesToRead = self.read_line(bytesToRead)
                toks = line.split(' ' , 1)
                if len(toks) < 2:
                    raise Exception("protocol error: only %d token(s) received" % len(toks))

                ident = toks[0]
                if ident == "MESSAGE":
                    msgs.append(toks[1])
                elif ident == "SUBSCRIPTIONS":
                    try:
                        packageSize = int(toks[1])
                    except:
                        raise Exception("protocol error: SUBSCRIPTIONS: invalid length: %s" % toks[1])

                    subscriptions = self.receive(packageSize)
                    bytesToRead -= packageSize

                    if len(subscriptions) != packageSize:
                        raise Exception("transmission error: SUBSCRIPTIONS: %d != %d" % (len(subscriptions), packageSize))

                    # Read protocols new line
                    line, bytesToRead = self.read_line(bytesToRead)
                elif ident == "BULLETIN":
                    toks = toks[1].split()
                    if len(toks) < 2:
                        raise Exception("protocol error: BULLETIN: only %d token(s) received" % len(toks))
                    serviceFilter = toks[0]
                    try: packageSize = int(toks[1])
                    except:
                        raise Exception("protocol error: BULLETIN: invalid length: %s" % toks[1])

                    mimeType = toks[2] if len(toks) > 2 and toks[2] and \
                                          len(toks[2]) > 0 else None

                    bulletin = self.receive(packageSize)
                    bytesToRead -= packageSize

                    if len(bulletin) != packageSize:
                        raise Exception("transmission error: BULLETIN: %d != %d" % (len(bulletin), packageSize))

                    bulletins[serviceFilter] = (bulletin, mimeType)

                    # Read protocols new line
                    line, bytesToRead = self.read_line(bytesToRead)
        else:
            while bytesToRead > 0:
                line, bytesToRead = self.read_line(bytesToRead)
                toks = line.split(' ' , 1)
                if len(toks) < 2:
                    raise Exception("protocol error: only %d token(s) received" % len(toks))

                ident = toks[0]
                if ident == "MESSAGE":
                    msgs.append(toks[1])
            raise GDSException(None, msgs)

        return subscriptions, bulletins, msgs


    def disseminate(self, author, data, subscriptions=[], queues=[],
                    revision='.', eventID=None, contentType=None, mimeType=None):
        if author is None or len(author) == 0:
            raise Exception("empty author")
        if data is None or len(data) == 0:
            raise Exception("empty data")
        if len(subscriptions) == 0 and len(queues) == 0:
            raise Exception("no subscriptions nor queues specified")

        for s in subscriptions:
            self.send("ADDSUBSCRIPTION " + s + "\n")
        for q in queues:
            self.send("ADDQUEUE " + q + "\n")

        command = "DISSEMINATE %d %s %s" % (len(data), revision, author)
        if eventID:
            if not contentType:
                raise Exception("eventID without contentType specified")
            command += " %s %s" % (eventID, contentType)
            if mimeType:
                command += " %s" % mimeType
        command += "\n"
        self.send(command)
        self.send(data)

        line, bytes = self.read_line()
        toks = line.split()
        if len(toks) != 2:
            raise Exception("protocol error: only %d toks received: %s" % (len(toks), line))

        stat = toks[0]
        try:
            bytesToRead = int(toks[1])
        except:
            raise Exception("protocol error: invalid length: %s" % toks[1])

        if bytesToRead > 0:
            lines = self.receive(bytesToRead).split('\n')
        else:
            lines = []

        msgs = []
        log_id = None

        for l in lines:
            toks = l.split(' ', 1)
            if len(toks) < 2: continue
            if toks[0] == "MESSAGE":
                msgs.append(toks[1])
            elif toks[0] == "LOGID":
                try: log_id = int(toks[1])
                except: msgs.append("Invalid server response, expected valid ID")

        if stat != "OK":
            raise GDSException(log_id, msgs)

        return (log_id, msgs)


    def sendBulletins(self, author, subscriptions, eventID, revision, bulletins):
        for s in subscriptions:
            self.send("ADDSUBSCRIPTION " + s + "\n")
        for key, value in bulletins.items():
            b = str(value[1])
            mime = " %s" % value[2] if value[2] else ""
            self.send("ADDBULLETIN %s %d%s\n" % (key, len(b), mime))
            self.send(b)
            self.send("\n")

        self.send("SEND %s %s %s\n" % (eventID, revision, author))

        line, bytes = self.read_line()
        toks = line.split()
        if len(toks) != 2:
            raise Exception("protocol error: only %d toks received: %s" % (len(toks), line))

        stat = toks[0]
        try:
            bytesToRead = int(toks[1])
        except:
            raise Exception("protocol error: invalid length: %s" % toks[1])

        if bytesToRead > 0:
            lines = self.receive(bytesToRead).split('\n')
        else:
            lines = []

        msgs = []
        log_id = None

        for l in lines:
            toks = l.split(' ', 1)
            if len(toks) < 2: continue
            if toks[0] == "MESSAGE":
                msgs.append(toks[1])
            elif toks[0] == "LOGID":
                try: log_id = int(toks[1])
                except: msgs.append("Invalid server response, expected valid ID")

        if stat != "OK":
            raise GDSException(log_id, msgs)

        return (log_id, msgs)


    def preview(self, bulletins):
        for key, value in bulletins.items():
            b = str(value[1])
            self.send("ADDBULLETIN %s %d\n" % (key, len(b)))
            self.send(b)
            self.send("\n")

        self.send("PREVIEW\n")

        line, bytes = self.read_line()
        toks = line.split()
        if len(toks) != 2:
            raise Exception("protocol error: only %d toks received: %s" % (len(toks), line))

        stat = toks[0]
        try:
            bytesToRead = int(toks[1])
        except:
            raise Exception("protocol error: invalid length: %s" % toks[1])

        msgs = []
        log_id = None
        bulletins = {}

        while bytesToRead > 0:
            l, bytesToRead = self.read_line(bytesToRead)
            toks = l.split(' ', 1)
            if len(toks) < 2: continue
            if toks[0] == "MESSAGE":
                msgs.append(toks[1])
            elif toks[0] == "BULLETIN":
                toks = toks[1].split()
                bull_id = toks[0]
                try: bull_size = int(toks[1])
                except:
                    msgs.append("Invalid server response, expected valid size")
                    break
                try: mimeType = toks[2]
                except: mimeType = ""

                bull = self.receive(bull_size)
                bytesToRead -= bull_size
                bulletins[bull_id] = (bull, mimeType)

        if stat != "OK":
            raise GDSException(0, msgs)

        return (bulletins, msgs)


    def read_line(self, byte_count = 0):
        import array
        line = ""
        buf = array.array("B", [0] * 1)
        while ( self._socket.recv_into(buf,1) == 1 ):
            byte_count = byte_count - 1
            ch = buf.tostring()
            if ( ch == '\r' ): continue
            if ( ch == '\n' ):
                return line, byte_count

            line += ch
        return line, byte_count


    def send(self, data):
        self._socket.sendall(data)

    def receive(self, bufsize):
        data = ""
        if bufsize == 0: return data

        while bufsize > 0:
            req = 1024
            if req > bufsize: req = bufsize
            buf = self._socket.recv(req)
            if len(buf) == 0: break
            data += buf
            bufsize -= len(buf)
        return data
