############################################################################
# Copyright (C) 2014 by gempa GmbH                                         #
#                                                                          #
# All Rights Reserved.                                                     #
#                                                                          #
# NOTICE: All information contained herein is, and remains                 #
# the property of gempa GmbH and its suppliers, if any. The intellectual   #
# and technical concepts contained herein are proprietary to gempa GmbH    #
# and its suppliers.                                                       #
# Dissemination of this information or reproduction of this material       #
# is strictly forbidden unless prior written permission is obtained        #
# from gempa GmbH.                                                         #
#                                                                          #
# Author: Stephan Herrnkind                                                #
# Email: herrnkind@gempa.de                                                #
#                                                                          #
############################################################################

from django.conf import settings
from django.contrib import admin
from django.contrib.admin.models import LogEntry, DELETION
from django.core.urlresolvers import reverse
from django.db.models import CharField, FloatField, IntegerField
from django.forms import TextInput
from django.utils.html import escape

from bitfield import BitField
from bitfield.forms import BitFieldCheckboxSelectMultiple
from forms import BitFieldSelectMultiple

import models

################################################################################
# Inline definitions

class BaseInline(admin.TabularInline):
    extra = 1

class CommentInline(BaseInline):
    model = models.Comment

class CriterionInline(BaseInline):#admin.StackedInline):
    #extra = 1
    model = models.Criterion
    formfield_overrides = {
        BitField: {'widget': BitFieldSelectMultiple},
        FloatField: { 'widget': TextInput(attrs={'size':5})},
    }

class QueueInline(BaseInline):
    model = models.Queue

class SubscriptionInline(BaseInline):
    model = models.Subscription

################################################################################
# Model admin definitions

class BaseAdmin(admin.ModelAdmin):
    list_per_page = settings.PAGE_ITEMS
#    save_on_top = True

class NameDescAdmin(BaseAdmin):
    list_display  = [ 'name', 'description' ]
    search_fields = list_display
    ordering = ( 'name', )

class CriterionAdmin(BaseAdmin):
    formfield_overrides = {
        BitField:   { 'widget': BitFieldCheckboxSelectMultiple },
    }
    inlines = [ CommentInline ]
    list_editable = [ 'enabled', 'region', 'external_criterion',
                      'min_magnitude', 'max_magnitude', 'min_depth',
                      'max_depth' ]
    list_display  = [ 'name', 'queue' ] + list_editable
    list_filter   = [ 'enabled', 'queue', 'region', 'eval_mode' ]
    search_fields = [ 'name', 'queue__name', 'region__name',
                      'external_criterion__name',
                      'external_criterion__description' ]
    ordering = ( 'name', )


class ExternalCriterionAdmin(NameDescAdmin):
    inlines       = [ CriterionInline ]

class FilterAdmin(NameDescAdmin):
    inlines       = [ SubscriptionInline ]

class QueueAdmin(NameDescAdmin):
    inlines       = [ CriterionInline, SubscriptionInline ]
    list_display  = NameDescAdmin.list_display + [ 'manual' ]
    list_filter   = [ 'manual' ]
    list_editable = [ 'manual' ]

class RegionAdmin(NameDescAdmin):
    inlines       = [ CriterionInline ]
    list_editable = [ 'min_lat', 'max_lat', 'min_lon', 'max_lon',
                      'lat', 'lon', 'radius' , 'bna_file' ]
    list_display  = NameDescAdmin.list_display + list_editable
    search_fields = NameDescAdmin.search_fields + [ 'bna_file' ]

class ServiceAdmin(NameDescAdmin):
    inlines       = [ SubscriptionInline ]
    list_editable = [ 'log_bulletins', 'log_cleanup', 'log_keep',
                      'ha_redundant' ]
    list_display  = NameDescAdmin.list_display + list_editable
    list_filter   = [ 'log_bulletins' ]

class SubscriberAdmin(NameDescAdmin):
    formfield_overrides = {
        BitField:   { 'widget': BitFieldCheckboxSelectMultiple },
    }
    inlines       = [ SubscriptionInline ]
    list_editable = [ 'enabled', 'priority', 'min_delay', 'max_delay',
                      'delta_origin_time', 'delta_epicenter', 'delta_depth',
                      'delta_magnitude' ]
    list_display  = NameDescAdmin.list_display + list_editable
    list_filter   = [ 'priority', 'enabled' ]

class SubscriptionAdmin(BaseAdmin):
    list_editable = [ 'filter', 'subscriber', 'service_target', 'description',
                      'resend', 'enabled' ]
    list_display  = [ 'service', 'queue' ] + list_editable
    list_filter   = [ 'service', 'queue', 'filter', 'resend', 'enabled' ]
    search_fields = [ 'service__name', 'queue__name', 'filter__name',
                      'subscriber__name', 'service_target' ]
    ordering = ( 'queue', 'service', )


################################################################################
# Global log
class LogEntryAdmin(admin.ModelAdmin):
    date_hierarchy  = 'action_time'
    readonly_fields = [f.name for f in LogEntry._meta.get_fields()]
    list_filter     = ['user', 'content_type', 'action_flag']
    search_fields   = ['object_repr', 'change_message']
    list_display    = ['action_time', 'user', 'content_type', 'object_link',
                       'action_flag', 'change_message']

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return request.user.is_superuser and request.method != 'POST'

    def has_delete_permission(self, request, obj=None):
        return False

    def object_link(self, obj):
        if obj.action_flag == DELETION:
            link = escape(obj.object_repr)
        else:
            ct = obj.content_type
            link = u'<a href="%s">%s</a>' % (
                reverse('admin:%s_%s_change' % (ct.app_label, ct.model), args=[obj.object_id]),
                escape(obj.object_repr),
            )
        return link
    object_link.allow_tags = True
    object_link.admin_order_field = 'object_repr'
    object_link.short_description = u'object'


################################################################################
# Registration

admin.site.register(LogEntry, LogEntryAdmin)
admin.site.register(models.Criterion, CriterionAdmin)
admin.site.register(models.ExternalCriterion, ExternalCriterionAdmin)
admin.site.register(models.Filter, FilterAdmin)
admin.site.register(models.Queue, QueueAdmin)
admin.site.register(models.Region, RegionAdmin)
admin.site.register(models.Service, ServiceAdmin)
admin.site.register(models.Subscriber, SubscriberAdmin)
admin.site.register(models.Subscription, SubscriptionAdmin)
