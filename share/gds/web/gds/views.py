############################################################################
# Copyright (C) 2014 by gempa GmbH                                         #
#                                                                          #
# All Rights Reserved.                                                     #
#                                                                          #
# NOTICE: All information contained herein is, and remains                 #
# the property of gempa GmbH and its suppliers, if any. The intellectual   #
# and technical concepts contained herein are proprietary to gempa GmbH    #
# and its suppliers.                                                       #
# Dissemination of this information or reproduction of this material       #
# is strictly forbidden unless prior written permission is obtained        #
# from gempa GmbH.                                                         #
#                                                                          #
# Author: Jan Becker                                                       #
# Email: jabe@gempa.de                                                     #
#                                                                          #
############################################################################

#from django.contrib.auth.decorators import login_required, permission_required
import urlparse, ast, urllib
import datetime, calendar, socket
import sys, re, random, mimetypes
import cStringIO as StringIO

from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.decorators import login_required
try:
    from functools import wraps
except ImportError:
    from django.utils.functional import wraps  # Python 2.4 fallback.
from django.utils.decorators import available_attrs

from django.template import loader
from django.http import HttpResponse, HttpResponseRedirect
from django.db import connection
from django.db.models import Max
from django.core.urlresolvers import reverse
from django.conf import settings
from django.core.paginator import Paginator
from django.contrib import messages
from models import EventLog, ServiceLog, ReceiverLog, Queue, Subscription

import quakelink, gds, bulletin
from gds import GDSException


def user_passes_test(test_func, login_url=None, redirect_field_name=REDIRECT_FIELD_NAME):
    """
    Decorator for views that checks that the user passes the given test,
    redirecting to the log-in page if necessary. The test should be a callable
    that takes the user object and returns True if the user passes.
    """

    def decorator(view_func):
        @wraps(view_func, assigned=available_attrs(view_func))
        def _wrapped_view(request, *args, **kwargs):
            if test_func(request.user):
                return view_func(request, *args, **kwargs)
            if not request.user.is_authenticated():
            #if True:
                path = request.build_absolute_uri()
                # If the login URL is the same scheme and net location then just
                # use the path as the "next" URL.
                login_scheme, login_netloc = urlparse.urlparse(login_url or
                                                               reverse("login"))[:2]
                current_scheme, current_netloc = urlparse.urlparse(path)[:2]
                if ((not login_scheme or login_scheme == current_scheme) and
                    (not login_netloc or login_netloc == current_netloc)):
                    path = request.get_full_path()
                from django.contrib.auth.views import redirect_to_login
                return redirect_to_login(path, login_url, redirect_field_name)
            else:
                t = loader.get_template('gds/no_perm.html')
                return HttpResponse(t.render({}, request))
        return _wrapped_view
    return decorator


def permission_required(perm, login_url=None):
    """
    Decorator for views that checks whether a user has a particular permission
    enabled, redirecting to the log-in page if necessary.
    """
    return user_passes_test(lambda u: u.has_perm(perm), login_url=login_url)



def connect2QuakeLink(sendOptions=False):
    if not hasattr(settings, 'QUAKELINK_NATIVE'):
        settings.QUAKELINK_NATIVE = False
    q = quakelink.Quakelink(settings.QUAKELINK_HOST,
                            settings.QUAKELINK_PORT,
                            settings.QUAKELINK_SSL,
                            settings.QUAKELINK_USER,
                            settings.QUAKELINK_PASSWORD,
                            settings.QUAKELINK_OPTIONS,
                            settings.QUAKELINK_FILTER,
                            settings.QUAKELINK_TIMEOUT,
                            settings.QUAKELINK_NATIVE)
    q.connect(sendOptions)
    return q


def connect2GDS():
    q = gds.GDS(settings.GDS_HOST,
                settings.GDS_PORT,
                settings.GDS_TIMEOUT)
    q.connect()
    return q


def calendarDate(request):
    now = datetime.datetime.utcnow()

    try: year = int(request.GET.get('y'))
    except: year = now.year

    try: month = int(request.GET.get('m'))
    except: month = now.month

    try: day = min(int(request.GET.get('d')), calendar.monthrange(year, month)[1])
    except: day = now.day

    return year, month, day


def createPaginator(request, objects):
    p = Paginator(objects, settings.PAGE_ITEMS)
    param_page = request.GET.get('p')
    if not param_page is None:
        try:
            page = p.page(param_page)
        except:
            page = p.page(1)
    else:
        page = p.page(1)
    return p, page


def strPrecision(s, precision):
    try:
        return ("%%0.%sf" % precision) % float(s)
    except:
        return s

#############################################################################
# Views

@login_required
def index(request):
    t = loader.get_template('gds/index.html')
    return HttpResponse(t.render({}, request))


@login_required
@permission_required('gds.view_event_log')
def log(request):
    t = loader.get_template('gds/log.html')
    return HttpResponse(t.render({}, request))


@login_required
@permission_required('gds.view_event_log')
def log_proc(request):
    t = loader.get_template('gds/log_proc.html')

    recent = 0
    if request.GET.get('recent') is not None:
        recent = max(int(settings.RECENT_ITEMS), 1)

    year, month, day = calendarDate(request)

    try:
        if recent:
            q = EventLog.objects\
                .annotate(max_status=Max('servicelog__status'))\
                .order_by('-timestamp')[:recent]
        else:
            q = EventLog.objects\
                .filter(timestamp__year=year, timestamp__month=month, \
                        timestamp__day=day)\
                .annotate(max_status=Max('servicelog__status'))\
                .order_by('-timestamp')
    except:
        q = None

    query_dict = request.GET.copy()
    if 'p' in query_dict:
        del query_dict['p']

    if q is not None:
        p, page = createPaginator(request, q)
    else:
        p = None
        page = None

    ctx = {
        'year'       : year,
        'month'      : month,
        'day'        : day,
        'years'      : [i for i in range(year-3,year+4)],
        'months'     : [(calendar.month_name[i][:3], i) for i in range(1,13)],
        'days'       : range(1,calendar.monthrange(year, month)[1]+1),
        'recent'     : recent,
        'paginator'  : p,
        'page_obj'   : page,
        'query_str'  : query_dict.urlencode(),
        'objects'    : page.object_list,
    }

    return HttpResponse(t.render(ctx, request))


@login_required
@permission_required('gds.view_event_log')
def log_event(request):
    t = loader.get_template('gds/log_event.html')

    recent = 0
    if request.GET.get('recent') is not None:
        recent = max(int(settings.RECENT_ITEMS), 1)

    year, month, day = calendarDate(request)

    # Replace by common Django query if possible
    try:
        sql = "SELECT e.event_id, MAX(e.origin_time) AS otime, "\
                  "MAX(e.timestamp) AS timestamp, "\
                  "COUNT(DISTINCT e.id) AS num_diss, "\
                  "MAX(s.status) AS max_status "\
              "FROM gds_log_event e, gds_log_service s "\
              "WHERE e.event_id != '' AND e.id = s.event_log_id "
        if not recent:
            sql += "AND EXTRACT(YEAR FROM e.origin_time) = %i AND "\
                   "EXTRACT(MONTH FROM e.origin_time) = %i AND "\
                   "EXTRACT(DAY FROM e.origin_time) = %i " % (year, month, day)
        sql += "GROUP BY event_id"
        if recent:
            sql += " ORDER BY otime DESC LIMIT %i" % recent

        cursor = connection.cursor()
        cursor.execute(sql)
        def dictfetchall(cursor):
            desc = cursor.description
            return [
                dict(zip([col[0] for col in desc], row))
                for row in cursor.fetchall()
            ]
        q = dictfetchall(cursor)
    except Exception, e:
        messages.add_message(request, messages.ERROR,
                             'Query for Event log failed: %s' % str(e))
        return HttpResponse(t.render({}, request))

    query_dict = request.GET.copy()
    if 'p' in query_dict:
        del query_dict['p']

    if q is not None:
        p, page = createPaginator(request, q)
        objlst = page.object_list
    else:
        p = None
        page = None
        objlst = None

    ctx = {
        'year'       : year,
        'month'      : month,
        'day'        : day,
        'years'      : [i for i in range(year-3,year+4)],
        'months'     : [(calendar.month_name[i][:3], i) for i in range(1,13)],
        'days'       : range(1,calendar.monthrange(year, month)[1]+1),
        'recent'     : recent,
        'paginator'  : p,
        'page_obj'   : page,
        'query_str'  : query_dict.urlencode(),
        'objects'    : objlst
    }

    return HttpResponse(t.render(ctx, request))


@login_required
@permission_required('gds.view_event_log')
def log_proc_id(request, elogid):
    t = loader.get_template('gds/log_proc_id.html')

    try: event_log = EventLog.objects.get(id = elogid)
    except: event_log = None

    if not event_log:
        messages.add_message(request, messages.ERROR, 'Procedure log not found')
        ctx = {}
    else:
        p, page = createPaginator(request, event_log.servicelog_set.all())

        # event log name
        eventname = event_log.event_id
        if event_log.revision is not None:
            eventname += ' (r%i)' % event_log.revision

        d = event_log.timestamp
        date_params = { 'y': d.year, 'm': d.month, 'd': d.day }

        ctx = {
            'paginator'    : p,
            'page_obj'     : page,
            'objects'      : page.object_list,
            'log'          : event_log,
            'eventname'    : eventname,
            'date_params'  : date_params,
        }

    return HttpResponse(t.render(ctx, request))


@login_required
@permission_required('gds.view_event_log')
def log_event_id(request, eventid):
    t = loader.get_template('gds/log_event_id.html')

    try: event_log = EventLog.objects \
                             .filter(event_id = eventid) \
                             .annotate(max_status=Max('servicelog__status')) \
                             .order_by('-timestamp')
    except: event_log = None

    if not event_log:
        messages.add_message(request, messages.ERROR, 'Event not found')
        ctx = {
            'event_id' : eventid,
        }
    else:
        p, page = createPaginator(request, event_log)
        d = page.object_list[0].origin_time
        if d is None:
            date_params = None
            date_query = None
        else:
            date_params = { 'y': d.year, 'm': d.month, 'd': d.day }
            date_query = urllib.urlencode(date_params)

        ctx = {
            'paginator'    : p,
            'page_obj'     : page,
            'objects'      : page.object_list,
            'event_id'     : eventid,
            'date_params'  : date_params,
            'date_query'   : date_query,
        }

    return HttpResponse(t.render(ctx, request))


@login_required
@permission_required('gds.view_event_log')
def log_service(request, slogid):
    t = loader.get_template('gds/log_service.html')

    try: service_log = ServiceLog.objects.get(id = slogid)
    except: service_log = None

    if not service_log:
        messages.add_message(request, messages.ERROR, 'Service log not found')
        ctx = {}
    else:
        p, page = createPaginator(request, service_log.receiverlog_set.all())

        # event log name
        event_log = service_log.event_log
        eventname = event_log.event_id
        if event_log.revision is not None:
            eventname += ' (r%i)' % event_log.revision

        # service log name
        name = service_log.service
        if service_log.filter:
            name += "/" + service_log.filter

        d = event_log.timestamp
        date_params = { 'y': d.year, 'm': d.month, 'd': d.day }

        ctx = {
            'paginator'    : p,
            'page_obj'     : page,
            'objects'      : page.object_list,
            'eventname'    : eventname,
            'name'         : name,
            'log'          : service_log,
            'date_params'  : date_params,
        }

    return HttpResponse(t.render(ctx, request))


@login_required
@permission_required('gds.view_event_log')
def log_service_content(request, slogid):
    t = loader.get_template('gds/log_service_content.html')

    try: service_log = ServiceLog.objects.get(id = slogid)
    except: service_log = None

    if not service_log:
        messages.add_message(request, messages.ERROR, 'Service log not found')
        ctx = {}
    else:
        # event log name
        event_log = service_log.event_log
        eventname = event_log.event_id
        filename = eventname
        if event_log.revision is not None:
            eventname += ' (r%i)' % event_log.revision
            filename += '_r%i' % event_log.revision

        # service log name
        name = service_log.service
        filename += '_' + name
        if service_log.filter:
            name += '/' + service_log.filter
            filename += '_' + service_log.filter

        mime = service_log.mime_type

        # return with content-disposition if download requested
        try: download = int(request.GET.get('download'))
        except: download = 0
        if service_log.content and download == 1:
            if mime is None:
                mime = 'application/octet-stream'
                ext = '.content'
            elif mime == 'application/gds':
                ext = '.gds'
            else:
                ext = mimetypes.guess_extension(mime, strict=False)
                if ext is None: ext = '.content'

            response = HttpResponse(service_log.content, content_type=mime)
            response['Content-Disposition'] = 'attachment; filename=%s%s' % (
                                              filename, ext)
            return response

        b = None
        if mime == 'application/gds':
            try:
                tmpBul = bulletin.Bulletin()
                tmpBul.read(service_log.content)
                b = tmpBul
            except Exception, e:
                messages.add_message(request, messages.ERROR, 'Error parsing '\
                                     'bulletin content: %s' % str(e))

        printable = isinstance(service_log.content, str) or \
                    isinstance(service_log.content, unicode)

        ctx = {
            'eventname'    : eventname,
            'name'         : name,
            'log'          : service_log,
            'bulletin'     : b,
            'printable'    : printable,
        }

    return HttpResponse(t.render(ctx, request))


@login_required
@permission_required('gds.view_disseminate')
def publish(request):
    t = loader.get_template('gds/publish.html')
    errors = []
    events = []

    recent = 0
    if request.GET.get('recent') is not None:
        recent = max(int(settings.RECENT_ITEMS), 1)

    year, month, day = calendarDate(request)
    start = datetime.datetime(year, month, day)
    end = start + datetime.timedelta(1)

    try:
        ql = connect2QuakeLink()

        if recent:
            api = ql.get_api()
            if api >= 1:
                msg = ql.get_event_list_recent(recent)
            else:
                recent = 0
                messages.add_message(request, messages.ERROR,
                                     'Limit queries not supported by QuakeLink '
                                     'API version %i, showing calender '
                                     'perspective instead' % api)
        if not recent:
            msg = ql.get_event_list(
                "%d,%02d,%02d,%02d,%02d,%02d" % (start.year,start.month,start.day,
                                                 start.hour,start.minute,start.second),
                "%d,%02d,%02d,%02d,%02d,%02d" % (end.year,end.month,end.day,
                                                 end.hour,end.minute,end.second))
        ql.close()

        for evt in msg.split('\n'):
            evt = evt.split(";")
            if len(evt) != 13: continue

            # SQL injection?
            event_id = evt[0].replace("'", "")
            try:
                lastsent = EventLog.objects.filter(event_id = event_id).extra(
                           where=["timestamp = (select max(timestamp) from %s \
                                                where event_id='%s')" % (EventLog._meta.db_table, event_id)])[0]
            except:
                lastsent = None

            data = {
                "event_id" : event_id,
                "ptime"    : evt[1],
                "otime"    : evt[2],
                "mag"      : float(evt[3]) if len(evt[3]) else None,
                "lat"      : float(evt[5]),
                "lon"      : float(evt[6]),
                "dep"      : float(evt[7]) if len(evt[7]) else None,
                "ph"       : evt[8],
                "agency"   : evt[9],
                "status"   : evt[10],
                "type"     : evt[11],
                "region"   : evt[12],
                "lastsent" : lastsent,
            }
            events.append(data)
    except socket.error, exc:
        messages.add_message(request, messages.ERROR, 'Connection to QuakeLink not available: %s' % str(exc))
    except Exception, exc:
        messages.add_message(request, messages.ERROR, str(exc))

    query_dict = request.GET.copy()
    if 'p' in query_dict:
        del query_dict['p']
    p, page = createPaginator(request, events)

    ctx = {
        'errors'     : errors,
        'year'       : year,
        'month'      : month,
        'day'        : day,
        'years'      : [i for i in range(year-3,year+4)],
        'months'     : [(calendar.month_name[i][:3], i) for i in range(1,13)],
        'days'       : range(1,calendar.monthrange(year, month)[1]+1),
        'recent'     : recent,
        'paginator'  : p,
        'page_obj'   : page,
        'query_str'  : query_dict.urlencode(),
        'events'     : page.object_list,
    }
    return HttpResponse(t.render(ctx, request))


@login_required
@permission_required('gds.view_disseminate')
def publish_event(request, eventid):
    t = loader.get_template('gds/publish_event.html')
    content = ""

    # SQL injection?
    eventid = eventid.replace("'", "")
    try:
        ql = connect2QuakeLink()
        content = ql.get_refinements(eventid)
        ql.close()
    except socket.error, exc:
        messages.add_message(request, messages.ERROR, 'Connection to QuakeLink not available: %s' % str(exc))
    except Exception, exc:
        messages.add_message(request, messages.ERROR, str(exc))

    refinements = []

    for line in content.split("\n"):
        line.strip()
        # Ignore empty or comment lines
        if not line or line.startswith('#'): continue

        ref = line.split(";")
        if len(ref) != 13: continue

        data = {
            "rev"      : int(ref[0]),
            "ptime"    : ref[1],
            "otime"    : ref[2],
            "mag"      : float(ref[3]) if len(ref[3]) else None,
            "mag_t"    : ref[4],
            "lat"      : float(ref[5]),
            "lon"      : float(ref[6]),
            "dep"      : float(ref[7]) if len(ref[7]) else None,
            "ph"       : ref[8],
            "agency"   : ref[9],
            "status"   : ref[10],
            "type"     : ref[11],
            "region"   : ref[12]
        }
        refinements.insert(0, data)

    if len(refinements) == 0:
        messages.add_message(request, messages.ERROR, 'Event not found')
        ctx = { 'eventid': eventid }
    else:
        p, page = createPaginator(request, refinements)

        ctx = {
            'eventid'     : eventid,
            'paginator'   : p,
            'page_obj'    : page,
            'refinements' : page.object_list,
        }
    return HttpResponse(t.render(ctx, request))



@login_required
@permission_required('gds.publish_events')
def do_publish(request, eventid, refinement, edit_mode):
    subscriptions = []
    bulletins = {}

    ref = int(refinement)
    if not edit_mode:
        ql = connect2QuakeLink(True)
        xml = ql.get_event_xml(eventid, ref)
        ql.close()

    for key in request.POST.keys():
        if not key.startswith('Q_'): continue
        try: q_id = int(key[2:])
        except: continue

        subs = request.POST.getlist(key)
        for s in subs:
            if not s.startswith('S_'): continue

            toks = s.split('_')
            if len(toks) != 3:
                print >> sys.stderr, "[warning] Ivalid POST parameter %s" % s
                continue;

            try: s_id = int(toks[1])
            except: continue

            try: match = int(toks[2])
            except: continue

            s = Subscription.objects.get(id=s_id)
            subscriptions.append("%s#%s#%s#%s#%i#%i" % (s.subscriber.name,
                                 s.service_target, s.service.name,
                                 "" if s.filter is None else s.filter.name,
                                 s.subscriber.priority, match))

            # Don't fetch bulletins in semi-automatic mode
            if not edit_mode: continue

            service_filter_id = s.service.name
            if s.filter != None:
                service_filter_id += "#" + s.filter.name
            try:
                b_type = int(request.POST[service_filter_id])
            except:
                messages.add_message(request, messages.ERROR, 'Internal error')
                continue

            if bulletins.has_key(service_filter_id): continue

            try: mime = request.POST[service_filter_id + "_mime"]
            except: mime = None

            if b_type == 0:
                b = bulletin.Bulletin()
                items = 0
                try:
                    b.subject = request.POST[service_filter_id + "_subject"]
                    items = items + 1
                except: pass

                try:
                    b.plain = request.POST[service_filter_id + "_plain"].replace("\r\n", "\n")
                    items = items + 1
                except: pass

                try:
                    b.html = request.POST[service_filter_id + "_html"].replace("\r\n", "\n")
                    items = items + 1
                except: pass

                if items == 0:
                    print >> sys.stderr, "[Warning] No bulletin available for %s" % service_filter_id
                    continue

                try:
                    idx = 1
                    while True:
                        try:
                            a = bulletin.Attachment()
                            a.name = request.POST[service_filter_id + "_attachment[%d].name" % idx]
                            try: a.path = request.POST[service_filter_id + "_attachment[%d].path" % idx]
                            except: pass
                            try: a.content = request.POST[service_filter_id + "_attachment[%d].content" % idx]
                            except: pass
                            try: a.MIME = request.POST[service_filter_id + "_attachment[%d].MIME" % idx]
                            except: pass
                            b.attachments.append(a)
                        except:
                            break
                        idx = idx + 1

                    bulletins[service_filter_id] = (0, b, mime)

                except:
                    print >> sys.stderr, "[Warning] No bulletin attachment available for %s" % service_filter_id

                idx = 1
                while True:
                    try:
                        key = request.POST[service_filter_id + "_extparam[%d].key" % idx]
                        value = request.POST[service_filter_id + "_extparam[%d].value" % idx]
                        b.extParameters[key] = value
                    except:
                        break
                    idx = idx + 1

            elif b_type == 1:
                d = {}
                prefix = "dict-" + service_filter_id + "-"
                for key in request.POST.keys():
                    if not key.startswith(prefix): continue
                    d[key[len(prefix):]] = request.POST[key]
                bulletins[service_filter_id] = (1, d, mime)

            elif b_type == 2:
                try:
                    raw = request.POST[service_filter_id + "_text"].replace("\r\n", "\n")
                    bulletins[service_filter_id] = (2, raw, mime)
                except:
                  print >> sys.stderr, "[Warning] No bulletin available for %s" % service_filter_id


    gds = connect2GDS()
    if edit_mode:
        return gds.sendBulletins(request.user.username, subscriptions, eventid, refinement, bulletins)
    else:
        return gds.disseminate(request.user.username, xml,
                               subscriptions=subscriptions, revision=refinement)

@login_required
@permission_required('gds.view_disseminate')
def publish_event_refinement(request, eventid, refinement):
    if request.method == "POST":
        # make sure at least one subscription is selected
        subscriptionFound = False
        for key in request.POST.keys():
            if key.startswith('Q_'):
                subscriptionFound = True
                break

        if not subscriptionFound:
            messages.add_message(request, messages.ERROR, 'No subscription selected. At least one subscription is required to send a message.')
        else:
            edit_mode = request.POST.get('gds_edit')

            try:
                log_id, msgs = do_publish(request, eventid, refinement, edit_mode)
                messages.add_message(request, messages.INFO, 'Event published successfully')
                for msg in msgs:
                    messages.add_message(request, messages.WARNING, msg)
                return HttpResponseRedirect(reverse('gds-log-proc-id', args=[log_id]))

            except socket.error, exc:
                messages.add_message(request, messages.ERROR, 'Connection to GDS server not available: %s' % str(exc))
            except GDSException, exc:
                for m in exc.messages:
                    messages.add_message(request, messages.ERROR, m)
                if exc.log_id:
                    return HttpResponseRedirect(reverse('gds-log-proc-id', args=[exc.log_id]))
            except Exception, exc:
                messages.add_message(request, messages.ERROR, str(exc))

        return HttpResponseRedirect(reverse('gds-publish-event', args=[eventid]))

    edit_mode = request.GET.get('edit')
    if edit_mode:
        t = loader.get_template('gds/publish_event_refinement_edit.html')
    else:
        t = loader.get_template('gds/publish_event_refinement.html')

    xml = None
    evaluation = None

    try:
        ref = int(refinement)
        ql = connect2QuakeLink(True)
        xml = ql.get_event_xml(eventid, ref)
        ql.close()
    except socket.error, exc:
        messages.add_message(request, messages.ERROR, 'Connection to QuakeLink not available: %s' % str(exc))
    except Exception, exc:
        messages.add_message(request, messages.ERROR, str(exc))

    bulletins = {}

    if xml:
        # return with content-disposition if download requested
        try: download = int(request.GET.get('download'))
        except: download = 0
        if download == 1:
            response = HttpResponse(xml, content_type="application/xml")
            response['Content-Disposition'] = 'attachment; filename=%s_rev%s.xml' % (
                                              eventid, refinement)
            return response

        # Get all queues and all subscriptions
        q_objs = Queue.objects.all().order_by('name')
        table = {}

        # Let the server evaluate the XML
        try:
            gds = connect2GDS()
            if edit_mode:
                subscriptions, bulletins, msgs = gds.evaluateBulletins(xml)
            else:
                subscriptions = gds.evaluate(xml)
                msgs = []
                bulletins = {}

            lines = subscriptions.split('\n')
            for l in lines:
                if not l: continue
                if l.startswith("MESSAGE "):
                    messages.add_message(request, messages.WARNING, l[8:])
                    continue
                # subscriber, target, service, filter
                row = l.split('#')
                if len(row) != 5:
                    print >> sys.stderr, "[warning] Skipping subscription %s" % l
                    continue

                try:
                    match = int(row[4])
                except:
                    print >> sys.stderr, "[warning] Skipping subscription %s, invalid match parameter" % l
                    continue

                table['#'.join(row[:4])] = match

            for m in msgs:
                messages.add_message(request, messages.WARNING, m)
        except socket.error, exc:
            messages.add_message(request, messages.ERROR, 'Connection to GDS server not available: %s' % str(exc))
        except GDSException, exc:
            for m in exc.messages:
                messages.add_message(request, messages.ERROR, "Exception: %s" % m)
        except Exception, exc:
            messages.add_message(request, messages.ERROR, str(exc))

        for key, value in bulletins.items():
            content = value[0]
            mimeType = value[1]
            servID = key.split('#')
            try:
                b = bulletin.Bulletin()
                b.read(content.decode("utf8"))
                bulletins[key] = (0, b, servID, mimeType)
            except Exception, exc:
                # No bulletin, default to plain text
                bulletins[key] = (2, content, servID, mimeType)

                # try to read a dictionary
                try:
                    value_dict = ast.literal_eval(content)
                    if type(value_dict) == dict:
                        bulletins[key] = (1, value_dict, servID, mimeType)
                except: pass

        queues = []
        for q in q_objs:
            queue = {}
            queue['id'] = q.id
            queue['name'] = q.name
            queue['description'] = q.description

            subscriptions = []

            nchk = 0
            nunchk = 0
            for s in q.subscription_set.all():
                if s.filter: f = s.filter.name
                else: f = ''
                subscr = {}
                subscr['id'] = s.id
                subscr['subscriber'] = s.subscriber
                subscr['service'] = s.service
                subscr['service_target'] = s.service_target
                subscr['description'] = s.description
                subscr['filter'] = f
                subscr['priority'] = s.subscriber.priority
                row = [s.subscriber.name, s.service_target, s.service.name, f]
                ret = table.get('#'.join(row), None)
                if ret != None:
                    subscr['match'] = ret
                    nchk += 1
                    subscr['selected'] = True
                else:
                    subscr['match'] = 0;
                    nunchk += 1
                    subscr['selected'] = False

                subscriptions.append(subscr)

            queue['subscriptions'] = subscriptions
            if nchk != 0 and nunchk != 0:
                queue['state'] = 'partial'
                queue['selected'] = True
            elif nchk != 0:
                queue['selected'] = True
            else:
                queue['selected'] = False

            queues.append(queue)

        if len(queues) == 0:
            messages.add_message(request, messages.ERROR, 'No queues defined')
            queues = None
    else:
        messages.add_message(request, messages.ERROR, 'Event/revision not found')
        queues = None

    ctx = {
        'eventid'    : eventid,
        'refinement' : refinement,
        'xml'        : xml,
        'queues'     : queues,
        'bulletins'  : bulletins,
    }
    return HttpResponse(t.render(ctx, request))


@login_required
@permission_required('gds.view_disseminate')
def preview_bulletin(request):
    #if request.method != "POST":
    #    return HttpResponse("Only POST method is supported")
    if request.method == "POST":
        data = request.POST
    else:
        data = request.GET

    try:
        service_filter_id = data['bulletin']
    except:
        return HttpResponse("No bulletin id specified")

    try:
        b_type = int(data[service_filter_id])
    except:
        return HttpResponse("Cannot get bulletin type")

    bulletins = {}

    if b_type == 0:
        b = bulletin.Bulletin()
        items = 0
        try:
            b.subject = data[service_filter_id + "_subject"]
            items = items + 1
        except: pass

        try:
            b.plain = data[service_filter_id + "_plain"]
            items = items + 1
        except: pass

        try:
            b.html = data[service_filter_id + "_html"]
            items = items + 1
        except: pass

        if items == 0:
            return HttpResponse("")

        try:
            idx = 1
            while True:
                try:
                    a = bulletin.Attachment()
                    a.name = data[service_filter_id + "_attachment[%d].name" % idx]
                    try: a.path = data[service_filter_id + "_attachment[%d].path" % idx]
                    except: pass
                    try: a.content = data[service_filter_id + "_attachment[%d].content" % idx]
                    except: pass
                    try: a.MIME = data[service_filter_id + "_attachment[%d].MIME" % idx]
                    except: pass
                    b.attachments.append(a)
                except:
                    break
                idx = idx + 1

            bulletins[service_filter_id] = (0, b)
        except Exception, exc:
            return HttpResponse(str(exc))

    elif b_type == 1:
        d = {}
        prefix = "dict-" + service_filter_id + "-"
        for key in data.keys():
            if not key.startswith(prefix): continue
            d[key[len(prefix):]] = data[key]
        bulletins[service_filter_id] = (1, d)
    elif b_type == 2:
        try:
            raw = data[service_filter_id + "_text"]
            bulletins[service_filter_id] = (2, raw)
        except Exception, exc:
            return HttpResponse(str(exc))

    try:
        gds = connect2GDS()
        bulletins, msgs = gds.preview(bulletins)
    except GDSException, exc:
        return HttpResponse(str(exc.messages))
    except Exception, exc:
        return HttpResponse(str(exc))

    if len(bulletins) == 0:
        msg = "Error creating preview";
        if len(msgs) > 0 :
            msg += ":<br>"
        for m in msgs:
            msg += m + "<br>"
        return HttpResponse(msg)
    elif len(bulletins) != 1:
        return HttpResponse("Too many bulletins in result set")

    try: mimeType = bulletins.values()[0][1]
    except: mimeType = ""

    if mimeType != "" and mimeType != 'application/gds':
        return HttpResponse(bulletins.values()[0][0], content_type=mimeType)
    else:
        content = bulletins.values()[0][0]

        try:
            b = bulletin.Bulletin()
            b.read(content)

            if b.html:
                r = re.compile(r'"cid:([^"]+)"')
                pos = 0

                while True:
                  m = r.search(b.html, pos)

                  if m is None: break

                  if len(m.groups()) < 1: continue

                  s1 = m.start(1)
                  e1 = m.end(1)

                  att = b.html[s1:e1]

                  s = m.start()
                  e = m.end()

                  subst = ""
                  for a in b.attachments:
                      if a.name == att and a.content:
                          subst = "data:;base64," + a.content
                          b.attachments.remove(a)
                          break

                  b.html = b.html[:s+1] + subst + b.html[e-1:]
                  pos = s + len(subst)

            b_content = b
            b_type = 0

        except Exception, exc:
            try:
                value_dict = ast.literal_eval(content)
                if type(value_dict) == dict:
                    b_type = 1
                    b_content = value_dict
                else:
                    b_type = 2
                    b_content = content
            except:
                b_type = 2
                b_content = content

        t = loader.get_template('gds/bulletin_preview.html')
        ctx = {
            'content'  : b_content,
            'type'     : b_type,
        }
        return HttpResponse(t.render(ctx, request))
