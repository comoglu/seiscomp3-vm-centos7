from __future__ import absolute_import

from django.forms import IntegerField, ValidationError
from django.forms.widgets import SelectMultiple

try:
    from django.utils.encoding import force_text
except ImportError:
    from django.utils.encoding import force_unicode as force_text

from bitfield.types import BitHandler


class BitFieldSelectMultiple(SelectMultiple):
    def render(self, name, value, attrs=None, choices=()):
        if isinstance(value, BitHandler):
            value = [k for k, v in value if v]
        elif isinstance(value, int):
            real_value = []
            div = 2
            for (k, v) in self.choices:
                if value % div != 0:
                    real_value.append(k)
                    value -= (value % div)
                div *= 2
            value = real_value
        return super(BitFieldSelectMultiple, self).render(
            name, value, attrs=attrs)

    def _has_changed(self, initial, data):
        if initial is None:
            initial = []
        if data is None:
            data = []
        if initial != data:
            return True
        initial_set = set([force_text(value) for value in initial])
        data_set = set([force_text(value) for value in data])


