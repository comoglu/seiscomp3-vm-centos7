-- implement High Availability (HA) feature
ALTER TABLE gds_service ADD COLUMN ha_redundant bool NOT NULL;
ALTER TABLE gds_log_event ADD COLUMN gds_id varchar(100);
