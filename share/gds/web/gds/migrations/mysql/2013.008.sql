-- min/max number of stations constributing in magnitude calculation
ALTER TABLE gds_criterion
  ADD COLUMN min_mags integer AFTER max_phases,
  ADD COLUMN max_mags integer AFTER min_mags;
