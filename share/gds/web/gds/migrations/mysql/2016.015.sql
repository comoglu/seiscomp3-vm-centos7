-- add indexes to speed up event log browsing
CREATE INDEX gds_log_event_67f1b7ce ON gds_log_event (timestamp);
CREATE INDEX gds_log_event_68b10995 ON gds_log_event (event_time);
