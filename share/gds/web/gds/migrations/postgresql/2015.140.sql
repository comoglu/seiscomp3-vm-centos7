-- introduce more resend options
ALTER TABLE gds_subscription
  ALTER COLUMN resend TYPE integer USING CASE WHEN false THEN 0 ELSE 1 END;
