-- add description field for each subscription
ALTER TABLE gds_subscription
  ADD COLUMN description varchar(200) NOT NULL;
