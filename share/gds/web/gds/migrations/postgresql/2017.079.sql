-- gds_criterion: status is renamed to eval_mode and becomes optional
ALTER TABLE gds_criterion
  RENAME COLUMN status TO eval_mode;
ALTER TABLE gds_criterion
  ALTER COLUMN eval_mode TYPE integer,
  ALTER COLUMN eval_mode DROP NOT NULL;
BEGIN;
  UPDATE gds_criterion SET eval_mode = NULL WHERE eval_mode = 2;
COMMIT;

-- gds_criterion:
-- - new column enabled
-- - region selection becomes optional
-- - new column external_criterion_id
-- - new column eval_status
-- - depth fields are converted to double
ALTER TABLE gds_criterion
  ADD COLUMN enabled boolean NOT NULL DEFAULT true,
  ALTER COLUMN region_id TYPE integer,
  ALTER COLUMN region_id DROP NOT NULL,
  ADD COLUMN external_criterion_id integer,
  ADD COLUMN eval_status bigint,
  ALTER COLUMN min_depth TYPE double precision,
  ALTER COLUMN max_depth TYPE double precision,
  ADD CONSTRAINT gds_criterion_external_criterion_id_fkey FOREIGN KEY (external_criterion_id)
    REFERENCES gds_external_criterion (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED,
  ADD CONSTRAINT gds_criterion_eval_mode_check CHECK ((eval_mode >= 0)),
  ADD CONSTRAINT gds_criterion_max_mags_check CHECK ((max_mags >= 0)),
  ADD CONSTRAINT gds_criterion_max_phases_check CHECK ((max_phases >= 0)),
  ADD CONSTRAINT gds_criterion_min_mags_check CHECK ((min_mags >= 0)),
  ADD CONSTRAINT gds_criterion_min_phases_check CHECK ((min_phases >= 0));

-- gds_criterion + gds_queue: external criterion becomes part of standard
-- criterion, for each queue using an external criterion a new standard
-- criterion is created
CREATE INDEX gds_criterion_external_criterion_id ON gds_criterion USING btree (external_criterion_id);
BEGIN;
  INSERT INTO gds_criterion (name, queue_id, external_criterion_id, eval_mode)
    SELECT CONCAT(SUBSTRING(ec.name, 1, 80), '_', LEFT(MD5(TO_CHAR(NOW(), 'YYYY-MM-DD HH:MM:SS')), 18)), q.id, ec.id, 2
    FROM gds_queue q INNER JOIN gds_external_criterion ec ON q.external_criterion_id = ec.id;
COMMIT;

DROP INDEX gds_queue_external_criterion_id;
ALTER TABLE gds_queue
  DROP CONSTRAINT gds_queue_external_criterion_id_fkey,
  DROP COLUMN external_criterion_id;


-- gds_region:
--   bounding box coordinates become optional
--   new columns for circular distance
--   new column for BNA polygon file
ALTER TABLE gds_region
  ALTER COLUMN max_lat TYPE double precision,
  ALTER COLUMN min_lat TYPE double precision,
  ALTER COLUMN min_lon TYPE double precision,
  ALTER COLUMN max_lon TYPE double precision,
  ALTER COLUMN max_lat DROP NOT NULL,
  ALTER COLUMN min_lat DROP NOT NULL,
  ALTER COLUMN min_lon DROP NOT NULL,
  ALTER COLUMN max_lon DROP NOT NULL,
  ADD COLUMN lat double precision,
  ADD COLUMN lon double precision,
  ADD COLUMN radius double precision,
  ADD COLUMN bna_file character varying(200);

-- gds_subscriber: new column enabled
ALTER TABLE gds_subscriber ADD COLUMN enabled boolean NOT NULL DEFAULT true;

-- gds_subscriber: add delta columns defining significant change
ALTER TABLE gds_subscriber
  ADD COLUMN delta_origin_time double precision,
  ADD COLUMN delta_epicenter double precision,
  ADD COLUMN delta_depth double precision,
  ADD COLUMN delta_magnitude double precision;

-- gds_subscription: new column enabled
ALTER TABLE gds_subscription ADD COLUMN enabled boolean NOT NULL DEFAULT true;

-- gds_log_event:
--   rename event_time to origin_time
--   add origin_time_ms, latitude, longitude, depth and magnitude columns
ALTER TABLE gds_log_event
  RENAME COLUMN event_time TO origin_time;
ALTER TABLE gds_log_event
  ADD COLUMN origin_time_ms integer CONSTRAINT gds_log_event_origin_time_ms_check CHECK ((origin_time_ms >= 0)),
  ADD COLUMN latitude  double precision,
  ADD COLUMN longitude double precision,
  ADD COLUMN depth     double precision,
  ADD COLUMN magnitude double precision;
ALTER INDEX gds_log_event_event_time RENAME TO gds_log_event_origin_time;
