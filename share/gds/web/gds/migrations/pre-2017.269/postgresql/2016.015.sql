-- add indexes to speed up event log browsing
CREATE INDEX gds_log_event_event_time
  ON gds_log_event USING btree (event_time);
CREATE INDEX gds_log_event_timestamp
  ON gds_log_event USING btree ("timestamp");
