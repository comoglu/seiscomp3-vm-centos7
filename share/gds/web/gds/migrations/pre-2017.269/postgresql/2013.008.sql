-- min/max number of stations constributing in magnitude calculation
ALTER TABLE gds_criterion
  ADD COLUMN min_mags integer,
  ADD COLUMN max_mags integer;
