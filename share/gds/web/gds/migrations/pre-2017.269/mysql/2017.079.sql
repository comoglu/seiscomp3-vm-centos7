-- gds_criterion:
-- - new column enabled
-- - region selection becomes optional
-- - new column external_criterion_id
-- - status is renamed to eval_mode
-- - phases and mags integer fields become UNSIGNED
-- - depth fields are converted to double
ALTER TABLE gds_criterion
  ADD COLUMN enabled tinyint(1) NOT NULL DEFAULT 1 AFTER name,
  MODIFY COLUMN region_id integer,
  CHANGE COLUMN status eval_mode integer UNSIGNED,
  MODIFY COLUMN min_phases integer UNSIGNED,
  MODIFY COLUMN max_phases integer UNSIGNED,
  MODIFY COLUMN min_mags integer UNSIGNED,
  MODIFY COLUMN max_mags integer UNSIGNED,
  MODIFY COLUMN min_depth double,
  MODIFY COLUMN max_depth double;

-- gds_criterion: new column eval_status
ALTER TABLE gds_criterion ADD COLUMN eval_status bigint AFTER eval_mode;

-- gds_criterion: eval_mode becomes optional
BEGIN;
UPDATE gds_criterion SET eval_mode = NULL WHERE eval_mode = 2;
COMMIT;

-- gds_criterion + gds_queue: external criterion becomes part of standard
-- criterion, for each queue using an external criterion a new standard
-- criterion is created
ALTER table gds_criterion
  ADD COLUMN external_criterion_id integer AFTER region_id,
  ADD KEY `gds_criterion_57d14931` (`external_criterion_id`),
  ADD CONSTRAINT `external_criterion_id_refs_id_c9c6f4ce` FOREIGN KEY (`external_criterion_id`) REFERENCES `gds_external_criterion` (`id`);
BEGIN;
  INSERT INTO gds_criterion (name, queue_id, external_criterion_id, eval_mode)
    SELECT CONCAT(SUBSTRING(ec.name, 1, 80), '_', LEFT(MD5(NOW()), 18)), q.id, ec.id, 2
    FROM gds_queue q INNER JOIN gds_external_criterion ec ON q.external_criterion_id = ec.id;
COMMIT;
ALTER TABLE gds_queue
  DROP FOREIGN KEY external_criterion_id_refs_id_f38e46da,
  DROP INDEX gds_queue_57d14931,
  DROP COLUMN external_criterion_id;

-- gds_region:
--   bounding box coordinates become optional
--   new columns for circular distance
--   new column for BNA polygon file
ALTER TABLE gds_region
  MODIFY COLUMN max_lat double,
  MODIFY COLUMN min_lat double,
  MODIFY COLUMN min_lon double,
  MODIFY COLUMN max_lon double,
  ADD COLUMN lat double,
  ADD COLUMN lon double,
  ADD COLUMN radius double,
  ADD COLUMN bna_file varchar(200) NULL;

-- gds_subscriber: new column enabled
ALTER TABLE gds_subscriber ADD COLUMN enabled tinyint(1) NOT NULL DEFAULT 1 AFTER description;

-- gds_subscriber: add delta columns defining significant change
ALTER TABLE gds_subscriber
  ADD COLUMN delta_origin_time double NULL,
  ADD COLUMN delta_epicenter double NULL,
  ADD COLUMN delta_depth double NULL,
  ADD COLUMN delta_magnitude double NULL;

-- gds_subscription: new column enabled
ALTER TABLE gds_subscription ADD COLUMN enabled tinyint(1) NOT NULL DEFAULT 1;

-- gds_log_event:
--   rename event_time to origin_time
--   add origin_time_ms, latitude, longitude, depth and magnitude columns
ALTER TABLE gds_log_event
  CHANGE COLUMN event_time origin_time datetime,
  ADD COLUMN origin_time_ms integer UNSIGNED AFTER origin_time,
  ADD COLUMN latitude  double AFTER origin_time_ms,
  ADD COLUMN longitude double AFTER latitude,
  ADD COLUMN depth     double AFTER longitude,
  ADD COLUMN magnitude double AFTER depth;

-- OPTIONAL: Rename key, supported only in MySQL 5.7 and above
-- ALTER TABLE gds_log_event
--   RENAME KEY gds_log_event_68b10995 TO gds_log_event_70a4f23e;

