-- cleanup mode
ALTER TABLE gds_service ADD COLUMN log_cleanup integer AFTER log_bulletins;
-- start cleanup after this number of days
ALTER TABLE gds_service ADD COLUMN log_keep integer AFTER log_cleanup;
