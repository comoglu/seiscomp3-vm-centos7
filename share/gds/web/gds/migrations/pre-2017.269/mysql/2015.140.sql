-- introduce more resend options
ALTER TABLE gds_subscription
  MODIFY COLUMN resend integer NOT NULL;
