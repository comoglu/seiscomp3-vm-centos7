############################################################################
# Copyright (C) 2014 by gempa GmbH                                         #
#                                                                          #
# All Rights Reserved.                                                     #
#                                                                          #
# NOTICE: All information contained herein is, and remains                 #
# the property of gempa GmbH and its suppliers, if any. The intellectual   #
# and technical concepts contained herein are proprietary to gempa GmbH    #
# and its suppliers.                                                       #
# Dissemination of this information or reproduction of this material       #
# is strictly forbidden unless prior written permission is obtained        #
# from gempa GmbH.                                                         #
#                                                                          #
# Author: Stephan Herrnkind                                                #
# Email: herrnkind@gempa.de                                                #
#                                                                          #
############################################################################

import os

# Number of items per page
PAGE_ITEMS = 25

# Number of recent items used for list based (non calendar) log and disseminate
# perspective
RECENT_ITEMS = 200

# Quakelink connection settings
QUAKELINK_HOST = "localhost"
QUAKELINK_PORT = 18010
QUAKELINK_SSL = False
QUAKELINK_USER = ""
QUAKELINK_PASSWORD = ""

# SQL like WHERE clause to filter the result set.
# clause    := condition[ AND|OR [(]clause[)]]"
# condition := MAG|DEPTH|LAT|LON|PHASES op float |
#              UPDATED|OTIME op time |"
#              AGENCY|STATUS|TYPE|REGION|MAG_T op 'string' |"
#              MAG|DEPTH|LAT|LON|PHASES|OTIME|UPDATED IS [NOT] NULL"
# op        := =|!=|>|>=|<|<=|eq|gt|ge|lt|ge"
# time      := %Y,%m,%d[,%H,%M,%S,%f]"
#
# example:
# QUAKELINK_FILTER = "MAG >= 3 AND (AGENCY = 'test' OR DEPTH < 300)"
QUAKELINK_FILTER = ""

QUAKELINK_TIMEOUT = 10

# QuakeLink options to set right after connecting. The required value type is a
# 2-byte integer mask:
#    1=Defaults       (Use the default options of the QuakeLink server),
#    2=XMLIndent      (Indent XML),
#    4=DataPicks      (Include picks),
#    8=DataAmplitudes (Include amplitudes),
#   16=DataStaMags    (Include station magnitudes),
#   32=DataArrivals   (Include arrivals),
#   64=DataStaMts     (Include moment tensor station contributions and phase settings),
#  128=DataPreferred  (Include only preferred origin and magnitude information),
QUAKELINK_OPTIONS = 188

# Enable QuakeLink native request format. If enabled the event XML is send
# unfiltered and more information, e.g. author, become available. Also the
# QUAKELINK_OPTIONS will be ignored.
QUAKELINK_NATIVE = False


# Dissemination server connection settings
GDS_HOST = "localhost"
GDS_PORT = 20000
GDS_TIMEOUT = 15



############################################################################
# Django specific settings

# Django base dir, folder holding the manage.py file
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Static directory used in production setup.
# Run 'python manange.py collectstatic' to update this folder.
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'llry53p8t(0+mvk223n(+smyr=6&v#xnrdbpkxp^9n0fvd^zo='

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# Comma-separated list of hostnames this application is available under
ALLOWED_HOSTS = [ ]

# Application definition
INSTALLED_APPS = [
    'gds',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_extensions',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'gds.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'gds.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE'  : 'django.db.backends.mysql',
        #'ENGINE'  : 'django.db.backends.postgresql_psycopg2',
        #'ENGINE': 'django.db.backends.sqlite3',
        #'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        'NAME'    : 'gds',
        'USER'    : 'sysop',
        'PASSWORD': 'sysop',
        'HOST'    : 'localhost',
        'PORT'    : '',
        # MySQL/MariaDB only:
        'OPTIONS': {
            'init_command': "SET sql_mode='STRICT_TRANS_TABLES'",
        },
    }
}


# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# URL base path of static files used in production
STATIC_URL = '/static/'

