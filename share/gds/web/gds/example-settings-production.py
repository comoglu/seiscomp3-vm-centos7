############################################################################
# Copyright (C) 2014 by gempa GmbH                                         #
#                                                                          #
# All Rights Reserved.                                                     #
#                                                                          #
# NOTICE: All information contained herein is, and remains                 #
# the property of gempa GmbH and its suppliers, if any. The intellectual   #
# and technical concepts contained herein are proprietary to gempa GmbH    #
# and its suppliers.                                                       #
# Dissemination of this information or reproduction of this material       #
# is strictly forbidden unless prior written permission is obtained        #
# from gempa GmbH.                                                         #
#                                                                          #
# Author: Stephan Herrnkind                                                #
# Email: herrnkind@gempa.de                                                #
#                                                                          #
############################################################################

import os

from settings import *

# Base URL. Make sure to not use a trailing slash
# Examples: "", "/gds", "http://myserver.com"
FORCE_SCRIPT_NAME = ''

# URL base path of static files used in production
STATIC_URL = '%s/static/' % FORCE_SCRIPT_NAME

# Login and logout urls
LOGIN_URL = '%s/accounts/login' % FORCE_SCRIPT_NAME
LOGOUT_URL = '%s/accounts/logout' % FORCE_SCRIPT_NAME

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

# Comma-separated list of hostnames this application is available under
ALLOWED_HOSTS = [ 'localhost' ]

# Application definition
INSTALLED_APPS = [
    'gds',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
]

# Use a separate file for the secret key. The key must be kept secret in
# production.
# To generate a key issue: python manage.py
keyfile = os.path.join(BASE_DIR, 'gds/secretkey.txt')
try:
    with open(keyfile) as f:
        SECRET_KEY = f.read().strip()
except:
    raise Exception("Could not read secret key file '%s'\n" \
                    "Use the following command to generate this file:\n" \
                    "  python manage.py generate_secret_key > %s" % (
                    keyfile, keyfile))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/
# and run 'python manage.py check --deploy'

# CSRF_COOKIE_SECURE = True
# SESSION_COOKIE_SECURE = True
# X_FRAME_OPTIONS = 'DENY'
# SECURE_SSL_REDIRECT = True
# SECURE_CONTENT_TYPE_NOSNIFF = True
# SECURE_BROWSER_XSS_FILTER = True
