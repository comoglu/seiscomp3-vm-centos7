############################################################################
# Copyright (C) 2013 by gempa GmbH                                         #
#                                                                          #
# All Rights Reserved.                                                     #
#                                                                          #
# NOTICE: All information contained herein is, and remains                 #
# the property of gempa GmbH and its suppliers, if any. The intellectual   #
# and technical concepts contained herein are proprietary to gempa GmbH    #
# and its suppliers.                                                       #
# Dissemination of this information or reproduction of this material       #
# is strictly forbidden unless prior written permission is obtained        #
# from gempa GmbH.                                                         #
#                                                                          #
# Author: Stephan Herrnkind                                                #
# Email: herrnkind@gempa.de                                                #
#                                                                          #
#                                                                          #
# GDS data model                                                           #
#                                                                          #
############################################################################


from __future__ import unicode_literals

from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.db import models
from django.contrib.auth.models import User as AuthUser
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible

from bitfield import BitField
from datetime import datetime

import base64
import re


def validateLatitude(value):
    if value > 90 or value < -90:
        raise ValidationError('%s is not in range (-90, 90)' % value)

def validateLongitude(value):
    if value > 180 or value < -180:
        raise ValidationError('%s is not in range (-180, 180)' % value)

def validateRadius(value):
    if value < 0 or value > 180:
        raise ValidationError('%s is not in range (0, 180)' % value)

def assertMinMax(minValue, maxValue, ctx=""):
    if minValue is not None and maxValue is not None and minValue > maxValue:
        prefix = "" if ctx == "" else "%s: " % ctx
        raise ValidationError("%sminimum value greater than maximum "
                              "value" % prefix)

def validateKeepDays(value):
    if value < 1:
        raise ValidationError('retention value must be >= 1')

def validateRegex(value):
    try:
        re.compile(value)
    except re.error, e:
        raise ValidationError("Regular expression error: %s" % str(e))


nameValidator = RegexValidator(
    regex=re.compile(r'^[\w]+$'),
    message="The field 'name' may only include the following characters: "
            "[a-z], [A-Z], [0-9], _")


################################################################################
@python_2_unicode_compatible
class Region(models.Model):
    name = models.CharField(
        max_length=100, unique=True,
        verbose_name="Name", validators=[nameValidator],
        help_text="A region defines a geographic bounding box, a bounding "
                  "circle, or polygon(s) the for earthquake location must "
                  "match. Polygons are defined in an external BNA file.")
    description = models.CharField(
        max_length=200, blank=True)
    max_lat = models.FloatField(
        null=True, blank=True,
        validators=[validateLatitude],
        verbose_name="BBox: Latitude North")
    min_lat = models.FloatField(
        null=True, blank=True,
        validators=[validateLatitude],
        verbose_name="BBox: Latitude South")
    min_lon = models.FloatField(
        null=True, blank=True,
        validators=[validateLongitude],
        verbose_name="BBox: Longitude West")
    max_lon = models.FloatField(
        null=True, blank=True,
        validators=[validateLongitude],
        verbose_name="BBox: Longitude East",
        help_text="If the value entered here is less than the western "
                  "longitude, the bounding box will cross the date line at "
                  "180/-180")
    lat = models.FloatField(
        null=True, blank=True,
        validators=[validateLatitude],
        verbose_name="BCircle: Latitude")
    lon = models.FloatField(
        null=True, blank=True,
        validators=[validateLongitude],
        verbose_name="BCircle: Longitude")
    radius = models.FloatField(
        null=True, blank=True,
        validators=[validateRadius],
        verbose_name="BCircle: Radius (degree)",
        help_text="The radius is compared to the epicenter distance.")
    bna_file = models.CharField(
        max_length=200, null=True, blank=True,
        verbose_name="BNA Polygon File",
        help_text="File containing one or many polygon definitions in BNA "
                  "format. Hint: Since version 2016.333 SeisComP3 map "
                  "applications allow drawing and exporting of polygons by "
                  "holding the Ctrl key pressed.")
    event_region = models.CharField(
        max_length=200, null=True, blank=True,
        validators=[validateRegex],
        verbose_name="Event Region Name",
        help_text="Region name reported in the event. Regular expressions "
                  "supported, e.g. use '.*Chile.*|*.Peru.*' to filter for "
                  "events in Chile and Peru.")

    class Meta:
        db_table = 'gds_region'

    def __str__(self):
        return self.name

    def clean(self):
        undefined = True
        bb = [ self.max_lat, self.min_lat, self.min_lon, self.max_lon ]
        bc = [ self.lat, self.lon, self.radius ]

        if all(x is not None for x in bb):
            if self.min_lat > self.max_lat:
                raise ValidationError(
                    "BBox: Latitude north must be greater or equal latitude "
                    "south")
            undefined = False
        elif any(x is not None for x in bb):
            raise ValidationError("BBox: Not all parameters defined")

        if all(x is not None for x in bc):
            undefined = False
        elif any(x is not None for x in bc):
            raise ValidationError("BCircle: Not all parameters defined")

        if self.bna_file is not None:
            if len(self.bna_file) > 0:
                undefined = False

        if self.event_region is not None:
            if len(self.event_region) > 0:
                undefined = False

        if undefined:
            raise ValidationError("Neither bounding box, bounding circle, "
                                  "BNA file nor event region defined")


CLEANUP_MODES = (
    (0, 'Complete'),
    (1, 'Bulletins and Recipients'),
    (2, 'Bulletins'),
    (3, 'Recipients')
)


################################################################################
@python_2_unicode_compatible
class Service(models.Model):
    name = models.CharField(
        max_length=100, unique=True,
        validators=[nameValidator],
        verbose_name="Name",
        help_text="A service defines one dissemination path. For the name "
                  "defined here a corresponding entry must exits in the GDS "
                  "server configuration file which defines a spool directory "
                  "as well as content filter and a spooler applications.")
    description = models.CharField(
        max_length=200, blank=True)
    log_bulletins = models.BooleanField(
        default=False,
        verbose_name="Log Bulletins",
        help_text="If enabled, all bulletins sent by this service are logged "
                  "in the database for later review")
    log_cleanup = models.IntegerField(
        choices=CLEANUP_MODES,
        null=True, blank=True,
        verbose_name="Log Cleanup Mode",
        help_text="Defines what log information (if any) is purged during a "
                  "cleanup run.")
    log_keep = models.IntegerField(
        default=30,
        null=True, blank=True,
        validators=[validateKeepDays],
        verbose_name="Log Retention Time",
        help_text="Number of days before a log entry is released for cleanup. "
                  "Note: The clean up is only performed if a cleanup mode is "
                  "selected.")
    ha_redundant = models.BooleanField(
        default=False,
        verbose_name="HA Redundant Output",
        help_text="Multiple GDS instances may operate in high availability "
                  "mode. In such configurations GDS slave instances will only "
                  "trigger if the master instance fails to disseminate an "
                  "event in a given timeout. By setting this parameter to "
                  "'True' the slave instances will no longer wait for the "
                  "master and produce a redundant output.")

    class Meta:
        db_table = 'gds_service'

    def __str__(self):
        return self.name


################################################################################
@python_2_unicode_compatible
class Filter(models.Model):
    name = models.CharField(
        max_length=100, unique=True,
        validators=[nameValidator],
        verbose_name="Name",
        help_text="A custom filter allows to override the default service "
                  "content template on a per subscription bases. For the name "
                  "defined here a corresponding entry must exist in the GDS "
                  "server configuration which defines the content filter "
                  "application.")
    description = models.CharField(
        max_length=200, blank=True)

    class Meta:
        db_table = 'gds_service_filter'

    def __str__(self):
        return self.name


################################################################################
@python_2_unicode_compatible
class ExternalCriterion(models.Model):
    name = models.CharField(
        max_length=100, unique=True,
        validators=[nameValidator],
        verbose_name="Name",
        help_text= "An external criterion allows to define enhanced earthquake "
                   "filtering rules. For the name defined here a corresponding "
                   "entry must exist in the GDS server configuration which "
                   "defines the criterion filter application.")
    description = models.CharField(
        max_length=200, blank=True)

    class Meta:
        db_table = 'gds_external_criterion'
        verbose_name_plural = "external criteria"

    def __str__(self):
        return self.name


SIGNIFICANT_CHANGE_CHOICES = (
    ('evalulationModeChanged', 'Evaluation mode changed'),
    ('evaluationStatusChanged', 'Evaluation status changed'),
    ('focalMechanismAdded', 'Focal mechanism added/removed'),
    ('regionChanged', 'Region changed'),
    ('eventTypeChanged', 'Event type changed'),
)

################################################################################
@python_2_unicode_compatible
class Subscriber(models.Model):
    name = models.CharField(
        _('name'), max_length=30, unique=True,
        validators=[nameValidator],
        help_text="A receiver or a class of receivers used when subscribing to "
                  "queues. A subscriber defines the dissemination order and "
                  "optional delay values. The actual dissemination target, "
                  "e.g. phone number, email address, is defined in the "
                  "subscription.")
    description = models.CharField(
        max_length=200, blank=True)
    queues = models.ManyToManyField(
        'Queue', through='Subscription')
    enabled = models.BooleanField(
        default=True,
        verbose_name="Enabled",
        help_text="Controls if the subscriber and all of its subscription "
                  "are used")

    lst = []
    for i in range(1, 11):
        lst.append((i,i))

    priority = models.IntegerField(
        choices=lst,
        verbose_name="Priority",
        help_text="Defines the dissemination priority (1=lowest, 10=highest)")
    min_delay = models.IntegerField(
        null=True, blank=True,
        verbose_name="Minimum Delay (min)",
        help_text="Number of minutes after the event time which must have been "
                  "elapsed to trigger dissemination")
    max_delay = models.IntegerField(
        null=True, blank=True,
        verbose_name="Maximum Delay (min)",
        help_text="Number of minutes after the event time which must not be "
                  "exceeded to triggering dissemination")
    delta_origin_time = models.FloatField(
        null=True, blank=True,
        verbose_name="Delta Origin Time (sec)",
        help_text="Definition of significant change in origin time, see "
                  "Resend Mode (Subscription)")
    delta_epicenter = models.FloatField(
        null=True, blank=True,
        verbose_name="Delta Epicenter (degree)",
        help_text="Definition of significant change in epicenter, see "
                  "Resend Mode (Subscription)")
    delta_depth = models.FloatField(
        null=True, blank=True,
        verbose_name="Delta Depth (km)",
        help_text="Definition of significant change in origin time, see "
                  "Resend Mode (Subscription)")
    delta_magnitude = models.FloatField(
        null=True, blank=True,
        verbose_name="Delta Magnitude",
        help_text="Definition of significant change in origin time, see "
                  "Resend Mode (Subscription)")
    sig_change = BitField(
        flags=SIGNIFICANT_CHANGE_CHOICES,
        null=True, blank=True,
        verbose_name="Significant Change Properties",
        help_text="Definition of significant change properties, see "
                  "Resend Mode (Subscription)")

    class Meta:
        db_table = 'gds_subscriber'

    def __str__(self):
        return self.name

    def clean(self):
        assertMinMax(self.min_delay, self.max_delay, "Delay")


################################################################################
@python_2_unicode_compatible
class Queue(models.Model):
    name = models.CharField(
        max_length=100, unique=True,
        validators=[nameValidator],
        verbose_name="Name",
        help_text="A queue is the central configuration unit having a set of "
                  "criteria and subscriptions associated to. If any of the "
                  "queue's criteria matches the received earthquake "
                  "information, processing of the configured subscriptions is "
                  "triggered.")
    description = models.CharField(
        max_length=200, blank=True)
    subscribers = models.ManyToManyField(
        'Subscriber', through='Subscription')
    manual = models.BooleanField(
        default=False,
        verbose_name="Manual",
        help_text="If enabled, this queue is not active for automatic "
                  "dissemination and only matches when disseminated manually "
                  "in the Web frontend.")

    class Meta:
        db_table = 'gds_queue'

    def __str__(self):
        return self.name


EVAL_MODE_CHOICES = (
    'UNSET',
    'manual',
    'automatic'
)

EVAL_STATUS_CHOICES = (
    'UNSET',
    'preliminary',
    'confirmed',
    'reviewed',
    'final',
    'rejected',
    'reported'
)

EVENT_TYPE_CHOICES = (
    'UNSET',
    'not existing',
    'not locatable',
    'outside of network interest',
    'earthquake',
    'induced earthquake',
    'quarry blast',
    'explosion',
    'chemical explosion',
    'nuclear explosion',
    'landslide',
    'rockslide',
    'snow avalanche',
    'debris avalanche',
    'mine collapse',
    'building collapse',
    'volcanic eruption',
    'meteor impact',
    'plane crash',
    'sonic boom',
    'duplicate',
    'other',
    'not reported',
    'anthropogenic event',
    'collapse',
    'cavity collapse',
    'accidental explosion',
    'controlled explosion',
    'experimental explosion',
    'industrial explosion',
    'mining explosion',
    'road cut',
    'blasting levee',
    'induced or triggered event',
    'rock burst',
    'reservoir loading',
    'fluid injection',
    'fluid extraction',
    'crash',
    'train crash',
    'boat crash',
    'atmospheric event',
    'sonic blast',
    'acoustic noise',
    'thunder',
    'avalanche',
    'hydroacoustic event',
    'ice quake',
    'slide',
    'meteorite'
)


################################################################################
@python_2_unicode_compatible
class Criterion(models.Model):
    name = models.CharField(
        max_length=100,
        validators=[nameValidator],
        verbose_name="Name",
        help_text="A criterion defines a set of earthquake parameter ranges "
                  "which all must match in order to evaluate the criterion to "
                  "true. A criterion is assigned to one particular queue. If "
                  "any of the queue's criteria evaluates to true, processing "
                  "of the queue's subscriptions is triggered.")
    enabled = models.BooleanField(
        default=True,
        verbose_name="Enabled",
        help_text="Controls if the criterion is currently in use")
    queue = models.ForeignKey(
        Queue,
        verbose_name="Queue")
    region = models.ForeignKey(
        Region,
        null=True, blank=True,
        verbose_name="Region",
        help_text="Geographic region")
    external_criterion = models.ForeignKey(
        ExternalCriterion,
        null=True, blank=True,
        verbose_name="External Criterion",
        help_text="If selected, this external criterion is tested in "
                  "combination with the standard criterion parameters.")
    eval_mode = BitField(
        flags=EVAL_MODE_CHOICES,
        null=True, blank=True,
        verbose_name="Evaluation Mode",
        help_text="Was the earthquake detected purely automatically or was it "
                  "manually revised")
    eval_status = BitField(
        flags=EVAL_STATUS_CHOICES,
        null=True, blank=True,
        verbose_name="Evaluation Status",
        help_text="Origin evaluation status")
    event_type = BitField(
        flags=EVENT_TYPE_CHOICES,
        null=True, blank=True,
        verbose_name="Event Type",
        help_text="Event type")
    min_phases = models.PositiveIntegerField(
        null=True, blank=True,
        verbose_name="Min. Phase Count",
        help_text="Minimum number of stations participating in the earthquake "
                  "location solution")
    max_phases = models.PositiveIntegerField(
        null=True, blank=True,
        verbose_name="Max. Phase Count")
    min_mags = models.PositiveIntegerField(
        null=True, blank=True,
        verbose_name="Min. Magnitude Count",
        help_text="Minimum number of stations participating in the magnitude "
                  "solution")
    max_mags = models.PositiveIntegerField(
        null=True, blank=True,
        verbose_name="Max. Magnitude Count")
    min_magnitude = models.FloatField(
        null=True, blank=True,
        verbose_name="Min. Magnitude")
    max_magnitude = models.FloatField(
        null=True, blank=True,
        verbose_name="Max. Magnitude")
    min_depth = models.FloatField(
        null=True, blank=True,
        verbose_name="Min. Depth")
    max_depth = models.FloatField(
        null=True, blank=True,
        verbose_name="Max. Depth")
    max_rms = models.FloatField(
        null=True, blank=True,
        verbose_name="Max. RMS",
        help_text="Maximum allowed standard error of the earthquake location. "
                  "Typical value range: 0-3")

    class Meta:
        db_table = 'gds_criterion'
        verbose_name_plural = "criteria"
        unique_together = ("name", "queue")

    def __str__(self):
        return self.name

    def clean(self):
        assertMinMax(self.min_phases, self.max_phases, "Phase Count")
        assertMinMax(self.min_mags, self.max_mags, "Magnitude Count")
        assertMinMax(self.min_magnitude, self.max_magnitude, "Magnitude")
        assertMinMax(self.min_depth, self.max_depth, "Depth")


################################################################################
@python_2_unicode_compatible
class Comment(models.Model):
    criterion = models.ForeignKey(Criterion)
    name = models.CharField(
        max_length=100,
        verbose_name="ID")
    string_value = models.CharField(
        max_length=200, blank=True,
        verbose_name="String Value")
    min_value = models.FloatField(
        null=True, blank=True,
        verbose_name="Min. Numeric Value")
    max_value = models.FloatField(
        null=True, blank=True,
        verbose_name="Max. Numeric Value")
    required = models.BooleanField(verbose_name="Required")

    class Meta:
        db_table = 'gds_criterion_comment'
        unique_together = ("name", "criterion")
        verbose_name_plural = "Comment filters. Evaluated against earthquake "\
                              "(event + origin) comments."

    def __str__(self):
        if self.min_value is None and self.max_value is None:
            desc = "empty string value" if self.string_value == "" else \
                   "string value of: %s" % self.string_value
        else:
            desc = "numeric value "
            if self.min_value is None:
                desc += "not greater than %f" % self.max_value
            elif self.max_value is None:
                desc += "greater than %f" % self.min_value
            else:
                desc += "between %f and %f" % (self.min_value, self.max_value)
        return "Comment with ID '%s' having %s" % (self.name, desc);

    def clean(self):
        if self.string_value != "" and not \
           (self.min_value is None and self.max_value is None):
            raise ValidationError("String and numeric values can not be mixed")
        assertMinMax(self.min_value, self.max_value)


RESEND_CHOICES = (
    (0, 'Only once'),     #'Trigger only once per event.'),
    (1, 'On every match'),#'Trigger on every matching event refinement.'),
    (2, 'Once matched'),  #'Trigger on the first matching event refinement and '
                          #'all following once regardless if they match or not.' ),
    (3, 'On sig. change'),#'Trigger on all matching refinements if they '
                          #'introduce a significant change as specified in the '
                          #'subscriber settings.'),
    (4, 'Once M. & SIG'), #'Once matched and on significant change as specified '
                          #'in the subscriber settings.')
)


################################################################################
@python_2_unicode_compatible
class Subscription(models.Model):
    queue = models.ForeignKey(
        Queue,
        help_text="The queue having a set of filter criteria assigned to")
    subscriber = models.ForeignKey(
        Subscriber,
        verbose_name="Subscriber",
        help_text="Receiver or class of receivers which defines the "
                  "dissemination order and optionally delay values")
    service = models.ForeignKey(
        Service,
        verbose_name="Service",
        help_text="The dissemination path to use, e.g. SMS, email.")
    filter = models.ForeignKey(
        Filter,
        null=True, blank=True,
        verbose_name="Custom Filter",
        help_text="Rarely used to override the default service content "
                  "template")
    service_target = models.CharField(
        max_length=100,
        verbose_name="Service Target",
        help_text="The service specific dissemination target, e.g. phone "
                  "number, email address")
    description = models.CharField(
        max_length=200, blank=True)
    resend = models.IntegerField(
        choices=RESEND_CHOICES,
        verbose_name="Resend mode",
        help_text="Defines how event updates are treated.\n"
                  "'Only once' makes sure that a subscription is triggered "
                      "only once per earthquake (given the queue matches "
                      "at all).\n"
                  "'On every match' triggers a dissemination for every "
                      "matching update.\n"
                  "'Once matched' triggers a dissemination for the first "
                      "matching update and all following updates regardless if "
                      "match or not.\n"
                  "'On sig. change' triggers on all matching refinements if "
                      "they introduce a significant change as specified in the "
                      "subscriber settings.\n"
                  "'Once M. & SIG': Once matched and on significant change as "
                      "described above")
    enabled = models.BooleanField(
        default=True,
        verbose_name="Enabled",
        help_text="Controls if the subscription is used")

    class Meta:
        db_table = 'gds_subscription'
        unique_together = ( "queue", "subscriber", "service", "filter",
                            "service_target" )

    def __str__(self):
        return "%s, %s, %s, %s, %s, %s" % (
               self.service, self.queue, self.filter if self.filter else "-",
               self.subscriber, self.service_target,
               self.description if self.description else "-")


################################################################################
@python_2_unicode_compatible
class EventLog(models.Model):
    event_id = models.CharField(
        max_length=100, db_index=True,
        verbose_name="Event ID")
    revision = models.IntegerField(
        null=True, blank=True,
        verbose_name="Revision")
    timestamp = models.DateTimeField(
        db_index=True,
        verbose_name="Dissemination Trigger Time")
    origin_time = models.DateTimeField(
        null=True, blank=True, db_index=True,
        verbose_name="Origin Time")
    origin_time_ms = models.PositiveIntegerField(
        null=True, blank=True,
        verbose_name="Millisecond fraction of origin time")
    latitude = models.FloatField(
        null=True, blank=True,
        verbose_name="Latitude")
    longitude = models.FloatField(
        null=True, blank=True,
        verbose_name="Longitude")
    depth = models.FloatField(
        null=True, blank=True,
        verbose_name="Depth")
    magnitude = models.FloatField(
        null=True, blank=True,
        verbose_name="Magnitude")
    eval_mode = models.PositiveIntegerField(
        null=True, blank=True,
        verbose_name="Evaluation Mode")
    eval_status = models.PositiveIntegerField(
        null=True, blank=True,
        verbose_name="Evaluation Status")
    focmech_available = models.BooleanField(
        default=False,
        verbose_name="Focal Mechanism Available")
    region = models.CharField(
        max_length=100, null=True, blank=True,
        verbose_name="Region Name")
    event_type = models.PositiveIntegerField(
        null=True, blank=True,
        verbose_name="Event Type")
    author = models.CharField(
        max_length=100, db_index=True,
        verbose_name="Author")
    content_type = models.CharField(
        max_length=30, null=True, blank=True,
        verbose_name="Content Type")
    gds_id = models.CharField(
        max_length=100, null=True, blank=True,
        verbose_name="GDS Instance")

    class Meta:
        db_table = 'gds_log_event'
        permissions = (
            ("view_event_log", "Can access event log perspective"),
            ("view_disseminate", "Can access dissemination perspective"),
            ("publish_events", "Can publish events"),
        )

    def __str__(self):
        return "%s - %s" % (self.timestamp, self.event_id)


################################################################################
@python_2_unicode_compatible
class ServiceLog(models.Model):
    event_log = models.ForeignKey(EventLog)
    service = models.CharField(
        max_length=100, db_index=True,
        verbose_name="Service")
    filter = models.CharField(
        max_length=100,
        null=True, blank=True,
        verbose_name="Filter")
    # status: 0=success, 1=pending, 2=warning, 3=error
    status = models.SmallIntegerField(
        verbose_name="Status")

    timestamp = models.DateTimeField(
        null=True, blank=True,
        verbose_name="Spool Time")

    # content: base 64 encoded
    _content = models.TextField(
        db_column="content",
        null=True, blank=True,
        verbose_name="Content")
    def set_content(self, content):
        self._content = base64.encodestring(content)
    def get_content(self):
        if self._content is None or self._content == "": return self._content
        try: return base64.b64decode(self._content)
        except TypeError: return "[Invalid Base64 encoding]"
    content = property(get_content, set_content)

    mime_type = models.CharField(
        max_length=30,
        null=True, blank=True,
        verbose_name="MIME Type")

    # message: base64 encoded
    _message = models.TextField(
        db_column="message",
        null=True, blank=True,
        verbose_name="Message")
    def set_message(self, message):
        self._message = base64.encodestring(message)
    def get_message(self):
        if self._message is None or self._message == "": return self._message
        try: return base64.b64decode(self._message)
        except TypeError: return "[Invalid Base64 encoding]"
    message = property(get_message, set_message)

    class Meta:
        db_table = 'gds_log_service'

    def __str__(self):
        res = self.service
        if not self.filter is None and self.filter != "":
            res += "/" + self.filter
        return res


################################################################################
@python_2_unicode_compatible
class ReceiverLog(models.Model):
    service_log = models.ForeignKey(
        ServiceLog)
    subscriber = models.ForeignKey(
        Subscriber,
        null=True, on_delete=models.SET_NULL)
    target = models.CharField(
        max_length=100, db_index=True,
        verbose_name="Target")
    # Timestamp of sending/failed sending by spooler
    timestamp = models.DateTimeField(
        null=True, blank=True,
        verbose_name="Sent Time")
    error = models.BooleanField(
        default=False,
        verbose_name="Error")

    # Message: base64 encoded
    _message = models.TextField(
        db_column="message",
        null=True, blank=True,
        verbose_name="Message")
    def set_message(self, message):
        self._message = base64.encodestring(message)
    def get_message(self):
        if self._message is None or self._message == "": return self._message
        try: return base64.b64decode(self._message)
        except TypeError: return "[Invalid Base64 encoding]"
    message = property(get_message, set_message)

    class Meta:
        db_table = 'gds_log_recv'

    def __str__(self):
        return self.target

