############################################################################
# Copyright (C) 2014 by gempa GmbH                                         #
#                                                                          #
# All Rights Reserved.                                                     #
#                                                                          #
# NOTICE: All information contained herein is, and remains                 #
# the property of gempa GmbH and its suppliers, if any. The intellectual   #
# and technical concepts contained herein are proprietary to gempa GmbH    #
# and its suppliers.                                                       #
# Dissemination of this information or reproduction of this material       #
# is strictly forbidden unless prior written permission is obtained        #
# from gempa GmbH.                                                         #
#                                                                          #
# Author: Stephan Herrnkind                                                #
# Email: herrnkind@gempa.de                                                #
#                                                                          #
############################################################################

from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views

import views

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', views.index, name="gds-index"),
    url(r'^accounts/login/$', auth_views.login, {'template_name': 'gds/login.html'}, name="login"),
    url(r'^accounts/logout/$', auth_views.logout, name="logout"),
    url(r'^log/$', views.log, name="gds-log"),
    url(r'^log/proc/$', views.log_proc, name="gds-log-proc"),
    url(r'^log/proc/(?P<elogid>\d+)/$', views.log_proc_id, name="gds-log-proc-id"),
    url(r'^log/event/$', views.log_event, name="gds-log-event"),
    url(r'^log/event/(?P<eventid>[\w#.\-%]+)/$', views.log_event_id, name="gds-log-event-id"),
    url(r'^log/service/(?P<slogid>\d+)/$', views.log_service, name="gds-log-service"),
    url(r'^log/service/(?P<slogid>\d+)/content$', views.log_service_content, name="gds-log-service-content"),
    url(r'^publish/$', views.publish, name="gds-publish"),
    url(r'^publish/(?P<eventid>[\w#.\-%]+)/$', views.publish_event, name="gds-publish-event"),
    url(r'^publish/(?P<eventid>[\w#.\-%]+)/(?P<refinement>\d+)/$', views.publish_event_refinement, name="gds-publish-event-refinement"),
    url(r'^preview/$', views.preview_bulletin, name="gds-preview"),
]
