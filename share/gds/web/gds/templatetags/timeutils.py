from datetime import datetime, timedelta
from django import template
from django.utils.translation import ungettext, ugettext

register = template.Library()

def utc_gradient(time):
    diff = datetime.utcnow() - time
    secs = diff.days * 86400 + diff.seconds
    if secs < 0: secs = 0
    if secs > 86400:
        return "xd"
    elif secs > 43200:
        return "t1d"
    elif secs > 21600:
        return "t12h"
    elif secs > 10800:
        return "t6h"
    elif secs > 3600:
        return "t3h"
    elif secs > 1800:
        return "t1h"
    elif secs > 900:
        return "t30m"
    elif secs > 600:
        return "t15m"
    elif secs > 300:
        return "t10m"
    elif secs > 180:
        return "t5m"
    elif secs > 60:
        return "t3m"
    else:
        return "t1m"

register.filter('utc_gradient_class', utc_gradient)


def timesince_utc(d, now=None, reversed=False):
    """
    Takes two datetime objects and returns the time between d and now
    as a nicely formatted string, e.g. "10 minutes".  If d occurs after now,
    then "0 minutes" is returned.

    Units used are years, months, weeks, days, hours, and minutes.
    Seconds and microseconds are ignored.  Up to two adjacent units will be
    displayed.  For example, "2 weeks, 3 days" and "1 year, 3 months" are
    possible outputs, but "2 weeks, 3 hours" and "1 year, 5 days" are not.

    Adapted from http://blog.natbat.co.uk/archive/2003/Jun/14/time_since
    """
    chunks = (
      (60 * 60 * 24 * 365, lambda n: ungettext('year', 'years', n)),
      (60 * 60 * 24 * 30, lambda n: ungettext('month', 'months', n)),
      (60 * 60 * 24 * 7, lambda n : ungettext('week', 'weeks', n)),
      (60 * 60 * 24, lambda n : ungettext('day', 'days', n)),
      (60 * 60, lambda n: ungettext('hour', 'hours', n)),
      (60, lambda n: ungettext('minute', 'minutes', n))
    )
    # Convert datetime.date to datetime.datetime for comparison.
    if not isinstance(d, datetime):
        d = datetime(d.year, d.month, d.day)
    if now and not isinstance(now, datetime):
        now = datetime(now.year, now.month, now.day)
    if not now:
        now = datetime.utcnow()

    delta = (d - now) if reversed else (now - d)
    # ignore microseconds
    since = delta.days * 24 * 60 * 60 + delta.seconds
    if since <= 0:
        # d is in the future compared to now, stop processing.
        return '0 ' + ugettext('minutes')
    for i, (seconds, name) in enumerate(chunks):
        count = since // seconds
        if count != 0:
            break
    s = ugettext('%(number)d %(type)s') % {'number': count, 'type': name(count)}
    if i + 1 < len(chunks):
        # Now get the second item
        seconds2, name2 = chunks[i + 1]
        count2 = (since - (seconds * count)) // seconds2
        if count2 != 0:
            s += ugettext(', %(number)d %(type)s') % {'number': count2, 'type': name2(count2)}
    return s

register.filter('utc_timesince', timesince_utc)
