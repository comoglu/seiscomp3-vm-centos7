from django import template

register = template.Library()

@register.filter
def gds_status_class(status):
    if status == 0: return "success"
    if status == 1: return "pending"
    if status == 2: return "warning"
    return "error"

@register.filter
def gds_string(v, max_len=0):
    if v is None: return "-"
    if max_len > 0 and len(v) > max_len:
        return v[:max_len] + "..."
    return v

@register.filter
def gds_float(v, precision=None):
    if v is None: return "-"
    if precision is None: return str(v)
    return "%0.*f" % (precision, v)

@register.filter
def gds_ms(v, precision=6):
    if v is None or precision <= 0: return ""
    return (".%06d" % v)[:precision+1]


@register.filter
def gds_att_title(v):
    if v is None:
        return None
    return ' title="%s"' % v

