import os

# Django settings for GDS web frontend project

# Base URL. Make sure to not use a trailing slash
# Examples: "", "/gds", "http://myserver.com"
FORCE_SCRIPT_NAME = ''

# Debug settings
DEBUG = False
TEMPLATE_DEBUG = DEBUG

# Number of items per page
PAGE_ITEMS = 25

# Number of recent items used for list based (non calendar) log and disseminate
# perspective
RECENT_ITEMS = 1000


# Database settings
DATABASES = {
    'default': {
        'ENGINE'  : 'django.db.backends.mysql',
        #'ENGINE'  : 'django.db.backends.postgresql_psycopg2',
        'NAME'    : 'gds',
        'USER'    : 'sysop',
        'PASSWORD': 'sysop',
        'HOST'    : '',
        'PORT'    : '',
    }
}

# #############################
# Quakelink connection settings
QUAKELINK_HOST = "localhost"
QUAKELINK_PORT = 18010
QUAKELINK_SSL = False
QUAKELINK_USER = ""
QUAKELINK_PASSWORD = ""

# SQL like WHERE clause to filter the result set.
# clause    := condition[ AND|OR [(]clause[)]]"
# condition := MAG|DEPTH|LAT|LON|PHASES op float |
#              UPDATED|OTIME op time |"
#              AGENCY|STATUS|TYPE|REGION|MAG_T op 'string' |"
#              MAG|DEPTH|LAT|LON|PHASES|OTIME|UPDATED IS [NOT] NULL"
# op        := =|!=|>|>=|<|<=|eq|gt|ge|lt|ge"
# time      := %Y,%m,%d[,%H,%M,%S,%f]"
#
# example:
# QUAKELINK_FILTER = "MAG >= 3 AND (AGENCY = 'test' OR DEPTH < 300)"
QUAKELINK_FILTER = ""

QUAKELINK_TIMEOUT = 10

# QuakeLink options to set right after connecting. The required value type is a
# 2-byte integer mask:
#    1=Defaults       (Use the default options of the QuakeLink server),
#    2=XMLIndent      (Indent XML),
#    4=DataPicks      (Include picks),
#    8=DataAmplitudes (Include amplitudes),
#   16=DataStaMags    (Include station magnitudes),
#   32=DataArrivals   (Include arrivals),
#   64=DataStaMts     (Include moment tensor station contributions and phase settings),
#  128=DataPreferred  (Include only preferred origin and magnitude information),
QUAKELINK_OPTIONS = 188

# Enable QuakeLink native request format. If enabled the event XML is send
# unfiltered and more information, e.g. author, become available. Also the
# QUAKELINK_OPTIONS will be ignored.
QUAKELINK_NATIVE = False




# Dissemination server connection settings
GDS_HOST = "localhost"
GDS_PORT = 20000
GDS_TIMEOUT = 15

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Berlin'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

# ID used to distinguish multiple apps in one django server
SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# Absolute path to directory containing this file
BASE_DIR = os.path.abspath(os.path.dirname(__file__))

# Absolute path to the directory that holds media.
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

# Absolute path to the directory that holds static files
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
MEDIA_URL = '%s/media/' % FORCE_SCRIPT_NAME
STATIC_URL = '%s/static/' % FORCE_SCRIPT_NAME

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'v+77e0!=e7-v!@^54ux=6#epyimzy8iqgc$=!&6_g_0%m)3jwf'

LOGIN_URL = '/login/'
LOGOUT_URL = '/logout/'
LOGIN_REDIRECT_URL = '/'


AUTH_PROFILE_MODULE = "gds.User"

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

ROOT_URLCONF = '%s.urls' % os.path.basename(BASE_DIR)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.i18n',
    'django.core.context_processors.request',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.contrib.messages.context_processors.messages',
)

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, "templates"),
)



STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

INSTALLED_APPS = (
#    'grappelli',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.admin',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'gds',
    'gds.contrib.frontend',
)

