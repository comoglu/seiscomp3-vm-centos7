#!/bin/bash

# Eastern Pacific, Eastern Australia (EPEA)
scrttv -u scrttEPNA --resortAutomatically="false" --autoApplyFilter="false" --maxDelay="21600" --streams.region.lonmin="129" --streams.region.lonmax="179.9" --streams.region.latmin="-89" --streams.region.latmax="89" --streams.sort.latitude="89" --streams.sort.longitude="160.5" --streams.codes="AM.*.*.?HZ,AT.*.*.?HZ,AU.*.*.?HZ,AU.*.*.?HZ,G.*.*.?HZ,GE.*.*.?HZ,GT.*.*.?HZ,IA.*.*.?HZ,IC.*.*.?HZ,II.*.*.?HZ,IU.*.*.?HZ,JP.*.*.?HZ,ND.*.*.?HZ,NZ.*.*.?HZ,NZ.*.*.?HZ,PS.*.*.?HZ,NN.*.*.?HZ,BK.*.*.?HZ,PB.*.*.?HZ,MI.*.*.?HZ" &

