#!/usr/bin/env python

import sys
import requests
import boto3

"""
Script to retrieve the Ids of the subnet and security group that wphase
instances will be launched into and with respectively. The Ids of the subnet
and security group will be printed to standard out in that order on a single
line separated by a space.

This script is called from ./finalisze_setup.sh. The first argument should be
the name of the secuirty group.
"""

METADATA_URL = r'http://169.254.169.254/latest/meta-data/'
macs = requests.get(METADATA_URL + 'network/interfaces/macs/').text
vpc_id = requests.get(METADATA_URL + 'network/interfaces/macs/' + macs + 'vpc-id').text
subnet_id = requests.get(METADATA_URL + 'network/interfaces/macs/' + macs + 'subnet-id').text

res = boto3.client('ec2').describe_security_groups(
    Filters = [
        {
            'Name': 'vpc-id',
            'Values': [vpc_id]
        },
        {
            'Name': 'group-name',
            'Values': [sys.argv[1]]
        }
    ]
)

print '{} {}'.format(subnet_id, res['SecurityGroups'][0]['GroupId'])
