#!/bin/bash

#-------------------------------------------------------------------------------
# Script that 'rolls' the current instance. This is called from
# https://bitbucket.org/geoscienceaustralia/sc3-infrastructure/src/master/stack/roll_stack.sh
#-------------------------------------------------------------------------------

source /opt/sc3_install/setup_env.sh

success=true

if [[ "$SHORT_HOST_NAME" =~ "proc" ]]; then
    THIS_HOSTS_AGENCY_BLACK_LIST="$THIS_HOSTS_AGENCY_ID"

    if [ "$1" == "1" ]; then
        # update the environment variables in .bashrc
        sed -i 's|\(CAPS2CAPS_INPUT_HOST_NAME=\).*$|\1localhost|' ${SETUP_SCRIPT_DIR}/setup_env.sh
        sed -i 's|\(CAPS2CAPS_OUTPUT_HOST_NAME=\).*$|\1$SECONDARY_PROC_HOST_NAME|' ${SETUP_SCRIPT_DIR}/setup_env.sh
        sed -i 's|\(QL_SECONDARY_PROC_HOST_NAME=\).*$|\1$SECONDARY_PROC_HOST_NAME|' ${SETUP_SCRIPT_DIR}/setup_env.sh
        if [ "$SEISCOMP_MACHINE_NUMBER" == "1" ]; then
            THIS_HOSTS_AGENCY_BLACK_LIST="$THIS_HOSTS_AGENCY_BLACK_LIST",GA
        fi
    else
        sed -i 's|\(CAPS2CAPS_INPUT_HOST_NAME=\).*$|\1$PRE_ROLL_SECONDARY_PROC_HOST_NAME|' ${SETUP_SCRIPT_DIR}/setup_env.sh
        sed -i 's|\(CAPS2CAPS_OUTPUT_HOST_NAME=\).*$|\1localhost|' ${SETUP_SCRIPT_DIR}/setup_env.sh
        sed -i 's|\(QL_SECONDARY_PROC_HOST_NAME=\).*$|\1$PRE_ROLL_SECONDARY_PROC_HOST_NAME|' ${SETUP_SCRIPT_DIR}/setup_env.sh
    fi

    # source this a second time to reflect changes
    source ${SETUP_SCRIPT_DIR}/setup_env.sh

    # change the blacklist for ql2sc
    if [ "$SEISCOMP_MACHINE_NUMBER" == "1" ]; then
        sed -i "s/processing\.blacklist\.agencies.*$/processing.blacklist.agencies = $THIS_HOSTS_AGENCY_BLACK_LIST/g" \
            ${SEISCOMP_HOME_FOLDER}/etc/ql2sc.cfg
    fi

    seiscomp update-config ql2sc caps2caps > /dev/null 2> /dev/null
    seiscomp restart > /dev/null 2> /dev/null
else
    if [ "$1" == "1" ]; then
        seiscomp enable gds
    else
        seiscomp disable gds
    fi

    if [[ $SHORT_HOST_NAME == gds2 ]]; then
        # NOTE: This must be kept in sync with finalise_setup.sh
        # TODO: define this in /opt/sce_install/setup_env.sh
        events_directory=$SEISCOMP_HOME_FOLDER/var/lib/quakelink/s3

        if mount | grep $events_directory > /dev/null; then
            # if $1 == 1, then we may not need to do this but this makes the script
            # easier to understand and avoids lots of checks and/or assumptions
            fusermount -u $events_directory
        else
            # recreate the directory that quakelink will use for XML files.
            rm -rf "$events_directory"
            mkdir -p "$events_directory"
        fi

        if [ "$1" == "1" ]; then
            # mount the events bucket as a file system. See the setup script in
            # packer for more discussion on the choice of tools.
            yas3fs s3://"$S3_EVENTS_BUCKET_NAME" "$events_directory"

            if ! (mount | grep $events_directory > /dev/null); then
                success=false
            else
                sudo systemctl enable lsyncd
                sudo systemctl start lsyncd
                # TODO: check that lsyncd is running
            fi
        else
            sudo systemctl disable lsyncd
            sudo systemctl stop lsyncd
            # TODO: check that lsyncd is not running

            # check that we don't have the S3 bucket mounted. If we do, we have failed.
            if mount | grep $events_directory > /dev/null; then
                success=false
            fi
        fi

    fi

    seiscomp update-config # supposedly not required, but...
fi

seiscomp restart
exit $($success)
