#!/usr/bin/env python
# -*- coding: utf-8 -*-

############################################################################
# Copyright (C) 2016 by gempa GmbH                                         #
#                                                                          #
# All Rights Reserved.                                                     #
#                                                                          #
# NOTICE: All information contained herein is, and remains                 #
# the property of gempa GmbH and its suppliers, if any. The intellectual   #
# and technical concepts contained herein are proprietary to gempa GmbH    #
# and its suppliers.                                                       #
# Dissemination of this information or reproduction of this material       #
# is strictly forbidden unless prior written permission is obtained        #
# from gempa GmbH.                                                         #
#                                                                          #
#  Author: Bernd Weber, Enrico Elllguth                                    #
#  Email: weber@gempa.de, enrico.elllguth@gempa.de                         #
############################################################################


import os
import sys

def main():
	if len(sys.argv) < 2:
		print >> sys.stderr, "Usage: event.py <text> <new_event> <eventID> <arrivalCount> <magnitude>"
		return 1

	# Store command line arguments for later usage
	text=sys.argv[1]
	newEvent=sys.argv[2]
	eventID=sys.argv[3]
	arrivalCount=int(sys.argv[4])
	magnitude=float(sys.argv[5])

	# Check wether it is a new event or an update of an existing one.
	if newEvent=="1":
		print >> sys.stdout, "Processing new event %s" % eventID
		newText=text.replace(",",", ")

		# Text to speech
		command="echo '" + newText + "' | festival --tts;"
		os.system(command)

		# Play different sound for different magnitudes
		if magnitude>=6.0:
			os.system("aplay /opt/seiscomp3/share/client/SEWSLong.wav")
		elif magnitude>=5.0:
			os.system("aplay /opt/seiscomp3/share/client/BeethovenMoltoVivace.wav")
		elif magnitude>=2.0:
			os.system("aplay /opt/seiscomp3/share/client/SEWSShort.wav")
	else:
		print >> sys.stdout, "Updated Event, do something else"

if __name__ == "__main__":
	sys.exit(main())
