function setupseiscompandcreatedb() {
    # $1 db root user password
    # $2 seiscomp db read/write user name
    # $3 seiscomp db read/write user password
    # $4 seiscomp db read only user name
    # $5 seiscomp db read only user password
    # $6 RDS endpoint
    # $7 machine number

    template='spawn seiscomp setup
        expect "Agency ID*" { send GA'"$7"'\r }
        expect "Datacenter ID*" { send GA'"$7"'\r }
        expect "Organization string*:" { send "Geoscience Australia\r" }
        expect "Enable database storage*:" { send \r }
        expect "*Database backend*" { send 0\r }
        expect "Create database*" { send yes\r }
        expect "MYSQL root password (input not echoed)*" { send '"$1"'\r }
        expect "Drop existing database*" { send yes\r }
        expect "Database name*" { send seiscomp3'"$7"'\r }
        expect "Database hostname*" { send '"$6"'\r }
        expect "Database read-write user*" { send '"$2"'\r }
        expect "Database read-write password*" { send '"$3"'\r }
        expect "Database public hostname*" { send '"$6"'\r }
        expect "Database read-only user*" { send '"$4"'\r }
        expect "Database read-only password*" { send '"$5"'\r }
        expect "*Command?" { send \r }
        expect eof { puts "done with auto configuration" }'

    echo "$template" | /usr/bin/expect -
}

function setupseiscompnocreatedb() {
    # $1 seiscomp db read/write user name
    # $2 seiscomp db read/write user password
    # $3 seiscomp db read only user name
    # $4 seiscomp db read only user password
    # $5 RDS endpoint
    # $6 machine number

    template='spawn seiscomp setup
        expect "Agency ID*" { send GA'"$6"'\r }
        expect "Datacenter ID*" { send GA'"$6"'\r }
        expect "Organization string*:" { send "Geoscience Australia\r" }
        expect "Enable database storage*:" { send \r }
        expect "*Database backend*" { send 0\r }
        expect "Create database*" { send no\r }
        expect "Database name*" { send seiscomp3'"$6"'\r }
        expect "Database hostname*" { send '"$5"'\r }
        expect "Database read-write user*" { send '"$1"'\r }
        expect "Database read-write password*" { send '"$2"'\r }
        expect "Database public hostname*" { send '"$5"'\r }
        expect "Database read-only user*" { send '"$3"'\r }
        expect "Database read-only password*" { send '"$4"'\r }
        expect "*Command?" { send \r }
        expect eof { puts "done with auto configuration" }'

    echo "$template" | /usr/bin/expect -
}

# this is only required on standalone instances.
function mysqlsecureinstallation() {
    # $1 db root user password

    template='spawn mysql_secure_installation
        expect "Enter current password for root*" { send \r }
        expect "Set root password*" { send \r }
        expect "New password*" { send '"$1"'\r }
        expect "Re-enter new password*" { send '"$1"'\r }
        expect "Remove anonymous users*" { send \r }
        expect "Disallow root login remotely*" { send \r }
        expect "Remove test database and access to it*" { send \r }
        expect "Reload privilege tables now*" { send \r }
        expect eof { puts "done with auto configuration" }'

    # mysql_secure_installation writes a file to the current directory, so
    # make sure that is possible
    thecurrentdirectoryalkjhdlkh=`pwd`
    cd /tmp
    echo "$template" | /usr/bin/expect -
    cd "$thecurrentdirectoryalkjhdlkh"
}
