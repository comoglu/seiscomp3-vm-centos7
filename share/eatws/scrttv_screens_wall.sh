#!/bin/bash

# Europe, Africa, Central Asia (EACA)
scrttv -u rttvEACA --resortAutomatically="false" --autoApplyFilter="false" --bufferSize="1800" --streams.region.lonmin="-35" --streams.region.lonmax="90" --streams.region.latmin="-89" --streams.region.latmax="89" --streams.sort.latitude="89" --streams.sort.longitude="28" -v &

# South East Asia, Indonesia, Western Australia (AIWA)
scrttv -u rttvAIWA --resortAutomatically="false" --autoApplyFilter="false" --bufferSize="1800" --streams.region.lonmin="90" --streams.region.lonmax="129" --streams.region.latmin="-89" --streams.region.latmax="89" --streams.sort.latitude="89" --streams.sort.longitude="115.5" --streams.codes="MY.K??.*.?HZ,AU.*..?HZ,AU.PSA00.*.BHZ,AU.MTKN.*.?HZ,AU.CARL.*.?HZ,IA.*.*.?HZ,GE.*.*.BHZ,IM.*.*.BHZ,II.*.*.BHZ,G.*.*.BHZ,IU.*.*.BHZ,JP.*.*.BHZ,TW.*.*.BHZ" -v &

# Eastern Pacific, Eastern Australia (EPEA)
scrttv -u scrttEPNA --resortAutomatically="false" --autoApplyFilter="false" --bufferSize="1800" --streams.region.lonmin="129" --streams.region.lonmax="179.9" --streams.region.latmin="-89" --streams.region.latmax="89" --streams.sort.latitude="89" --streams.sort.longitude="160.5" --streams.codes="AU.*..?HZ,AU.NTLH.*.?HZ,AU.NAPP.*.?HZ,AU.TWOA.*.?HZ,AU.CORO.*.?HZ,AU.GEXS.*.?HZ,AU.CATI.*.?HZ,AU.YARR.*.?HZ,AU.WTPK.*.?HZ,AU.DRS.*.?HZ,AU.CN2S.*.?HZ,AU.BW2s.*.?HZ,AU.RL2S.*.?HZ,IA.*.*.?HZ,GE.*.*.BHZ,IM.*.*.BHZ,II.*.*.BHZ,G.*.*.BHZ,IU.*.*.BHZ,JP.*.*.BHZ,TW.*.*.BHZ,NZ.*.*.?HZ,GT.*.*.?HZ,ND.*.*.?HZ," -v &

# Americas (NSA)
scrttv -u scrttNSA --resortAutomatically="false" --autoApplyFilter="false" --bufferSize="1800" --streams.region.lonmin="-179.9" --streams.region.lonmax="-35" --streams.region.latmin="-89" --streams.region.latmax="89" --streams.sort.latitude="89" --streams.sort.longitude="-107.5" -v &
