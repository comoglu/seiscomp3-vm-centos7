#!/usr/bin/env python

import os
import sys
import shutil
import logging
from datetime import datetime, timedelta

"""
Script to load the recent quakelink history (from S3) into this machine. On the
GDS, this is required to populate gaps. This is called on launch from
[finalise_setup.sh](./finalise_setup.sh).
"""

PROC_HOST = os.environ['SHORT_HOST_NAME'].startswith('proc')
HOME = os.path.expanduser("~")
TIME_WINDOW_IN_DAYS = 15 if PROC_HOST else 5
SOURCE_S3_BUCKET = 'sc3-quakelink'
TMP_DIR = os.path.sep + os.path.join('tmp', 'starting-archive')
ZIP_PREFIX = 'ga'



logging.basicConfig(
    filename=os.path.join(HOME, 'load_quakelink.log'),
    level=logging.DEBUG)
logger = logging.getLogger('load-quakelink')



def seiscomp(action):
    os.system("""
    seiscomp {} \
        l1autoloc l1autopick  l1scanloc  l1sceval  l1screloc  scamp  scanloc \
        scautoloc scautomt  scautopick  scautowp  sceval  scevent  scmag  scqc \
        ql2sc
    """.format(action))



def retrieve_days_events(year, month, day):
    """
    Copy the events for the specified dat from S3 to this machine.
    """
    tostr = lambda x: '{}{}'.format('' if x > 9 else '0', x)
    source = 's3://{}/{}/{}/{}'.format(
        SOURCE_S3_BUCKET,
        year,
        tostr(month),
        tostr(day))

    dest = os.path.join(
        TMP_DIR,
        str(year),
        tostr(month),
        tostr(day))

    try:
        os.makedirs(dest)
        # It was surprisingly not obvious how to do this concisely with boto.
        os.system('aws s3 cp --recursive {} {} > /dev/null'.format(source, dest))
    except Exception as e:
        logger.error('{} => {} failed: {}'.format(source, dest, str(e)))



def unzip_and_dispatch(root, fn):
    fn = os.path.join(root, fn)
    try:
        logger.info('merging: {}'.format(fn))
        ofn = fn[:-3]
        os.system('gunzip -c {} > {}'.format(fn, ofn))
        return ofn
    except Exception as e:
        logger.error(str(e))


def unzip(root, fn):
    """
    Unzip *fn*.

    These files are zipped by Gempa and we use gunzip (via :py:func:`os.system`
    to do this.
    """

    fn = os.path.join(root, fn)
    try:
        logger.info('extracting: {}'.format(fn))
        ofn = fn[:-3]
        os.system('gunzip -c {} > {}'.format(fn, ofn))
        return ofn
    except Exception as e:
        logger.error(str(e))


unzipper = unzip_and_dispatch if PROC_HOST else unzip
def do_load(day, keepOutput):
    """
    Retrieve a days events and load them.

    The files from each event are combined using a utility written specifically
    for us by Gempa. The resulting file is then pused to quakelink using
    *qlpush*.
    """

    retrieve_days_events(day.year, day.month, day.day)
    for root, dirs, files in os.walk(TMP_DIR):
        fns = (unzipper(root, f) for f in sorted(files) \
                if (f.startswith(ZIP_PREFIX) and f.endswith('gz')))
        fns = [f for f in fns if f is not None]
        if len(fns):
            # then we are in the directory for a specific event
            os.system('mergeep {}/*.xml > /tmp/merged.xml'.format(root))
            if PROC_HOST:
                os.system(
                    'scdb -i /tmp/merged.xml '
                    '-d mysql://${PROC_MYSQL_SC3_USER}:${PROC_MYSQL_SC3_PASSWORD}@${SEISCOMP_RDS_ENDPOINT}/seiscomp3${SEISCOMP_MACHINE_NUMBER} '
                    '-b 1000 > /dev/null')
                if keepOutput:
                    shutil.copyfile(
                        '/tmp/merged.xml',
                        os.path.join(OUTPUT_DIR, '{}.xml'.format(os.path.basename(root))))

            else:
                os.system('qlpush  -H localhost /tmp/merged.xml')
    shutil.rmtree(TMP_DIR)



if __name__ == "__main__":
    if PROC_HOST:
        if len(sys.argv) > 1:
            from dateutil.parser import parse
            then = parse(sys.argv[1])
            now = parse(sys.argv[2])
            print now
            print then
            keepOutput = True
            OUTPUT_DIR = os.path.join(os.path.expanduser('~'), 'ims-builetin')
            if os.path.exists(OUTPUT_DIR):
                print 'output directory exists, please delete or move, or ... it'
                sys.exit(1)
            os.makedirs(OUTPUT_DIR)

        else:
            now = datetime.utcnow()
            then = now - timedelta(days=TIME_WINDOW_IN_DAYS)
            keepOutput = False
    else:
        now = datetime.utcnow()
        then = now - timedelta(days=TIME_WINDOW_IN_DAYS)
        keepOutput = None

    try:
        shutil.rmtree(TMP_DIR)
    except:
        pass

    # load data
    while now >= then:
        do_load(now, keepOutput)
        now -= timedelta(days=1)
