#!/bin/bash

# Europe, Africa, Central Asia (EACA)
scrttv -u rttvEACA --resortAutomatically="false" --autoApplyFilter="false" --maxDelay="21600" --streams.region.lonmin="-35" --streams.region.lonmax="90" --streams.region.latmin="-89" --streams.region.latmax="89" --streams.sort.latitude="89" --streams.sort.longitude="28" --streams.codes="AF.*.*.?HZ,AU.*.*.?HZ,G.*.*.?HZ,GE.*.*.?HZ,GT.*.*.?HZ,II.*.*.?HZ,IN.*.*.?HZ,IU.*.*.?HZ,KZ.*.*.?HZ,MN.*.*.?HZ,PL.*.*.?HZ,PM.*.*.BHZ,NN.*.*.?HZ,BK.*.*.?HZ,PB.*.*.?HZ,MI.*.*.?HZ"  &

