#!/usr/bin/env python

import sys
import base64
from seiscomp3 import DataModel, Logging, IO, Core
from seiscomp3.Client import Application



class App(Application):
    def __init__(self, argc, argv):
        Application.__init__(self, argc, argv)

        # enable messaging support
        self.setMessagingEnabled(True)

        # disable database access
        self.setDatabaseEnabled(False, False)

        # the application name exceeds the maximum allowed of spread so we use an abbreviation
        self.setMessagingUsername("impttwcga")

        # send all objects to the location mechanism group
        self.setPrimaryMessagingGroup("LOCATION")

        # initialize members
        self.output = ""


    def createCommandLineDescription(self):
        """
        SC3 specific method.
        """

        self.commandline().addGroup("Output")
        self.commandline().addStringOption("Output", "file,o", "File to write output to. By default output is send via messing system")

        self.commandline().addGroup("Event")
        self.commandline().addStringOption("Event", "lat",     "Latitude of the event.")
        self.commandline().addStringOption("Event", "lon",     "Longitude of the event.")
        self.commandline().addStringOption("Event", "depth",   "Depth of the event.")
        self.commandline().addStringOption("Event", "evtime",  "Epoch time of the event.")
        self.commandline().addStringOption("Event", "magtype", "The type of magnitude.")
        self.commandline().addStringOption("Event", "mag",     "The magnitude.")
        self.commandline().addStringOption("Event", "agency",  "Agency time of the event.")


    def printUsage(self):
        """
        SC3 specific method.
        """

        print "\nimport_ptwc - imports ptwc events\n"
        Application.printUsage(self)


    def init(self):
        """
        SC3 specific method.

        Returning False means that we do not enter the SeisComP3 run loop.
        """

        if Application.init(self) == False:
            return False

        lat = float(self.commandline().optionString("lat"))
        lon = float(self.commandline().optionString("lon"))
        depth = float(self.commandline().optionString("depth"))
        evtime =  int(self.commandline().optionString("evtime"))
        magtype =  self.commandline().optionString("magtype")
        mag =  float(self.commandline().optionString("mag"))
        agency = self.commandline().optionString("agency")


        # create SeiscomP3 objects
        (derivedOrigin, mag) = self.createObjects(lat, lon, depth, evtime, magtype, mag, agency)

        # write output to file or send it via messaing
        if self.output:
            self.writeXML(derivedOrigin, mag)
        else:
            self.sendObjects(derivedOrigin, mag)

        return False


    def createObjects(self, lat, lon, depth, evtime, magtype, magnitude, agency):
        # get current time in UTC
        time = Core.Time.GMT()

        # create creation info
        ci = DataModel.CreationInfo()
        ci.setCreationTime(time)
        ci.setAgencyID(agency)
        ci.setAuthor(agency)

        originTime = DataModel.TimeQuantity(Core.Time(float(evtime)))

        # fill derived origin
        derivedOrigin = DataModel.Origin.Create()
        derivedOrigin.setCreationInfo(ci)
        derivedOrigin.setTime(originTime)
        derivedOrigin.setLatitude(DataModel.RealQuantity(lat))
        derivedOrigin.setLongitude(DataModel.RealQuantity(lon))
        derivedOrigin.setDepth(DataModel.RealQuantity(depth))
        derivedOrigin.setEvaluationMode(DataModel.AUTOMATIC)
        derivedOrigin.setEvaluationStatus(DataModel.REVIEWED)

	# add the origin reference
	originRef = DataModel.OriginReference()
	originRef.setOriginID(derivedOrigin.publicID())

        # fill magnitude
        mag = DataModel.Magnitude.Create()
        mag.setMagnitude(DataModel.RealQuantity(magnitude))
        mag.setCreationInfo(ci)
        mag.setOriginID(derivedOrigin.publicID())
        mag.setType(magtype)
        derivedOrigin.add(mag)

        return (derivedOrigin, mag)


    def sendObjects(self, origin, mag):
        wasEnabled = DataModel.Notifier.IsEnabled()
        try:
            # create event parameter set
            ep = DataModel.EventParameters()

            # watch for notifiers
            DataModel.Notifier.SetEnabled(True)

            # add origin to event parameter set
            ep.add(origin)

            # serialize objects
            msg = DataModel.Notifier.GetMessage()

            # forward message to the messaging system
            self.connection().send(msg)
            Logging.info("sent location successfully")

            return True
        except Exception, e:
            Logging.warning("caught unexpected error in sending message to messaging system %s" % str(e))
            return False
        finally:
            DataModel.Notifier.SetEnabled(wasEnabled)


    def writeXML(self, origin, mag):
        try:
            # create SeisComP3 XMl Archive used to serialize objects
            ar = IO.XMLArchive()

            # enable formatted output
            ar.setFormattedOutput(True)

            # try to create the output file
            ar.create(self.output)

            # Serialize the objects
            ar.writeObject(origin)
            ar.writeObject(mag)
            ar.close()
            Logging.info("Stored objects as %s" %self.output)

            return True
        except Exception, e:
            Logging.warning("failed to write caught unexpected error in XML %s" % str(e))
            return False


    def validateParameters(self):
        """
        SeisComP3 specific method.
        """

        if Application.validateParameters(self) == False:
            return False

        try:
            # this will raise if the option is not available, in which case
            # the call to setMessagingEnabled(false) will not happen.
            self.output = self.commandline().optionString("file")

        except:
            pass
        else:
            # In case of the output should be written to a file we disable the # messaing.
            self.setMessagingEnabled(False)

        return True



def main(argv):
    app = App(len(argv), argv)
    return app()



if __name__ == "__main__":
    sys.exit(main(sys.argv))
