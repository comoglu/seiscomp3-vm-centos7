[back to parent](../../README.md)

# Scripts and utilities developed for use in the EATWS.


## From the acquistion branch.

- ***[cleanup_ac3_db.sh](./cleanup_ac3_db.sh)***: Script for removing old data
  from the SeisComP3 database. This is run on a cron job that is created in
  *[finalise_setup_proc.sh](./finalise_setup_proc.sh)* when an instance is
  launched.

- ***[finalise_setup_proc.sh](./finalise_setup_proc.sh)***: Do most of the launch time
  configuration of the current instance on launch. This is called from
  *[setup_gds.sh](https://bitbucket.org/geoscienceaustralia/sc3-infrastructure/src/master/packer/files/proc/setup_proc.sh)*
  on launch.

- ***[finalise_setup_acq.sh](./finalise_setup_acq.sh)***: The same as
  *finalise_setup_proc.sh*, but for an acquisition machine. This was created
  when building the acquistion stack but has never been used.

- ***[import_ptwc.py](./import_ptwc.py)***: Imports a PTWC event. This is called
  by a script that lives on a machine inside GA. Please see
  *[Sending PTWC Events](https://bitbucket.org/geoscienceaustralia/eatws-docs/raw/master/run_books/sending_ptwc_events.md)*
  for more details on how this works.

- ***[load_quakelink.py](./load_quakelink.py)***: Load the recent quakelink
  history (from S3) into this machine. This is required to populate the
  SeisComP3 database with recent data. This is called on launch from
  [finalise\_setup.sh](./finalise_setup.sh).

- ***[run_playback.sh](./run_playback.sh)***: Runs a playback. This is called
  from a machine that provides the faux seedlink server for a playback.

- ***[seiscomp_setup.sh](./seiscomp_setup.sh)***: (bash) functions used for
  setting up the seiscomp database when the stack is launched. These are
  called from *[finalise_setup_proc.sh](./finalise_setup_proc.sh)* when a stack
  is launched.

- ***[setup_playback.sh](./setup_playback.sh)***: Configures a machine for
  playback. This script is called from *[run_playback.sh](./run_playback.sh)*
  on the machine providing the waveform data via SSH.

- ***[roll.sh](./roll.sh)***: 'Rolls' the current instance. This is called from
  [roll\_stack.sh](https://bitbucket.org/geoscienceaustralia/sc3-infrastructure/src/master/stack/roll_stack.sh).

- ***[aiwa.sh](./aiwa.sh)***: Wall screen script.

- ***[eaca.sh](./eaca.sh)***: Wall screen script.

- ***[nsa.sh](./nsa.sh)***: Wall screen script.

- ***[epea.sh](./epea.sh)***: Wall screen script.

## From the GDS branch

Further documentation is available, in most cases, in the scripts themselves.

- ***[change_gds_password.sh](./change_gds_password.sh)***: (bash) shell script
  that changes the GDS password. Called from
  [roll\_stack.sh](https://bitbucket.org/geoscienceaustralia/sc3-infrastructure/src/master/stack/roll_stack.sh).

- ***[get_wphase_sg_id.py](./get_wphase_sg_id.py)***: Get the subnet and
  security groups to be used by wphase instances. This is called on launch from
  [finalise\_setup.sh](./finalise_setup.sh).

- ***[httpd.conf(./httpd.conf)***: The (Apache 2) httpd configuration to be used
  on GDS instances. This is copied to */etc/httpd/conf/httpd.conf* on launch by
  [finalise\_setup.sh](./finalise_setup.sh).

- ***[load_quakelink.py](./load_quakelink.py)***: Load the recent quakelink
  history (from S3) into this machine. On the GDS, this is required to populate
  gaps. This is called on launch from [finalise\_setup.sh](./finalise_setup.sh).

- ***[setup_playback.sh](./setup_playback.sh)***: Configure the current instance
  for playback. This gets called from *run\_playback.sh* on a proc machine via
  SSH.

- ***[finalise_setup.sh](./finalise_setup.sh)***: Do most of the launch time
  configuration of the current instance on launch. This is called from
  *[setup_gds.sh](https://bitbucket.org/geoscienceaustralia/sc3-infrastructure/src/master/packer/files/gds/setup_gds.sh)*
  on launch.

- ***[inventory_filter.ini](./inventory_filter.ini)***: Used to determine which
  networks are restricted (i.e. required username and password for access) in
  fdsn. This file is referenced from /opt/seiscomp3/etc/fdsnws.cfg.

- ***[roll.sh](./roll.sh)***: 'Rolls' the current instance. This is called from
  [roll\_stack.sh](https://bitbucket.org/geoscienceaustralia/sc3-infrastructure/src/master/stack/roll_stack.sh).
