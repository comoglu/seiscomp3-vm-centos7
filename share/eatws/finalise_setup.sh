#!/bin/bash

#-------------------------------------------------------------------------------
# Do most of the launch time configuration of an instance.
# - This gets called from https://bitbucket.org/geoscienceaustralia/sc3-infrastructure/src/master/packer/files/gds/setup_gds.sh.
# - It assumes that the environment has been setup (i.e. the file /opt/sc3_install/setup_env.sh has been sourced).
#-------------------------------------------------------------------------------

mkdir -p /$SEISCOMP_HOME_FOLDER/var/run
mkdir -p /$SEISCOMP_HOME_FOLDER/var/lib/eqevents/spool

# remove the greens functions. This line should be removed when we puth Greans on EFS
rm -rf $EC2_USER_HOME_FOLDER/data/greensfunctions

#-------------------------------------------------------------------------------
# configuration configuration
#-------------------------------------------------------------------------------
# escape & so sed works (by default substitutes the whole match)
NSW_WATER_ACCESS_KEY="${NSW_WATER_ACCESS_KEY//&/\\&}"

# escape % for the python config parser
NSW_WATER_ACCESS_KEY="${NSW_WATER_ACCESS_KEY//%/%%}"

# get the id of the security group to use for wphase instances
wphase_subnet_and_sg=($("$SEISCOMP_HOME_FOLDER"/share/eatws/get_wphase_sg_id.py "$WPHASE_SG_NAME"))
wphase_snet_id=${wphase_subnet_and_sg[0]}
wphase_sg_id=${wphase_subnet_and_sg[1]}

sed -i "s|__SEISCOMP_HOME_FOLDER__|$SEISCOMP_HOME_FOLDER|g" $SEISCOMP_HOME_FOLDER/share/www/httpd/gempa.conf
sed -i "s|__QUAKELINK_HOST_NAME__|$QUAKELINK_HOST_NAME|g" $SEISCOMP_HOME_FOLDER/share/www/httpd/gempa.conf
sed -i "s|__PROC1_HOST_NAME__|$PROC1_HOST_NAME|g" $SEISCOMP_HOME_FOLDER/share/www/httpd/gempa.conf
sed -i "s|__PROC2_HOST_NAME__|$PROC2_HOST_NAME|g" $SEISCOMP_HOME_FOLDER/share/www/httpd/gempa.conf
sed -i "s|__SC3_STACK_PRIVATE_ZONE_NAME__|$SC3_STACK_PRIVATE_ZONE_NAME|g" $SEISCOMP_HOME_FOLDER/share/gds/web/gds/settings.py
sed -i "s|__GDS_DJANGO_ADMIN_PASSWORD__|$GDS_DJANGO_ADMIN_PASSWORD|g" $SEISCOMP_HOME_FOLDER/share/gds/web/gds/settings.py
sed -i "s|__GDS_DJANGO_ADMIN_USERNAME__|$GDS_DJANGO_ADMIN_USERNAME|g" $SEISCOMP_HOME_FOLDER/share/gds/web/gds/settings.py
sed -i "s|__GDS_RDS_ENDPOINT__|$GDS_RDS_ENDPOINT|g"  $SEISCOMP_HOME_FOLDER/share/gds/web/gds/settings.py
sed -i "s|__PUBLIC_DOMAIN_NAME__|$PUBLIC_DOMAIN_NAME|g" $SEISCOMP_HOME_FOLDER/share/gds/web/gds/settings-production.py
sed -i "s|__EATWS_STACK_NUMBER__|$EATWS_STACK_NUMBER|g" $SEISCOMP_HOME_FOLDER/share/gds/web/gds/settings-production.py
sed -i "s|__TWITTER_TEST_SERVICE_ACCESS_SECRET__|$TWITTER_TEST_SERVICE_ACCESS_SECRET|g"  $SEISCOMP_HOME_FOLDER/share/gds/tools/ga_send_twitter.cfg
sed -i "s|__TWITTER_TEST_SERVICE_ACCESS_TOKEN__|$TWITTER_TEST_SERVICE_ACCESS_TOKEN|g"  $SEISCOMP_HOME_FOLDER/share/gds/tools/ga_send_twitter.cfg
sed -i "s|__TWITTER_TEST_SERVICE_APP_SECRET__|$TWITTER_TEST_SERVICE_APP_SECRET|g"  $SEISCOMP_HOME_FOLDER/share/gds/tools/ga_send_twitter.cfg
sed -i "s|__TWITTER_TEST_SERVICE_APP_KEY__|$TWITTER_TEST_SERVICE_APP_KEY|g"  $SEISCOMP_HOME_FOLDER/share/gds/tools/ga_send_twitter.cfg
sed -i "s|__TWITTER_PROD_SERVICE_ACCESS_SECRET__|$TWITTER_PROD_SERVICE_ACCESS_SECRET|g"  $SEISCOMP_HOME_FOLDER/share/gds/tools/ga_send_twitter.cfg
sed -i "s|__TWITTER_PROD_SERVICE_ACCESS_TOKEN__|$TWITTER_PROD_SERVICE_ACCESS_TOKEN|g"  $SEISCOMP_HOME_FOLDER/share/gds/tools/ga_send_twitter.cfg
sed -i "s|__TWITTER_PROD_SERVICE_APP_SECRET__|$TWITTER_PROD_SERVICE_APP_SECRET|g"  $SEISCOMP_HOME_FOLDER/share/gds/tools/ga_send_twitter.cfg
sed -i "s|__TWITTER_PROD_SERVICE_APP_KEY__|$TWITTER_PROD_SERVICE_APP_KEY|g"  $SEISCOMP_HOME_FOLDER/share/gds/tools/ga_send_twitter.cfg
sed -i "s|__ASK_GEO_SERVICE_ID__|$ASK_GEO_SERVICE_ID|g" $SEISCOMP_HOME_FOLDER/share/gds/tools/ga_filter_twitter.cfg
sed -i "s|__ASK_GEO_SERVICE_API_KEY__|$ASK_GEO_SERVICE_API_KEY|g" $SEISCOMP_HOME_FOLDER/share/gds/tools/ga_filter_twitter.cfg
sed -i "s|__TWILIO_ACCOUNT_ID__|$TWILIO_ACCOUNT_ID|g" $SEISCOMP_HOME_FOLDER/share/gds/tools/ga_ring_phones.cfg
sed -i "s|__TWILIO_API_TOKEN__|$TWILIO_API_TOKEN|g" $SEISCOMP_HOME_FOLDER/share/gds/tools/ga_ring_phones.cfg
sed -i "s|__TWILIO_PHONE_NUMBER__|$TWILIO_PHONE_NUMBER|g" $SEISCOMP_HOME_FOLDER/share/gds/tools/ga_ring_phones.cfg
sed -i "s|__EATWS_ENVIRONMENT__|$EATWS_ENVIRONMENT|g" $SEISCOMP_HOME_FOLDER/share/gds/tools/ga_send_email.cfg
sed -i "s|__EATWS_ENVIRONMENT__|$EATWS_ENVIRONMENT|g" $SEISCOMP_HOME_FOLDER/share/gds/tools/ga_send_bom_xml.cfg
sed -i "s|__EATWS_ENVIRONMENT__|$EATWS_ENVIRONMENT|g" $SEISCOMP_HOME_FOLDER/share/gds/tools/ga_run_wphase.cfg
sed -i "s|__EATHQUAKES_AT_GA_BUCKET_NAME__|$EATHQUAKES_AT_GA_BUCKET_NAME|g" $SEISCOMP_HOME_FOLDER/share/gds/tools/ga_save_to_s3_bucket.cfg
sed -i "s|__NSW_WATER_CONTAINER_NAME__|$NSW_WATER_CONTAINER_NAME|g" $SEISCOMP_HOME_FOLDER/share/gds/tools/ga_send_nsw_water.cfg
sed -i "s|__NSW_WATER_ACCOUNT_NAME__|$NSW_WATER_ACCOUNT_NAME|g" $SEISCOMP_HOME_FOLDER/share/gds/tools/ga_send_nsw_water.cfg
sed -i "s|__NSW_WATER_ACCESS_KEY__|${NSW_WATER_ACCESS_KEY}|g" $SEISCOMP_HOME_FOLDER/share/gds/tools/ga_send_nsw_water.cfg
sed -i "s|__WPHASE_AMI_ID__|$WPHASE_AMI_ID|g" $SEISCOMP_HOME_FOLDER/share/gds/tools/ga_run_wphase.cfg
sed -i "s|__WPHASE_SUBNET_ID__|$wphase_snet_id|g" $SEISCOMP_HOME_FOLDER/share/gds/tools/ga_run_wphase.cfg
sed -i "s|__WPHASE_SG_ID__|$wphase_sg_id|g" $SEISCOMP_HOME_FOLDER/share/gds/tools/ga_run_wphase.cfg
sed -i "s|__WPHASE_KEY_NAME__|$WPHASE_KEY_NAME|g" $SEISCOMP_HOME_FOLDER/share/gds/tools/ga_run_wphase.cfg
sed -i "s|__WPHASE_RESULT_BUCKET_NAME__|$WPHASE_RESULT_BUCKET_NAME|g" $SEISCOMP_HOME_FOLDER/share/gds/tools/ga_run_wphase.cfg
sed -i "s|__WPHASE_INSTANCE_PROFILE_NAME__|$WPHASE_INSTANCE_PROFILE_NAME|g" $SEISCOMP_HOME_FOLDER/share/gds/tools/ga_run_wphase.cfg
sed -i "s|__WPHASE_REVIEWER_EMAIL__|$WPHASE_REVIEWER_EMAIL|g" $SEISCOMP_HOME_FOLDER/share/gds/tools/ga_run_wphase.cfg
sed -i "s|__SEND_BOM_XML_LAMBDA_NAME__|$SEND_BOM_XML_LAMBDA_NAME|g" $SEISCOMP_HOME_FOLDER/share/gds/tools/ga_run_wphase.cfg
sed -i "s|__SEND_BOM_XML_ROLE_ARN__|$SEND_BOM_XML_ROLE_ARN|g" $SEISCOMP_HOME_FOLDER/share/gds/tools/ga_run_wphase.cfg
sed -i "s|__QUAKELINK_HOST_NAME__|$QUAKELINK_HOST_NAME|g" $SEISCOMP_HOME_FOLDER/share/gds/tools/ga_run_wphase.cfg

#-------------------------------------------------------------------------------
# firewall configuration
#-------------------------------------------------------------------------------
sudo systemctl enable firewalld
sudo systemctl start firewalld

# http and https access
sudo firewall-cmd --zone=public --add-service=http --permanent
sudo firewall-cmd --reload

#-------------------------------------------------------------------------------
# httpd configuration
#-------------------------------------------------------------------------------
# create the htdocs folder
sudo mkdir -p /etc/httpd/htdocs

# generate the htpasswd file
password_file=/etc/httpd/htdocs/.htpasswd
function add_ht_password() {
    echo "$2" | sudo htpasswd -i"$3" $password_file $1
}
add_ht_password "$EATWS_WEB_USER_UNAME" "$EATWS_WEB_USER_PASS" c
add_ht_password "$JATWC_WEB_USER_UNAME" "$JATWC_WEB_USER_PASS" 

# move the apache and django configs into place
sudo cp $EATWS_SHARE_FOLDER/httpd.conf /etc/httpd/conf/httpd.conf
sudo cp $SEISCOMP_HOME_FOLDER/share/www/httpd/gempa.conf /etc/httpd/conf.d/gempa.conf
sudo cp $SEISCOMP_HOME_FOLDER/share/www/httpd/httpd /etc/sysconfig/httpd

# replace apache HTML directory with symlink to SC3 folder
sudo rm -rf /var/www/html
sudo ln -s ${SEISCOMP_HOME_FOLDER}/share/www/html /var/www/html

# touch the health check file. This is only required until we get health checking
# sorted out
touch /opt/seiscomp3/share/www/html/health_check.html

# fire up apache
sudo systemctl enable httpd
sudo systemctl restart httpd

# TODO: generate the ssl certificate

#-------------------------------------------------------------------------------
# Django configuration
#-------------------------------------------------------------------------------
# This is all captured in Git. It may be required if Gempa move to a more recent
# version of Django, or, perhaps if they change the GDS data model.

# Install Django
#wget https://www.djangoproject.com/download/1.4.22/tarball/ -O Django-1.4.22.tar.gz
# or
#aws s3api get-object --bucket sc3-install-large-files --key Django-1.4.22.tar.gz --request-payer requester /tmp/Django-1.4.22.tar.gz
#tar -xzf /tmp/Django-1.4.22.tar.gz -C $SEISCOMP_HOME_FOLDER/share/gds/web
#rm -f /tmp/Django-1.4.22.tar.gz

# needed to set these for Django 1.4 which has a bug. Probably won't be needed
# for later releases.  TODO: might be good to put these in .bashrc or alike
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8

# The GDS database must be created and permissions have to be granted to user sysop.
# Now we are using an amazon rds.
#mysql -h localhost -u root --password=... < $SETUP_SCRIPT_DIR/gds.sql

# Load the GDS configuration into the GDS database. The configuration includes
# all the stuff setup through the web api (subscribers, subscriptions, ...).
# Now we are using an amazon rds.
#mysql -h localhost -u root --password=... gds < $SETUP_SCRIPT_DIR/gds_state.sql

# This (plus a lot more) is done by loading the script gds_state.sql
#$SETUP_SCRIPT_DIR/gds_manage_syncdb.exp

# Might need except scripts for this.
# python manage.py collectstatic

# To test the installation of the GDS server
#python manage.py runserver

#-------------------------------------------------------------------------------
# misc sc3 configuration
#-------------------------------------------------------------------------------
# Captured in git
#[/opt/seiscomp3/etc/kernel.cfg]
#messaging.enable  'false'

# touch seiscomp.init file to prevent scconfig from showing first start dialog
touch ${SEISCOMP_HOME_FOLDER}/var/run/seiscomp.init

# The directory that we will use to sync to S3.
# NOTE: This must be kept in sync with roll.sh.
# TODO: define this in /opt/sce_install/setup_env.sh.
events_directory=$SEISCOMP_HOME_FOLDER/var/lib/quakelink/s3

# The directory that quakelink will use for XML files.
# NOTE: This must be kept in sync with the quakelink configuration
#       in etc/quakelink.cfg.
quakelink_archive=$SEISCOMP_HOME_FOLDER/var/lib/quakelink/archive/data

if [ "${INSTANCE_IS_LAUNCHING:-no}" == "yes" ]; then
    rm -rf "$quakelink_archive"
    mkdir -p "$quakelink_archive"

    if [ "$SHORT_HOST_NAME" == "gds2" ]; then
        rm -rf "$events_directory"
        mkdir -p "$events_directory"

        # modify the settings for lsyncd
        sudo bash -c "echo '
sync{
    default.rsync,
    source = \"$quakelink_archive\",
    target = \"$events_directory\",
    exclude = \"last.xml.gz\",
    delete = false,
    init = false,
    delay = 15,
    rsync = {
        copy_links = true,
        copy_dirlinks = true,
        inplace = true
   }
}' > /etc/lsyncd.conf"
    fi
fi

# Get the stack number from dynamodb. Assumes that *awschange* has been
# called to assume the appropriate role before hand.
current_stack_number=$(aws dynamodb get-item \
    --table-name stackdetails \
    --projection-expression "val" \
    --key "{\"key\": {\"S\": \"number\"}}" \
    --query "[Item.val]" \
    --output text)

# turn of every module
for x in `seiscomp list enabled`; do
    seiscomp disable $x
done

# and now enable the ones we want
seiscomp enable \
    eqevents \
    fdsnws \
    gaps \
    gis \
    ql2ql \
    quakelink

# Fire up seiscomp3. Do this here so when we can load the quakelink data
seiscomp update-config
seiscomp restart

if [ "$EATWS_STACK_NUMBER" == "$current_stack_number" ]; then
    # this is a live stack
    seiscomp enable gds
    seiscomp update-config gds
    seiscomp start gds
else
    # then we are using a single db or all instances and we don't want the
    # new gds to start publishing until it is rolled in.
    sudo systemctl disable lsyncd
    sudo systemctl stop lsyncd # probably not necessary

    # GDS should be disabled in the repository... but just in case it is not.
    seiscomp disable gds
    seiscomp stop gds
fi

# load the events into quakelink (for gaps)
$SEISCOMP_HOME_FOLDER/share/eatws/load_quakelink.py

# we need to do this after we have loaded quakelink
if [ "$SHORT_HOST_NAME" == "gds2" ]; then
    sudo sed -i'' 's/^# *user_allow_other/user_allow_other/' /etc/fuse.conf

    if [ "$EATWS_STACK_NUMBER" == "$current_stack_number" ]; then
        # mount the events bucket as a file system (after unmounting... just
        # in case). See the setup script in packer for more discussion on
        # the choice of tools.
        if [ "${INSTANCE_IS_LAUNCHING:-no}" == "no" ]; then
            if mount | grep $events_directory > /dev/null; then
                fusermount -u $events_directory
            fi
        fi

        yas3fs s3://$S3_EVENTS_BUCKET_NAME $events_directory
        sudo systemctl enable lsyncd
        sudo systemctl start lsyncd
    fi
fi
