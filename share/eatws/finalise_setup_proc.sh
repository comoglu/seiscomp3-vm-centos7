#!/bin/bash

#-------------------------------------------------------------------------------
# Do most of the launch time configuration of an instance.
# - This gets called from https://bitbucket.org/geoscienceaustralia/sc3-infrastructure/src/master/packer/files/gds/setup_gds.sh.
# - It assumes that the environment has been setup (i.e. the file
#   /opt/sc3_install/setup_env.sh has been sourced). At the time of writing,
#   this was done in the userdata.
#-------------------------------------------------------------------------------
source /opt/sc3_install/setup_env.sh
source "$EATWS_SHARE_FOLDER"/seiscomp_setup.sh

WF_ARCHIVE_FOLDER="$SEISCOMP_HOME_FOLDER"/var/lib/archive

#-------------------------------------------------------------------------------
# misc configuration
#-------------------------------------------------------------------------------
# so the machine can find itself
sudo bash -c "echo '127.0.0.1 primary' >> /etc/hosts"
sudo bash -c "echo '192.104.43.82 ansn.eatws.proxy' >> /etc/hosts"
sudo bash -c "echo '139.17.3.177 gfz.eatws.proxy' >> /etc/hosts"
sudo bash -c "echo '194.254.225.45 geoscope.eatws.proxy' >> /etc/hosts"
sudo bash -c "echo '169.228.44.151 ida.eatws.proxy' >> /etc/hosts"
sudo bash -c "echo '128.95.166.19 iris.eatws.proxy' >> /etc/hosts"
sudo bash -c "echo '137.227.224.97 usgs.eatws.proxy' >> /etc/hosts"

# set the hostname (so it shows up in the prompt)
sudo hostname proc"$SEISCOMP_MACHINE_NUMBER"

# touch seiscomp.init file to prevent scconfig from showing first start dialog
mkdir -p $SEISCOMP_HOME_FOLDER/var/run
touch ${SEISCOMP_HOME_FOLDER}/var/run/seiscomp.init

if [[ "$THIS_HOSTS_AGENCY_ID" != "NA" ]]; then
    #---------------------------------------------------------------------------
    # create the duty account
    #---------------------------------------------------------------------------
    RANDOM_PASS=$(openssl rand -base64 32)

    cd /tmp
    sudo chmod a+x $SETUP_SCRIPT_DIR/key_store.sh
    sudo groupadd eatws
    sudo useradd duty -g eatws -p "$RANDOM_PASS"
    sudo mkdir /home/duty/.ssh
    sudo touch /home/duty/.ssh/authorized_keys
    $SETUP_SCRIPT_DIR/key_store.sh getall /PubKey

    for u_id in `ls /tmp/PubKey/*`; do
        cat < $u_id |sudo tee -a /home/duty/.ssh/authorized_keys
    done

    sudo chmod 700 /home/duty/.ssh
    sudo chmod 600 /home/duty/.ssh/authorized_keys
    sudo chown -R duty:eatws /home/duty/.ssh

    # copy licence files and stuff from centos
    sudo cp -r "$EC2_USER_HOME_FOLDER"/.seiscomp3 /home/duty/
    sudo chown -R duty:eatws /home/duty/.seiscomp3

    rm -rf /tmp/PubKey

    #---------------------------------------------------------------------------
    # download key to sync data from other stack
    #---------------------------------------------------------------------------
    $SETUP_SCRIPT_DIR/key_store.sh get /PrivKey/archive
    mv PrivKey/archive/archive.txt "$EC2_USER_HOME_FOLDER"/.ssh/id_rsa
    chmod 600 "$EC2_USER_HOME_FOLDER"/.ssh/id_rsa

    #---------------------------------------------------------------------------
    # mount EFS
    #---------------------------------------------------------------------------
    FILESYSTEM_ID=`aws efs describe-file-systems --creation-token sc3-stack-$EATWS_STACK_NUMBER-vol$SEISCOMP_MACHINE_NUMBER --query 'FileSystems[*].[FileSystemId]' --output text`
    MOUNT_URL="$FILESYSTEM_ID.efs.ap-southeast-2.amazonaws.com"
    mkdir -p ${SEISCOMP_HOME_FOLDER}/var/lib/archive
    sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 $MOUNT_URL:/ ${SEISCOMP_HOME_FOLDER}/var/lib/archive
    sudo chown $EC2_USER_NAME:$EC2_USER_NAME ${SEISCOMP_HOME_FOLDER}/var/lib/archive

    #---------------------------------------------------------------------------
    # misc configuration
    #---------------------------------------------------------------------------
    # start httpd (so that we can start monitoring)
    sudo bash -c "echo 'hello from proc $SEISCOMP_MACHINE_NUMBER' > /var/www/html/index.html"

    # restart httpd in case anything has changed
    sudo service httpd start

    # put the script that starts the wall screens in the correct place
    scrttv_screens_script_path=/home/duty
    sudo cp $SEISCOMP_HOME_FOLDER/share/eatws/aiwa.sh $scrttv_screens_script_path
    sudo cp $SEISCOMP_HOME_FOLDER/share/eatws/eaca.sh $scrttv_screens_script_path
    sudo cp $SEISCOMP_HOME_FOLDER/share/eatws/epea.sh $scrttv_screens_script_path
    sudo cp $SEISCOMP_HOME_FOLDER/share/eatws/nsa.sh $scrttv_screens_script_path
    sudo cp $SEISCOMP_HOME_FOLDER/share/eatws/scrttv_screens_wall.sh $scrttv_screens_script_path

    # not sure why but `sudo ls $scrttv_screens_script_path/*.sh` does not work.
    sudo ls $scrttv_screens_script_path | grep "\.sh$" | xargs -I{} sudo chown centos:eatws $scrttv_screens_script_path/{}
    sudo ls $scrttv_screens_script_path | grep "\.sh$" | xargs -I{} sudo chmod 755 $scrttv_screens_script_path/{}

    #---------------------------------------------------------------------------
    # configuration configuration
    #---------------------------------------------------------------------------
    # Adjust the config files for this machine type. This should be kept in sync
    # with roll.sh on the proc machines.
    #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    #!!!!!!!!!!!!!! NOTE THAT THE SINGLE QUOTES ARE DELIBERATE !!!!!!!!!!!!!!!!!
    #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    if [ "$1" == "cur" ]; then
        echo 'export CAPS2CAPS_INPUT_HOST_NAME=localhost' >> "$SETUP_SCRIPT_DIR"/setup_env.sh
        echo 'export CAPS2CAPS_OUTPUT_HOST_NAME=$SECONDARY_PROC_HOST_NAME' >> "$SETUP_SCRIPT_DIR"/setup_env.sh
        echo 'export QL_SECONDARY_PROC_HOST_NAME=$SECONDARY_PROC_HOST_NAME' >> "$SETUP_SCRIPT_DIR"/setup_env.sh
    else
        echo 'export CAPS2CAPS_INPUT_HOST_NAME=$PRE_ROLL_SECONDARY_PROC_HOST_NAME' >> "$SETUP_SCRIPT_DIR"/setup_env.sh
        echo 'export CAPS2CAPS_OUTPUT_HOST_NAME=localhost' >> "$SETUP_SCRIPT_DIR"/setup_env.sh
        echo 'export QL_SECONDARY_PROC_HOST_NAME=$PRE_ROLL_SECONDARY_PROC_HOST_NAME' >> "$SETUP_SCRIPT_DIR"/setup_env.sh
    fi

    source "$SETUP_SCRIPT_DIR"/setup_env.sh

    sed -i 's/^database\.parameters/#database.parameters/g' ${SEISCOMP_HOME_FOLDER}/etc/global.cfg

    #---------------------------------------------------------------------------
    # Configure the firewall
    #---------------------------------------------------------------------------
    # this is not a standalone machine.
    sudo systemctl enable firewalld
    sudo systemctl start firewalld

    # health checks
    sudo firewall-cmd --zone=public --add-port=80/tcp --permanent

    # Spread
    sudo firewall-cmd --zone=public --add-port=4803/tcp --permanent

    # FDSN
    sudo firewall-cmd --zone=public --add-port=8081/tcp --permanent

    # seedlink
    sudo firewall-cmd --zone=public --add-port=18000/tcp --permanent

    # arclink
    sudo firewall-cmd --zone=public --add-port=18001/tcp --permanent

    # quakelink
    sudo firewall-cmd --zone=public --add-port=18012/tcp --permanent

    # reload the firewall config
    sudo firewall-cmd --reload

else
    # then this is a standalone machine of some kind and we need to setup a
    # local db

    mkdir -p ${SEISCOMP_HOME_FOLDER}/var/lib/archive
    sudo chown $EC2_USER_NAME:$EC2_USER_NAME ${SEISCOMP_HOME_FOLDER}/var/lib/archive

    # run the installation scripts that come with seiscomp.
    sudo chmod 777 $INSTALL_SCRIPTS_FOLDER/install-mysql-server.sh
    sudo sed -i "s/yum/yum -y/g" $INSTALL_SCRIPTS_FOLDER/install-mysql-server.sh
    sudo $INSTALL_SCRIPTS_FOLDER/install-mysql-server.sh

    # modify the mysql setup
    sudo sed -i \
        's/\[mysqld\]/[mysqld]\nbind-address = 0.0.0.0\ninnodb_buffer_pool_size = 64M\ninnodb_flush_log_at_trx_commit = 2/' \
        /etc/my.cnf.d/server.cnf

    # turn on mariadb
    sudo systemctl enable mariadb
    sudo service mariadb start

    # see if this helps with ensuring things go smoothly
    sleep 3

    # secure the mysql installation
    mysqlsecureinstallation "$RDS_ROOT_PASSWORD"
fi

#-------------------------------------------------------------------------------
# prepare the db
#-------------------------------------------------------------------------------
# determine if the db already exists
db_exists=$(mysql -s -N \
    -h "$SEISCOMP_RDS_ENDPOINT" \
    -u root \
    --password="$RDS_ROOT_PASSWORD" \
    -e "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME='seiscomp3$SEISCOMP_MACHINE_NUMBER'")

if [[ -z "$db_exists" ]]; then
    # then the db does not exist, so we need to create it
    setupseiscompandcreatedb \
        "$RDS_ROOT_PASSWORD" \
        "$PROC_MYSQL_SC3_USER" \
        "$PROC_MYSQL_SC3_PASSWORD" \
        "$PROC_MYSQL_SC3_USER" \
        "$PROC_MYSQL_SC3_PASSWORD" \
        "$SEISCOMP_RDS_ENDPOINT" \
        "$SEISCOMP_MACHINE_NUMBER"
else
    setupseiscompnocreatedb \
        "$PROC_MYSQL_SC3_USER" \
        "$PROC_MYSQL_SC3_PASSWORD" \
        "$PROC_MYSQL_SC3_USER" \
        "$PROC_MYSQL_SC3_PASSWORD" \
        "$SEISCOMP_RDS_ENDPOINT" \
        "$SEISCOMP_MACHINE_NUMBER"
fi

#-------------------------------------------------------------------------------
# seiscomp configuration
#-------------------------------------------------------------------------------
rm $SEISCOMP_HOME_FOLDER/etc/kernel.cfg

# turn of every module
for x in `seiscomp list enabled`; do
    seiscomp disable $x
done

#-------------------------------------------------------------------------------
# sync data from other places
#-------------------------------------------------------------------------------
if [[ "$THIS_HOSTS_AGENCY_ID" != "NA" ]]; then

    # and now enable the ones we want (more done in blocks below)
    seiscomp enable \
        spread \
        scmaster \
        arclinkproxy \
        fdsnws \
        l1autoloc \
        l1autopick \
        l1scanloc \
        l1sceval \
        ql2sc \
        quakelink \
        sc2ql \
        scamp \
        scanloc \
        scautoloc \
        scautomt \
        scautopick \
        scautowp \
        sceval \
        scevent \
        scmag \
        scqc \
        seedlink \
        slarchive

    seiscomp update-config
    seiscomp restart

    if [[ "$SHORT_HOST_NAME" == "proc1" ]]; then
        # we want to mirror config changes to proc2
        seiscomp enable scimport
        seiscomp start scimport

        if [[ "$1" == "cur" ]]; then
            # We always push to the other stack. While this may not be necessary (
            # because the other stack may not exist) determining whether there is
            # another stack or not is a little tricky. The default behaviour of
            # scimport is to try to reconnect after two minutes. This will not be
            # expensive and cannot (???) hurt.
            seiscomp enable scimposp1
            seiscomp start scimposp1
        fi
    fi

    if [[ "$1" != "cur" ]] && [[ -z "$db_exists" ]] && [[ "${INSTANCE_IS_LAUNCHING:-no}" == "yes" ]]; then
        # the check for db_exists implicitly assumes that we managed to load
        # the previous history before the previous machine was terminated.
        # TODO: Check that the entire history was loaded previously.  This may
        #       be done, for instance by setting a flag in dynamodb at the end
        #       of load_quakelink.py (or two lines below this one...  after
        #       that script exits).

        # dump the config from the same machine in the other stack
        python $SEISCOMP_HOME_FOLDER/share/eatws/import_cs \
            -d ${PROC_MYSQL_SC3_USER}:${PROC_MYSQL_SC3_PASSWORD}@${OTHER_SEISCOMP_RDS_ENDPOINT}/seiscomp3${SEISCOMP_MACHINE_NUMBER} > /tmp/sc3-config.txt

        # load the config from the other same machine in the other stack
        python $SEISCOMP_HOME_FOLDER/share/eatws/import_cs \
            -f /tmp/sc3-config.txt

        # we should really only have to do this on proc1
        # TODO: Determine if we really cant only do this on proc1.
        python "$EATWS_SHARE_FOLDER"/load_quakelink.py

        #create a ssh config file to silently connect to remote host
        echo  "StrictHostKeyChecking no" > "$EC2_USER_HOME_FOLDER"/.ssh/config
        echo  "UserKnownHostsFile /dev/null" >> "$EC2_USER_HOME_FOLDER"/.ssh/config
        chmod 600 "$EC2_USER_HOME_FOLDER"/.ssh/config

        # sync waveform data from old stack to current
        rsync -r -a -v -e ssh --delete duty@$CAPS2CAPS_INPUT_HOST_NAME:$WF_ARCHIVE_FOLDER/* $WF_ARCHIVE_FOLDER &
    fi

    # restart httpd in case anything has changed
    sudo service httpd restart
fi

# add cron job to cleanup database
(crontab -l; echo "30 0 * * * /opt/seiscomp3/share/eatws/cleanup_sc3_db.sh")|crontab -
# add cronjob to cleanup slarchive miniseed archive
(crontab -l; echo "0 0 * * * /opt/seiscomp3/var/lib/slarchive/purge_datafiles >/dev/null 2>&1")|crontab -

# remove the key file used in rsync
rm -f "$EC2_USER_HOME_FOLDER"/.ssh/id_rsa
rm -f "$EC2_USER_HOME_FOLDER"/.ssh/config
