#!/bin/bash

#-------------------------------------------------------------------------------
# Change the password for the GDS user.
#
# The GDS web interface is built on Django. This script uses the manage.py
# script in that web app to change the password for the login user.
#
# It assumes that the environment variables SEISCOMP_HOME_FOLDER,
# GDS_WEB_USER_UNAME and GDS_WEB_USER_PASS are appropriately set. At the time of
# writing those variables are set in the script /opt/sc3_install/setup_env.sh,
# which is sourced in this script.
#
# At the time of writing, this script was called from the script
# https://bitbucket.org/geoscienceaustralia/sc3-infrastructure/src/master/stack/roll_stack.sh
#-------------------------------------------------------------------------------

source /opt/sc3_install/setup_env.sh

# Change the GDS password.
function changedjangopass() {
    # $1 the name of the user
    # $2 the new password
    template='spawn python '"$SEISCOMP_HOME_FOLDER"'/share/gds/web/manage.py changepassword '"$1"'
        expect "Password*" { send '"$2"'\r }
        expect "Password*" { send '"$2"'\r }
        expect eof { puts "changed password for '"$1"'" }'
    echo "$template" | /usr/bin/expect -
}

changedjangopass "$GDS_WEB_USER_UNAME" "$GDS_WEB_USER_PASS"
