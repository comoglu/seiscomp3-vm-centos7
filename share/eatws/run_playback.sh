#!/bin/bash

#-------------------------------------------------------------------------------
# Script to run a playback 'into' a stack. This should be called from the
# machine on the phaux acquuistion system.
#
# Assumptions:
# - The acquisition machine can SSH to the machines in the stack (that is a
#   suitable SSH private key is in the ssh agent.
#
# - That the proc machines in the stack can accesss port 18000 (or more
#   generally the seedlink port) of the acquisition machine. In AWS, this
#   requires that the appropriate inbound rules are configured in (one of) the
#   security group(s) of the acquisition machine.
#
# The arguments of this script are:
# - $1 is the name miniseed file containing the data. This must be sorted and
#      have had any other twerks seiscomp requires applied to it.
# - $2 is the stack number (0 or 1),
# - $3 is the name of the name of the environment (test or prod),
#-------------------------------------------------------------------------------

input_data="$1"
stack_number="$2"
eatws_env="${3-test}"

# check that the data file exists
if [ ! -f "$input_data" ]; then
    echo "input data file does not exist"
    exit 1
fi

# check that the stack number is valid.
if [[ "$stack_number" != "0" && "$stack_number" != "1" ]]; then
    echo "stack number must be 0 or 1"
    exit 1
fi

# check that the environment is valid and set the name of the public zone.
if [ "$eatws_env" == "test" ]; then
    r53_zone="gagempa.net"
elif [ "$eatws_env" == "prod" ]; then
    r53_zone="eatws.net"
else
    echo 'stack number must be "test" or "prod"'
    exit 1
fi

# The IP address of this machine (passed to the proc machines so the addresses
# of the seedlink servers can be updated).
my_ip=`curl ipconfig.io/ip`

function setupplayback() {
    # Call the script setup_playback.sh on a host.
    #
    # Arguments:
    # - $1 is the stack number (0 or 1).
    # - $2 is the machine number (1 or 2).
    # - $3 is ip of the current machine.
    # - $4 is the machine type (defaults to gds)
    # - $5 is the name of the name of the environment
    # - $6 is the name of the name of the public zone
    jump=centos@jump"$1"."$6"
    host="${4}${2}.sc3${5}stack${1}.private"
    echo "host $host"
    ssh -tA \
        -o StrictHostKeyChecking=no \
        -o UserKnownHostsFile=/dev/null \
        $jump ssh \
            -o StrictHostKeyChecking=no \
            -o UserKnownHostsFile=/dev/null \
            $host /opt/seiscomp3/share/eatws/setup_playback.sh $3
}

# allow access to the seedlink port
sudo firewall-cmd --zone=public --add-port=18000/tcp --permanent
sudo firewall-cmd --reload

# setup each of the machines in the stack
setupplayback "$stack_number" 1 "$my_ip" proc "$eatws_env" "$r53_zone"
setupplayback "$stack_number" 2 "$my_ip" proc "$eatws_env" "$r53_zone"
setupplayback "$stack_number" 1 "$my_ip" gds "$eatws_env" "$r53_zone"
setupplayback "$stack_number" 2 "$my_ip" gds "$eatws_env" "$r53_zone"

seiscomp stop

# configure the current machine for playback
sed -i 's/msrtsimul.*/msrtsimul = true/g' "$SEISCOMP_HOME_FOLDER"/etc/seedlink.cfg

# only have the seedlink module running
for mod in `seiscomp list enabled`; do seiscomp disable "$mod"; done
seiscomp enable seedlink

# cleanup the seedlink buffer
rm -rvf "$SEISCOMP_HOME_FOLDER"/var/lib/seedlink/buffer/*

seiscomp update-config
seiscomp start

# we have noticed (but don't understand why) that the command msrtsimul often
# fails the first time it runs... so if it does, try again. Not sure if the
# update config and restart are required.
if ! msrtsimul -v -j 8 "$input_data"; then
    seiscomp update-config
    seiscomp restart
    msrtsimul -v -j 8 "$input_data"
fi
