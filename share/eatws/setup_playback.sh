#!/bin/bash

#-------------------------------------------------------------------------------
# Setup a proc machine for playback. This script is called from the script
# run_playback.sh on the machine providing the waveform data via SSH.
#
# $1: The ip address of the acuquisition machine.
#-------------------------------------------------------------------------------

source /opt/sc3_install/setup_env.sh

acq_ip="$1"

# stop seiscomp so it does not undo the work we are about to do
seiscomp stop

# cleanup quakelink
rm -rvf "$SEISCOMP_HOME_FOLDER"/var/lib/quakelink/data/*

if [[ "$SHORT_HOST_NAME" =~ "proc" ]]; then
    # modify the hosts file so the seedlink servers point to the phaux acquisition machine
    sudo sed -ri 's/^.+ ([^.]+)\.eatws\.proxy/'"$acq_ip"' \1.eatws.proxy/g' /etc/hosts
    sudo bash -c 'echo '"$acq_ip"' link.geonet.org.nz >> /etc/hosts'
    sudo bash -c 'echo '"$acq_ip"' seedlink.met.gov.my >> /etc/hosts'
    sudo bash -c 'echo '"$acq_ip"' imslserver.ipma.pt >> /etc/hosts'
    sudo bash -c 'echo 169.254.0.0 proc.gempa.de >> /etc/hosts'

    # cleanup the database
    scdbstrip --days 0 -d mysql://${PROC_MYSQL_SC3_USER}:${PROC_MYSQL_SC3_PASSWORD}@${SEISCOMP_RDS_ENDPOINT}/seiscomp3${SEISCOMP_MACHINE_NUMBER}

    # cleanup the SDS archive
    rm -rvf "$SEISCOMP_HOME_FOLDER"/var/lib/archive/*

    # cleanup the seedlink buffer
    rm -rvf "$SEISCOMP_HOME_FOLDER"/var/lib/seedlink/buffer/*

    # if we run this soon after a stack has started, rsync mucks with our waveforms
    pkill rsync
else
    # reconfigure and start seiscomp
    seiscomp enable gds # other modules should be running already
fi

# reconfigure and start seiscomp
seiscomp update-config
seiscomp start
