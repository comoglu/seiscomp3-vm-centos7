#!/bin/bash

# Americas (NSA)
scrttv -u scrttNSA --resortAutomatically="false" --autoApplyFilter="false" --maxDelay="21600" --streams.region.lonmin="-179.9" --streams.region.lonmax="-35" --streams.region.latmin="-89" --streams.region.latmax="89" --streams.sort.latitude="89" --streams.sort.longitude="-107.5"--streams.codes="AK.*.*.?HZ,AT.*.*.?HZ,AU.*.*.?HZ,BL.*.*.?HZ,C.*.*.?HZ,C1.*.*.?HZ,CM.*.*.?HZ,CN.*.*.?HZ,CU.*.*.?HZ,EC.*.*.?HZ,G.*.*.?HZ,GE.*.*.?HZ,GI.*.*.?HZ,GT.*.*.?HZ,II.*.*.?HZ,IU.*.*.?HZ,MX.*.*.?HZ,NZ.*.*.?HZ,ON.*.*.?HZ,OV.*.*.?HZ,TC.*.*.?HZ,US.*.*.?HZ,UW.*.*.?HZ,NN.*.*.?HZ,BK.*.*.?HZ,PB.*.*.?HZ,MI.*.*.?HZ" &
