#!/bin/bash

# South East Asia, Indonesia, Western Australia (AIWA)
scrttv -u rttvAIWA --resortAutomatically="false" --autoApplyFilter="false" --maxDelay="21600" --streams.region.lonmin="90" --streams.region.lonmax="129" --streams.region.latmin="-89" --streams.region.latmax="89" --streams.sort.latitude="89" --streams.sort.longitude="115.5" --streams.codes="AM.*.*.?HZ,AU.*.*.?HZ,AU.*.*.?HZ,G.*.*.?HZ,GE.*.*.?HZ,HK.*.*.?HZ,IA.*.*.?HZ,IC.*.*.?HZ,II.*.*.?HZ,IN.*.*.?HZ,IU.*.*.?HZ,JP.*.*.?HZ,MS.*.*.?HZ,MY.*.*.?HZ,PS.*.*.?HZ,TW.*.*.?HZ,NN.*.*.?HZ,BK.*.*.?HZ,PB.*.*.?HZ,MI.*.*.?HZ" &

