#!/usr/bin/env python

import seiscomp3.Config, seiscomp3.System, seiscomp3.Math

import eqelib.filters, eqelib.settings as settings
from jinja2 import Environment, FileSystemLoader
import sqlite3 as db
import os, sys

from wsgiref.simple_server import make_server
from cgi import parse_qs, escape


templateDir = ""
dbPath = None
resultLimit = 100


class HTTPException(Exception):
	def __init__(self, code, content = "", content_type = "text/plain"):
		self.code = code
		self.content = content
		self.content_type = content_type


def normalizeLon(value):
	while value < -180: value += 360
	while value > 180: value -= 360
	return value


def init():
	global dbPath
	global dbHandle
	global templateDir
	global resultLimit

	env = seiscomp3.System.Environment.Instance()
	cfg = seiscomp3.Config.Config()

	env.initConfig(cfg, "eqevents")

	dbPath = env.absolutePath('@ROOTDIR@/var/lib/eqevents/eqevents.db')

	try: templateDir = env.absolutePath(cfg.getString("templateDirectory"))
	except: pass

	try:
		settings.STATIC_URL = cfg.getString('staticURL')
	except:
		settings.STATIC_URL = 'static/'

	try:
		settings.PLUGINS = cfg.getStrings("processingPlugins")
	except:
		settings.PLUGINS = []

	try:
		resultLimit = cfg.getInt("search.resultLimit")
	except:
		pass

	try:
		settings.SEARCH_DISTANCE_UNIT = cfg.getString("search.distanceUnit")
		if settings.SEARCH_DISTANCE_UNIT != "km" and settings.SEARCH_DISTANCE_UNIT != "mi":
			print >> sys.stderr, "Invalid distance unit '%s', falling back to 'km'" % settings.SEARCH_DISTANCE_UNIT
			settings.SEARCH_DISTANCE_UNIT = "km"
	except:
		settings.SEARCH_DISTANCE_UNIT = "km"

	# Setup plugin search path
	sys.path.append(os.path.join(env.shareDir(), 'eqevents'))


def importClass(name, klass):
	mod = __import__(name)
	components = name.split('.')
	for comp in components[1:]:
		mod = getattr(mod, comp)
	return getattr(mod, klass)


class CircularArea:
	def __init__(self, lat, lon, radius):
		self.lat = lat
		self.lon = lon
		self.radius = radius

	def isInside(self, lat, lon):
		delta, az, baz = seiscomp3.Math.delazi(self.lat, self.lon, lat, lon)
		print >> sys.stderr, delta
		return delta <= self.radius


def generateSearch(datemin=None, datemax=None, magmin=None, magmax=None, latmin=None, latmax=None, lonmin=None, lonmax=None, centerlat=None, centerlon=None, radius=None):
	filter = ""

	query_tuple = []
	circularArea = None

	if datemin:
		if filter: filter += " and "
		else: filter += " where "
		filter += "otime>=?"
		query_tuple.append(datemin[0])

	if datemax:
		if filter: filter += " and "
		else: filter += " where "
		filter += "otime<=?"
		query_tuple.append(datemax[0])

	if latmin:
		if filter: filter += " and "
		else: filter += " where "
		filter += "lat>=?"
		query_tuple.append(latmin[0])

	if latmax:
		if filter: filter += " and "
		else: filter += " where "
		filter += "lat<=?"
		query_tuple.append(latmax[0])

	if lonmin:
		if filter: filter += " and "
		else: filter += " where "
		filter += "lon>=?"
		query_tuple.append(lonmin[0])

	if lonmax:
		if filter: filter += " and "
		else: filter += " where "
		filter += "lon<=?"
		query_tuple.append(lonmax[0])

	if magmin:
		if filter: filter += " and "
		else: filter += " where "
		filter += "mag>=?"
		query_tuple.append(magmin[0])

	if magmax:
		if filter: filter += " and "
		else: filter += " where "
		filter += "mag<=?"
		query_tuple.append(magmax[0])

	# Radius is in km or miles
	if centerlat or centerlon or radius:
		if centerlat is None:
			raise HTTPException("400 Bad Request", "Center latitude is not defined")
		else:
			try:
				centerlat = float(centerlat[0])
			except:
				raise HTTPException("400 Bad Request", "Center latitude is not a number")

		if centerlon is None:
			raise HTTPException("400 Bad Request", "Center longitude is not defined")
		else:
			try:
				centerlon = float(centerlon[0])
			except:
				raise HTTPException("400 Bad Request", "Center longitude is not a number")

		if radius is None:
			raise HTTPException("400 Bad Request", "Radius is not defined")
		else:
			try:
				radius = float(radius[0])
			except:
				raise HTTPException("400 Bad Request", "Radius is not a number")

		# Box width in degrees
		if settings.SEARCH_DISTANCE_UNIT == "mi":
			# Convert miles to kilometer
			radius *= 1.609344

		box_width = radius / 111.1329149013519096
		circularArea = CircularArea(centerlat, centerlon, box_width)

		latmin = circularArea.lat - box_width
		latmax = circularArea.lat + box_width
		lonmin = normalizeLon(circularArea.lon - box_width)
		lonmax = normalizeLon(circularArea.lon + box_width)

		if filter: filter += " and "
		else: filter += " where "

		if latmax > 90:
			filter += "lat>=?"
			latmax = 180 - latmax
			if latmax < latmin: latmin = latmax
			query_tuple.append(latmin)
		elif latmin < -90:
			filter += "lat<=?"
			latmin = -180 - latmin
			if latmin < latmax: latmax = latmin
			query_tuple.append(latmax)
		else:
			# Add rectangular region and filter later in software again
			filter += "lat>=?"
			query_tuple.append(latmin)
			filter += "and lat<=?"
			query_tuple.append(latmax)
			filter += "and lon>=?"
			query_tuple.append(lonmin)
			filter += "and lon<=?"
			query_tuple.append(lonmax)

	query = '''select eventID,otime,lat,lat_err,lon,lon_err,depth,depth_err,phases,mag,mag_err,mag_t,
	           agency,status,type,region,updated
	           from events%s
	           order by otime desc ''' % filter

	if not circularArea:
		query += "limit %d" % resultLimit

	dbHandle = db.connect(dbPath)
	if not dbHandle:
		raise Exception("Cannot open database %s" % dbPath)

	c = dbHandle.cursor()
	rows = c.execute(query, query_tuple)

	ctx = {}
	ctx['ROOT_URL'] = ''
	ctx['settings'] = settings

	try:
		ctx['STATIC_URL'] = ctx['ROOT_URL'] + settings.STATIC_URL
	except:
		ctx['STATIC_URL'] = ctx['ROOT_URL']

	items = []
	for row in rows:
		lat = row[2]
		lon = row[4]
		if circularArea and not circularArea.isInside(lat, lon):
			continue

		item = {}
		item['path'] = 'events/' + row[0].replace('#', '_').replace('/', '_') + '/overview.html'
		item['eventID'] = row[0]
		item['otime'] = eqelib.filters.dbdate(row[1])
		item['lat'] = lat
		item['lat_err'] = row[3]
		item['lon'] = lon
		item['lon_err'] = row[5]
		item['depth'] = row[6]
		item['depth_err'] = row[7]
		item['phases'] = row[8]
		item['mag'] = row[9]
		item['mag_err'] = row[10]
		item['mag_t'] = row[11]
		item['agency'] = row[12]
		item['status'] = row[13]
		item['type'] = row[14]
		item['region'] = row[15]
		item['upd'] = eqelib.filters.dbdate(row[16])
		items.append(item)
		if len(items) >= resultLimit:
			break

	ctx['eventTable'] = items

	dbHandle.close()

	env = Environment(loader=FileSystemLoader(templateDir), trim_blocks=True)
	env.filters['lat'] = eqelib.filters.lat
	env.filters['lon'] = eqelib.filters.lon
	env.filters['qmldate'] = eqelib.filters.qmldate
	env.filters['deg2km'] = eqelib.filters.deg2km
	env.filters['date'] = eqelib.filters.date

	# Initialize plugins
	for p in settings.PLUGINS:
		try:
			klass = importClass("plugins." + p, "Plugin")
			if not issubclass(klass, eqelib.plugin.PluginBase):
				raise Exception("must subclass plugin.PluginBase")
			ver = klass.VERSION
			ctx[klass.CONTEXT_TAG] = True
		except Exception, e:
			# Ignore plugins
			pass

	template = env.get_template("results.html")

	return template.render(ctx).encode("utf-8")


def application(environ, start_response):
	# Returns a dictionary in which the values are lists
	d = parse_qs(environ['QUERY_STRING'])

	try:
		content = generateSearch(**d)
		content_type = 'text/html'
		status = '200 OK'
	except HTTPException as e:
		content = e.content
		content_type = e.content_type
		status = e.code

	# Now content type is text/html
	response_headers = [
		('Content-Type', content_type),
		('Content-Length', str(len(content)))
	]

	start_response(status, response_headers)
	return [content]


init()
