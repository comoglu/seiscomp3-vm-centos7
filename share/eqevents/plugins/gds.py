import sys, os
import base64

import seiscomp3.IO
import seiscomp3.Core
import seiscomp3.Logging

import eqelib.plugin as plugin
import eqelib.settings as settings


class Plugin(plugin.PluginBase):
	VERSION = "0.1"

	def __init__(self, app, generator, env, db, cursor):
		try:
			settings.GDS_DB = app.configGetString("gds.db")
		except:
			raise Exception("gds.db not set")

	def reportEventProcessing(self, status, msg, filename):
		seiscomp3.Logging.debug("[%s] reportEventProcessing()" % self._name)
		base, ext = os.path.splitext(os.path.basename(filename))
		try:
			fComponents = base.split("-")
			logID = int(fComponents[1])
			eventID = fComponents[2]
		except:
			raise Exception("invalid filename format")

		b64Msg = base64.b64encode(msg)

		db = seiscomp3.IO.DatabaseInterface.Open(settings.GDS_DB)
		if not db:
			raise Exception("database %s not available" % settings.GDS_DB)

		if not db.isConnected():
			raise Exception("could not connect to: %s" % settings.GDS_DB)

		try:
			gds_stat = 0
			if status == 2:
				gds_stat = 3

			# update service log
			q = "UPDATE gds_log_service SET status = %i, message = '%s' "\
				"WHERE id = %i" % (gds_stat, b64Msg, logID)
			if not db.execute(q):
				raise Exception("could not update service log")

			if status == 2:
				gds_stat = 1

			# update receiver log
			ts = seiscomp3.Core.Time.GMT().toString("%FT%T.%f")
			q = "UPDATE gds_log_recv SET timestamp = '%s', error = '%i', "\
				"message = NULL WHERE service_log_id = %i" % (ts, gds_stat, logID)
			if not db.execute(q):
				raise Exception("could not update receiver log")
		finally:
			if db and db.isConnected():
				db.disconnect()

		return True
