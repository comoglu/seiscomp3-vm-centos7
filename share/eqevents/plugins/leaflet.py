import os, json, datetime

import seiscomp3.Logging

import eqelib.plugin as plugin
import eqelib.settings as settings
import eqelib.filters as filters


class Plugin(plugin.PluginBase):
	VERSION = "0.1"

	def __init__(self, app, generator, env, db, cursor):
		self._generator = generator
		self._cursor = cursor

		settings.INDEX_MAP_LAT_INIT = 53
		settings.INDEX_MAP_LON_INIT = 13
		settings.INDEX_MAP_ZOOM_INIT = 4

		try: settings.INDEX_MAP_LAT_INIT = app.configGetDouble("leaflet.init.lat")
		except: pass

		try: settings.INDEX_MAP_LON_INIT = app.configGetDouble("leaflet.init.lon")
		except: pass

		try: settings.INDEX_MAP_ZOOM_INIT = app.configGetInt("leaflet.init.zoom")
		except: pass

		self._numberOfEvents = getattr(settings, 'INDEX_EVENTS', 50)
		self._numberOfDays = getattr(settings, 'INDEX_DAYS', None)


	def processIndex(self, ctx, forceUpdate):
		seiscomp3.Logging.debug("[%s] processIndex()" % self._name)
		ctx['INDEX_MAP_AVAIL'] = True
		fn = os.path.join(settings.OUT_DIR, "map-events.json")
		seiscomp3.Logging.debug("+ %s" % fn)
		j = open(fn, 'w')
		q = 'select eventID,otime,lat,lon,depth,mag from events'
		if self._numberOfDays is not None:
			q += " WHERE otime >= '%s' ORDER BY otime DESC" % (datetime.datetime.utcnow() - datetime.timedelta(self._numberOfDays)).strftime("%F %T")
		else:
			q += ' ORDER BY otime DESC LIMIT %d' % self._numberOfEvents
		rows = self._cursor.execute(q)
		j.write('{"events":[')
		first = True
		for row in rows:
			if not first: j.write(',')
			else: first = False
			j.write('{')
			j.write('"eventID":%s' % json.dumps(row[0]))
			j.write(',"otime":"%s"' % row[1])
			j.write(',"lat":%f' % row[2])
			j.write(',"lon":%f' % row[3])
			j.write(',"depth":%f' % row[4])
			if row[5]:
				j.write(',"mag":%f' % row[5])
			j.write(',"path":%s' % json.dumps(self._generator.getEventPath(row[0])))
			j.write('}')
		j.write(']}')
		j.close()
