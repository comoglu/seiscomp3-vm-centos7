import os, sys
import urllib2

import seiscomp3.Logging

import eqelib.plugin as plugin
import eqelib.settings as settings


class Plugin(plugin.PluginBase):
	VERSION = "0.1"

	def __init__(self, app, generator, env, db, cursor):
		try:
			settings.GIS_URL = app.configGetString("gis.url")
		except:
			raise Exception("gis.url not set")

		try:
			settings.GIS_TIMEOUT = app.configGetInt("gis.timeout")
		except:
			settings.GIS_TIMEOUT = 5

		try:
			settings.GIS_POST = app.configGetBool("gis.post")
		except:
			settings.GIS_POST = False


	def processEvent(self, ctx, path):
		org = ctx['origin']
		lat = org.latitude.value
		lon = org.longitude.value

		if not settings.GIS_POST:
			try: dep = org.depth.value
			except: dep = 10

			magnitude = ctx['magnitude']
			try: mag = magnitude.magnitude.value
			except: mag = 3

		max_dist = 10
		try: max_dist = min(max_dist, max(5, org.quality.maximumDistance))
		except: pass

		filename = os.path.join(path, '%s-map.png' % ctx['ID'])

		maxSize = (10*1024)**2;

		url = settings.GIS_URL + "/map?reg=%f,%f,%f,%f" % (lat, lon, max_dist, max_dist)
		url += "&dim=512,512&fmt=png&qua=0"

		if settings.GIS_POST:
			url += "&stations=*"
			u = urllib2.urlopen(url, ctx['xml'], settings.GIS_TIMEOUT)
		else:
			url += "&ori=%f,%f,%f" % (lat, lon, dep)
			url += "&mag=%f" % mag
			u = urllib2.urlopen(url, None, settings.GIS_TIMEOUT)

		info = u.info()

		length = info.getheaders("Content-Length")
		if len(length) != 1:
			raise Exception("could not read content length header")
		if int(length[0]) > maxSize:
			raise Exception("content length of %i exceeds maximum of %i" % (
			                length[0], maxSize))

		type = info.getheaders("Content-Type")
		if len(type) != 1:
			raise Exception("could not read content type header")

		f = open(filename, "w")
		f.write(u.read())
		f.close()

		# Flag existence in the context
		ctx['GIS_HAS_MAP'] = True

		seiscomp3.Logging.debug("+ %s" % filename)

		return True
