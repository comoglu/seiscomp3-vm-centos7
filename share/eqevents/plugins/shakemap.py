import os, sys, time, json
import urllib2
import StringIO

import seiscomp3.Logging
import seiscomp3.System

import eqelib.plugin as plugin
import eqelib.settings as settings

from shutil import copyfile

class Plugin(plugin.PluginBase):
	VERSION = "0.2"

	def __init__(self, app, generator, env, db, cursor):
		env = seiscomp3.System.Environment.Instance()
		try:
			settings.SHAKEMAP_CMD = env.absolutePath(app.configGetString("shakemap.cmd"))
		except:
			raise Exception("shakemap.cmd not set")

		try:
			settings.SHAKEMAP_MINMAG = app.configGetDouble("shakemap.minMag")
		except:
			settings.SHAKEMAP_MINMAG = 5

		try:
			settings.SHAKEMAP_RESULT_FILE = env.absolutePath(app.configGetString("shakemap.resultFile"))
		except:
			raise Exception("shakemap.resultFile not set")


	def processEvent(self, ctx, path):
		org = ctx['origin']

		# check minimum magnitude threshold
		try:
			mag = ctx['magnitude'].magnitude.value
		except:
			seiscomp3.Logging.warning("[shakemap] could not read magnitude value")
			return False

		if mag < settings.SHAKEMAP_MINMAG:
			seiscomp3.Logging.info("[shakemap] magnitude %f below threshold " \
			                        "of %f, skipping" % (
			                        mag, settings.SHAKEMAP_MINMAG))
			return True

		resultFile = settings.SHAKEMAP_RESULT_FILE
		xmlFile = ctx['filename']

		# remove old json file
		if os.path.isfile(resultFile):
			try:
				os.remove(resultFile)
			except:
				seiscomp3.Logging.error("[shakemap] could not remove old " \
				                        "result file: %s" % resultFile)
				return False

		# run external shakemap application, e.g. autosigma
		cmd = settings.SHAKEMAP_CMD + " %s" % xmlFile
		os.system(cmd)

		if not os.path.isfile(resultFile):
			seiscomp3.Logging.error("[shakemap] no result file generated, " \
			                        "check log file of shakemap application")
			return False

		filename = os.path.join(path, '%s-shakemap.png' % ctx['ID'])
		copyfile(resultFile, filename)


		# Flag existence in the context
		ctx['SHAKEMAP_HAS_MAP'] = True
		seiscomp3.Logging.debug("[shakemap] image generated: %s" % filename)

		return True


