#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys, os, datetime, codecs

import seiscomp3.Logging

import eqelib.plugin as plugin
import eqelib.settings as settings
import eqelib.filters as filters
import eqelib.bulletin as bulletin

from BeautifulSoup import BeautifulSoup

class Plugin(plugin.PluginBase):
	VERSION = "0.0"

	def __init__(self, app, generator, env, db, cursor):
		self._app = app
		self._cursor = cursor
		self._generator = generator
		self._db = db
		try:
			self._eventBulletinTemplate = env.get_template("event/bulletin.html")
		except Exception, e:
			raise Exception("loading event/bulletin.html: %s" % str(e))
		try:
			self._eventBulletinOverviewTemplate = env.get_template("event/bulletinoverview.html")
		except Exception, e:
			raise Exception("loading event/bulletinoverview.html: %s" % str(e))

		self._cursor.execute("select count(*) from sqlite_master where name = 'bulletins'")
		if not self._cursor.fetchone()[0]:
			seiscomp3.Logging.info("[%s] initializing bulletin table" % self._name)

			self._cursor.execute('''CREATE TABLE bulletins
			             (
			              id INTEGER PRIMARY KEY,
			              idx INTEGER NOT NULL,
			              eventID VACHAR(80) NOT NULL,
			              ctime DATETIME default current_timestamp,
			              subject VARCHAR(80),
			              type VARCHAR
			             )''')

		self._db.commit()

	def processInvalid(self, ctx, filename, path):
		seiscomp3.Logging.debug("[%s] processInvalid()" % self._name)
		if not isinstance(ctx, dict):
			ctx = {}

		ctx["styles"] = []
		ctx['settings'] = settings
		ctx['ROOT_URL'] = '../../'

		try:
			ctx['STATIC_URL'] = ctx['ROOT_URL'] + settings.STATIC_URL
		except:
			ctx['STATIC_URL'] = ctx['ROOT_URL']

		f = codecs.open(filename,"r", "utf-8")
		content = f.read()
		f.close()

		# try to parse bulletin format
		try:
			b = bulletin.Bulletin()
			b.read(content)
		except Exception, e:
			raise Exception("could not parse bulletin: %s" % str(e))

		# use soap to modify html content
		try:
			soup = BeautifulSoup(b.html)
		except:
			raise Exception("skipping invalid html format: %s" % b.html)
			os.remove(filename)
			return False

		# get styles from the template
		for elem in soup.findAll(['style']):
			ctx["styles"].append(elem)

		# replace img cids by the base64 image
		for a in b.attachments:
			# skip empty names
			if not a.name or a.name == "":
				continue

			# skip non images
			if a.MIME.split("/")[0] != "image":
				continue

			img = soup.find('img', {'src': "cid:%s" % a.cid})
			# replace images by name and set content
			if img:
				img["src"] = "data:%s;base64,%s" % (a.MIME, a.content)

		# replace first h1 with h2
		if soup.first("h1"):
			soup.first("h1").name = "h2"

		# set content
		ctx["html"] = soup.body.renderContents().decode("utf-8")

		base, ext = os.path.splitext(os.path.basename(filename))

		try:
			fComponents = base.split("-")
			logID = int(fComponents[1])
			ctx["eventID"] = fComponents[2]
		except:
			raise Exception("skipping file with invalid file name format: %s" % filename)
			os.remove(filename)
			return False

		outdir = os.path.join(path, ctx["eventID"])
		if not os.path.exists(outdir):
			os.makedirs(outdir)

		ctx["ARRIVALS_AVAIL"] = os.path.exists(os.path.join(outdir, "arrivals.html"))
		ctx["PLOTS_QUALITY_AVAIL"] = os.path.exists(os.path.join(outdir, "quality.html"))
		ctx["BULLETIN_AVAIL"] = True
		ctx['PAGE'] = 'bulletin'

		# try to get the region
		self._cursor.execute('select region from events where eventID=?', (ctx["eventID"],))
		region = self._cursor.fetchone()
		if not region:
			ctx["origin"] = {"evaluationMode": None}
		else:
			ctx["origin"] = {"evaluationMode": True}
			ctx["event"] = {"region": region[0]}

		if not ctx.has_key("event"):
			ctx["event"] = {"publicID": ctx["eventID"]}
		elif not ctx["event"].has_key("publicID"):
			ctx["event"]["publicID"] = ctx["eventID"]

		# add bulletin to database
		bulletin_type = b.extParameters.get("Type", "")
		subject = b.subject

		# get last inserted bulletin id
		self._cursor.execute("select max(idx) from bulletins where eventID=?", (ctx["eventID"],))
		idx = self._cursor.fetchone()[0]

		# first bulletin
		if not idx:
			idx = 0

		idx += 1

		dbtuple = (b.subject, bulletin_type, ctx["eventID"], idx)
		self._cursor.execute('''insert into bulletins
		                        (
		                          subject, type, eventID, idx
		                        )
		                        values
		                        (
		                          ?,?,?,?
		                        )''', dbtuple)

		self._db.commit()

		# refresh overview site
		self._refreshOverview(outdir, ctx["eventID"], ctx)

		self._generator.processTemplate(os.path.join(outdir, "bulletin_" + str(idx) + ".html"), self._eventBulletinTemplate, ctx)
		self._app.publishXML(ctx["eventID"], self._cursor)

		return True

	def _refreshOverview(self, outdir, eid, ctx):
		# load bulletins from database and create an overview PAGE
		rows = self._cursor.execute('''select ctime, type, subject, idx
		                               from bulletins where eventID=? order by type, idx''', (eid,))


		bulletins = {}
		for row in rows:
			item = {}
			item['href'] = 'bulletin_' + str(row[3]) + '.html'
			item['ctime'] = filters.dbdate(row[0])
			item['type'] = row[1]
			item['subject'] = row[2]
			item['idx'] = row[3]

			# sort by types
			if row[1] not in bulletins.keys():
				bulletins[row[1]] = {"bulletins": [item]}
			else:
				bulletins[row[1]]["bulletins"].append(item)

		ctx["bulletins"] = bulletins
		self._generator.processTemplate(os.path.join(outdir, "bulletinoverview.html"), self._eventBulletinOverviewTemplate, ctx)

	def processEvent(self, ctx, path):
		seiscomp3.Logging.debug("[%s] processEvent()" % self._name)
		ctx["BULLETIN_AVAIL"] = True

		ctx['PAGE'] = 'bulletin'
		event = ctx['event']
		eid = str(event.publicID)
		self._refreshOverview(path, eid, ctx)

		return True
