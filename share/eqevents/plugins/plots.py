import math
import matplotlib, os
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import traceback
from pylab import *

import seiscomp3.Logging

import eqelib.plugin as plugin, eqelib.filters
import eqelib.settings as settings


class EvalType:
	AUTOMATIC = 0
	MANUAL    = 1
	UNUSED    = 2


class ArrivalData:

	def __init__(self):
		self.pHase        = True
		self.evalType     = EvalType.AUTOMATIC

		self.distance     = None
		self.azimuth      = None
		self.timeResidual = None
		self.travelTime   = None

	def __str__(self):
		return "P: %s, ET: %s, D: %s, A: %s, R: %s, TT: %s" % (
		       self.pPhase, self.evalType, self.distance,
		       self.azimuth, self.timeResidual, self.travelTime)

	def __getattr__(self, name):
		if name == 'distance':
			return self.distance
		elif name == 'azimuth':
			return self.azimuth
		elif name == 'timeResidual':
			return self.timeResidual
		elif name == 'travelTime':
			return self.travelTime

		return None



class Plugin(plugin.PluginBase):
	VERSION = "0.2"

	def __init__(self, app, generator, env, db, cursor):
		self._generator = generator
		try:
			self._eventQualityTemplate = env.get_template("event/quality.html")
		except Exception, e:
			raise Exception("loading event/quality.html: %s" % str(e))



	def processEvent(self, ctx, path):
		seiscomp3.Logging.debug("[%s] processEvent()" % self._name)
		ctx['PLOTS_QUALITY_AVAIL'] = False

		if not ctx['ARRIVALS_AVAIL']:
			return False

		data = self.readArrivals(ctx)

		plotAvail = self.plotResidual(data, "distance", "timeResidual", os.path.join(path, "plot-dr.png"))
		plotAvail = self.plotResidual(data, "azimuth", "timeResidual", os.path.join(path, "plot-ar.png")) or plotAvail
		plotAvail = self.plotPolar(data, "azimuth", "distance", os.path.join(path, "plot-pl.png")) or plotAvail
		plotAvail = self.plotTT(data, "distance", "travelTime", os.path.join(path, "plot-tt.png")) or plotAvail

		if plotAvail:
			ctx['PLOTS_QUALITY_AVAIL'] = True
			ctx['PAGE'] = 'quality'
			self._generator.processTemplate(os.path.join(path, "quality.html"), self._eventQualityTemplate, ctx)

		return plotAvail



	def readArrivals(self, ctx):
		origin = ctx['origin']
		pool = ctx['POOL']
		data = []

		# read origin time
		try:
			otime = eqelib.filters.qmldate(origin.time.value)
		except:
			otime = None

		# collect arrivals in array of same type
		for arr in eqelib.filters.minWeight(origin.arrival, settings.MIN_ARRIVAL_WEIGHT):
			ad = ArrivalData()

			# phase type
			if arr.phase is None or len(arr.phase) == 0:
				continue
			ad.pPhase = arr.phase[-1] == "P"
			if not ad.pPhase and arr.phase[-1] != "S":
				continue

			# read evaluation mode and pick time from referenced pick
			pick = pool[arr.pickID]
			if pick is not None:
				if pick.evaluationMode == "manual":
					ad.evalType = EvalType.MANUAL
				try:
					pickTime = eqelib.filters.qmldate(pick.time.value)
					ad.travelTime = (pickTime - otime).total_seconds()
				except:
					pass

			ad.distance = arr.distance
			ad.azimuth = arr.azimuth
			ad.timeResidual = arr.timeResidual

			if arr.weight is None or arr.weight == 0:
				ad.evalType = EvalType.UNUSED

			data.append(ad)

		return data



	def plot(self, ax, data, getX, getY):
		x1 = [] # P automatic
		y1 = []
		x2 = [] # S automatic
		y2 = []
		x3 = [] # P manual
		y3 = []
		x4 = [] # S manual
		y4 = []
		x5 = [] # P weight=0
		y5 = []
		x6 = [] # S weight=0
		y6 = []

		# collect arrivals in array of same type
		for ad in data:
			try:
				x = getX(ad)
				y = getY(ad)
				if x is None or y is None:
					continue
			except:
				continue

			if ad.evalType == EvalType.AUTOMATIC:
				if ad.pPhase:
					x1.append(x)
					y1.append(y)
				else:
					x2.append(x)
					y2.append(y)
			elif ad.evalType == EvalType.MANUAL:
				if ad.pPhase:
					x3.append(x)
					y3.append(y)
				else:
					x4.append(x)
					y4.append(y)
			else:
				if ad.pPhase:
					x5.append(x)
					y5.append(y)
				else:
					x6.append(x)
					y6.append(y)

		# plot marker
		ms = 4
		ax.plot(x1, y1, "o", markersize=ms, color="red")
		ax.plot(x2, y2, "s", markersize=ms, color="red")
		ax.plot(x3, y3, "o", markersize=ms, color="green")
		ax.plot(x4, y4, "s", markersize=ms, color="green")
		ax.plot(x5, y5, "o", markersize=ms, color="grey")
		ax.plot(x6, y6, "s", markersize=ms, color="grey")

		# set x and y range
		x = x1 + x2 + x3 + x4 + x5 + x6
		y = y1 + y2 + y3 + y4 + y5 + y6
		return x, y



	def plotResidual(self, data, attr_x, attr_y, filename):
		try:
			fig = plt.figure(figsize=(4.65, 3.15))
			ax = fig.add_subplot(1, 1, 1)
			fig.subplots_adjust(left=0.12, right=0.96, bottom=0.12, top=0.95)

			x, y = self.plot(ax, data,
			                 lambda a: getattr(a, attr_x),
			                 lambda a: getattr(a, attr_y))

			yRange = max(abs(max(y)), abs(min(y))) * 1.2 if len(y) > 0 else 1
			ax.set_xlim(0, max(x) * 1.1 if len(x) > 0 else 1)
			ax.set_ylim(-yRange, yRange)

			# plot grid and tics
			ax.grid(color='grey', linestyle='-', linewidth=0.2)
			xticklabels = getp(gca(), 'xticklabels')
			yticklabels = getp(gca(), 'yticklabels')
			setp(yticklabels, fontsize='xx-small')
			setp(xticklabels, fontsize='xx-small')

			# labels
			if attr_x == "timeResidual":
				xLabel ="residual (sec.)"
			elif attr_x == "distance" or attr_x == "azimuth":
				xLabel = attr_x.lower() + " (degree)"
			else:
				xLabel = "x-axis"

			if attr_y == "timeResidual":
				yLabel = "residual (sec.)"
			elif attr_y == "distance" or attr_y == "azimuth":
				yLabel = attr_y.lower() + " (degree)"
			else:
				yLabel = "y-axis"

			plt.xlabel(xLabel, fontsize='xx-small')
			plt.ylabel(yLabel, fontsize='xx-small')
			plt.axhline(linewidth=1.1, color='b')

			# save image
			fig.savefig(filename, facecolor='#FFFFFF', edgecolor='#FFFFFF')
			plt.close()

		except Exception, e:
			seiscomp3.Logging.error("[%s] could not create %s (%s)\ntraceback: %s" % (
			                        self._name, filename, str(e), traceback.format_exc()))
			try:
				plt.close()
			except:
				pass
			return False

		seiscomp3.Logging.debug("+ %s" % filename)
		return True



	def plotPolar(self, data, attr_x, attr_y, filename):
		try:
			fig = plt.figure(figsize=(3.15, 3.15))
			ax = fig.add_axes([0.08, 0.08, 0.84, 0.84], projection='polar')
			ax.set_theta_zero_location('N')
			ax.set_theta_direction(-1)

			x, y = self.plot(ax, data,
			                 lambda a: math.radians(getattr(a, attr_x)),
			                 lambda a: getattr(a, attr_y))
			maxY = float(max(y)) * 1.05 if len(y) > 0 else 1

			c0 = '#FFFFFF'
			c60 = 'E6E6E6'
			c90 = 'CCCCCC'
			c120 = 'B3B3B3'

			if maxY > 60.0:
				step = 20.0
			elif maxY > 10.0:
				step = 10.0
			else:
				step = 2.0

			maxY = int(math.ceil(maxY / step) * step)

			ax.bar(0, maxY, width=10, bottom=0.0, edgecolor="#FFFFFF", color="#FFFFFF")
			if maxY >= 60:
				ax.bar(60, maxY-60, width=10, bottom=60.0, edgecolor="#E6E6E6", color="#E6E6E6")
			if maxY >= 90:
				ax.bar(90, maxY-90, width=10, bottom=90.0, edgecolor="#CCCCCC", color="#CCCCCC")
			if maxY >= 120:
				ax.bar(120, maxY-120, width=10, bottom=120.0, edgecolor="#B3B3B3", color="#B3B3B3")

			xticklabels = getp(gca(), 'xticklabels')
			yticklabels = getp(gca(), 'yticklabels')
			setp(yticklabels, fontsize='x-small')
			setp(xticklabels, fontsize='x-small')

			fig.savefig(filename, facecolor="#FFFFFF", edgecolor='#FFFFFF')
			plt.close()

		except Exception, e:
			seiscomp3.Logging.error("[%s] could not create %s (%s)\ntraceback: %s" % (
			                        self._name, filename, str(e), traceback.format_exc()))
			try:
				plt.close()
			except:
				pass
			return False

		seiscomp3.Logging.debug("+ %s" % filename)
		return True



	def plotTT(self, data, attr_x, attr_y, filename):
		try:
			fig = plt.figure(figsize=(4.65, 3.15))
			ax = fig.add_subplot(1, 1, 1)
			fig.subplots_adjust(left=0.12, right=0.96, bottom=0.12, top=0.95)

			x, y = self.plot(ax, data,
			                 lambda a: getattr(a, attr_x),
			                 lambda a: getattr(a, attr_y))

			ax.set_xlim(0, max(x) * 1.1 if len(x) > 0 else 1)
			ax.set_ylim(0, max(y) * 1.2 if len(y) > 0 else 1)

			# plot grid and tics
			ax.grid(color='grey', linestyle='-', linewidth=0.2)
			xticklabels = getp(gca(), 'xticklabels')
			yticklabels = getp(gca(), 'yticklabels')
			setp(yticklabels, fontsize='xx-small')
			setp(xticklabels, fontsize='xx-small')

			# labels
			if attr_x == "distance":
				xLabel ="distance (degree)"
			elif attr_x == "travelTime":
				xLabel ="travel time (sec)"
			else:
				xLabel = "x-axis"

			if attr_y == "distance":
				yLabel ="distance (degree)"
			elif attr_y == "travelTime":
				yLabel ="travel time (sec)"
			else:
				yLabel = "y-axis"

			plt.xlabel(xLabel, fontsize='xx-small')
			plt.ylabel(yLabel, fontsize='xx-small')
			#l = plt.axhline(linewidth=1.1, color='b')

			# save image
			fig.savefig(filename, facecolor='#FFFFFF', edgecolor='#FFFFFF')
			plt.close()

		except Exception, e:
			seiscomp3.Logging.error("[%s] could not create %s (%s)\ntraceback: %s" % (
			                        self._name, filename, str(e), traceback.format_exc()))
			try:
				plt.close()
			except:
				pass
			return False

		seiscomp3.Logging.debug("+ %s" % filename)
		return True



