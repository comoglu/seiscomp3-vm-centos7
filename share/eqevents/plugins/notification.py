import os, sys, codecs

from socketIO_client import SocketIO, BaseNamespace

import seiscomp3.Logging

import eqelib.plugin as plugin
import eqelib.settings as settings
import eqelib.bulletin as bulletin

class Plugin(plugin.PluginBase):
	VERSION = "0.1"

	def __init__(self, app, generator, env, db, cursor):
		self._app = app
		self._cursor = cursor

		try:
			settings.NOTIFICATION_SERVER = app.configGetString("notification.server")
		except:
			raise Exception("notification.server not set")

		try:
			settings.NOTIFICATION_PORT = app.configGetInt("notification.port")
		except:
			settings.NOTIFICATION_PORT = 80

		try:
			settings.NOTIFICATION_NAMESPACE = app.configGetString("notification.namespace")
		except:
			settings.NOTIFICATION_NAMESPACE = "/event"


	def processEvent(self, ctx, path):
		seiscomp3.Logging.debug("[%s] processEvent()" % self._name)
		socketIO = SocketIO(settings.NOTIFICATION_SERVER, settings.NOTIFICATION_PORT)
		namespace = socketIO.define(BaseNamespace, settings.NOTIFICATION_NAMESPACE)

		namespace.emit('event',ctx['ID'])
		socketIO.wait(seconds=1)

		print >> sys.stderr, "Send Notification"

		return True

	def processInvalid(self, ctx, filename, path):
		seiscomp3.Logging.debug("[%s] processInvalid()" % self._name)
		if not ctx:
			ctx = {}

		f = codecs.open(filename,"r", "utf-8")
		content = f.read()
		f.close()

		# try to parse bulletin format
		try:
			b = bulletin.Bulletin()
			b.read(content)
		except Exception, e:
			raise Exception("could not parse bulletin: %s" % str(e))

		base, ext = os.path.splitext(os.path.basename(filename))

		try:
			fComponents = base.split("-")
			logID = int(fComponents[1])
			ctx["eventID"] = fComponents[2]
		except:
			raise Exception("skipping file with invalid file name format: %s" % filename)
			os.remove(filename)
			return False

		# get last inserted bulletin id
		self._cursor.execute("select max(idx) from bulletins where eventID=?", (ctx["eventID"],))
		idx = self._cursor.fetchone()[0]

		# first bulletin
		if not idx:
			idx = 0

		idx += 1

		socketIO = SocketIO(settings.NOTIFICATION_SERVER, settings.NOTIFICATION_PORT)
		namespace = socketIO.define(BaseNamespace, settings.NOTIFICATION_NAMESPACE)

		namespace.emit('bulletin',ctx["eventID"], idx)
		socketIO.wait(seconds=1)

		print >> sys.stderr, "Send Notification for bulletin"



		return True
