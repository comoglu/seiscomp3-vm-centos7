import sys, os, datetime, calendar

import seiscomp3.Logging

import eqelib.plugin as plugin
import eqelib.settings as settings
import eqelib.filters as filters


class CountItem:
	def __init__(self, m):
		self.cnt = 0
		self.map = m

	def format(self, key, depth=0):
		print "%s%s: %i" % (" " * depth * 4, str(key), self.cnt)
		for k,v in self.map.items():
			v.format(k, depth + 1)

class AutoVivification(dict):
	"""Implementation of perl's autovivification feature."""
	def __getitem__(self, item):
		try:
			return dict.__getitem__(self, item)
		except KeyError:
			value = self[item] = CountItem(type(self)())
			return value

	def format(self):
		for k,v in self.items():
			v.format(k)


MONTH_NAMES = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
MONTH_NAMES_ABBR = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

def monthName(value):
	return MONTH_NAMES[value-1]

def monthAbbr(value):
	return MONTH_NAMES_ABBR[value-1]


class Plugin(plugin.PluginBase):
	VERSION = '0.2'
	CONTEXT_TAG = 'BROWSER_AVAIL'

	def __init__(self, app, generator, env, db, cursor):
		self._generator = generator
		self._db = db
		self._cursor = cursor

		env.filters['month_name'] = monthName
		env.filters['month_abbr'] = monthAbbr

		self._tplRoot = env.get_template("browser/index.html")
		self._tplYear = env.get_template("browser/year.html")
		self._tplMonth = env.get_template("browser/month.html")
		self._tplDay = env.get_template("browser/day.html")

		self._browserDir = os.path.join(settings.OUT_DIR, 'browser')

		self._eventCounts = AutoVivification()
		rows = self._cursor.execute('''select strftime("%Y",otime),
		                                      strftime("%m",otime),
		                                      strftime("%d",otime),
		                                      count(*)
		                               from events
		                               group by strftime("%Y %m %d",otime)''')
		for row in rows:
			y = int(row[0])
			m = int(row[1])
			d = int(row[2])
			cnt = int(row[3])
			self._eventCounts[y].map[m].map[d].cnt = cnt

		for ky,vy in self._eventCounts.iteritems():
			for km,vm in vy.map.iteritems():
				for kd,vd in vm.map.iteritems():
					vm.cnt += vd.cnt
				vy.cnt += vm.cnt

		self._dirtyTree = AutoVivification()


	def pushIndex(self, old_time, new_time):
		if old_time:
			# Decrease counts
			self._dirtyTree[old_time.year].map[old_time.month].map[old_time.day].cnt -= 1
			self._dirtyTree[old_time.year].map[old_time.month].cnt -= 1
			self._dirtyTree[old_time.year].cnt -= 1

		if new_time:
			# Increase counts
			# Decrease counts
			self._dirtyTree[new_time.year].map[new_time.month].map[new_time.day].cnt += 1
			self._dirtyTree[new_time.year].map[new_time.month].cnt += 1
			self._dirtyTree[new_time.year].cnt += 1

		return True


	def processIndex(self, ctx, forceUpdate):
		seiscomp3.Logging.debug("[%s] processIndex()" % self._name)
		ctx[Plugin.CONTEXT_TAG] = False

		if not os.path.exists(self._browserDir):
			os.makedirs(self._browserDir)

		bckStatic = ctx['STATIC_URL']
		bckRoot = ctx['ROOT_URL']
		ctx['STATIC_URL'] = "../" + ctx['STATIC_URL']
		ctx['ROOT_URL'] = "../" + ctx['ROOT_URL']

		updateBrowserIndex = forceUpdate

		incremental = True

		# Apply dirty tree
		for ky,vdy in self._dirtyTree.items():
			vy = self._eventCounts[ky]
			vy.cnt += vdy.cnt
			for km,vdm in vdy.map.items():
				vm = vy.map[km]
				vm.cnt += vdm.cnt
				for kd,vdd in vdm.map.items():
					vd = vm.map[kd]
					vd.cnt += vdd.cnt
					if vd.cnt == 0:
						updateBrowserIndex = True
						del vm.map[kd]
						del vdm.map[kd]
						fn = os.path.join(self._browserDir, str(ky), str(km), "%d.html" % kd)
						try:
							os.remove(fn)
							seiscomp3.Logging.debug("- %s" % str(fn))
						except: pass

				if vm.cnt == 0:
					updateBrowserIndex = True
					del vy.map[km]
					del vdy.map[km]
					fn = os.path.join(self._browserDir, str(ky), "%d.html" % km)
					try:
						os.remove(fn)
						seiscomp3.Logging.debug("- %s" % str(fn))
					except: pass

			if vy.cnt == 0:
				updateBrowserIndex = True
				del self._eventCounts[ky]
				del self._dirtyTree[ky]
				fn = os.path.join(self._browserDir, "%d.html" % ky)
				try:
					os.remove(fn)
					seiscomp3.Logging.debug("- %s" % str(fn))
				except: pass

		if incremental:
			iteritems = self._dirtyTree.iteritems()
		else:
			iteritems = self._eventCounts.iteritems()

		# Update dirty pages
		for ky,vdy in iteritems:
			updateBrowserIndex = True

			vy = self._eventCounts[ky]

			ctx['BROWSER_YEAR'] = ky
			ctx['BROWSER_MONTHS'] = vy.map

			seiscomp3.Logging.debug("[%s] updating year %d (%d)" % (self._name, ky, vy.cnt))
			self._generator.processTemplate(os.path.join(self._browserDir, "%d.html" % ky), self._tplYear, ctx)

			dirYear = os.path.join(self._browserDir, str(ky))
			if not os.path.exists(dirYear):
				os.makedirs(dirYear)

			bckStaticM = ctx['STATIC_URL']
			bckRootM = ctx['ROOT_URL']
			ctx['STATIC_URL'] = "../" + ctx['STATIC_URL']
			ctx['ROOT_URL'] = "../" + ctx['ROOT_URL']

			for km,vdm in vdy.map.iteritems():
				# Get real map from event counts
				vm = vy.map[km]

				seiscomp3.Logging.debug("[%s] updating month %d (%d)" % (self._name, km, vm.cnt))

				monthrange = calendar.monthrange(ky,km)

				ctx['BROWSER_MONTH'] = km
				ctx['BROWSER_DAYS_PER_MONTH'] = monthrange[1]
				ctx['BROWSER_DAY_OFFSET'] = monthrange[0]
				ctx['BROWSER_DAYS'] = vm.map
				self._generator.processTemplate(os.path.join(dirYear, "%d.html" % km), self._tplMonth, ctx)

				dirMonth = os.path.join(dirYear, str(km))
				if not os.path.exists(dirMonth):
					os.makedirs(dirMonth)

				bckStaticD = ctx['STATIC_URL']
				bckRootD = ctx['ROOT_URL']
				ctx['STATIC_URL'] = "../" + ctx['STATIC_URL']
				ctx['ROOT_URL'] = "../" + ctx['ROOT_URL']

				for kd,vdd in vdm.map.iteritems():
					# Get real map from event counts
					vd = vm.map[kd]

					seiscomp3.Logging.debug("[%s] updating day %d (%d)" % (self._name, kd, vd.cnt))

					startDate = datetime.datetime(ky,km,kd)
					# Read all events for that day
					rows = self._cursor.execute('''select eventID,otime,lat,lon,depth,phases,mag,mag_t,
					                                      agency,status,type,region,updated
					                               from events
					                               where otime>=? and otime<?
					                               order by otime asc''', (startDate, startDate+datetime.timedelta(1)))

					items = []
					for row in rows:
						item = {}
						item['path'] = 'events/' + row[0].replace('#', '_').replace('/', '_') + '/overview.html'
						item['eventID'] = row[0]
						item['otime'] = filters.dbdate(row[1])
						item['lat'] = row[2]
						item['lon'] = row[3]
						item['depth'] = row[4]
						item['phases'] = row[5]
						item['mag'] = row[6]
						item['mag_t'] = row[7]
						item['agency'] = row[8]
						item['status'] = row[9]
						item['type'] = row[10]
						item['region'] = row[11]
						item['upd'] = filters.dbdate(row[12])
						items.append(item)

					ctx['BROWSER_DAY'] = kd
					ctx['eventTable'] = items
					self._generator.processTemplate(os.path.join(dirMonth, "%d.html" % kd), self._tplDay, ctx)

				ctx['STATIC_URL'] = bckStaticD
				ctx['ROOT_URL'] = bckRootD

			ctx['STATIC_URL'] = bckStaticM
			ctx['ROOT_URL'] = bckRootM

		ctx['STATIC_URL'] = bckStatic
		ctx['ROOT_URL'] = bckRoot

		# Update index if years changed
		if updateBrowserIndex:
			ctx['BROWSER_YEARS'] = self._eventCounts
			self._generator.processTemplate(os.path.join(settings.OUT_DIR, "browser.html"), self._tplRoot, ctx)

		# Reset dirty tree
		self._dirtyTree = AutoVivification()

		# Flag availability of browser pages
		ctx[Plugin.CONTEXT_TAG] = True

		return True
