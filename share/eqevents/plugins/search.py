import os

import seiscomp3.Logging

import eqelib.plugin as plugin
import eqelib.settings as settings
import eqelib.filters as filters


class Plugin(plugin.PluginBase):
	VERSION = '0.2'
	CONTEXT_TAG = 'SEARCH_AVAIL'

	def __init__(self, app, generator, env, db, cursor):
		self._generator = generator

		try:
			self._template = env.get_template("search.html")
		except Exception, e:
			raise Exception("loading search.html: %s" % str(e))

		try:
			settings.SEARCH_DISTANCE_UNIT = app.configGetString("search.distanceUnit")
			if settings.SEARCH_DISTANCE_UNIT != "km" and settings.SEARCH_DISTANCE_UNIT != "mi":
				seiscomp3.Logging.warning("Invalid distance unit '%s', falling back to 'km'" % settings.SEARCH_DISTANCE_UNIT)
				settings.SEARCH_DISTANCE_UNIT = "km"
		except:
			settings.SEARCH_DISTANCE_UNIT = "km"

		try:
			settings.SEARCH_WITH_MAP = app.configGetBool("search.enableMap")
		except:
			settings.SEARCH_WITH_MAP = False


	def processIndex(self, ctx, forceUpdate):
		seiscomp3.Logging.debug("[%s] processIndex()" % self._name)
		ctx[Plugin.CONTEXT_TAG] = True
		self._generator.processTemplate(os.path.join(settings.OUT_DIR, "search.html"), self._template, ctx)
