import sys, os, datetime

import seiscomp3.Logging

import eqelib.plugin as plugin
import eqelib.settings as settings
import eqelib.filters as filters

class Plugin(plugin.PluginBase):
	VERSION = "0.1"
	CONTEXT_TAG = 'RSS_AVAIL'

	def __init__(self, app, generator, env, db, cursor):
		self._cursor = cursor
		self._generator = generator

		try:
			self._rssTemplate = env.get_template("feed.rss")
		except Exception, e:
			raise Exception("loading feed.rss: %s" % str(e))

		try:
			settings.RSS_DAYS = app.configGetInt("rss.days")
		except:
			settings.RSS_DAYS = 1

		if settings.RSS_DAYS > 1:
			days = "%d days" % settings.RSS_DAYS
		else:
			days = "day"

		try:
			settings.RSS_TITLE = app.configGetString("rss.title")
		except:
			settings.RSS_TITLE = "EQ Events RSS Channel - last ${days}"

		settings.RSS_TITLE = settings.RSS_TITLE.replace("${days}", days)

		try:
			settings.RSS_DESC = app.configGetString("rss.desc")
		except:
			settings.RSS_DESC = "Real-time earthquake locations"

		try:
			settings.RSS_LINK = app.configGetString("rss.link")
		except:
			raise Exception("rss.link not set")


	def processIndex(self, ctx, forceUpdate):
		seiscomp3.Logging.debug("[%s] processIndex()" % self._name)
		ctx[Plugin.CONTEXT_TAG] = False

		pubdate = datetime.datetime.utcnow()
		ctx["RSS_PUBDATE"] = pubdate
		startdate = pubdate - datetime.timedelta(settings.RSS_DAYS)
		rows = self._cursor.execute('''select eventID,otime,lat,lon,depth,phases,mag,mag_t,
		                               agency,status,type,region,updated
		                               from events
		                               where otime >= ?
		                               order by otime desc''', (startdate,))
		items = []
		for row in rows:
			item = {}
			item['path'] = 'events/' + row[0].replace('#', '_').replace('/', '_') + '/overview.html'
			item['eventID'] = row[0]
			item['otime'] = filters.dbdate(row[1])
			item['lat'] = row[2]
			item['lon'] = row[3]
			item['depth'] = row[4]
			item['phases'] = row[5]
			item['mag'] = row[6]
			item['mag_t'] = row[7]
			item['agency'] = row[8]
			item['status'] = row[9]
			item['type'] = row[10]
			item['region'] = row[11]
			item['upd'] = filters.dbdate(row[12])
			items.append(item)

		ctx['rssEvents'] = items
		self._generator.processTemplate(os.path.join(settings.OUT_DIR, "feed.rss"), self._rssTemplate, ctx)
		del ctx['rssEvents']

		ctx[Plugin.CONTEXT_TAG] = True
