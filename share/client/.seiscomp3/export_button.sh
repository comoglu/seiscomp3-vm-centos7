#!/bin/bash


############################################################################
# Copyright (C) 2017 by gempa GmbH                                         #
#                                                                          #
# All Rights Reserved.                                                     #
#                                                                          #
# NOTICE: All information contained herein is, and remains                 #
# the property of gempa GmbH and its suppliers, if any. The intellectual   #
# and technical concepts contained herein are proprietary to gempa GmbH    #
# and its suppliers.                                                       #
# Dissemination of this information or reproduction of this material       #
# is strictly forbidden unless prior written permission is obtained        #
# from gempa GmbH.                                                         #
#                                                                          #
# Author: Dirk Roessler                                                    #
# Email: roessler@gempa.de                                                 #
############################################################################

# create event bulletin from scolv export

for ((i=1;i<=$#;i++)); do
        if [ ${!i} = "-e" ];
                then ((i++))
                evtFile=${!i};
        fi
done

evtID=`sed -n 's/.*<event publicID="\(.*\)".*/\1/p' $evtFile`

scbulletin -E $evtID -d mysql://sysop:sysop@127.0.0.1:3308/seiscomp31 -3 1> /tmp/$evtID.bul

# kate / gedit
kate /tmp/$evtID.bul &

rm -f $evtFile
