#!/bin/bash

# West Australia
scrttv -u scrttvwa --resortAutomatically="false" --autoApplyFilter="true"  \
   --bufferSize="1800" --streams.codes="AU.*.*.BHZ,AU.*.*.SHZ" --streams.sort.latitude="-10" --streams.sort.longitude="110" \
   --streams.region.lonmin="110" --streams.region.lonmax="135" --streams.region.latmin="-45" --streams.region.latmax="-10" -v &

# East Australia
scrttv -u scrttvea --resortAutomatically="false" --autoApplyFilter="true"  \
   --bufferSize="1800" --streams.codes="AU.*.*.BHZ,AU.*.*.SHZ" --streams.sort.latitude="-10" --streams.sort.longitude="135" \
   --streams.region.lonmin="135" --streams.region.lonmax="155" --streams.region.latmin="-45" --streams.region.latmax="-10" -v &

