#!/bin/bash

#-------------------------------------------------------------------------------
# update the route53 records
#-------------------------------------------------------------------------------
. /opt/sc3_install/setup_env.sh

function getzoneid() {
    q="HostedZones[?Name==\`$2.\`][Id]" \
    mpzid=$(aws route53 list-hosted-zones \
        --query "$q" \
        --output text)

    export $1=$mpzid
}

# get the id of the 'main' private hosted zone (sc3prodstack.private)
getzoneid main_pz_id "$SC3_STACK_PRIVATE_ZONE_NAME" || (echo "failed to retrieve id of the private hosted zone"; exit 1)

myip=`hostname -i | sed 's/^.* //g'`

# modify the records of sc3prodstack.private
aws route53 change-resource-record-sets \
    --hosted-zone-id "$main_pz_id" \
    --change-batch "{ \
        \"Changes\": [{ \
            \"Action\": \"UPSERT\", \
            \"ResourceRecordSet\": { \
                \"Name\" : \"${SHORT_HOST_NAME}.${SC3_STACK_PRIVATE_ZONE_NAME}.\", \
                \"Type\" : \"A\", \
                \"TTL\"  : 60, \
                \"ResourceRecords\": [{ \
                    \"Value\": \"$myip\" \
                }] \
            } \
        }] \
    }" > /dev/null \
    || echo "failed to configure private zone DNS: you should check this"
