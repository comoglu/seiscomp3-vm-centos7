#!/bin/bash

#-------------------------------------------------------------------------------
# this should be excecuted as $EC2_USER_NAME. Unlike the setup  scripts for
# acquistion, this is the only script to be run so we use sudo rather than a
# separate script.
#-------------------------------------------------------------------------------

# this must be kept in sync with ../files/create_base_image.sh.
source /opt/sc3_install/setup_env.sh

# We source .bashrc because this script is run by root and we want the . We do,
# however, want some of the environment variables from root, so we preserve
# most of its environment.
source $EC2_USER_HOME_FOLDER/.bashrc

# modify the route 53 records
$SETUP_SCRIPT_DIR/setup_route53.sh

# ensure that seiscomp is stopped before we try to cleanup its installation dir.
seiscomp stop

# get the release we are interested in
seiscomp_archive="$STACK_TAG".tar.gz
rm -rf $SEISCOMP_HOME_FOLDER/seiscomp3
aws s3 cp s3://"$SC3_CONFIG_BUCKET"/"$seiscomp_archive" /tmp/"$seiscomp_archive"
tar -xzf /tmp/"$seiscomp_archive" -C /opt

# finalise the installation (this file gets created by the command above)
# These are the permissions in git (or should be) but just to make sure...)
chmod +x $SEISCOMP_HOME_FOLDER/share/eatws/finalise_setup.sh
$SEISCOMP_HOME_FOLDER/share/eatws/finalise_setup.sh
