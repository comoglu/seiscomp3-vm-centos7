#!/bin/bash

# make all scripts executable
chown -R $EC2_USER_NAME:eatws $SETUP_SCRIPT_DIR/*
chmod -R +x $SETUP_SCRIPT_DIR/*sh

# create a config file
# ensure system does not prompt for confirming
# host key file addtion to know hosts.
touch config
echo "StrictHostKeyChecking no" >> config
echo "UserKnownHostsFile /dev/null" >> config
chmod 600 config
mkdir -p $EC2_USER_HOME_FOLDER/.ssh/
cp config $EC2_USER_HOME_FOLDER/.ssh/

echo "Creating the aws config folder."
## Copy AWS credentials
mkdir -p $EC2_USER_HOME_FOLDER/.aws
cp $SETUP_SCRIPT_DIR/aws_*.template $EC2_USER_HOME_FOLDER/.aws
mv  $EC2_USER_HOME_FOLDER/.aws/aws_config.template  $EC2_USER_HOME_FOLDER/.aws/config
mv  $EC2_USER_HOME_FOLDER/.aws/aws_credentials.template  $EC2_USER_HOME_FOLDER/.aws/credentials

sed -i 's/__role__/'"${AWS_ROLE}"'/' $EC2_USER_HOME_FOLDER/.aws/config
sed -i 's/__username__/'"${AWS_USER}"'/' $EC2_USER_HOME_FOLDER/.aws/config
sed -i 's/__accesskey__/'"${AWS_ACCESS_KEY_ID}"'/' $EC2_USER_HOME_FOLDER/.aws/credentials
sed -i 's/__secretkey__/'"${AWS_SECRET_ACCESS_KEY}"'/' $EC2_USER_HOME_FOLDER/.aws/credentials

## make all scripts executable
sudo chmod a+x $SETUP_SCRIPT_DIR/*.py
sudo chmod a+x $SETUP_SCRIPT_DIR/*.sh
sudo chmod a+x $SETUP_SCRIPT_DIR/*.desktop

# copy all files from sc3_install to client foldre
cp $SETUP_SCRIPT_DIR/* $SEISCOMP_HOME_FOLDER/share/client/

export AWS_PROFILE=eatws_admin
# just incase this is not set elsewhere
export AWS_DEFAULT_REGION=ap-southeast-2

# install the connection monitor services
sudo cp $SETUP_SCRIPT_DIR/sshmonitor*.service /etc/systemd/system
rm $SETUP_SCRIPT_DIR/sshmonitor*.service
sudo chmod 644 /etc/systemd/system/sshmonitor*.service

# enable the connection monitor services
sudo systemctl enable sshmonitorserver
# the following does not work. I'm not sure what to put in the [install] section
# of the systemd unit. Instead, one should run
# `sudo systemctl start sshmonitorclient` when the user logs on.
#sudo systemctl enable sshmonitorclient

### DO I NEED THE EVENT DOWNLOADER HERE......
### Event downloader
git clone git@bitbucket.org:geoscienceaustralia/eatws-event-download.git
sudo pip install ./eatws-event-download

# Download the list of users to  a file users.txt 
# Download the public key for each of the users
cd $EC2_USER_HOME_FOLDER
source $SETUP_SCRIPT_DIR/key_store.sh listusers /PubKey
source $SETUP_SCRIPT_DIR/key_store.sh getall /PubKey
source $SETUP_SCRIPT_DIR/key_store.sh getall /PrivKey
sudo $SETUP_SCRIPT_DIR/createVMusers.sh
bash sc3-infrastructure/packer/files/client/changename.sh

# Concatenate the contents of the downloaded publici/private keys to the duty users
# authorized keys file, along with the ssh config file.
for each_user in `cat users.txt`; do
    #sudo mkdir /home/$each_user/.ssh
    sudo cp $EC2_USER_HOME_FOLDER/PubKey/$each_user.txt /home/$each_user/.ssh/id_rsa.pub
    sudo cp $EC2_USER_HOME_FOLDER/PrivKey/$each_user.txt /home/$each_user/.ssh/id_rsa
    sudo cp $SETUP_SCRIPT_DIR/config /home/$each_user/.ssh
done

#copy the private/pub key of a user for centos to perform its health checks 
#this needs to be revisited to create a specific user to perform this check.
cp $EC2_USER_HOME_FOLDER/PubKey/u50046.txt $EC2_USER_HOME_FOLDER/.ssh/id_rsa.pub
cp $EC2_USER_HOME_FOLDER/PrivKey/u50046.txt $EC2_USER_HOME_FOLDER/.ssh/id_rsa
chmod 600 $EC2_USER_HOME_FOLDER/.ssh/id_rsa*

# copy the scripts and service for each user
for each_user in `cat users.txt`; do
    echo "Creating autostart folder for $each_user"
    sudo runuser -l $each_user -c "mkdir -p /home/$each_user/.config/autostart/"
    sudo runuser -l $each_user -c "cp $SETUP_SCRIPT_DIR/start-sshmonitor.desktop /home/$each_user/.config/autostart/"
    #sudo chown $each_user:eatws /home/$each_user/.config/autostart/start-sshmonitor.desktop
    echo "Creating the desktop folder for $each_user"
    sudo runuser -l $each_user -c "mkdir -p /home/$each_user/Desktop/"
    sudo runuser -l $each_user -c "cp $SETUP_SCRIPT_DIR/Proc*desktop /home/$each_user/Desktop/"
    sudo runuser -l $each_user -c "cp $SETUP_SCRIPT_DIR/Tier2*desktop /home/$each_user/Desktop/"
    #sudo chown -R $each_user:eatws /home/$each_user/
done

# Copy config file to centos ssh folder
cp $SETUP_SCRIPT_DIR/health_check_config $EC2_USER_HOME_FOLDER/.ssh/config

# create the client folder if it does not exist
mkdir -p $SEISCOMP_HOME_FOLDER/share/client 

tar xzf $SETUP_SCRIPT_DIR/scvoice_scripts.tar.gz -C $SEISCOMP_HOME_FOLDER/share/client

# Alias for running sisecomp3 commands 
sudo cp $SETUP_SCRIPT_DIR/*alias.sh /etc/profile.d/
sudo cp $SETUP_SCRIPT_DIR/scrttv_wall?.sh /$SEISCOMP_HOME_FOLDER/share/client
sudo cp $SETUP_SCRIPT_DIR/scrttv_analyst.sh /$SEISCOMP_HOME_FOLDER/share/client

# Copy SC3 configs for the VM
cp $SETUP_SCRIPT_DIR/*.cfg $SEISCOMP_HOME_FOLDER/etc/

# Disable everything with the execption of scmaster and spread
seiscomp disable caps caps2capsG l1autoloc l1autopick l1scanloc l1sceval \
 quakelink sc2ql scamp scanloc scautoloc scautomt scautopick scautowp sceval \
scevent scmag scqc slink2caps scimport gaps gis eqevents

# A dirty hack to remove python package six (multiple installed), hopefully
# we find a better way of doing this later.

/usr/bin/yes |sudo pip uninstall six
#while [ $? == 0 ];
#do 
  /usr/bin/yes |sudo pip uninstall six
#done

sudo pip install six

### Remove AWS credentials
rm -rf $EC2_USER_HOME_FOLDER/.aws/*

## Add new credentials for front desk users
echo "[default]" > $EC2_USER_HOME_FOLDER/.aws/config
echo "region = ap-southeast-2" >> $EC2_USER_HOME_FOLDER/.aws/config

echo "[default]" > $EC2_USER_HOME_FOLDER/.aws/credentials
echo "aws_access_key_id = ${FRONTDESK_ACCESS_KEY_ID}" >> $EC2_USER_HOME_FOLDER/.aws/credentials
echo "aws_secret_access_key = ${FRONTDESK_SECRET_ACCESS_KEY}" >> $EC2_USER_HOME_FOLDER/.aws/credentials

#copy config to all user profiles
cp -r $EC2_USER_HOME_FOLDER/.aws /tmp/aws_config
chmod -R a+r /tmp/aws_config 

for each_user in `cat users.txt`; do
    sudo runuser -l $each_user -c "cp -r /tmp/aws_config /home/$each_user/.aws"
done

#cleanup
rm -rf /tmp/aws_config

# Install Passcal
wget https://www.passcal.nmt.edu/ftp/software/passoft/linux/centOS_7/passcal.2016.098.tar.bz2 2>/dev/null
tar xf passcal.2016.098.tar.bz2 -C /opt/

#add the users to the sudoers, this is so they can execute start sshmonitor as sudo
sudo bash -c 'echo "%eatws ALL=(ALL)NOPASSWD:/usr/bin/systemctl start sshmonitorclient.service" >> /etc/sudoers'
sudo sed -i "s/# %wheel/%wheel/g" /etc/sudoers
