#!/bin/bash

export NO_AT_BRIDGE=1 # Gets rid of some warning

region_list=$(echo "Australia!Southwest!Northwest!North!Central!South!Northeast!Southeast")
buffer_size=$(echo "1!2!3!4!5!6!7!8")
filter_list=$(echo "Default!BP 0.7 - 2 Hz 3rd Order!BP 0.7 - 2 Hz 5th Order!BP 1 - 3 Hz 3rd Order!BP 2 - 4 Hz 3rd Order!BP 3 - 6 Hz 3rd Order!BP 4 - 8 Hz 3rd Order!BP 5 - 10 Hz 3rd Order!HP 5 Hz 3rd Order!HP 8 Hz 3rd Order")
show_picks=$(echo "TRUE!FALSE")
proto_list=$(echo "SLINK!FDSN!CAPS")
current_date=$(date "+%Y-%m-%d %H:%M:%S")
default_date=$(date "+%Y-%m-%d %H:00:00")
remote_host="127.0.0.1:4805"
db_strg="mysql://sysop:sysop@127.0.0.1:3308/seiscomp31"
cond=true

while ($cond);do
    frmdata=$(yad --width=800 --title "Trace View Selection" \
	      --image="./jd_scan.png" \
              --form --date-format="%Y-%m-%d %H:%M:%S"\
              --field="Buffer End Time (UTC)":DT \
              --field="Buffer Length (hours)":CB \
              --field="Protocol":CB \
              --field="Region":CB \
              --field="Filter":CB \
	      --field="Show picks":CB \
	      --field="Show last hour only?":CHK \
	      --button=gtk-quit:1 \
	      --button=gtk-ok:0 \
              "$default_date" "$buffer_size" "$proto_list" "$region_list" "$filter_list" "$show_picks" "$last_hour" )

    foo=$?

    frmdate=$(echo $frmdata | awk 'BEGIN {FS="|" } { print $1 }')
    frmbuffer=$(echo $frmdata | awk 'BEGIN {FS="|" } { print $2 }')
    frmprotocol=$(echo $frmdata | awk 'BEGIN {FS="|" } { print $3 }')
    frmregion=$(echo $frmdata | awk 'BEGIN {FS="|" } { print $4 }') 
    frmfilter=$(echo $frmdata | awk 'BEGIN {FS="|" } { print $5 }')
    frmshowpicks=$(echo $frmdata | awk 'BEGIN {FS="|" } { print $6 }')
    frmlasthour=$(echo $frmdata | awk 'BEGIN {FS="|" } { print $7 }')

    # fix form date to prevent error when leading zeros not present
    year=`echo $frmdate | awk -F"-| |:" '{printf "%04d", $1}'`
    month=`echo $frmdate | awk -F"-| |:" '{printf "%02d", $2}'`
    day=`echo $frmdate | awk -F"-| |:" '{printf "%02d", $3}'`
    hour=`echo $frmdate | awk -F"-| |:" '{printf "%02d", $4}'`
    minute=`echo $frmdate | awk -F"-| |:" '{printf "%02d", $5}'`
    second=`echo $frmdate | awk -F"-| |:" '{printf "%02d", $6}'`
    frmdate="$year-$month-$day $hour:$minute:$second"

    if [[ "$frmdate" > "$default_date" ]]; then
        $(yad --text "Selected date is in the future, Try again." --button=OK:0 --buttons-layout=center)
        else
        
        if [[ $frmprotocol == 'CAPS' ]];then
           endpoint="caps://127.0.0.1:28003"
        elif [[ $frmprotocol == 'FDSN' ]];then
           endpoint="fdsnws://127.0.0.1:28081"
        elif [[ $frmprotocol == 'SLINK' ]];then
           endpoint="slink://127.0.0.1:28000"
        fi


	# Regions

        if [[ $frmregion == 'Australia' ]]; then
          streams_codes="AU.KDU.*.BHZ,AU.MTN.*.?HZ,AU.DRS.*.SHZ,AU.KNRA.00.BHZ,AU.WRAB.*.?HZ,AU.AS31.*.?HZ,AU.FITZ.*.?HZ,AU.MBWA.*.?HZ,AU.PSA00.*.?HZ,AU.GIRL.*.BHZ,AU.WRKA.*.?HZ,AU.MEEK.*.?HZ,AU.MORW.*.?HZ,AU.BLDU.*.?HZ,AU.MTKN.*.?HZ,AU.CARL.*.?HZ,AU.KLBR.*.?HZ,AU.MUN.*.?HZ,AU.NWAO.*.?HZ,AU.RKGY.*.?HZ,AU.KMBL.*.BHZ,AU.FORT.*.BHZ,AU.COEN.*.?HZ,AU.MTSU.*.SHZ,AU.QIS.*.BHZ,AU.RK1H.*.?HZ,AU.EIDS.*.?HZ,AU.BN2S.*.SHZ,AU.GC2F.*.SHZ,AU.TW1H.*.SHZ,AU.RMQ.*.SHZ,AU.QLP.*.BHZ,AU.ARMA.*.?HZ,AU.MGCD.*.SHZ,AU.RIV.*.BHZ,AU.YNG.*.BHZ,AU.CAN.*.?HZ,AU.CNB.*.?HZ,AU.MILA.*.BHZ,AU.TOO.00.BHZ,AU.MOO.*.?HZ,AU.CORO.*.SHZ,AU.ARPS.*.?HZ,AU.CMSA.*.?HZ,AU.STKA.*.?HZ,AU.NAPP.*.SHZ,AU.PTPS.*.?HZ,AU.GHSS.*.?HZ,AU.BBOO.*.?HZ,AU.LCRK.*.BHZ,AU.INKA.*.BHZ,AU.OOD.*.BHZ"

        elif [[ $frmregion == 'Southwest' ]]; then
          streams_codes="AU.MUN.*.BH?,AU.RKGY.*.SH?,AU.NWAO.*.BH1,AU.NWAO.*.BH2,AU.NWAO.*.BHZ,AU.BLDU.*.BH?,AU.KLBR.*.SH?,AU.MORW.*.BH?,AU.MEEK.*.BH*,AU.KMBL.*.BH?,AU.WRKA.*.BH?,AU.FORT.*.BH?"

        elif [[ $frmregion == 'Northwest' ]]; then
          streams_codes="AU.KNRA.00.BH?,AU.FITZ.*.BH1,AU.FITZ.*.BH2,AU.FITZ.*.BHZ,IU.MBWA.00.BH?,AU.PSA00.00.BH?,AU.GIRL.*.BH?,AU.WRKA.*.BH?,AU.MEEK.*.BH?"
        
        elif [[ $frmregion == 'North' ]]; then
          streams_codes="AU.FITZ.*.BH1,AU.FITZ.*.BH2,AU.FITZ.*.BHZ,AU.KNRA.00.BH?,AU.DRS.*.SH?, AU.MTN.*.BH?,AU.KDU.*.BH?,AU.COEN.*.BH?,AU.MTSU.*.SH?,II.WRAB.10.BH?"
        
	elif [[ $frmregion == 'Central' ]]; then
          streams_codes="AU.WB10.*.BHZ,AU.WC4.*.BHZ,AU.WR10.*.BHZ,AU.QIS.*.BH?,AU.AS31.*.BH?,AU.WRKA.*.BH?,AU.OOD.*.BH?,AU.INKA.*.BH?,AU.QLP.*.BH?"
        
	elif [[ $frmregion == 'South' ]]; then
          streams_codes="AU.OOD.*.BH?,AU.INKA.*.BH?,AU.MULG.*.BHZ,AU.LCRK.*.BH?,AU.FORT.*.BH?,AU.STKA.*.BHZ,AU.BBOO.*.BH?,AU.HTT.*.BH?,AU.TWOA.*.BH?"
	elif [[ $frmregion == 'Northeast' ]]; then
          streams_codes="AU.COEN.*.BH?,AU.MTSU.*.SH?,AU.QIS.*.BH?,IU.CTAO.*H?,AU.EIDS.*.BH?,AU.QLP.*.BH?,AU.RMQ.*.SH?,AU.INKA.*.BH?"
        
	elif [[ $frmregion == 'Southeast' ]]; then
          streams_codes="AU.ARMA.*.BH?,AU.CMSA.*.BH,?,AU.STKA.*.BHE,AU.STKA.*.BHN,AU.STKA.*.BHZ,AU.MGCD.*.SH?,AU.YNG.*.BH?,AU.DLN.*.SH?,AU.ARPS.*.SH?,AU.MILA.*.BH?,AU.TOO.00.BHE,AU.TOO.00.BHN,AU.TOO.00.BHZ,AU.GLAD.*.BH?,AU.MOO.*.BH?"
        
        fi

	# Set buffer size

	if [ $frmlasthour == "TRUE" ]; then
	  frmdate=$current_date
	  bufferSize=3600
        else
	bufferSize="$(($frmbuffer*3600))"
        fi

	# Show phase picks
	if [[ $frmshowpicks == "TRUE" ]]; then
	  showpicks="--showPicks=\"true\""
	elif [[ $frmshowpicks == "FALSE" ]]; then
	  showpicks="--showPicks=\"false\""
	fi

	# Optional pre-filter

	if [[ $frmfilter == "BP 0.7 - 2 Hz 3rd Order" ]]; then
	  filter="--filter \"BW(3,0.7,2)\""
	elif [[ $frmfilter == "BP 0.7 - 2 Hz 5th Order" ]]; then
	  filter="--filter \"BW(3,0.7,2)\""
	elif [[ $frmfilter == "BP 1 - 3 Hz 3rd Order" ]]; then
	  filter="--filter \"BW(3,1,3)\""
	elif [[ $frmfilter == "BP 2 - 4 Hz 3rd Order" ]]; then
	  filter="--filter \"BW(3,2,4)\""
	elif [[ $frmfilter == "BP 3 - 6 Hz 3rd Order" ]]; then
	  filter="--filter \"BW(3,3,6)\""
	elif [[ $frmfilter == "BP 4 - 8 Hz 3rd Order" ]]; then
	  filter="--filter \"BW(3,4,8)\""
	elif [[ $frmfilter == "BP 5 - 10 Hz 3rd Order" ]]; then
	  filter="--filter \"BW(3,5,10)\""
	elif [[ $frmfilter == "HP 5 Hz 3rd Order" ]]; then
	  filter="--filter \"BW_HP(3,5)\""
	elif [[ $frmfilter == "HP 8 Hz 3rd Order" ]]; then
	  filter="--filter \"BW_HP(3,8)\""
        fi

        sc3cmd="scrttv -u $RANDOM -H $remote_host -d $db_strg --maxDelay=0 -I \
                  $endpoint --resortAutomatically=\"true\" --autoApplyFilter=\"true\" \
                  --buffer-size=\"$bufferSize\"  --end-time=\"$frmdate\" --streams.codes=\"$streams_codes\" \
		  --scheme.colors.records.gaps=\"ff7f7f\" $filter $showpicks"
	
	eval $sc3cmd &

    fi
done

