#!/bin/bash

# West Australia
scrttv -u rttvwa --debug -H 127.0.0.1:4805 -d mysql://sysop:sysop@127.0.0.1:3308/seiscomp31 -I caps://127.0.0.1:28003 --resortAutomatically="true" --autoApplyFilter="true" --bufferSize="1800" --streams.codes="AU.*.*.BHZ,AU.*.*.SHZ,IA.*.*.?HZ,GE.*.*.BHZ,ND*.*.*.?HZ,IM.*.*.BHZ,II.*.*.BHZ,G.*.*.BHZ,IU.*.*.BHZ" --streams.region.lonmin="107" --streams.region.lonmax="129" --streams.region.latmin="-45" --streams.region.latmax="-4" -v &

# Central Australia/Northern Territory
scrttv -u rttvcea --debug -H 127.0.0.1:4805 -d mysql://sysop:sysop@127.0.0.1:3308/seiscomp31 -I caps://127.0.0.1:28003 --resortAutomatically="true" --autoApplyFilter="true" --bufferSize="1800" --streams.codes="AU.*.*.BHZ,AU.*.*.SHZ,IA.*.*.?HZ,GE.*.*.BHZ,ND*.*.*.?HZ,IM.*.*.BHZ,II.*.*.BHZ,G.*.*.BHZ,IU.*.*.BHZ" --streams.region.lonmin="129" --streams.region.lonmax="145" --streams.region.latmin="-45" --streams.region.latmax="-4" -v &

# Eastern Australia
scrttv -u scrttvea --debug  -H 127.0.0.1:4805 -d mysql://sysop:sysop@127.0.0.1:3308/seiscomp31 -I caps://127.0.0.1:28003 --resortAutomatically="true" --autoApplyFilter="true" --bufferSize="1800" --streams.codes="AU.*.*.BHZ,AU.*.*.SHZ,IA.*.*.?HZ,GE.*.*.BHZ,ND*.*.*.?HZ,IM.*.*.BHZ,II.*.*.BHZ,G.*.*.BHZ,IU.*.*.BHZ" --streams.region.lonmin="145" --streams.region.lonmax="167" --streams.region.latmin="-45" --streams.region.latmax="-4" -v &

