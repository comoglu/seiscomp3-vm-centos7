#!/bin/bash

region_list=$(echo "AU_NC!AU_NE!AU_NW!AU_SC!AU_SE!AU_SELECT!AU_SW")
buffer_size=$(echo "1!2!3!4!5!6!7!8!9!10!11!12!13!14!15!16!17!18!19!20!21!22!23!24")
proto_list=$(echo "SLINK!FDSN!CAPS")
default_date=$(date "+%Y-%m-%d %H:%M:%S")
remote_host="127.0.0.1:4805"
db_strg="mysql://sysop:sysop@127.0.0.1:3308/seiscomp3"
cond=true

while ($cond);do
    frmdata=$(yad --title "Trace View Selection" \
              --form --date-format="%Y-%m-%d %H:%M:%S"\
              --field="Date":DT \
              --field="Buffer Size (hours)":CB \
              --field="Protocol":CB \
              --field="Region":CB \
              "$default_date" "$buffer_size" "$proto_list" "$region_list" )

    frmdate=$(echo $frmdata | awk 'BEGIN {FS="|" } { print $1 }')
    frmbuffer=$(echo $frmdata | awk 'BEGIN {FS="|" } { print $2 }')
    frmprotocol=$(echo $frmdata | awk 'BEGIN {FS="|" } { print $3 }')
    frmregion=$(echo $frmdata | awk 'BEGIN {FS="|" } { print $4 }')

    if [[ "$frmdate" > "$default_date" ]]; then
        $(yad --text "Selected date is in the future, Try again." --button=OK:0 --buttons-layout=center)
        else
        
        if [[ $frmprotocol == 'CAPS' ]];then
           endpoint="caps://127.0.0.1:28003"
        elif [[ $frmprotocol == 'FDSN' ]];then
           endpoint="fdsnws://127.0.0.1:28081"
        elif [[ $frmprotocol == 'SLINK' ]];then
           endpoint="slink://127.0.0.1:28000"
        fi

        bufferSize=$(($frmbuffer*3600))
        #frmdate=$(date "+%Y-%m-%d %H:%M:%S" --date "$frmdate UTC +$bufferSize seconds")

        if [[ $frmregion == 'AU_NC' ]]; then
          streams_codes="AU.KDU.*.?HZ,AU.MTN.*.?HZ,S.AUKAT.*.?HZ,AU.KNRA.*.?HZ,AU.AS01.*.?HZ,S.AUALC.*.?HZ,AU.QIS.*.?HZ,AU.WB2.*.?HZ,AU.DPH.*.?HZ,AU.DRS.*.?HZ,S.AUDHS.*.?HZ,S.AUNHS"

          sc3cmd="scrttv -u 1-${USER} -H $remote_host -d $db_strg --maxDelay=0 -I \
                  $endpoint --resortAutomatically=\"true\" --autoApplyFilter=\"true\" --showPicks=\"true\" \
                  --buffer-size=\"$bufferSize\"  --end-time=\"$frmdate\" --streams.codes=\"$streams_codes\""

        elif [[ $frmregion == 'AU_NE' ]]; then
          streams_codes="AU.COEN.*.?HZ,S.AUCSH.*.?HZ,AU.MTSU.*.?HZ,AU.CTA.*.?HZ,AU.EIDS.*.?HZ,S.AUMOU.*.?HZ,AU.QIS.*.?HZ,AU.QLP.*.?HZ,AU.RMQ.*.?HZ,AU.AS01.*.?HZ,AU.BN2S.*.?HZ,S.AUBSH.*.?HZ,S.AUWSH.*.?HZ,S.AUTOO.*.?HZ,AU.GC1S.*.?HZ,AU.GC2F.*.?HZ,S.AUMUL.*.?HZ,AU.RK2S.*.?HZ,S.AUNRC.*.?HZ,AU.TW1H.*.?HZ,S.AUAYR"
         
          sc3cmd="scrttv -u 2-${USER} -H $remote_host -d $db_strg --maxDelay=0  -I \
                  $endpoint --resortAutomatically=\"true\" --autoApplyFilter=\"true\" --showPicks=\"true\" \
                  --buffer-size=\"$bufferSize\"  --end-time=\"$frmdate\" --streams.codes=\"$streams_codes\""

        elif [[ $frmregion == 'AU_NW' ]]; then
          streams_codes="AU.AS01.*.?HZ,AU.FITZ.*.?HZ,AU.KDU.*.?HZ,AU.KNRA.*.?HZ,AU.MBWA.*.?HZ,AU.WB2.*.?HZ,S.AUKAR.*.?HZ,AU.GIRL.*.?HZ,S.AUCAR"

          sc3cmd="scrttv -u 3-${USER} -H $remote_host -d $db_strg --maxDelay=0  -I \
                  $endpoint --resortAutomatically=\"true\" --autoApplyFilter=\"true\" --showPicks=\"true\" \
                  --buffer-size=\"$bufferSize\"  --end-time=\"$frmdate\" --streams.codes=\"$streams_codes\""

        elif [[ $frmregion == 'AU_SC' ]]; then
          streams_codes="AU.ARPS.*.?HZ,AU.BBOO.*.BHZ,AU.LCRK.*.?HZ,AU.MULG.*.BHZ,AU.INKA.*.?HZ,AU.OOD.*.BHZ,S.AUROX.*.?HZ,S.AUJCS.*.?HZ,S.AUCAS.*.?HZ,S.AUMAR.*.?HZ,AU.CMSA.*.?HZ,AU.FORT.*.BHZ,AU.STKA.*.?HZ,AU.AS01.*.?HZ,AU.GHSS.*.?HZ,AU.NAPP.*.SHZ,AU.HTT.*.BHZ,AU.WHYH.*.?HZ,AU.PTPS.*.?HZ,AU.TWOA.*.?HZ,AU.KELC.*.BHZ,AU.SDAN.*.BHZ,AU.YAPP.*.BHZ,S.AURMK"

          sc3cmd="scrttv -u 4-${USER} -H $remote_host -d $db_strg --maxDelay=0  -I \
                  $endpoint --resortAutomatically=\"true\" --autoApplyFilter=\"true\" --showPicks=\"true\" \
                  --buffer-size=\"$bufferSize\"  --end-time=\"$frmdate\" --streams.codes=\"$streams_codes\""

        elif [[ $frmregion == 'AU_SE' ]]; then
          streams_codes="AU.ARMA.*.?HZ,S.AULRC.*.HHZ,S.AUPHS.*.?HZ,S.AUDCS.*.?HZ,S.AUKHS.*.?HZ,S.AUTKS.*.HHZ,AU.MGCD.*.?HZ,AU.RIV.*.BHZ,S.AUUHS.*.?HZ,AU.YNG.*.BHZ,AU.DLN.*.SHZ,AU.CAN.*.?HZ,S.AUMTS.*.?HZ,S.AUDAR.*.?HZ,S.AUSIS.*.HHZ,AU.CNB.*.?HZ,S.AUSMG.*.?HZ,AU.MILA.*.BHZ,S.AUMAG.*.?HZ,S.AUHPC.*.?HZ,S.AUMTC.*.?HZ,S.AUSSC.*.HHZ,S.AUKSC.*.?HZ,S.AURSC.*.HHZ,S.AULHS.*.?HZ,S.AUTAR.*.?HZ,AU.TOO.*.?HZ,AU.MOO.*.?HZ,AU.ARPS.*.?HZ,AU.CMSA.*.?HZ,AU.STKA.*.?HZ,AU.BBOO.*.BHZ,AU.YAPP.*.?HZ"

          sc3cmd="scrttv -u 5-${USER} -H $remote_host -d $db_strg --maxDelay=0  -I \
                  $endpoint --resortAutomatically=\"true\" --autoApplyFilter=\"true\" --showPicks=\"true\" \
                  --buffer-size=\"$bufferSize\"  --end-time=\"$frmdate\" --streams.codes=\"$streams_codes\""

        elif [[ $frmregion == 'AU_SELECT' ]]; then
          streams_codes="AU.KDU.*.BHZ,AU.MTN.*.?HZ,AU.DRS.*.SHZ,S.AUDHS.*.?HZ,S.AUNHS.*.?HZ,S.AUKAT.*.?HZ,AU.KNRA.00.BHZ,AU.WRAB.*.?HZ,AU.AS31.*.?HZ,S.AUALC.*.?HZ,AU.FITZ.*.?HZ,AU.MBWA.*.?HZ,AU.PSA00.*.?HZ,AU.GIRL.*.BHZ,AU.WRKA.*.?HZ,AU.MEEK.*.?HZ,AU.MORW.*.?HZ,AU.BLDU.*.?HZ,AU.MTKN.*.?HZ,AU.CARL.*.?HZ,AU.KLBR.*.?HZ,AU.MUN.*.?HZ,AU.NWAO.*.?HZ,AU.RKGY.*.?HZ,AU.KMBL.*.BHZ,AU.FORT.*.BHZ,AU.MCQ.*.?HZ,AU.CASY.*.?HZ,AU.COEN.*.?HZ,AU.MTSU.*.SHZ,AU.QIS.*.BHZ,AU.CTA.*.?HZ,AU.RK2S.*.?HZ,AU.EIDS.*.?HZ,AU.BN2S.*.?HZ,AU.GC2F.*.?HZ,AU.GC1S.*.?HZ,AU.TW1H.*.?HZ,AU.RMQ.*.HHZ,AU.QLP.*.BHZ,AU.ARMA.*.?HZ,S.AULRC.*.HHZ,S.AUPHS.*.?HZ,S.AUDCS.*.?HZ,S.AUKHS.*.?HZ,S.AUTKS.*.HHZ,S.AUUHS.*.HHZ,AU.AUMTS.*.?HZ,S.AUDAR.*.?HZ,S.AUSIS.*.?HZ,AU.MGCD.*.?HZ,AU.RIV.*.BHZ,AU.YNG.*.BHZ,AU.CAN.*.?HZ,AU.CNB.*.?HZ,AU.MILA.*.BHZ,S.AUSMG.*.HHZ,S.AUHPC.*.HHZ,S.AUMTC.*.HHZ,S.AUSSC.*.?HZ,S.AUKSC*.?HZ,S.AURSC.*.?HZ,S.AULHS.*.?HZ,S.AUTAR.*.HHZ,AU.TOO.*.?HZ,AU.MOO.*.?HZ,AU.CORO.*.SHZ,AU.GLAD.*.SHZ,AU.ARPS.*.?HZ,AU.CMSA.*.?HZ,AU.STKA.*.?HZ,AU.NAPP.*.SHZ,AU.PTPS.*.?HZ,AU.GHSS.*.?HZ,AU.TWOA.*.?HZ,AU.BBOO.*.?HZ,AU.LCRK.*.BHZ,AU.MULG.*.BHZ,AU.INKA.*.BHZ,AU.OOD.*.BHZ,S.AUCAS.*.HHZ,S.AUMAR.*.HHZ,S.AURMK.*.?HZ,S.AUROX"

          sc3cmd="scrttv -u 6-${USER} -H $remote_host -d $db_strg --maxDelay=0  -I \
                  $endpoint --resortAutomatically=\"true\" --autoApplyFilter=\"true\" --showPicks=\"true\" \
                  --buffer-size=\"$bufferSize\"  --end-time=\"$frmdate\" --streams.codes=\"$streams_codes\""

        elif [[ $frmregion == 'AU_SW' ]]; then
          streams_codes="AU.MEEK.*.?HZ,AU.MORW.*.?HZ,AU.BLDU.*.?HZ,AU.KLBR.*.?HZ,AU.MUN.*.?HZ,AU.NWAO.*.?HZ,AU.RKGY.*.?HZ,AU.KMBL.*.?HZ,AU.FORT.*.?HZ,AU.CARL.*.?HZ,AU.MTKN.*.?HZ,S.AUKAL.*.?HZ,S.AUKUL.*.?HZ,S.AUALB.*.?HZ,S.AUBUS.*.?HZ,S.AUHAR.*.?HZ,S.AUMA"

          sc3cmd="scrttv -u 7-${USER} -H $remote_host -d $db_strg --maxDelay=0  -I \
                  $endpoint --resortAutomatically=\"true\" --autoApplyFilter=\"true\" --showPicks=\"true\" \
                  --buffer-size=\"$bufferSize\"  --end-time=\"$frmdate\" --streams.codes=\"$streams_codes\""

        fi

        cond=false
        eval $sc3cmd &

    fi
done
