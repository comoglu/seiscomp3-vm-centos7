#!/bin/sh
if [ "$2" = "1" ]; then
echo " $1" | sed 's/,/, ,/g'   | festival --tts;
else
echo "Event updated, $1" | sed 's/,/, ,/g'   | festival --tts;
fi
