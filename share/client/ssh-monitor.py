#!/bin/env python

import os
import sys
import collections
import signal
import random
import logging
import logging.handlers
from uuid import uuid1
from contextlib import contextmanager
from datetime import datetime
from threading import Thread
from time import sleep
from playsound import playsound
from Tkinter import Tk, Label, W, E
from tkMessageBox import showwarning
import boto3 as boto
from sqlalchemy import (
    Column,
    Boolean,
    DateTime,
    Integer,
    String,
    create_engine)
from sqlalchemy.sql.expression import and_, or_, not_
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import StaticPool
from sqlalchemy.ext.declarative import declarative_base



if __name__ != "__main__":
    raise Exception('This is a program not a module')



RUNNING_CHECKS = len(sys.argv) < 2 or sys.argv[1] == 'server'
RUNNING_GUI =    len(sys.argv) < 2 or sys.argv[1] == 'client'

if RUNNING_CHECKS and RUNNING_GUI:
    RUNNING_MODE = 'application'
elif RUNNING_GUI:
    RUNNING_MODE = 'client'
elif RUNNING_CHECKS:
    RUNNING_MODE = 'server'

WARNING_SOUND_FILE = os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        'connect_alarm.wav')
SQLITE_DB_FILE = '/var/tmp/connection-monitor.db'
DB_CONNECTION_STRING = 'sqlite:///{}'.format(SQLITE_DB_FILE)
NO_PING_ATTEMPTS = 1
PING_TIMEOUT = 3
TIME_TO_SLEEP_BETWEEN_CHECKS = 10
HOSTS = [
    'proc1c',
    'proc2c']
THINGYS = [
    'AWS',
    'internet']
AWS_DNS_LIST = [
    'ns-1297.awsdns-34.org',
    'ns-544.awsdns-04.net',
    'ns-1625.awsdns-11.co.uk',
    'ns-371.awsdns-46.com'
    ]
RELIABLE_DNS_SERVERS = [
    '4.2.2.2', # ???
    '8.8.8.8', # google DNS
    '8.8.4.4', # google DNS
    '124.47.130.30' # mactel DNS
    ]



# found at https://stackoverflow.com/questions/3968669/how-to-configure-logging-to-syslog-in-python
logger = logging.getLogger('connection-monitor-{}'.format(RUNNING_MODE))
logger.setLevel(logging.INFO)
sys_log_handler = logging.handlers.SysLogHandler('/dev/log')
formatter = logging.Formatter('%(name)s: %(message)s')
sys_log_handler.setFormatter(formatter)
logger.addHandler(sys_log_handler)
logger.info('starting connection monitor {}'.format(RUNNING_MODE))

_Base = declarative_base()



class DownTime(_Base):
    __tablename__ = "downtime"
    id = Column(Integer, primary_key=True)

    #: The name of the 'host' (could also be 'the internet' or
    #: AWS or some other 'thingy').
    host_name = Column(String())

    #: The time the 'host' first failed.
    start_time = Column(DateTime())

    #: The time the 'host' was again accessible.
    end_time = Column(DateTime())

    #: The time the 'host' was last checked.'
    last_checked = Column(DateTime())

    #: Whether this *DownTime* is still being monitored. This
    #: will be set to false when the monitoring service is started.
    #: It's primary use to to identify that the 'host' was down when
    #: the monitoring service failed.
    is_tracked = Column(Boolean(), default=True)

    #: Flag to track whether this downtime has been sent to the
    #: DynamoDB table.
    sent_to_dynamo = Column(Boolean(), default=False)

    #: Flag to track if a notification is required.
    needs_notification = Column(Boolean(), default=True)

    def __nonzero__(self):
        return False

    @property
    def down_time(self):
        """
        How long has the 'host' been down.
        """

        return self.time_delta.seconds

    @property
    def time_delta(self):
        end = self.end_time or self.last_checked
        return end - self.start_time

    def as_dict(self):
        """
        Convert this *DownTime* to a dictionary which is suitable for
        storage in the DynamoDB table.
        """

        res = {name: str(getattr(self, name)) for name in [
            'start_time',
            'end_time',
            'last_checked',
            'host_name',
            'down_time']}
        res['id'] = '{}-{}'.format(self.host_name, self.start_time)
        return res



@contextmanager
def scoped_session():
    """
    Provide a transactional scope around a series of DB operations.
    """

    session = _Session()
    try:
        yield session
        session.commit()
    except Exception as e:
        session.rollback()
        raise e
    finally:
        session.close()



def sys_call(call):
    """
    A wrapper around :py:func:`subprocess.check_call`.
    """

    call = '{}>/dev/null 2>/dev/null'.format(' '.join(call))

    try:
        status = os.system(call)
        return False if status else True
    except Exception as e:
        logger.error('exception in {}: {}'.format(call, str(e)))
        return False



def ssh_check(host_name):
    """
    Try and establish an SSH connection to *host_name*.
    """

    return sys_call(['ssh', host_name, ':'])



def ping_check(host_name):
    """
    Try to ping *host_name*.
    """

    return sys_call(['ping',
        '-c', str(NO_PING_ATTEMPTS),
        '-w', str(PING_TIMEOUT),
        host_name])



def thingy_check(thingy):
    """
    Check whether *thingy* (e.g. 'the internet' or AWS) is accessible
    by pinging a series of servers. If any of them are accessible,
    then *thingy* is deemed to be accessible.
    """

    hosts = (AWS_DNS_LIST if thingy.upper() == 'AWS' else  RELIABLE_DNS_SERVERS)[:]
    random.shuffle(hosts)
    return any(ping_check(host) for host in hosts)



def db_check(host_name):
    """
    Check with *host_name* is accessible by checking the local database.

    This is used by clients to look at the results returned by the server,
    which saves a list of 'thingys' (hosts, 'the internet', AWS) which
    are down in the database.
    """

    with scoped_session() as session:
        # check that the monitoring service is running
        server_up = RUNNING_CHECKS or \
            sys_call(['systemctl', 'is-active', '--quiet', 'sshmonitorserver'])
        if not server_up: return None

        # check if there is an active downtime
        res = session.query(DownTime).filter(and_(
            DownTime.host_name==host_name,
            DownTime.end_time==None,
            DownTime.is_tracked)).one_or_none()
        if res is None: return True

        # check that the connection has been checked recently
        curr_time = datetime.now()
        server_up = (curr_time - res.last_checked).seconds < 2 * TIME_TO_SLEEP_BETWEEN_CHECKS
        if not server_up: return None
        return res




def sound_alarm(host_name):
    class FilePlayer(object):
        def __init__(self, sound_file, host_name):
            self.sound_file = sound_file
            self.alarm_acknowleged = False
            alarmthread = Thread(target=self)
            alarmthread.daemon = True
            alarmthread.start()
            showwarning(
                    'Host Down',
                    'host name: {}'.format(host_name))
            self.alarm_acknowleged = True

        def __call__(self):
            while not self.alarm_acknowleged:
                playsound(self.sound_file)

    FilePlayer(WARNING_SOUND_FILE, host_name)




class HostStatusTracker(object):
    """
    Manage the state of a :py:class:`Tkinter.Label`.
    """

    def __init__(self, status_label, down_time_label):
        self.status_label = status_label
        self.down_time_label = down_time_label

    def is_up(self, status):
        """
        Set the text and background color of *self.status_label* based on *status*.
        """

        if status is None:
            self.status_label.config(text='      UNKNOWN     ', bg='orange')
        elif status:
            self.status_label.config(text='        UP        ', bg='green')
        else:
            self.status_label.config(text='       DOWN       ', bg='red')
            if isinstance(status, DownTime):
                down_time = status.time_delta
                days = down_time.days
                hours = int(down_time.seconds / 3600)
                minutes = int((down_time.seconds - hours * 3600) / 60)
                seconds = int(down_time.seconds - hours * 3600 - minutes * 60)
                if days > 0:
                    content = '{}d, {}h, {}m, {}s'.format(
                        days, hours, minutes, seconds)
                elif hours > 0:
                    content = '{}h, {}m, {}s'.format(
                        hours, minutes, seconds)
                elif minutes > 0:
                    content = '{}m, {}s'.format(
                        minutes, seconds)
                else:
                    content = '{}s'.format(
                            seconds)
                self.down_time_label.config(text=content)




class BaseDownTimeTracker(object):
    def __init__(self, host_name, status_checker, *args, **kwargs):
        super(BaseDownTimeTracker, self).__init__(*args, **kwargs)
        self.host_name = host_name
        self.status_checker = status_checker
        self.down_time = None

    def check(self):
        down_time = None
        status = self.status_checker(self.host_name)
        if hasattr(self, '_make_update_downtime'):
            down_time = self._make_update_downtime(status)
        if hasattr(self, '_update_trackers'):
            # Hack to pass the down time to the trackers when we are running
            # as standalone (i.e. when we are not running as client/server,
            # which will generally when developing/debugging).
            if down_time is not None and isinstance(status, bool):
                status = down_time
            self._update_trackers(status)



class _DownTimeManagerMixin(object):
    def __init__(self, *args, **kwargs):
        super(_DownTimeManagerMixin, self).__init__ (*args, **kwargs)

    def _make_update_downtime(self, status):
        curr_time = datetime.now()
        if not status:
            with scoped_session() as session:
                if self.down_time is None:
                    self.down_time = DownTime(
                        host_name=self.host_name,
                        start_time=curr_time,
                        last_checked=curr_time)
                    session.add(self.down_time)
                else:
                    session.add(self.down_time)
                    self.down_time.last_checked = curr_time

        elif self.down_time is not None:
            with scoped_session() as session:
                session.add(self.down_time)
                self.down_time.end_time = curr_time
                self.down_time.is_tracked = False
            self.down_time = None

        return self.down_time



class _TrackerManagerMixin(object):
    def __init__(self, status_trackers=None, *args, **kwargs):
        super(_TrackerManagerMixin, self).__init__(*args, **kwargs)

        if isinstance(status_trackers, collections.Iterable):
            self.trackers = status_trackers
        elif status_trackers is None:
            self.trackers = []
        else:
            self.trackers = [status_trackers]

    def _update_trackers(self, status):
        if isinstance(status, DownTime):
            with scoped_session() as session:
                session.add(status)

                for tracker in self.trackers:
                    tracker.is_up(status)

                    if status.needs_notification \
                            and status.down_time > 1.5*TIME_TO_SLEEP_BETWEEN_CHECKS:
                        status.needs_notification = False
                        sound_alarm(status.host_name)

        else:
            for tracker in self.trackers:
                tracker.is_up(status)



def sync_with_dynamo():
    dynamo_down_times = boto.resource('dynamodb').Table('downtimes')
    with scoped_session() as session:
        items = session.query(DownTime).filter(and_(
            not_(DownTime.sent_to_dynamo),
            or_(
                DownTime.end_time!=None,
                not_(DownTime.is_tracked)))).all()

        for item in items:
            try:
                dynamo_down_times.put_item(Item=item.as_dict())
            except Exception as e:
                item.sent_to_dynamo = False
            else:
                item.sent_to_dynamo = True



if len(sys.argv) < 2:
    class DownTimeTracker(BaseDownTimeTracker, _DownTimeManagerMixin, _TrackerManagerMixin):
        def __init__(self, *args, **kwargs):
            super(DownTimeTracker, self).__init__(*args, **kwargs)

elif sys.argv[1] == 'server':
    class DownTimeTracker(BaseDownTimeTracker, _DownTimeManagerMixin):
        def __init__(self, *args, **kwargs):
            super(DownTimeTracker, self).__init__(*args, **kwargs)

elif sys.argv[1] == 'client':
    ssh_check = ping_check = thingy_check = db_check

    class DownTimeTracker(BaseDownTimeTracker, _TrackerManagerMixin):
        def __init__(self, *args, **kwargs):
            super(DownTimeTracker, self).__init__(*args, **kwargs)

else:
    raise Exception('Invalid second argument: {}'.format(sys.argv[1]))



class ServerCheckerManager(object):
    def __init__(self):
        self.hosts = []

        for host in HOSTS:
            self._add_host(host, ssh_check)

        for host in ['AWS', 'INTERNET']:
            self._add_host(host, thingy_check)

    def _add_host(self, host_name, status_checker):
        self.hosts.append(DownTimeTracker(host_name, status_checker))



class GUI(object):
    def __init__(self, master):
        self.hosts = []
        self.row = 0
        master.title("EATWS Host Statuses")

        for host in HOSTS:
            self._add_host(host, ssh_check, master)

        for host in ['AWS', 'INTERNET']:
            self._add_host(host, thingy_check, master)


    def _add_host(self, host_name, status_checker, view):
        host_name_label = Label(
            view,
            text=host_name)

        host_status_label = Label(
            view,
            text='       DOWN       ',
            fg='white',
            bg='red')

        down_time_label = Label(
            view,
            text='                  ')

        host_name_label.grid(row=self.row, column=0, sticky=W)
        host_status_label.grid(row=self.row, column=1, sticky=W+E)
        down_time_label.grid(row=self.row, column=2, sticky=E)

        self.row += 1
        self.hosts.append(DownTimeTracker(
            host_name,
            status_checker,
            HostStatusTracker(host_status_label, down_time_label)))



def _signaled_terminate(signum, frame):
    """
    Handle termination of the service.

    .. remark:: Probably not required. This is asking a daemon thread to
        exit, but the program exists when only daemon threads remain anyway.
    """

    if RUNNING_GUI:
        _root.destroy()

    logger.info('exiting from signal {}'.format(signum))
    sys.exit(0)



def _check_hosts():
    """
    Check the status of all 'hosts'
    """
    for host in _checker_manager.hosts:
        host.check()



def _check_loop():
    """
    An infinite look which continually checks all hosts and
    syncs results to the DynamoDB table.
    """

    while True:
        start_time = datetime.now()
        try:
            _check_hosts()
            if RUNNING_CHECKS:
                sync_with_dynamo()
        except KeyboardInterrupt:
            break

        time_to_wait = TIME_TO_SLEEP_BETWEEN_CHECKS - (datetime.now() - start_time).seconds
        sleep(max(time_to_wait, 0))



_engine = create_engine(
    DB_CONNECTION_STRING,
    echo=False,
    connect_args={'check_same_thread': False},
    poolclass=StaticPool)
_Session = sessionmaker(bind=_engine)

if not os.path.exists(SQLITE_DB_FILE):
    logger.info('about to create database: {}'.format(SQLITE_DB_FILE))
    _Base.metadata.create_all(_engine)



if RUNNING_CHECKS:
    with scoped_session() as session:
        # Mark all existing recurds as untracked. Note that this may be a
        # good time to remove all records which have been synced with
        # DynamoDB.
        dts = session.query(DownTime).filter(DownTime.is_tracked).all()
        for dt in dts:
            dt.is_tracked = False

    signal.signal(signal.SIGINT,  _signaled_terminate)
    signal.signal(signal.SIGTERM, _signaled_terminate)



if RUNNING_GUI:
    if not RUNNING_CHECKS:
        # then we need to sound the alarm for any current downtimes. In the case
        # that we are running checks, then the alarms will be sounded via other
        # means.

        with scoped_session() as session:
            # Mark all existing recurds as untracked. Note that this may be a
            # good time to remove all records which have been synced with
            # DynamoDB.
            dts = session.query(DownTime).filter(DownTime.is_tracked).all()
            for dt in dts:
                dt.needs_notification = True

    _root = Tk()
    _checker_manager = GUI(_root)
    _thread = Thread(target=_check_loop)
    _thread.daemon = True
    _thread.start()
    _root.mainloop()

elif RUNNING_CHECKS:
    with scoped_session() as session:
        # Mark all existing recurds as untracked. Note that this may be a
        # good time to remove all records which have been synced with
        # DynamoDB.
        dts = session.query(DownTime).filter(DownTime.is_tracked).all()
        for dt in dts:
            dt.is_tracked = False

    _checker_manager = ServerCheckerManager()
    _thread = None
    _check_loop()
