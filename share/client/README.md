This repository contains the scripts required to create SeisComP3 users on local instances of 
CentOS installed on VirtualBox.
Each instance will have the same set of public/private keys (uniquie) for every
user irrespective of the VirtualBox instance. This reduces the need to generate 
public/pirvate keys for every new instance.
