#!/bin/bash

region_list=$(echo "AU_NC!AU_NE!AU_NW!AU_SC!AU_SE!AU_SELECT!AU_SW")
buffer_size=$(echo "1!2!3!4!5!6!7!8!9!10!11!12!13!14!15!16!17!18!19!20!21!22!23!24")
proto_list=$(echo "CAPS!FDSN")
default_date=$(date "+%Y-%m-%d %H:%M:%S")
remote_host="127.0.0.1:4805"
db_strg="mysql://sysop:sysop@127.0.0.1:3308/seiscomp31"
cond=true

while ($cond);do
    frmdata=$(yad --title "Trace View Selection" \
              --form --date-format="%Y-%m-%d %H:%M:%S"\
              --field="Date":DT \
              --field="Buffer Size (hours)":CB \
              --field="Protocol":CB \
              --field="Region":CB \
              "$default_date" "$buffer_size" "$proto_list" "$region_list" )

    frmdate=$(echo $frmdata | awk 'BEGIN {FS="|" } { print $1 }')
    frmbuffer=$(echo $frmdata | awk 'BEGIN {FS="|" } { print $2 }')
    frmprotocol=$(echo $frmdata | awk 'BEGIN {FS="|" } { print $3 }')
    frmregion=$(echo $frmdata | awk 'BEGIN {FS="|" } { print $4 }')

    if [[ "$frmdate" > "$default_date" ]]; then
        $(yad --text "Selected date is in the future, Try again." --button=OK:0 --buttons-layout=center)
        else

        if [[ $frmprotocol == 'CAPS' ]];then
           endpoint="caps://127.0.0.1:28003"
        fi

        bufferSize=$(($frmbuffer*3600))

        if [[ $frmregion == 'AU_NC' ]]; then
          streams_codes="AU.KDU.*.?HZ,AU.MTN.*.?HZ,S.AUKAT.*.?HZ,AU.KNRA.*.?HZ,AU.AS01.*.?HZ,S.AUALC.*.?HZ,AU.QIS.*.?HZ,AU.WB2.*.?HZ,AU.DPH.*.?HZ,AU.DRS.*.?HZ,S.AUDHS.*.?HZ,S.AUNHS"

          sc3cmd="scrttv -u d_rttvnc -H $remote_host -d $db_strg -I \
                  $endpoint --resortAutomatically=\"true\" --autoApplyFilter=\"true\" --showPicks=\"false\" \
                  --buffer-size=\"$bufferSize\"  --end-time=\"$frmdate\" --streams.codes=\"$streams_codes\""

        elif [[ $frmregion == 'AU_NE' ]]; then
          streams_codes="AU.COEN.*.?HZ,S.AUCSH.*.?HZ,AU.MTSU.*.?HZ,AU.CTA.*.?HZ,AU.EIDS.*.?HZ,S.AUMOU.*.?HZ,AU.QIS.*.?HZ,AU.QLP.*.?HZ,AU.RMQ.*.?HZ,AU.AS01.*.?HZ,AU.BN2S.*.?HZ,S.AUBSH.*.?HZ,S.AUWSH.*.?HZ,S.AUTOO.*.?HZ,AU.GC1S.*.?HZ,AU.GC2F.*.?HZ,S.AUMUL.*.?HZ,AU.RK2S.*.?HZ,S.AUNRC.*.?HZ,AU.TW1H.*.?HZ,S.AUAYR"
         
          sc3cmd="scrttv -u d_rttvne -H $remote_host -d $db_strg -I \
                  $endpoint --resortAutomatically=\"true\" --autoApplyFilter=\"true\" --showPicks=\"false\" \
                  --buffer-size=\"$bufferSize\"  --end-time=\"$frmdate\" --streams.codes=\"$streams_codes\""

        elif [[ $frmregion == 'AU_NW' ]]; then
          streams_codes="AU.AS01.*.?HZ,AU.FITZ.*.?HZ,AU.KDU.*.?HZ,AU.KNRA.*.?HZ,AU.MBWA.*.?HZ,AU.WB2.*.?HZ,S.AUKAR.*.?HZ,AU.GIRL.*.?HZ,S.AUCAR"

          sc3cmd="scrttv -u d_rttvnw -H $remote_host -d $db_strg -I \
                  $endpoint --resortAutomatically=\"true\" --autoApplyFilter=\"true\" --showPicks=\"false\" \
                  --buffer-size=\"$bufferSize\"  --end-time=\"$frmdate\" --streams.codes=\"$streams_codes\""

        elif [[ $frmregion == 'AU_SC' ]]; then
          streams_codes="AU.ARPS.*.?HZ,AU.BBOO.*.?HZ,AU.LCRK.*.?HZ,AU.MULG.*.?HZ,AU.INKA.*.?HZ,AU.OOD.*.?HZ,S.AUROX.*.?HZ,S.AUJCS.*.?HZ,S.AUCAS.*.?HZ,S.AUMAR.*.?HZ,AU.CMSA.*.?HZ,AU.FORT.*.?HZ,AU.STKA.*.?HZ,AU.AS01.*.?HZ,AU.GHSS.*.?HZ,AU.NAPP.*.?HZ,AU.HTT.*.?HZ,AU.WHYH.*.?HZ,AU.PTPS.*.?HZ,AU.TWOA.*.?HZ,S.AURMK"

          sc3cmd="scrttv -u d_rttvsc -H $remote_host -d $db_strg -I \
                  $endpoint --resortAutomatically=\"true\" --autoApplyFilter=\"true\" --showPicks=\"false\" \
                  --buffer-size=\"$bufferSize\"  --end-time=\"$frmdate\" --streams.codes=\"$streams_codes\""

        elif [[ $frmregion == 'AU_SE' ]]; then
          streams_codes="AU.ARMA.*.?HZ,S.AULRC.*.?HZ,S.AUPHS.*.?HZ,S.AUDCS.*.?HZ,S.AUKHS.*.?HZ,S.AUTKS.*.?HZ,AU.MGCD.*.?HZ,AU.RIV.*.?HZ,S.AUUHS.*.?HZ,AU.YNG.*.?HZ,AU.DLN.*.?HZ,AU.CAN.*.?HZ,S.AUMTS.*.?HZ,S.AUDAR.*.?HZ,S.AUSIS.*.?HZ,AU.CNB.*.?HZ,S.AUSMG.*.?HZ,AU.MILA.*.?HZ,S.AUMAG.*.?HZ,S.AUHPC.*.?HZ,S.AUMTC.*.?HZ,S.AUSSC.*.?HZ,S.AUKSC.*.?HZ,S.AURSC.*.?HZ,S.AULHS.*.?HZ,S.AUTAR.*.?HZ,AU.TOO.*.?HZ,AU.MOO.*.?HZ,AU.ARPS.*.?HZ,AU.CMSA.*.?HZ,AU.STKA.*.?HZ,AU.BBOO"

          sc3cmd="scrttv -u d_rttvse -H $remote_host -d $db_strg -I \
                  $endpoint --resortAutomatically=\"true\" --autoApplyFilter=\"true\" --showPicks=\"false\" \
                  --buffer-size=\"$bufferSize\"  --end-time=\"$frmdate\" --streams.codes=\"$streams_codes\""

        elif [[ $frmregion == 'AU_SELECT' ]]; then
          streams_codes="AU.KDU.*.?HZ,AU.MTN.*.?HZ,AU.DPH.*.?HZ,AU.DRS.*.?HZ,S.AUDHS.*.?HZ,S.AUNHS.*.?HZ,S.AUKAT.*.?HZ,AU.KNRA.*.?HZ,AU.WRAB.*.?HZ,AU.AS01.*.?HZ,S.AUALC.*.?HZ,AU.FITZ.*.?HZ,AU.MBWA.*.?HZ,S.AUCAR.*.?HZ,AU.PSA00.*.?HZ,AU.GIRL.*.?HZ,S.AUKAR.*.?HZ,AU.WRKA.*.?HZ,AU.MEEK.*.?HZ,AU.MORW.*.?HZ,AU.BLDU.*.?HZ,AU.MTKN.*.?HZ,AU.CARL.*.?HZ,AU.KLBR.*.?HZ,AU.MUN.*.?HZ,AU.NWAO.*.?HZ,AU.RKGY.*.?HZ,AU.KMBL.*.?HZ,AU.FORT.*.?HZ,AU.MCQ.*.?HZ,AU.CASY.*.?HZ,AU.COEN.*.?HZ,AU.MTSU.*.?HZ,AU.QIS.*.?HZ,AU.CTA.*.?HZ,AU.RK2S.*.?HZ,AU.EIDS.*.?HZ,AU.BN2S.*.?HZ,AU.GC2F.*.?HZ,AU.GC1S.*.?HZ,AU.TW1H.*.?HZ,AU.RMQ.*.?HZ,AU.QLP.*.?HZ,AU.ARMA.*.?HZ,S.AULRC.*.?HZ,S.AUPHS.*.?HZ,S.AUDCS.*.?HZ,S.AUKHS.*.?HZ,S.AUTKS.*.?HZ,S.AUUHS.*.?HZ,AU.AUMTS.*.?HZ,S.AUDAR.*.?HZ,S.AUSIS.*.?HZ,AU.MGCD.*.?HZ,AU.RIV.*.?HZ,AU.YNG.*.?HZ,AU.CAN.*.?HZ,AU.CNB.*.?HZ,AU.MILA.*.?HZ,S.AUSMG.*.?HZ,S.AUMAG.*.?HZ,S.AUHPC.*.?HZ,S.AUMTC.*.?HZ,S.AUSSC.*.?HZ,S.AUKSC.*.?HZ,S.AURSC.*.?HZ,S.AULHS.*.?HZ,S.AUTAR.*.?HZ,AU.TOO.*.?HZ,AU.MOO.*.?HZ,AU.CORO.*.?HZ,AU.GLAD.*.?HZ,AU.ARPS.*.?HZ,AU.CMSA.*.?HZ,AU.STKA.*.?HZ,AU.NAPP.*.?HZ,AU.WHYH.*.?HZ,AU.PTPS.*.?HZ,AU.GHSS.*.?HZ,AU.TWOA.*.?HZ,AU.BBOO.*.?HZ,AU.LCRK.*.?HZ,AU.MULG.*.?HZ,AU.INKA.*.?HZ,AU.OOD.*.?HZ,S.AUJCS.*.?HZ,S.AUCAS.*.?HZ,S.AUMAR.*.?HZ,S.AURMK.*.?HZ,S.AUROX"

          sc3cmd="scrttv -u d_rttvsel -H $remote_host -d $db_strg -I \
                  $endpoint --resortAutomatically=\"true\" --autoApplyFilter=\"true\" --showPicks=\"false\" \
                  --buffer-size=\"$bufferSize\"  --end-time=\"$frmdate\" --streams.codes=\"$streams_codes\""

        elif [[ $frmregion == 'AU_SW' ]]; then
          streams_codes="AU.MEEK.*.?HZ,AU.MORW.*.?HZ,AU.BLDU.*.?HZ,AU.KLBR.*.?HZ,AU.MUN.*.?HZ,AU.NWAO.*.?HZ,AU.RKGY.*.?HZ,AU.KMBL.*.?HZ,AU.FORT.*.?HZ,AU.CARL.*.?HZ,AU.MTKN.*.?HZ,S.AUKAL.*.?HZ,S.AUKUL.*.?HZ,S.AUALB.*.?HZ,S.AUBUS.*.?HZ,S.AUHAR.*.?HZ,S.AUMA"

          sc3cmd="scrttv -u d_rttvsw -H $remote_host -d $db_strg -I \
                  $endpoint --resortAutomatically=\"true\" --autoApplyFilter=\"true\" --showPicks=\"false\" \
                  --buffer-size=\"$bufferSize\"  --end-time=\"$frmdate\" --streams.codes=\"$streams_codes\""

        fi

        cond=false
        eval $sc3cmd &

    fi
done
