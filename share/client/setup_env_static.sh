# the name of the tar zip that contains the seiscomp distribution
# (in the s3://sc3-install)
export SC3_VERSION=seiscomp3-jakarta-2017.334-centos-7-x86_64

# the version of Centos we are using
export CENTOS_VERSION=7

# the name of the S3 buckets we store installation files in (e.g. seiscomp
# source map tiles, greens functions)
export S3_CONFIG_BUCKET_NAME=sc3-install
export S3_LARGE_FILES_CONFIG_BUCKET_NAME=sc3-install-large-files

# this must be kept in sync with the ../build.json.
export EC2_USER_NAME=centos
export EC2_USER_HOME_FOLDER=/home/$EC2_USER_NAME

# where files get copied to the host by packer. This must also be kept in sync
# with ../build.json
export INSTALL_TMP_DIR=/tmp/sc3_install

# where we move the files copied by packer (we don't get packer to put them here
# directly because of permissions issues)
export SETUP_SCRIPT_DIR=/opt/sc3_install

# where will we put the seiscomp3 installation
export SEISCOMP_PARENT_FOLDER=/opt
export SEISCOMP_HOME_FOLDER=$SEISCOMP_PARENT_FOLDER/seiscomp3

# where we keep our setup scripts and alike
export EATWS_SHARE_FOLDER=$SEISCOMP_HOME_FOLDER/share/eatws

# the folder that contains the installation scripts distributed
# with seiscomp
export INSTALL_SCRIPTS_FOLDER=$SEISCOMP_HOME_FOLDER/share/deps/centos/$CENTOS_VERSION
