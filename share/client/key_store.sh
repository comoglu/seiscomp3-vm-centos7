#!/bin/bash

################################################################################
# Use this utility to store ssh keys in the parameter store on AWS
#   "Usage : ./key_store.sh get <parameter_name>" 
#   "      : ./key_store.sh put <parameter_name> <path to ssh key>"
#   e.g.   ./key_store.sh put /PubKey/user1 ~/.ssh/id_rsa.pub
#          ./key_store.sh put /PrivKey/user1 ~/.ssh/id_rsa
#
#          ./key_store.sh get /PubKey/user1 
#          ./key_store.sh getall /PrivKey
#          ./key_store.sh listusers /PrivKey
# Output of the get argument will be saved to a file ssh_key.txt
#
#The public/private keys are converted to a base64 encryption and then
#saved to the SSM on AWS
################################################################################

######################fucntion block begin################################
function put {
    aws ssm put-parameter --name "$1" --type StringList --value "`base64 $2`"
}

function get {
    aws ssm get-parameter --name "$1" > key_store_out.txt
}

function getall {
    aws ssm describe-parameters --filters "Key=Name,Values=$1" |grep $1
}

function readJson {  
    #script derived from http://dailyraisin.com/read-json-value-in-bash/
     UNAMESTR=`uname`
     if [[ "$UNAMESTR" == 'Linux' ]]; then
       SED_EXTENDED='-r'
     elif [[ "$UNAMESTR" == 'Darwin' ]]; then
       SED_EXTENDED='-E'
     fi; 
     VALUE=`grep -m 1 "\"${2}\"" ${1} | sed ${SED_EXTENDED} 's/^ *//;s/.*: *"//;s/",?//'`
   
     if [ ! "$VALUE" ]; then
       echo "Error: Cannot find \"${2}\" in ${1}" >&2;
       exit 1;
     else
       echo $VALUE ;
     fi; 
}

######################fucntion block bend##################################

OPER=$1
PARAM_NAME=$2

if [ "$OPER" == "put" ]; then
   if [[ $# != 3 ]]; then
      echo "require 2 arguments"
   else
      KEY_VAL=$3
      put "$PARAM_NAME" "$KEY_VAL"
   fi
elif [ "$OPER" == "get" ]; then
   if [[ $# != 2 ]]; then
      echo "require 1 arguments"
   else
      get $PARAM_NAME
      mkdir -p $PWD/$PARAM_NAME
      u_id=$(echo $PARAM_NAME|sed -n -e 's/.*\///p'|sed "s/\"//g"|sed "s/\,//g")

      VAL=`readJson key_store_out.txt Value`
      echo $VAL | sed 's/\\n/\n/g' > temp.txt
      echo "writing file to $PWD/$PARAM_NAME/$u_id.txt"
      base64 -d temp.txt > $PWD/$PARAM_NAME/$u_id.txt
   fi
elif [ "$OPER" == "getall" ];then
   if [[ $# != 2 ]]; then
      echo "require 1 argument"
   else
      mkdir -p $PWD/$PARAM_NAME
      UserList=$(getall $PARAM_NAME)
      for each_item in $UserList; do
            u_id=$(echo $each_item|sed -n -e 's/.*\///p'|sed "s/\"//g"|sed "s/\,//g")
            if [[ $u_id != "" ]]; then
                get "$PARAM_NAME/$u_id"
    
                VAL=`readJson key_store_out.txt Value`
                echo $VAL | sed 's/\\n/\n/g' > temp.txt
                echo "writing file to $PWD/$PARAM_NAME/$u_id.txt"
                base64 -d temp.txt > $PWD/$PARAM_NAME/$u_id.txt
            fi
      done
   fi
elif [ "$OPER" == "listusers" ];then
   if [[ $# != 2 ]]; then
      echo "require 1 argument: /PubKey or /PrivKey"
   else
      echo "" > users.txt
      User_List=$(aws  ssm describe-parameters --filters "Key=Name,Values=$PARAM_NAME" |grep $PARAM_NAME)
      for each_item in $User_List; do
            u_id=$(echo $each_item|sed -n -e 's/.*\///p'|sed "s/\"//g"|sed "s/\,//g")
            if [[ "$u_id" != "" ]]; then
               echo $u_id >> users.txt
            fi
      done
      
      echo "List of current users exported to file users.txt"
   fi

else
   echo "Usage : ./key_store.sh get parameter_name" 
   echo "      : ./key_store.sh put parameter_name vlaue"
   echo "      : ./key_store.sh getall parameter_name "
   echo "      : ./key_store.sh listusers parameter_name "
        
fi

unset OPER
unset PARAM_NAME
unset KEY_VAL
unset VAL
rm -f key_store_out.txt
rm -f temp.txt
