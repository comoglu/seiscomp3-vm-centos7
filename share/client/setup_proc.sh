#!/bin/bash

#-------------------------------------------------------------------------------
# Bootstraps the setup of a processing machine.
#-------------------------------------------------------------------------------

# the location of this script must be kept in sync with
# ../files/create_base_image.sh.
source /opt/sc3_install/setup_env.sh

# We source .bashrc because this script may be run by root and we want the.
source "$EC2_USER_HOME_FOLDER/.bashrc"

if [[ "$THIS_HOSTS_AGENCY_ID" != "NA" ]]; then
    # modify the route 53 records
    "$SETUP_SCRIPT_DIR"/setup_route53.sh
fi

# ensure that seiscomp is stopped before we try to cleanup its installation dir.
seiscomp stop

# get the release we are interested in
seiscomp_archive="$STACK_TAG".tar.gz
rm -rf $SEISCOMP_HOME_FOLDER/seiscomp3
aws s3 cp s3://"$SC3_CONFIG_BUCKET"/"$seiscomp_archive" /tmp/"$seiscomp_archive"
tar -xzf /tmp/"$seiscomp_archive" -C /opt

# These are the permissions in git (or should be) but just to make sure...
chmod +x "$SEISCOMP_HOME_FOLDER"/share/eatws/finalise_setup*.sh

current_stack_number=$(aws dynamodb get-item \
    --table-name stackdetails \
    --projection-expression "val" \
    --key "{\"key\": {\"S\": \"number\"}}" \
    --query "[Item.val]" \
    --region ap-southeast-2 \
    --output text)

if [[ "$EATWS_STACK_NUMBER" != "$current_stack_number" ]]; then
    # finalise the installation (this file gets created by the command above)
    $SEISCOMP_HOME_FOLDER/share/eatws/finalise_setup_"$1".sh nxt
else
    $SEISCOMP_HOME_FOLDER/share/eatws/finalise_setup_"$1".sh cur
fi
