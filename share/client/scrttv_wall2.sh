#!/bin/bash

# West Australia
scrttv -u scrttvwa --resortAutomatically="true" --autoApplyFilter="true" --bufferSize="1800" \
         --streams.codes=AU.KDU.*.*HZ,AU.MTN.*.*HZ,AU.DPH.*.*HZ,AU.DRS.*.*HZ,AU.KNRA.*.*HZ,AU.WRAB.*.*HZ,AU.AS31.*.*HZ,AU.FITZ.*.*HZ,AU.MBWA.*.*HZ,AU.GIRL.*.*HZ,AU.MEEK.*.*HZ,AU.WRKA.*.*HZ,AU.MORW.*.*HZ,AU.BLDU.*.*HZ,AU.MTKN.*.*HZ,AU.CARL.*.*HZ,AU.KLBR.*.*HZ,AU.NWAO.*.*HZ,AU.RKGY.*.*HZ,AU.KMBL.*.*HZ,AU.FORT.*.*HZ,AU.MCQ.*.*HZ,AU.MAW.*.*HZ,AU.CASY.*.*HZ,AU.COEN.*.*HZ,AU.MTSU.*.*HZ,AU.QIS.*.*HZ,AU.CTA.*.*HZ,AU.RK2S.*.*HZ,AU.EIDS.*.*HZ,AU.BN2S.*.*HZ,AU.GC2F.*.*HZ,AU.GC1S.*.*HZ,AU.TW1H.*.*HZ,AU.RMQ.*.*HZ,AU.QLP.*.*HZ,AU.ARMA.*.*HZ,AU.MGCD.*.*HZ,AU.RIV.*.*HZ,AU.YNG.*.*HZ,AU.CAN.*.*HZ,AU.CNB.*.*HZ,AU.MILA.*.*HZ,AU.TOO.*.*HZ,AU.MOO.*.*HZ,AU.CORO.*.*HZ,AU.GLAD.*.*HZ,AU.ARPS.*.*HZ,AU.CMSA.*.*HZ,AU.STKA.*.*HZ,AU.NAPP.*.*HZ,AU.WHYH.*.*HZ,AU.PTPS.*.*HZ,AU.GHSS.*.*HZ,AU.TWOA.*.*HZ,AU.BBOO.*.*HZ&
# East Australia
scrttv -u scrttvea --resortAutomatically="true" --autoApplyFilter="true"  --bufferSize="1800" \
         --streams.codes=AU.COEN.*.*HZ,AU.MTSU.*.*HZ,AU.CTA.*.*HZ,AU.EIDS.*.*HZ,AU.QIS.*.*HZ,AU.QLP.*.*HZ,AU.RMQ.*.*HZ,AU.AS31.*.*HZ,AU.BN2S.*.*HZ,AU.GC1S.*.*HZ,AU.GC2F.*.*HZ,AU.RK2S.*.*HZ,AU.TW1H.*.*HZ&
#2 
scrttv -u scrttv2 --resortAutomatically="true" --autoApplyFilter="true" --bufferSize="1800" \
         --streams.codes=AU.COEN.*.*HZ,AU.MTSU.*.*HZ,AU.CTA.*.*HZ,AU.EIDS.*.*HZ,AU.QIS.*.*HZ,AU.QLP.*.*HZ,AU.RMQ.*.*HZ,AU.AS31.*.*HZ,AU.BN2S.*.*HZ,AU.GC1S.*.*HZ,AU.GC2F.*.*HZ,AU.RK2S.*.*HZ,AU.TW1H.*.*HZ& 
#3
scrttv -u scrttv4 --resortAutomatically="true" --autoApplyFilter="true" --bufferSize="1800" \
         --streams.codes=AU.ARMA.*.*HZ,AU.MGCD.*.*HZ,AU.RIV.*.*HZ,AU.YNG.*.*HZ,AU.CAN.*.*HZ,AU.CNB.*.*HZ,AU.MILA.*.*HZ,AU.TOO.*.*HZ,AU.MOO.*.*HZ,AU.ARPS.*.*HZ,AU.CMSA.*.*HZ,AU.STKA.*.*HZ,AU.BBOO.*.*HZ&
#4
scrttv -u scrttv5 --resortAutomatically="true" --autoApplyFilter="true" --bufferSize="1800" \
         --streams.codes=AU.MEEK.*.*HZ,AU.MORW.*.*HZ,AU.BLDU.*.*HZ,AU.KLBR.*.*HZ,AU.NWAO.*.*HZ,AU.RKGY.*.*HZ,AU.KMBL.*.*HZ,AU.FORT.*.*HZ,AU.CARL.*.*HZ,AU.MTKN.*.*HZ&
#5
scrttv -u scrttv6 --resortAutomatically="true" --autoApplyFilter="true" --bufferSize="1800" \
         --streams.codes=AU.MEEK.*.*HZ,AU.MORW.*.*HZ,AU.BLDU.*.*HZ,AU.KLBR.*.*HZ,AU.NWAO.*.*HZ,AU.RKGY.*.*HZ,AU.KMBL.*.*HZ,AU.FORT.*.*HZ,AU.CARL.*.*HZ,AU.MTKN.*.*HZ&
