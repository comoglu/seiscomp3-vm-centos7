#!/bin/bash
pkill autossh
pkill ssh

autossh -M 0 $1 && notify-send "$1 disconnected" -t 60000
