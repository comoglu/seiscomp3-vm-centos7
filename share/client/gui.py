"""
A very simple tool for gathering input for a script. It provides the following
parameter types:

- :py:class:`String`,
- :py:class:`Folder`,
- :py:class:`InputFile`,
- :py:class:`OutputFile`,
- :py:class:`Integer`,
- :py:class:`Float`,
- :py:class:`Boolean`,
- :py:class:`StringTuple`,
- :py:class:`IntegerTuple`, and
- :py:class:`FloatTuple`

The other class of interest is :py:class:`Register`, which is where a bunch of
parameter descripions are defined an managed. An example of usage follows. To
see this example in action, simply run this file.::

    # create a register with some parameters
    reg = Register("tester",
        String("aString", "string", "A test string", "A String"),
        Folder("aFolder", "folder", "A test folder", "A Folder"),
        InputFile("anInputFile", "inputfile", "An input file", "Input File"),
        OutputFile("anOutputFile", "outputfile", "An output file", "Output File"),
        Integer("anInteger", 42, "An integer", "The Integer"),
        Float("aFloat", 42.0, "A float", "The Float"))

    # add some more paramters to it
    reg(Boolean("aBoolean", True, "A Boolean", "The Boolean"))
    reg(StringTuple("aStringTuple", ("hello", "world"), "A string tuple", "String Tuple"))
    reg(IntegerTuple("anIntTuple", (1,2,3), "An interger tuple", "Integer Tuple"))
    reg(FloatTuple("aFloatTuple", (1.,2.,3.), "A float tuple", "Float Tuple"))

    # ask the user for input
    reg.show()

    # print the values of the parameters
    print 'aString', reg.aString
    print 'aFolder', reg.aFolder
    print 'anInputFile', reg.anInputFile
    print 'anOutputFile', reg.anOutputFile
    print 'anInteger', reg.anInteger
    print 'aFloat', reg.aFloat
    print 'aBoolean', reg.aBoolean
    print 'aStringTuple', reg.aStringTuple
    print 'anIntTuple', reg.anIntTuple
    print 'aFloatTuple', reg.aFloatTuple

"""

import types, os, pickle
from datetime import datetime
from Tkinter import *
import tkMessageBox
from tkFileDialog import askdirectory, askopenfilename



RUN_PICKLE_PATH = os.sep + 'tmp'
MIN_GUI_WIDTH = 120







class ValidationException(Exception):
    def __init__(self, *args, **kwargs):
        super(ValidationException, self).__init__(*args, **kwargs)





def checkValidVariableName(name):
    """
    Checks that the string given by \a name is a valid Python variable name.

    :param str name: The name to be used for the variable.

    :return: *name*
    """

    try: eval(name)
    except SyntaxError:
        raise Exception("'%s' is not a valid parameter name" % name)
    except NameError:
        pass
    return name





class _Parm(object):
    """
    Base class for parameters for a script. This class should not be
    instanciated directly.

    :param type parmType: A type the parameter must be convertible to.
    :param str name: The name of the parameter.
    :param default: The default value of the parameter.
    :type default: *paramType*
    :param str description: A description of the parameter.
    :param str displayName: A human friendly name for the parameter.
    """

    NO_OF_COLUMNS = 1


    def __init__(self, parmType, name, default, description, displayName):
        self.name=checkValidVariableName(name)
        self.displayName = displayName if displayName else name
        self.parmType = parmType
        self.value = default
        self.description = description


    def getRawValue(self):
        return self.value


    def loadFromEntry(self):
        self.value = self.entry.get()


    def getVal(self):
        """Get the value of the parameter.

        :return: The validated value of the parameter.
        """

        return self.__validate__()


    def __validate__(self):
        """
        Validate the value of the parameter.

        This checks that the value can be converted to *self.paramType*.

        Overrides of this method should throw :py:exc:`ValidationException` if
        validation fails (though any exception class would do in practice).

        The typical pattern to override this method is::

            def __validate__(self):
                value = super(Class, self).__validate__()
                if value is not OK:
                    raise ValidationException("why self.value is not OK")
                return value

        :return: The value of this parameter converted to the appropritate type.
        """

        return self.parmType(self.value)


    def __str__(self):
        return '{} ({}) {}: "{} with value |{}|"'.format(
            self.name,
            self.displayName,
            self.parmType,
            self.description,
            self.value)


    def addToView(self, row, columnspan, fullColumnspan, view):
        """
        Add this to the GUI.

        :param int row: The row that the parameter should be added on.

        :param int columnspan: The total number of columns of the display of this
            parameter. This is generally provided by the overriding class.

        :param int fullColumnspan: The total number of columns in the \a view
            which holds the parameters.

        :param view: The view to add this param to.

        .. note:: *view* must have a grid layout.

        :return Tuple containting:

            - the frame that further elements of \a self should be added to,
            - the row that the next parameter should be added on,
            - the number of rows in the view in the display of \a self.
        """

        self.columnspan = columnspan
        self.row = row
        self.fr=LabelFrame(view, text=str(self.displayName))
        self.fr.grid(row=row, column=0, sticky=W+E)
        self.fr.columnconfigure(0,weight=1)
        self.entry = Entry(self.fr)
        self.entry.grid(row=row, column=0, columnspan=columnspan, sticky=W+E)
        self.entry.insert(INSERT, self.value)
        self.entry.rowconfigure(0,weight=1)
        if self.description:
            row = row+1
            self.desc = Label(self.fr, text=self.description, width=max(len(self.description), MIN_GUI_WIDTH), anchor=W)
            self.desc.grid(row=row, column=0, columnspan=columnspan, sticky=W+E)
        return (self.fr, row+1, 2)


    def getWidth(self):
        """
        The number of columns required to display.
        """

        return _Parm.NO_OF_COLUMNS


    def setError(self, error):
        """
        Set an error string, for instance, if validation fails.

        :param str error: Description of the error.
        """

        self.errorLabel = Label(self.fr, text=str(error), anchor=W, fg='red')
        self.errorLabel.grid(row=self.row+2, column=0, columnspan=self.columnspan, sticky=W+E)


    def clearError(self):
        """
        Clear an error from associated with this from the gui.
        """

        if hasattr(self, 'errorLabel') and self.errorLabel:
            self.errorLabel.grid_forget()


    def pickle(self, out):
        """
        Save this in a pickle file.

        :param out: The (open) file to pickle this parameter to.
        """

        pickle.dump(self.name, out)
        pickle.dump(self.displayName, out)
        pickle.dump(self.parmType, out)
        pickle.dump(self.value, out)
        pickle.dump(self.description, out)


    def unPickle(self, infile, inView):
        """
        Load from a (pickle) file.

        :param infile: The (open) file to load from.

        :param bool inView: A logical denoting whether self is currently visibl
            in the gui.
        """
        self.name = pickle.load(infile)
        self.displayName = pickle.load(infile)
        self.parmType = pickle.load(infile)
        self.value = pickle.load(infile)
        self.description = pickle.load(infile)
        if inView:
            self.updateExistingView()


    def updateExistingView(self):
        """
        If the gui is visible, then update it for this parameter.
        """

        self.entry.delete(0, END)
        self.entry.insert(INSERT, self.value)
        self.fr.text = self.displayName
        self.desc.text = self.description




class String(_Parm):
    """
    Represents a parameter which is a String

    :param str name: The name of the parameter.
    :param str default: The default value of the parameter.
    :param str description: A description of the parameter.
    :param str displayName: A human friendly name for the parameter.
    :param bool emptyOK: Is it OK for the string to be empty?
    """

    def __init__(self, name, default='', description=None, displayName=None, emptyOK=False):
        super(String, self).__init__(str, name, default, description, displayName)
        self.emptyOK = emptyOK


    def __validate__(self):
        """
        Override of :py:meth:`_Parm.__validate__`.

        Checks that the string is not empty if not *self.emptyOK*.
        """

        val = super(String, self).__validate__()
        if val.strip() == '' and not self.emptyOK:
            raise ValidationException(
                '"{}" cannot be an empty string'.format(self.displayName))
        return val





class _Path(String):
    """
    Represents a parameter which is a path in the file system. This class
    should not be instanciated directly.

    :param str name: The name of the parameter.
    :param str default: The default value of the parameter.
    :param str description: A description of the parameter.
    :param str displayName: A human friendly name for the parameter.
    :param bool mustExist: Must the file exist at the time the script is run?
    """

    def __init__(self, name, default, description=None, displayName=None, mustExist=False):
        super(_Path, self).__init__(name, default, description, displayName, False)
        self.mustExist = mustExist


    def addToView(self, row, columnspan, fullColumnspan, view):
        """
        Overload of :py:meth:`_Parm.addToView`.
        """

        ncols=1
        (view, row, nrow) = _Parm.addToView(self, row, columnspan-ncols, fullColumnspan, view)
        Button(view, text="Browse", command=self._browse).grid(row=row-nrow, column=columnspan-ncols, padx=3)
        return (view, row+1, nrow)


    def getWidth(self):
        """
        The number of columns required in the display.
        """

        return _Parm.getWidth(self) + 2


    def __validate__(self):
        """
        Overload of :py:meth:`String.__validate__`.

        Checks that the path exists if *self.mustExist*.
        """

        value = super(_Path, self).__validate__()
        if self.mustExist and not os.path.exists(value) :
            raise ValidationException('"{}" path "{}" does not exist'.format(
                self.displayName, value))
        return os.path.normpath(value)





class _File(_Path):
    """
    Represents a parameter which is a path in the file system. This class
    should not be instanciated directly.

    :param str name: The name of the parameter.
    :param str default: The default value of the parameter.
    :param str description: A description of the parameter.
    :param str displayName: A human friendly name for the parameter.
    :param bool mustExist: Must the file exist at the time the script is run?
    """

    def __init__(self, name, default, description=None, displayName=None, mustExist=False):
        super(_File, self).__init__(name, default, description, displayName, mustExist)


    def __validate__(self):
        """
        Overload of :py:meth:`_Path.__validate__`.

        Checks that the path is a file if *self.mustExist*.
        """

        value = super(_File, self).__validate__()
        if self.mustExist and not os.path.isfile(value):
            raise Exception("'{}' is not a file".format(value))
        return value


    def _browse(self):
        """
        Open a file chooser to allow the user to select a file (or directory).
        """

        newText = askopenfilename(
            initialdir=os.path.dirname(self.value),
            multiple=False).replace('/', os.path.sep)
        if newText:
            self.entry.delete(0, END)
            self.entry.insert(INSERT, newText)





class Folder(_Path):
    """
    Represents a parameter which is an input file.

    :param str name: The name of the parameter.
    :param str default: The default value of the parameter.
    :param str description: A description of the parameter.
    :param str displayName: A human friendly name for the parameter.
    """

    def __init__(self, name, default='', description=None, displayName=None):
        super(Folder, self).__init__(name, default, description, displayName, True)


    def __validate__(self):
        """
        Overload of :py:meth:`_Path.__validate__`.

        Checks that the path is a directory if *self.mustExist*.
        """

        value = super(Folder, self).__validate__()
        if self.mustExist and not os.path.isdir(value):
            raise Exception("'{}' is not a directory".format(value))
        return value


    def _browse(self):
        """
        Open a file chooser to allow the user to select a file (or directory).
        """

        newText = askdirectory(
            initialdir=os.path.dirname(self.value),
            mustexist=self.mustExist).replace('/', os.path.sep)
        if newText:
            self.entry.delete(0, END)
            self.entry.insert(INSERT, newText)





class InputFile(_File):
    """
    Represents a parameter which is an input file.

    :param str name: The name of the parameter.
    :param str default: The default value of the parameter.
    :param str description: A description of the parameter.
    :param str displayName: A human friendly name for the parameter.
    """

    def __init__(self, name, default='', description=None, displayName=None):
        super(InputFile, self).__init__(name, default, description, displayName, True)





class OutputFile(_File):
    """
    Represents a parameter which is an outputFile.

    :param str name: The name of the parameter.
    :param str default: The default value of the parameter.
    :param str description: A description of the parameter.
    :param str displayName: A human friendly name for the parameter.
    """

    def __init__(self, name, default='', description=None, displayName=None):
        super(OutputFile, self).__init__(name, default, description, displayName, False)


    def __validate__(self):
        """
        Overload of :py:meth:`_File.__validate__`.

        Checks that the path is not directory if the path exists.
        """

        value = super(OutputFile, self).__validate__()
        if os.path.exists(value) and os.path.isdir(value):
            raise Exception("path {} is a directory".format(value))
        return value





class Integer(_Parm):
    """
    Represents a parameter which is an Integer

    :param str name: The name of the parameter.
    :param int default: The default value of the parameter.
    :param str description: A description of the parameter.
    :param str displayName: A human friendly name for the parameter.
    """

    def __init__(self, name, default, description=None, displayName=None):
        super(Integer, self).__init__(int, name, default, description, displayName)





class Float(_Parm):
    """
    Represents a parameter which is an floating point number.

    :param str name: The name of the parameter.
    :param float default: The default value of the parameter.
    :param str description: A description of the parameter.
    :param str displayName: A human friendly name for the parameter.
    """
    def __init__(self, name, default, description=None, displayName=None):
        super(Float, self).__init__(float, name, default, description, displayName)





class Boolean(_Parm):
    """
    Represents a parameter which is a Boolean.

    :param str name: The name of the parameter.
    :param bool default: The default value of the parameter.
    :param str description: A description of the parameter.
    :param str displayName: A human friendly name for the parameter.
    """

    def __init__(self, name, default, description=None, displayName=None):
        super(Boolean, self).__init__(bool, name, default, description, displayName)


    def addToView(self, row, columnspan, fullColumnspan, view):
        """
        Add this to the GUI.

        :param int row: The row that the parameter should be added on.

        :param int columnspan: The total number of columns of the display of this
            parameter. This is generally provided by the overriding class.

        :param int fullColumnspan: The total number of columns in the \a view
            which holds the parameters.

        :param view: The view to add this param to.

        .. note:: *view* must have a grid layout.

        :return Tuple containting:

            - the frame that further elements should be added to,
            - the row that the next parameter should be added on,
            - the number of rows that this consumes.
        """

        self.columnspan = columnspan
        self.row = row
        self.fr = LabelFrame(view, text=str(self.displayName))
        self.fr.grid(row=row, column=0, sticky=W+E)
        self.fr.columnconfigure(0, weight=1)

        self.entryVar = IntVar()
        self.entry = Checkbutton(self.fr, text=self.description, variable=self.entryVar)
        self.entry.grid(row=row, column=0, columnspan=columnspan, sticky=W)
        self.entry.rowconfigure(0, weight=1)

        self.updateExistingView()

        return (self.fr, row+1, 1)


    def updateExistingView(self):
        """
        If the gui is visible, then update it for this parameter.
        """

        if self.value:
            self.entry.select()
        else:
            self.entry.deselect()

        self.fr.text = self.displayName


    def loadFromEntry(self):
        self.value = self.entryVar.get()





class _Tuple(_Parm):
    """
    Reprents a tuple of values of the same type.

    :param type paramType: The type of elements in the tuple.
    :param str name: The name of the parameter.
    :param tuple default: The default value of the parameter.
    :param str description: A description of the parameter.
    :param str displayName: A human friendly name for the parameter.
    """

    def __init__(self, parmType, name, default, description=None, displayName=None):
        if type(default) not in (types.TupleType, types.ListType, types.GeneratorType):
            raise TypeError(
                'parameter "default" to {} constructr must be tuple, list or generator'.format(
                type(self).__name__))
        default = ', '.join((str(v) for v in default))
        super(_Tuple, self).__init__(str, name, default, description, displayName)


    def __validate__(self):
        """
        Overload of :py:meth:`_Parm.__validate__`.

        Splits *self.value* on commas and checks that all elements can be
        converted to *self.paramType*.
        """

        value = super(_Tuple, self).__validate__()
        return tuple([self.parmType(x.strip()) for x in value.split(',')])





class StringTuple(_Tuple):
    """
    Represents a tuple of strings.

    :param str name: The name of the parameter.
    :param tuple default: The default value of the parameter.
    :param str description: A description of the parameter.
    :param str displayName: A human friendly name for the parameter.
    """

    def __init__(self, name, default, description=None, displayName=None):
        super(StringTuple, self).__init__(str, name, default, description, displayName)





class IntegerTuple(_Tuple):
    """
    Represents a tuple of integers.

    :param str name: The name of the parameter.
    :param tuple default: The default value of the parameter.
    :param str description: A description of the parameter.
    :param str displayName: A human friendly name for the parameter.
    """

    def __init__(self, name, default, description=None, displayName=None):
        super(IntegerTuple, self).__init__(int, name, default, description, displayName)





class FloatTuple(_Tuple):
    """
    Represents a tuple of integers.

    :param str name: The name of the parameter.
    :param tuple default: The default value of the parameter.
    :param str description: A description of the parameter.
    :param str displayName: A human friendly name for the parameter.
    """

    def __init__(self, name, default, description=None, displayName=None):
        super(FloatTuple, self).__init__(float, name, default, description, displayName)





class Register(object):
    """
    Holds a set of parameters for a script.

    :param str scriptName: A name for the script. This does not have to be the
        same as the file name and is used for naming the files parameter sets
        are saved to/loaded from.

    :param args: Instances of subclasses of :py:class:`_Parm`.
    """

    def __init__(self, scriptName, *args):
        self.scriptName = scriptName
        self.parms = [arg for arg in args]


    def __validate__(self):
        """
        Ensure that all parameters are valid.
        """

        res = True
        for parm in self.parms:
            try:
                parm.__validate__()
            except Exception, e:
                parm.setError(e)
                res = False
        return res


    def __getattr__(self, name):
        """
        Gets the value of the parameter attribute.

        If a registered parameter has name *name*, then return its value.

        This is a special method. See the python documentation for how it works.
        """

        for parm in self.parms:
            if parm.name == name:
                return parm.getVal()

        # this should throw the appropriate exception, as __getattr__ will only
        # get called if usual lookup fails.
        return super(Register, self).__getattribute__(name)


    def as_dict(self):
        return {p.name: p.getVal() for p in self.parms}


    def show(self, action=sys.exit, runButtonLabel='Run', showSaveLoad=True):
        """
        Display all the parameters in a GUI.

        :param action: A callable that will be called if the cancel button is pressed.
        :param runButtonLabel: The label to display on the 'Run' button.
        :param showSaveLoad: Should 'Save' and 'Load' buttons be included?
        """

        self.view = Tk()
        #iconPath = os.path.join(os.path.dirname(__file__), "gerbil.ico")
        #self.view.wm_iconbitmap(iconPath)
        self.view.title(self.scriptName)
        self.view.protocol('WM_DELETE_WINDOW', lambda: self._Register__cancel(action))
        (self.frame, self.canvas) = getScrollingFrame(self.view)
        columnSpan = max([x.getWidth() for x in self.parms])
        if columnSpan < 2: columnSpan=2
        row=0
        for arg in self.parms:
            row = arg.addToView(row, columnSpan, columnSpan, self.frame)[1]
        fr=LabelFrame(self.frame, borderwidth=0)
        fr.grid(row=row, column=0, rowspan=1, columnspan=1)
        Button(fr, text=runButtonLabel, command=self._Register__run).grid(row=row, column=0, sticky=W+E+N+S, padx=5, pady=5)
        Button(fr, text="Cancel", command=lambda: self._Register__cancel(action)).grid(row=row, column=1, sticky=W+E+N+S, padx=5, pady=5)
        if showSaveLoad:
            Button(fr, text="Save", command=self.save).grid(row=row, column=2, sticky=W+E+N+S, padx=5, pady=5)
            Button(fr, text="Load", command=self.load).grid(row=row, column=3, sticky=W+E+N+S, padx=5, pady=5)

        self._Register__setupView()
        self.inView = True
        self.view.attributes('-topmost', 1)
        self.view.attributes('-topmost', 0)
        self.view.mainloop()


    def __setupView(self):
        """
        Perform the layout of the GUI.
        """

        ## Update display to get correct dimensions and disable resizing.
        self.frame.update_idletasks()
        wdth, hgt = (self.frame.winfo_width()+21, min(600, self.frame.winfo_height()))
        self.view.geometry("%dx%d%+d%+d" % (wdth, hgt, (self.view.winfo_screenwidth()-wdth)/2, (self.view.winfo_screenheight()-hgt)/2))
        self.view.resizable(width=False, height=False)
        ## Configure size of canvas's scrollable zone
        self.canvas.configure(scrollregion=(0, 0, self.frame.winfo_width(), self.frame.winfo_height()))


    def __call__(self, parm):
        """
        Register a parameter.
        """

        self.parms.append(parm)


    def __str__(self):
        return ", ".join((str(p) for p in self.parms))


    def __cancel(self, action):
        """
        Abort running the script.

        The GUI is closed and the system exited.
        """

        self.view.destroy()
        if action: action()


    def __run(self):
        """
        Validate the parameters and close the GUI.
        """

        for parm in self.parms:
            parm.clearError()
            parm.loadFromEntry()

        if self.__validate__():
            self.inView=False
            self.view.destroy()
        else:
            self._Register__setupView()


    @property
    def fileExt(self):
        return '.{}_parms'.format(self.scriptName)


    def save(self, saveDir=None, filePrefix=None):
        """
        Save the parameters to a file.

        .. warning:: No validation of the paramters is done before saving.
        """

        notifySaved = saveDir is None

        if saveDir is None:
            saveDir = RUN_PICKLE_PATH
        if filePrefix is None:
            filePrefix = datetime.now().strftime("%d-%m-%y-%H-%M")
        leafName = "{}{}".format(
            filePrefix,
            self.fileExt)
        path = os.path.join(saveDir, leafName)

        for parm in self.parms:
            parm.value = parm.getRawValue()
        try:
            with open(path, 'wb') as output:
                pickle.dump(len(self.parms), output)
                pickle.dump(self.scriptName, output)
                for parm in self.parms:
                    parm.pickle(output)
        except:
            if notifySaved:
                tkMessageBox.showerror("Cannot save parameters to file", path)
        else:
            if notifySaved:
                tkMessageBox.showinfo("Parameters saved", leafName)
        return leafName


    def load(self, fileName=None):
        """
        Load a set of parameters from a (pickle) file.
        """

        class Ret(object):
            val=None

        if not fileName:
            pDir = RUN_PICKLE_PATH if not hasattr(self, 'previousLoadDir') else self.previousLoadDir
#            prog = re.compile('{}$'.format(fileExt))
#            items = ifilter(lambda x : prog.match(x), os.listdir(pDir))
#            ret = Ret()
#            #d = MyDialog(self.view, [item[:-4] for item in items], ret)
#            self.view.wait_window(d.top)
#            if ret.val:
#                fileName = os.path.join(pDir, ret.val + '.pkl')
#                print fileName
            fileName = askopenfilename(initialdir=pDir, filetypes=(('params', self.fileExt),))
        if fileName:
            try:
                inFile = open(fileName, 'rb')
                nParms = pickle.load(inFile)
                scriptName = pickle.load(inFile)
                if len(self.parms) != nParms:
                    raise Exception("Wrong number of parameters (%i != %i)" % (len(self.parms), nParms))
                if self.scriptName != scriptName:
                    raise Exception("The name of the register is not correct (%s != %s)" % (self.scriptName, scriptName))
                for parm in self.parms:
                    parm.unPickle(inFile, self.inView)
                inFile.close()
            except Exception, e:
                print str(e)
                tkMessageBox.showerror("Load faild", "could not load %s: %s" % (fileName, str(e)))
            else:
                tkMessageBox.showinfo("Load succeeded", "%s loaded" % fileName)
                self.previousLoadDir = os.path.dirname(fileName)





def getScrollingFrame(root):
    """
    Add a scrolling frame to \a root.
    """

    root.grid_rowconfigure(0, weight=1)
    root.grid_columnconfigure(0, weight=1)
    cnv = Canvas(root)
    cnv.grid(row=0, column=0, sticky='nswe')
    ## Scrollbars for canvas
    vScroll = Scrollbar(root, orient=VERTICAL, command=cnv.yview)
    vScroll.grid(row=0, column=1, sticky='ns')
    cnv.configure(yscrollcommand=vScroll.set)#cnv.configure(xscrollcommand=hScroll.set, yscrollcommand=vScroll.set)
    ## Frame in canvas
    frm = Frame(cnv)
    ## This puts the frame in the canvas's scrollable zone
    cnv.create_window(0, 0, window=frm, anchor='nw')
    return (frm, cnv)





class MyDialog:
    """
    A dialog box that displays saved versions of a register.

    :param parent: The 'view' that is to be the parent of \a self.
    :param items: The items to display in the dialog.
    :param res: An object that will have contain the chosen item.
    """

    def __init__(self, parent, items, res):
        self.res = res
        top = self.top = Toplevel(parent)
        Label(top, text="Please choose from a previous run").pack()
        self.listbox = Listbox(top, selectmode=SINGLE)
        self.listbox.config(width=MIN_GUI_WIDTH)
        for item in items:
            self.listbox.insert(END, item)
        self.listbox.pack()
        fr=LabelFrame(top, borderwidth=0)
        fr.grid(row=0, column=0, rowspan=1, columnspan=1)
        fr.pack()
        Button(fr, text="OK", command=self.ok).grid(row=0, column=1, sticky=W+E+N+S, padx=5, pady=5)
        Button(fr, text="Cancel", command=self.cancel).grid(row=0, column=2, sticky=W+E+N+S, padx=5, pady=5)


    def ok(self):
        """
        Called when the OK button is pressed.

        Gets the selected item, sets it as attribute *val* on *self*, then
        closes the dialog. Will do nothing if no item is selected.
        """

        self.res.val = self.listbox.get(self.listbox.curselection()[0])
        self.top.destroy()


    def cancel(self):
        """
        Called when the Cancel button is pressed and closes the dialog.
        """

        self.top.destroy()


if __name__ == "__main__":
    # create a register with some parameters
    reg = Register("tester",
        String("aString", "string", "A test string", "A String"),
        Folder("aFolder", "folder", "A test folder", "A Folder"),
        InputFile("anInputFile", "inputfile", "An input file", "Input File"),
        OutputFile("anOutputFile", "outputfile", "An output file", "Output File"),
        Integer("anInteger", 42, "An integer", "The Integer"),
        Float("aFloat", 42.0, "A float", "The Float"))

    # add some more paramters to it
    reg(Boolean("aBoolean", True, "A Boolean", "The Boolean"))
    reg(StringTuple("aStringTuple", ("hello", "world"), "A string tuple", "String Tuple"))
    reg(IntegerTuple("anIntTuple", (1,2,3), "An interger tuple", "Integer Tuple"))
    reg(FloatTuple("aFloatTuple", (1.,2.,3.), "A float tuple", "Float Tuple"))

    # ask the user for input
    reg.show()

    # print the values of the parameters
    print 'aString:', reg.aString
    print 'aFolder:', reg.aFolder
    print 'anInputFile:', reg.anInputFile
    print 'anOutputFile:', reg.anOutputFile
    print 'anInteger:', reg.anInteger
    print 'aFloat:', reg.aFloat
    print 'aBoolean:', reg.aBoolean
    print 'aStringTuple:', reg.aStringTuple
    print 'anIntTuple:', reg.anIntTuple
    print 'aFloatTuple:', reg.aFloatTuple

    # or just
    print reg

    raw_input("Press enter to exit.")
