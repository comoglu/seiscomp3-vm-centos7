#!/bin/bash

#################################################
#Utility to create users/passwords on the VM
#the script is to be run with sudo
#pre-requisite Seiscomp installed in 
#/opt/seiscomp3 folder and
#license files for seiscomp3 are copied to the
#folder /opt/sc3license
#################################################


SCRIPT_DIR=`pwd`
cat /dev/null > $SCRIPT_DIR/password.txt

for NEWUSER in $(cat $SCRIPT_DIR/users.txt);
do
    mkdir -p $SCRIPT_DIR/sshkeys/$NEWUSER
    PASSWD=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 7 | head -n 1);
    UCMD='ssh-keygen -b 2048 -t rsa -C "'$NEWUSER'@eatws" -f /home/'$NEWUSER'/.ssh/id_rsa -q -N ""'

    useradd -g eatws -m "$NEWUSER"
    echo "$NEWUSER:$PASSWD"|chpasswd
    # make the user part of group (with privileges)
    usermod -aG wheel "$NEWUSER"
    
    # create a new group docker, add users to the group
    # this is so users can run the docker command without
    # having to type sudo.
    groupadd docker
    usermod -aG docker "$NEWUSER"

    su - "$NEWUSER" -c "$UCMD";
    su - "$NEWUSER" -c 'cat /home/'$NEWUSER'/.ssh/id_rsa' |tee $SCRIPT_DIR/sshkeys/$NEWUSER/id_rsa
    su - "$NEWUSER" -c 'cat /home/'$NEWUSER'/.ssh/id_rsa.pub' |tee $SCRIPT_DIR/sshkeys/$NEWUSER/id_rsa.pub

    cp $SCRIPT_DIR/config /home/$NEWUSER/.ssh/
    chown $NEWUSER:eatws  /home/$NEWUSER/.ssh/*

    #copy the seiscomp env variables in users bashrc
    #the --asroot is passed to seiscomp since the script is run as root
    /opt/seiscomp3/bin/seiscomp --asroot print env >> /home/$NEWUSER/.bashrc
    echo "source /opt/sc3_install/setup_env_static.sh" >> /home/$NEWUSER/.bashrc
    echo 'PATH=$PATH:~/.seiscomp3' >> /home/$NEWUSER/.bashrc

    # this is to ensure that all files created on the VM has rw permissions for
    # the group
    echo "umask 0002" >> /home/$NEWUSER/.bashrc
    
    echo "PATH=\$PATH:/opt/passcal/bin:/opt/passcal" >> /home/$NEWUSER/.bashrc

    #link seiscomp licenses for user
    mkdir /home/$NEWUSER/.seiscomp3
    chmod a+rwX /home/$NEWUSER/.seiscomp3
    chown $NEWUSER:eatws /home/$NEWUSER/.seiscomp3
    
    #this is causing unwanted files to be copied/linked
    #ln -s /opt/seiscomp3/share/client/* /home/$NEWUSER/.seiscomp3

    echo $NEWUSER $PASSWD >> $SCRIPT_DIR/password.txt;
    PASSWD=""
    UCMD=""
done

sh /home/centos/sc3-users/changename.sh 
