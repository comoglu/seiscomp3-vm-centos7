#!/bin/env python

import os
import sys
import time
import logging
import logging.handlers
from Tkinter import *
from gui import *
from tkMessageBox import showerror
import boto
import seiscomp3.Core as Core



if __name__ != "__main__":
    raise Exception('This is a program not a module')



BUCKET_NAME = 'eatws-earthquakes-at-ga'

XML_TEMPLATE = """
<?xml version="1.0" encoding="UTF-8"?>
<seiscomp xmlns="http://geofon.gfz-potsdam.de/ns/seiscomp3-schema/0.10" version="0.10">
  <Event publicID="{evid}">
    <preferredOriginID>Origin/{ctimee}.1</preferredOriginID>
    <preferredMagnitudeID>Magnitude/{ctimee}.2</preferredMagnitudeID>
    <creationInfo>
      <agencyID>GA</agencyID>
      <author>GA</author>
      <creationTime>{ctime}</creationTime>
    </creationInfo>
  </Event>
  <Origin publicID="{ctimee}.1">
    <time>
      <value>{evtime}</value>
    </time>
    <latitude>
      <value>{lat}</value>
    </latitude>
    <longitude>
      <value>{lon}</value>
    </longitude>
    <depth>
      <value>{depth}</value>
    </depth>
    <evaluationMode>manual</evaluationMode>
    <evaluationStatus>confirmed</evaluationStatus>
    <creationInfo>
      <agencyID>GA</agencyID>
      <author>GA</author>
      <creationTime>{ctime}</creationTime>
    </creationInfo>
    <magnitude publicID="Magnitude/{ctimee}.2">
      <magnitude>
        <value>{mag}</value>
      </magnitude>
      <type>magType</type>
      <originID>Origin/{ctimee}.1</originID>
      <creationInfo>
        <agencyID>GA</agencyID>
        <author>GA</author>
        <creationTime>{ctime}</creationTime>
      </creationInfo>
    </magnitude>
  </Origin>
  <Magnitude publicID="Magnitude/{ctimee}.2">
    <magnitude>
      <value>{mag}</value>
    </magnitude>
    <type>{magType}</type>
    <originID>Origin/{ctimee}.1</originID>
    <creationInfo>
      <agencyID>GA</agencyID>
      <author>GA</author>
      <creationTime>{ctime}</creationTime>
    </creationInfo>
  </Magnitude>
</seiscomp>
"""



logger = logging.getLogger('connection-monitor-{}'.format('app'))
logger.setLevel(logging.INFO)
sys_log_handler = logging.handlers.SysLogHandler('/dev/log')
formatter = logging.Formatter('%(name)s: %(message)s')
sys_log_handler.setFormatter(formatter)
logger.addHandler(sys_log_handler)
logger.info('starting connection monitor {}'.format('app'))



def create_xml(evid, evtime, lat, lon, depth, mag, magType):
    ctimee = time.time()
    ctime = Core.Time(ctimee).iso()
    return XML_TEMPLATE.format(
        evid=evid,
        evtime=evtime,
        lat=lat,
        lon=lon,
        depth=depth,
        ctime=ctime,
        ctimee=ctimee,
        mag=mag,
        magType=magType)



def send_to_s3(event_id, content):
    conn = boto.connect_s3()
    b = conn.get_bucket(BUCKET_NAME)
    key = boto.s3.key.Key(b)
    key.key = '{}.xml'.format(event_id)
    key.set_contents_from_string(content)



def main():
    # create a register with some parameters
    reg = Register("Push to EQ@GA",
        String("evid", description="The id of the event.", displayName="Event ID"),
        String("evtime", description="The time of the event as ISO string", displayName="Event Time"),
        Float("lat", '', description="The latitude of the event.", displayName="Latitude"),
        Float("lon", '', description="The longitude of the event.", displayName="Longitude"),
        Float("depth", '', description="The depth of the event.", displayName="Depth"),
        Float("mag", '', description="The magnitude of the event.", displayName="Magnitude"),
        String("magType", description="The magnitude type of the event", displayName="Magnitude Type"))

    # ask the user for input
    reg.show(showSaveLoad=False)

    try:
        xml_content = create_xml(**reg.as_dict())
    except Exception as e:
        root = Tk()
        root.withdraw()
        tkMessageBox.showerror('Failed to create XML', str(e))
        return 1

    try:
        send_to_s3(reg.evid, xml_content)
    except Exception as e:
        root = Tk()
        root.withdraw()
        tkMessageBox.showerror('Failed to send to EQ@GA', str(e))
        return 1
    else:
        root = Tk()
        root.withdraw()
        tkMessageBox.showinfo(
            'Succes',
            'sent event {} to {}'.format(reg.evid, BUCKET_NAME))

    return 0



if __name__ == "__main__":
    sys.exit(main())
