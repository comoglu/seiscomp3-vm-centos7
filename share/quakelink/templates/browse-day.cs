<div class="browser" id="browser-day">
	<div class="breadcrumb">
		<?cs each:item = Browser.Path?><a href="<?cs var:Page.PathToRoot ?><?cs var:item.URL?>"><?cs var:item.Name?> &gt;</a><?cs /each?><span><?cs var:Browser.Current?></span>
	</div>
	<table id="list" class="bordered striped hover">
		<thead >
			<tr>
				<th class="text-center"><strong>Origin Time</strong><br/>UTC</th>
				<th class="text-center"><strong>Mag</strong></th>
				<th class="text-center"><strong>Latitude</strong><br/>degrees</th>
				<th class="text-center"><strong>Longitude</strong><br/>degrees</th>
				<th class="text-center"><strong>Depth</strong><br/>km</th>
				<th class="text-center"><strong>A<br/>M</strong></th>
				<th class="text-center"><strong>Agency</strong></th>
				<th class="text-left"><strong>Region Name</strong></th>
			</tr>
		</thead>
		<tbody><?cs each:item = Browser.Items?>
			<tr onclick="document.location = '<?cs var:Page.PathToRoot ?><?cs var:item.URL?>';">
				<td><?cs var:item.OT?></td>
				<td class="text-center"><?cs var:float(item.Mag,1)?></td>
				<td class="text-right" ><?cs var:strfcoord(item.Lat, "%.2F°%c")?></td>
				<td class="text-right"><?cs var:strfcoord(item.Lon,"%.2F°%C")?></td>
				<td class="text-right"><?cs var:float(item.Dep,0)?></td>
				<td class="text-center <?cs var:item.Stat?>"><?cs var:item.Stat?></td>
				<td class="text-center"><?cs var:item.Agency?></td>
				<td><?cs var:item.Reg?></td>
			</tr><?cs /each?>
		</tbody>
	</table>
</div>
