<div class="browser" id="browser-day">
	<div class="breadcrumb">
		<?cs each:item = Browser.Path?><a href="<?cs var:Page.PathToRoot ?><?cs var:item.URL?>"><?cs var:item.Name?> &gt;</a><?cs /each?><span><?cs var:Browser.EventID?></span>
	</div>
	<table id="revlist" class="bordered striped">
		<thead>
			<tr><?cs each:item = Browser.Header?>
				<th><?cs var:item?></th><?cs /each?>
			</tr>
		</thead>
		<tbody><?cs each:item = Browser.Items?>
			<tr><?cs each:cell = item?><td><?cs var:cell?></td><?cs /each?></tr><?cs /each?>
		</tbody>
	</table>
</div>
