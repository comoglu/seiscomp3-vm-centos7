window.addEventListener('load', function () {

    // form
    var form = document.getElementById("mapMultipartForm");
    var regInput = document.getElementById("regInput");
    var fileOL = document.getElementById('fileOL');
    var sendButton = document.getElementById("sendButton");
    var showButton = document.getElementById("showButton");
    var showLink = document.getElementById("showLink");
    var clearButton = document.getElementById("clearButton");
    var showSubmit = false;
    var clearSubmit = false;

    // canvas for drawing the result image
    var cnv = document.getElementById('cnv');
    var loadImg = document.getElementById('loadImg');

    // file array
    var files = [];

    var requestOngoing = false;
    var imageLoaded = false;
    updateControls();

    function addFile() {
        var i = files.length;
        console.log('addFile: ' + i);
        var prevFile = files[i - 1];
        var clone = prevFile.dom.cloneNode(true);;
        clone.id = 'f' + i;
        var children = clone.getElementsByTagName('*');
        for ( var i = 0; i < children.length; i++ ) {
            var e = children[i];
            if ( e.id && e.id.startsWith(prevFile.dom.id) ) {
                e.id = clone.id + e.id.substr(prevFile.dom.id.length);
            }
            if ( e.name === prevFile.dom.id ) {
                e.name = clone.id;
            }
            if ( e.type === "file" ) {
                e.value = null;
            }
        }

        console.log(clone);
        prevFile.dom.parentNode.appendChild(clone);
        registerFile();
    }

    function registerFile() {
        var i = files.length;
        console.log('registerFile: ' + i);
        var id = 'f' + i;
        var file = {
            idx:           i,
            dom:           document.getElementById(id),
            domFile:       document.getElementById(id + 'File'),
            domX:          document.getElementById(id + 'X'),
            domXRectType:  document.getElementById(id + 'XRectType'),
            domXRectValue: document.getElementById(id + 'XRectValue'),
            domXAlpha:     document.getElementById(id + 'XAlpha'),
            data:          null,
            base64:        false,
            reader:        new FileReader()
        };

        // register file reader callback
        file.reader.addEventListener("loadend", fileLoaded.bind(this, file), false);

        // register file change callback
        file.domFile.addEventListener("change", readFile.bind(this, file), false);

        files.push(file);
        resetFile(file);
    }

    function resetFile(file, clearValue) {
        console.log('resetting file ' + file.idx);
        file.data = null;
        file.base64 = false;
        file.domX.style.display = 'none';

        if ( clearValue ) {
            file.domFile.value = null;
        }

        if ( file.reader.readyState === FileReader.LOADING ) {
            file.reader.abort();
        }
    }

    function readFile(file) {
        var f = file.domFile.files[0];

        console.log('reading file ' + file.idx + ': ' + f.name + ' (' +
                    f.size + ', ' + f.type + ')');
        resetFile(file);

        if ( f.type === 'application/vnd.geo+json' ||
             f.type === 'application/json' || f.type === 'text/xml' ) {
            file.reader.readAsText(f);
        }
        else if ( f.type.startsWith('image') ) {
            file.domX.style.display = 'block';
            file.base64 = true;
            file.reader.readAsArrayBuffer(f);
        }
        else {
            alert('unsupported file type: '+ f.type);
            file.domFile.value = null;
            return;
        }

        if ( file.idx === files.length - 1 ) {
            addFile();
        }

        updateControls();
    }

    function fileLoaded(file) {
        updateControls();

        if ( file.reader.readyState !== FileReader.DONE ) {
            console.log('loading failed: ' + file.reader.readyState + ' (' + file.reader.error + ')');
            resetFile(file, true);
            return;
        }

        if ( file.base64 ) {
            buf = new Uint8Array(file.reader.result);
            s = "";
            for ( var i = 0; i < buf.length; ) {
                s += String.fromCharCode.apply(null, buf.slice(i, i += 4096));
            }
            file.data = btoa(s);
        }
        else {
            file.data = file.reader.result;
        }

        console.log('file ' + file.idx + ' loaded');
    }

    function updateControls() {
        clearButton.disabled = files.length <= 1 &&  !imageLoaded;
        if ( requestOngoing ) {
            loadImg.style.display = 'block';
            cnv.style.display = 'none';
            sendButton.disabled = true;
            showButton.disabled = true;
        }
        else {
            loadImg.style.display = 'none';
            cnv.style.display = imageLoaded ? 'block' : 'none';

            var readyToSend = true;
            for ( var i = 0; i < files.length; i++ ) {
                if ( files[i].reader.readyState === FileReader.LOADING ) {
                    readyToSend = false;
                    break;
                }
            }
            //link.click();
            sendButton.disabled = !readyToSend;
            showButton.disabled = !readyToSend;
        }
    }

    function sendData() {
        var boundary = "448e4b8b10c67df873352722035a3a84";
        var data     = "";
        var file;
        var url = 'map?dim=1024,512';

        if ( regInput.value ) {
            var s = regInput.value.trim().replace(/\s+/g, ',');
            if ( s.length ) {
                url += '&reg=' + s;
            }
        }

        for ( var i = 0; i < files.length; i++ ) {
            file = files[i];
            if ( !file.domFile.files[0] ) continue;

            data += "--" + boundary + "\r\n";
            data += 'Content-Disposition: form-data; ' +
                    'name="' + file.domFile.name + '"; ' +
                    'filename="' + file.domFile.files[0].name + '"\r\n';
            data += 'Content-Type: ' + file.domFile.files[0].type + '\r\n';
            if ( file.base64 ) {
                data += 'Content-Transfer-Encoding: base64\r\n';
                data += file.domXRectType.value + ': ' + file.domXRectValue.value + '\r\n';
                if ( file.domXAlpha.value !== 255 ) {
                    data += 'X-Alpha: ' + file.domXAlpha.value + '\r\n';
                }
            }
            data += '\r\n';
            data += file.data + '\r\n';
        }

        data += "--" + boundary + "--";

        if ( showSubmit ) {
            var header = "POST " + location.pathname + url + " HTTP/1.1\r\n";
            header += "Host: " + location.host + "\r\n";
            header += "Content-Type: multipart/form-data; boundary=" + boundary + "\r\n";
            header += "Content-Length: " + data.length + "\r\n";
            header += "Referer: " + document.referrer + "\r\n";
            header += "\r\n";

            showLink.href = encodeURI("data:application/text," + header + data);
            showLink.click();
        }
        else {
            var XHR = new XMLHttpRequest();
            XHR.addEventListener('load', function(event) {
                requestOngoing = false;
                imageLoaded = true;
                var img = new Image();
                img.src = window.URL.createObjectURL(XHR.response);
                img.onload = function() {
                    cnv.getContext('2d').drawImage(img, 0, 0);
                }
                updateControls();
            });

            XHR.addEventListener('error', function(event) {
                requestOngoing = false;
                imageLoaded = false;
                updateControls();
                alert('Error sending image request!');
            });

            XHR.open('POST', url);
            XHR.responseType = "blob";
            XHR.setRequestHeader('Content-Type','multipart/form-data; boundary=' + boundary);
            XHR.setRequestHeader('Content-Length', data.length);

            console.log("request:\n " + data);
            XHR.send(data);
            requestOngoing = true;
        }
    }

    // register file inputs found in dom
    for ( var i = 0; i < fileOL.children.length; ++i ) {
        registerFile();
    }

    // read files if previously supplied
    for ( var i = 0; i < files.length; ++i ) {
        if ( files[i].domFile.files[0] ) {
            readFile(files[i]);
        }
    };

    form.addEventListener('submit', function(event) {
        event.preventDefault();
        console.log("submit");
        if ( clearSubmit ) {
            while ( files.length > 1 ) {
                var file = files.pop();
                resetFile(file, true);
                fileOL.removeChild(file.dom);
            }
            resetFile(files[0], true);
            imageLoaded = false;
        }
        else {
            sendData();
        }

        showSubmit = false;
        clearSubmit = false;
        updateControls();
    });

    showButton.addEventListener("click", function() {
        console.log("show");
        showSubmit = true;
    });

    clearButton.addEventListener("click", function() {
        console.log("clear");
        clearSubmit = true;
    });
});
