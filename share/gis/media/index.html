<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title>GIS - 2019.099#c061470 (gempa GmbH)</title>
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
		<link rel="stylesheet" href="gis.css" media="screen"/>
		<script src="query.js" type="text/javascript"></script>
	</head>
	<body>
		<div id="page">
			<div id="header">
				<div id="header-content">
					<span id="GIS">gempa Image Server</span>
					<span id="GISver">2019.099#c061470</span>
				</div>
			</div>
			<div id="content">
				<h1>Overview</h1>
				<p>The gempa Image Server (GIS) provides a HTTP interface to render earthquake related maps, waveform traces and spectra. The GIS provides a significant performance advantage in comparison to the SeisComP3 module scmapcut since it is started once and caches image and vector data.</p>
				<h1>Table of Contents</h1>
				<ul>
					<li><a href="#req">Requesting Data</a></li>
					<li><a href="#map">Query for Maps</a>
						<ul>
							<li><a href="#map-feat">Features</a></li>
							<li><a href="#map-order">Drawing Order</a></li>
							<li><a href="#map-url">URL Parameters</a></li>
							<li><a href="#map-sample">Examples</a></li>
						</ul>
					</li>
					<li><a href="#trace">Query for Traces</a>
						<ul>
							<li><a href="#trace-feat">Features</a></li>
							<li><a href="#trace-url">URL Parameters</a></li>
							<li><a href="#trace-sample">Examples</a></li>
						</ul>
					</li>
					<li><a href="#spectra">Query for Spectra</a>
						<ul>
							<li><a href="#spectra-feat">Features</a></li>
							<li><a href="#spectra-url">URL Parameters</a></li>
							<li><a href="#spectra-sample">Examples</a></li>
						</ul>
					</li>
				</ul>
				<a name="req"></a>
				<h2>Requesting Data</h2>
				<p>The GIS supports HTTP-GET and HTTP-POST requests. POST requests may be used to transmit SC3 XML (event, inventory or configuration information), vector overlays in GeoJSON format or arbitrary overlay images. Using the multipart format (<tt>Content-Type: multipart/form-data; boundary=...</tt>) multiple files may be transmitted at once.</p>
				<p>When posting SC3 XML the header <tt>Content-Type: application/xml</tt> should be used. The data may be transmitted in a single <tt>&lt;seiscomp&gt;</tt> document or in separate documents using a multipart message.</p>
				<p>If inventory data is transmitted it overrides the inventory read from the database at startup. If event data is send via HTTP-POST it overrides origin information transmitted through URL parameters.</p>
				<a name="map"></a>
				<h2>Query for Maps</h2>
				<p>The <a href="map">/map</a> query allows to cut out an arbitrary map area. The resulting image will include all layers configured at the server.</p>
				<a name="map-feat"></a>
				<h3>Features</h3>
				<ul>
					<li>Supports standard SeisComP3 map functionality, e.g. cities, vector data, and gempa add-ons such as custom projections</li>
					<li>Hypocenter is drawn as circle with magnitude-dependent radius and depth-dependent color, a custom symbol is configurable</li>
					<li>Moment tensor solution</li>
					<li>Stations participating in the solution</li>
					<li>Additional vector overlay supplied as HTTP-POST data in GeoJSON format<br/>
					    Header: <tt>Content-Type: application/json</tt></li>
					<li>Image overlay supplied as HTTP-POST data<br/>
					    Header: <tt>Content-Type: image[/subtype]</tt><br/>
					    Supported subtypes: <tt>bmp, gif, jpg, jpeg, png, pbm, pgm, ppm, tiff, xbm, xpm</tt>. If subtype is ommited <tt>jpeg</tt> is assumed.<br/>
					    The image may either be drawn using geo or screen coordinates. If no coordinates are specified the geo graphic region (see <tt>reg</tt> URL parameter) is used. The following extension headers are supported and may occur either in the HTTP or multipart header:
						<ul>
							<li><tt>X-Geo-Rect: lat1 lon1 lat2 lon2</tt><br/>
							    Draw the image with the specified geo coordinates using the configured projection.</li>
							<li><tt>X-Screen-Rect: x1 y1 x2 y2</tt><br/>
							    Draw the image using the specified screen coordinates.</li>
							<li><tt>X-Alpha: value</tt><br/>
							    Override the alpha channel by the specified value (0-255)</li>
						</ul>
						If the image is send as part of a multipart message it may be encoded either binary (default) or base64. For the latter case the multipart header <tt>Content-Transfer-Encoding: base64</tt> must be set.
					</li>
				</ul>
				<a name="map-order"></a>
				<h3>Drawing Order</h3>
				<p>The different map layers are drawn in the following order:</p>
				<ul>
					<li>Map Tiles - loaded from local tile store or map server</li>
					<li>Geo Features - loaded from BNA directory</li>
					<li>Geo Features - transmitted as JSON documents via HTTP POST</li>
					<li>Geo Images - transmitted via HTTP POST in order of their transmission</li>
					<li>Vector Layer - coordinate grid, cities</li>
					<li>Station Lines - great circle lines from station to origin</li>
					<li>Stations - triangular station symbols</li>
					<li>Origin(s) - earthquake symbol(s), circle or configured icon</li>
					<li>Moment Tensor(s) - moment tensor symbol(s)</li>
					<li>Screen Images - transmitted via HTTP POST in order of their transmission</li>
				</ul>
				<a name="map-url"></a>
				<h3>URL Parameters</h3>
				<table class="doc">
					<thead>
						<tr>
							<td>Key(s)</td>
							<td>Value</td>
							<td>Description</td>
							<td>Default</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>reg</td>
							<td>lat,lon,[margin_lat,margin_lon]|[zoom_level]</td>
							<td>Geographic region: Center followed by margin in degrees or zoom level (1-24)</td>
							<td>0,0,90,180</td>
						</tr>
						<tr>
							<td>dim</td>
							<td>width,height</td>
							<td>Image dimensions in pixel, retricted to 10 Mega pixel, min: 32x32</td>
							<td>1024,512</td>
						</tr>
						<tr>
							<td>ori,origin</td>
							<td>lat,lon[,depth]</td>
							<td>Origin of earthquake, unit of depth: km</td>
							<td>Not set</td>
						</tr>
						<tr>
							<td>origin-color</td>
							<td>color</td>
							<td>Color of the origin symbol, if unset derived from depth, e.g. rgb(255, 0, 0), rgba(255,0,0,128), FF0000, FF000080</td>
							<td>Not set</td>
						</tr>
						<tr>
							<td>origin-size</td>
							<td>width[,height]</td>
							<td>Size of origin symbol, if unset derived from magnitude value</td>
							<td>Not set</td>
						</tr>
						<tr>
							<td>mag</td>
							<td>magnitude</td>
							<td>Magnitude of earthquake</td>
							<td>Not set</td>
						</tr>
						<tr>
							<td>stations</td>
							<td>comma separated list</td>
							<td>Stations to show, wild cards supported</td>
							<td>Not set</td>
						</tr>
						<tr>
							<td>showDisabled</td>
							<td>boolean</td>
							<td>Include stations even if they have been disabled in configuration</td>
							<td>False</td>
						</tr>
						<tr>
							<td>weight</td>
							<td>float</td>
							<td>Minimum weight of the arrival time for computation of the associated origin, used for station selection if an origin is posted</td>
							<td>Not set</td>
						</tr>
						<tr>
							<td>fmt</td>
							<td>[PNG|JPG|JPEG|BMP|PPM|TIFF|XBM|XPM]</td>
							<td>Output format of image</td>
							<td>PNG</td>
						</tr>
						<tr>
							<td>qua</td>
							<td>[-1,100]</td>
							<td>Quality of image, from 0 (worst) to 100 (best), -1 = autodetect</td>
							<td>-1</td>
						</tr>
					</tbody>
				</table>
				<a name="map-sample"></a>
				<h3>Examples</h3>
				<ul>
					<li><a href="map?reg=51,10.5,5,5&amp;dim=512,256">Sample query for Germany</a></li>
					<li><a href="sample.json">GeoJSON example</a></li>
					<li>MultiPart example, select files to POST
						<form id="mapMultipartForm" method="POST" enctype="multipart/form-data" target="map">
							<p>
								<label for="regInput">Geographical Region (reg):</label>
								<input id="regInput" type="text" value="52,13,20,10"/>
							</p>
							<ol id="fileOL">
								<li id="f0">
									<p>
										<input id="f0File" name="f0" type="file" accept=".xml,.json,.geojson,.png,.jpg,.jpeg,.bmp,.ppm,.tiff,.xbm,.xpm"/>
									</p>
									<p id="f0X" style="display:none">
										<select id="f0XRectType">
											<option>X-Geo-Rect</option>
											<option>X-Screen-Rect</option>
										</select>
										<input id="f0XRectValue" type="text" size="10" maxlength="20" value="40 -20 60 20"/>&nbsp;&nbsp;&nbsp;
										<label>X-Alpha:</label>
										<input id="f0XAlpha" type="number" min="0" max="255" value="255" style="width: 50px"/>
									</p>
								</li>
							</ol>
							<button id="sendButton">Send</button>&nbsp;
							<button id="showButton">Show request</button>&nbsp;
							<a id="showLink" style="display:none" download="gis-request.txt"></a>
							<button id="clearButton">Clear</button>
						</form>
					</li>
				</ul>
				<img src="load.gif" id="loadImg" width="1000" height="512" style="display:none"/>
				<canvas id="cnv" width="1000" height="512" style="display:none"></canvas>

				<a name="trace"></a>
				<h2>Query for Traces</h2>
				<p>The <a href="traces">/traces</a> query draws timeseries data of multiple data streams.</p>
				<a name="trace-feat"></a>
				<h3>Features</h3>
				<ul>
					<li>Visualization of picks</li>
					<li>Estimated travel times</li>
					<li>Data filtering using SC3 filter grammar, e.g. BW(3, 0.5, 10.0)</li>
					<li>Various modes, e.g. default, qplot, relative time window</li>
				</ul>
				<a name="trace-url"></a>
				<h3>URL Parameters</h3>
				<table class="doc">
					<thead>
						<tr>
							<td>Key(s)</td>
							<td>Value</td>
							<td>Description</td>
							<td>Default</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>mode</td>
							<td>[default,relconst,reldist,qplot]</td>
							<td>Output mode:
								<ul>
									<li>DEFAULT: absolute time window, fixed vertical alignment</li>
									<li>QPLOT: absolute time window, fixed vertical alignment, image similar to SC2 qplot</li>
									<li>RELCONST: relative time window, fixed vertical alignment</li>
									<li>RELDIST: relative time window, vertival alignment relative to epicenter distance</li>
								</ul>
							</td>
							<td>default</td>
						</tr>
						<tr class="category"><td colspan="5">Image parameters</td></tr>
						<tr>
							<td>dim</td>
							<td>width,height</td>
							<td>Image dimensions in pixel, retricted to 10 Mega pixel, min: 32x32</td>
							<td>1024,512</td>
						</tr>
						<tr>
							<td>fmt</td>
							<td>[png,jpg,jpeg,bmp,ppm,tiff,xbm,xpm]</td>
							<td>Output format of image</td>
							<td>png</td>
						</tr>
						<tr>
							<td>qua</td>
							<td>integer [-1,100]</td>
							<td>Quality of image, from 0 (worst) to 100 (best), -1 = autodetect</td>
							<td>100</td>
						</tr>
						<tr class="category"><td colspan="5">Event parameters</td></tr>
						<tr>
							<td>ori,origin</td>
							<td>lat,lon[,depth]</td>
							<td>Origin of earthquake, unit of depth: km</td>
							<td>Not set</td>
						</tr>
						<tr>
							<td>time</td>
							<td>time</td>
							<td>Origin time</td>
							<td>Not set</td>
						</tr>
						<tr class="category"><td colspan="5">Data selection</td></tr>
						<tr>
							<td>streams</td>
							<td>comma separated list</td>
							<td>Streams to show, wild cards supported</td>
							<td>*</td>
						</tr>
						<tr>
							<td>showDisabled</td>
							<td>boolean</td>
							<td>Include stations even if they have been disabled in configuration</td>
							<td>False</td>
						</tr>
						<tr>
							<td>start</td>
							<td>time</td>
							<td>Begin of the timewindow</td>
							<td>end - bufferSize</td>
						</tr>
						<tr>
							<td>end</td>
							<td>time</td>
							<td>End of the timewindow</td>
							<td>now()</td>
						</tr>
						<tr>
							<td>bufferSize</td>
							<td>seconds</td>
							<td>Length of the timewindow</td>
							<td>1800.0</td>
						</tr>
						<tr>
							<td>num</td>
							<td>integer</td>
							<td>Number of streams to show</td>
							<td>5</td>
						</tr>
						<tr>
							<td>weight</td>
							<td>float</td>
							<td>Minimum weight of the arrival time for computation of the associated origin, used for stream selection if an origin is posted</td>
							<td>Not set</td>
						</tr>
						<tr>
							<td>mindist</td>
							<td>float</td>
							<td>Minimum distance of station to origin (km or degree, see distunit)</td>
							<td>Not set</td>
						</tr>
						<tr>
							<td>maxdist</td>
							<td>float</td>
							<td>Maximum distance of station to origin (km or degree, see distunit)</td>
							<td>Not set</td>
						</tr>
						<tr>
							<td>minazi</td>
							<td>float</td>
							<td>Minimum azimuth from station to origin</td>
							<td>Not set</td>
						</tr>
						<tr>
							<td>maxazi</td>
							<td>float</td>
							<td>Maximum azimuth from station to origin</td>
							<td>Not set</td>
						</tr>
						<tr class="category"><td colspan="5">Data representation</td></tr>
						<tr>
							<td>filter</td>
							<td>SeisComP3 filter grammar</td>
							<td>Record filter chain, e.g. RMHP(10)&gt;&gt;BW(3,2,4)</td>
							<td>Not set</td>
						</tr>
						<tr>
							<td>removeoffset</td>
							<td>boolean</td>
							<td>Toggles removal of offset prior to filtering and plotting</td>
							<td>False</td>
						</tr>
						<tr>
							<td>orderby</td>
							<td>[name,distance,azi]</td>
							<td>Sort order of trace, the values <tt>distance</tt> and <tt>azi</tt> refer to the epi center</td>
							<td>name</td>
						</tr>
						<tr>
							<td>distunit</td>
							<td>[km,deg]</td>
							<td>Distance unit, see <tt>mindist</tt> and <tt>maxdist</tt> parameter</td>
							<td>km</td>
						</tr>
						<tr>
							<td>showtt</td>
							<td>boolean</td>
							<td>Show travel time information</td>
							<td>False</td>
						</tr>
						<tr>
							<td>ttt</td>
							<td>string</td>
							<td>Travel time model</td>
							<td>Not set</td>
						</tr>
						<tr>
							<td>showpicks</td>
							<td>boolean</td>
							<td>Show picks</td>
							<td>False</td>
						</tr>
						<tr>
							<td>usegain</td>
							<td>boolean</td>
							<td>Use configured gain instead of digital counts. If set to <tt>true</tt> and no gain is available then the corresponding stream will not be plotted.</td>
							<td>False</td>
						</tr>
						<tr>
							<td>amprange</td>
							<td>float</td>
							<td>Amplitude range. Depended on the <tt>usegain</tt> parameter the value will either be intepreted as digital counts or as parts per billion of the gain unit.</td>
							<td>Not set</td>
						</tr>
						<tr>
							<td>title</td>
							<td>string</td>
							<td>Title to show</td>
							<td>Not set</td>
						</tr>
						<tr class="category"><td colspan="5">Relative modes: alignment parameters</td></tr>
						<tr>
							<td>align</td>
							<td>[ot,p,s]</td>
							<td>Controls alignment of traces which may be relative to origin time (OT), P wave, or S wave</td>
							<td>ot</td>
						</tr>
						<tr>
							<td>before</td>
							<td>seconds</td>
							<td>Begin of the timewindow in seconds before alignment time</td>
							<td>5.0</td>
						</tr>
						<tr>
							<td>after</td>
							<td>seconds</td>
							<td>End of the timewindow in seconds after alignment time</td>
							<td>30.0</td>
						</tr>
						<tr>
							<td>nalign</td>
							<td>[ot,p,s]</td>
							<td>Controls alignment of normalization time window which may be relative to origin time (OT), P wave, or S wave</td>
							<td>ot</td>
						</tr>
						<tr>
							<td>nbefore</td>
							<td>seconds</td>
							<td>Begin of the normalization timewindow in seconds before alignment time</td>
							<td>5.0</td>
						</tr>
						<tr>
							<td>nafter</td>
							<td>seconds</td>
							<td>End of the normalization timewindow in seconds after alignment time</td>
							<td>30.0</td>
						</tr>
						<tr class="category"><td colspan="5">QPlot mode</td></tr>
						<tr>
							<td>amplegend</td>
							<td>[hidden,top,center,bottom]</td>
							<td>Controls the vertical position of the amplitude legend</td>
							<td>bottom</td>
						</tr>
					</tbody>
				</table>
				<a name="trace-sample"></a>
				<h3>Examples</h3>
				<ul>
					<li><a href="traces?bufferSize=30000&amp;streams=GE*BHZ&amp;dim=1024,700&amp;num=5">Sample query for GE network</a></li>
				</ul>

				<a name="spectra"></a>
				<h2>Query for Spectra</h2>
				<p>The <a href="spectra">/spectra</a> query plots amplitude, power or phase spectra</p>
				<a name="spectra-feat"></a>
				<h3>Features</h3>
				<ul>
					<li>Spectrum Modes: amplitude, power, phase</li>
					<li>Window Functions: box car, cosine, hamming, hann</li>
					<li>Linear or logarithmic scales</li>
					<li>Plotting of raw spectrum, response corrected spectrum and response function</li>
				</ul>
				<a name="spectra-url"></a>
				<h3>URL Parameters</h3>
				<table class="doc">
					<thead>
						<tr>
							<td>Key</td>
							<td>Value</td>
							<td>Description</td>
							<td>Default</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>mode</td>
							<td>[amplitude, power, phase]</td>
							<td>Spectra mode</td>
							<td>amplitude</td>
						</tr>
						<tr class="category"><td colspan="5">Image parameters</td></tr>
						<tr>
							<td>dim</td>
							<td>width,height</td>
							<td>Image dimensions in pixel, retricted to 10 Mega pixel, min: 32x32</td>
							<td>1024,512</td>
						</tr>
						<tr>
							<td>fmt</td>
							<td>[png,jpg,jpeg,bmp,ppm,tiff,xbm,xpm]</td>
							<td>Output format of image</td>
							<td>png</td>
						</tr>
						<tr>
							<td>qua</td>
							<td>integer [-1,100]</td>
							<td>Quality of image, from 0 (worst) to 100 (best), -1 = autodetect</td>
							<td>100</td>
						</tr>
						<tr class="category"><td colspan="5">Data selection</td></tr>
						<tr>
							<td>stream</td>
							<td>stream id</td>
							<td>Stream ID to show</td>
							<td></td>
						</tr>
						<tr>
							<td>start</td>
							<td>time</td>
							<td>Begin of the timewindow</td>
							<td>end - bufferSize</td>
						</tr>
						<tr>
							<td>end</td>
							<td>time</td>
							<td>End of the timewindow</td>
							<td>now()</td>
						</tr>
						<tr>
							<td>bufferSize</td>
							<td>seconds</td>
							<td>Length of the timewindow</td>
							<td>60.0</td>
						</tr>
						<tr class="category"><td colspan="5">Data representation</td></tr>
						<tr>
							<td>logScaleX</td>
							<td>boolean</td>
							<td>Toggles logarithmic scale of x-axis</td>
							<td>True</td>
						</tr>
						<tr>
							<td>logScaleY</td>
							<td>boolean</td>
							<td>Toggles logarithmic scale of y-axis, not supported in phase mode</td>
							<td>True</td>
						</tr>
						<tr>
							<td>usegain</td>
							<td>boolean</td>
							<td>Use configured gain instead of digital counts. If set to <tt>true</tt> and no gain is available then the corresponding stream will not be plotted.</td>
							<td>True</td>
						</tr>
						<tr>
							<td>filter</td>
							<td>SeisComP3 filter grammar</td>
							<td>Record filter chain applied prior to FFT calculation, e.g. RMHP(10)&gt;&gt;BW(3,2,4)</td>
							<td>Not set</td>
						</tr>
						<tr>
							<td>minFreq</td>
							<td>double</td>
							<td>Minimum Frequency to plot</td>
							<td>SR / 2^(ceil(log_2(bufferSize * SR)))</td>
						</tr>
						<tr>
							<td>maxFreq</td>
							<td>double</td>
							<td>Maximum Frequency to plot</td>
							<td>Nyquist FQ</td>
						</tr>
						<tr>
							<td>showRAW</td>
							<td>boolean</td>
							<td>Toggles visibility of raw spectrum graph</td>
							<td>True</td>
						</tr>
						<tr>
							<td>showCorrected</td>
							<td>boolean</td>
							<td>Toggles visibility of corrected spectrum graph</td>
							<td>False</td>
						</tr>
						<tr>
							<td>showResponse</td>
							<td>boolean</td>
							<td>Toggles visibility of response spectrum graph</td>
							<td>False</td>
						</tr>
						<tr>
							<td>windowfunc</td>
							<td>[boxcar, cosine, hamming, hann]</td>
							<td>Window function</td>
							<td>boxcar</td>
						</tr>
						<tr>
							<td>windowlen</td>
							<td>percent [0,50]</td>
							<td>Pecentage the window function should be applied</td>
							<td>5.0</td>
						</tr>
						<tr>
							<td>showTitle</td>
							<td>boolean</td>
							<td>Toggles visibility of title</td>
							<td>True</td>
						</tr>
						<tr>
							<td>margin</td>
							<td>integer</td>
							<td>outer image margin</td>
							<td>10</td>
						</tr>
					</tbody>
				</table>
				<a name="spectra-sample"></a>
				<h3>Examples</h3>
				<ul>
					<li><a href="spectra?stream=IU.ANMO.00.BHZ&amp;dim=1024,500&amp;start=2018-03-16">Sample query for station IU.ANMO</a></li>
				</ul>

			</div>
			<div id="footer">gempa Image Server &copy; 2018 gempa GmbH</div>
		</div>
	</body>
</html>
